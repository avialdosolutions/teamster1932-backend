<?php
        include("headers/connect.php");
        include '_user-details.php';
        $returnArray = array();
        function folderSize ($dir)
        {
            $size = 0;
            foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
                $size += is_file($each) ? filesize($each) : folderSize($each);
            }
            return $size;
        }
        function filesize_formatted($size)
        {	
        $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;	
        //($size * .0009765625) * .0009765625	
        return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
        }
        $folder_path = $_GET['folder_path'];
        $query = "SELECT * FROM `file_managment` where path = '$folder_path' order by timestamp desc";
        $sth = $dbh->prepare($query);
        $sth->execute();
        $count = $sth->rowCount();
        if($count >= 1){
//        $returnArray['status'] = 1; // Data Available    
        while($row = $sth->fetch(PDO::FETCH_ASSOC))
        {
         $count++;
        $id = $row['id'];    
        $name = $row['name'];
        $path = $row['path'];    
        $timestamp = strtotime($row['timestamp'] .' UTC');            
        $timestamp = date('M d, Y h:i A',$timestamp);
        $file = $_SERVER[DOCUMENT_ROOT]."$app_name/backoffice/$path$name";     
            
//        if(is_dir($file)){
//            $size = filesize_formatted(folderSize($file));
//        }
//        else{
//            $size = filesize_formatted(filesize($file));
//        }    
              $size = filesize_formatted(filesize($file));
        $type = mime_content_type($file);  
        if($type == 'directory'){
            $row['icon'] = '<i class="fa fa-folder-open" style="font-size:18px"></i>';
        }
        if($type == 'image/jpeg' || $type == 'image/png' || $type == 'image/jpg'){
            $row['icon'] = '<i class="fa fa-image" style="font-size:18px"></i>';
        }    
        if($type == 'text/plain' || $type == 'text/html'){
            $row['icon'] = '<i class="fa fa-file-text" style="font-size:18px"></i>';
        }
        if($type == 'application/pdf'){
            $row['icon'] = '<i class="fa fa-file-pdf-o" style="font-size:18px"></i>';
        }
        if($type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
            $row['icon'] = '<i class="fa fa-file-text-o" style="font-size:18px"></i>';
            
        }
        if($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            $row['icon'] = '<i class="fa fa-file-excel-o" style="font-size:18px"></i>';
        }
        $row['size'] = $size;
        $row['type'] = $type;
        $row['fullUrl'] = "https://linkedunion.com$app_name/backoffice$path$name";
        $row['timestamp'] = $timestamp;
        $row['count'] = 1;
        $returnArray[] = $row;    
        }
        }
        else{
//            $returnArray['status'] = 2; // No data found
            $returnArray['count'] = 0;
        }

        $returnArray['MainSizeMb'] = number_format(folderSize('home') / 1048576, 2);
        $returnArray['MainSize'] = folderSize('home');

        echo json_encode($returnArray);

?>	
