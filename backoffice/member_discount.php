<?php
    include("headers/connect.php");
    include '_user-details.php';
    include("header.php");


$categoryID = $_GET['categoryID'];
$url = "";
$redirect="";
$name = "";
//$query = "SELECT w.name FROM `webservice_category` wc, `webservices` w WHERE wc.`category` like '{$categoryID}' AND wc.webservice = w.id";
//$result = mysqli_query($con,$query);
//$row = mysqli_fetch_array($result);
//$fileName = $row['name'];
//if(isset($fileName))
//{
//	if($fileName 	!= "view.php" && $fileName != "news_category.php"){
//		$url = "{$fileName}?categoryID={$categoryID}";
//		$redirect = 1;
//		//header("Location: {$fileName}?categoryID={$categoryID}");
//	}
//}

?>


<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<style>
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -23px;
    margin-bottom: 15px;
}    
.col-sm-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -43px;
    position: relative;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    display: none;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
a {
    color: #58595b !important;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -108px;
    margin-left: 26px;
    margin-right: 26px;    
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.previous a{
    color: #919191 !important;
        border: none !important;

}
li.active {
        border: none !important;
}
        .pagination>.active>a{
        background: none !important;    
        }
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next a{
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}    
table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
    left: 5px;
        }        
    
</style>    
 </head>
<head>
    <?php
    
    $query_Category = "SELECT name from categories where id  = $categoryID";
    $stm = $dbh->prepare($query_Category);
    $stm->execute();
    $count = $stm->rowCount();
    if($count >= 1){
    $rows = $stm->fetch(PDO::FETCH_ASSOC);
    $page_name = $rows['name'];
    }
    else{
        $query_Category = null;
        $query_Category = "SELECT name from subcategories where id  = $categoryID";
        $stm = $dbh->prepare($query_Category);
        $stm->execute();
        $rows = $stm->fetch(PDO::FETCH_ASSOC);
        $page_name = $rows['name'];
    }
    
    
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>'.$page_name.' | '.$title.'</title>';
    
    ?>   
<body class="fixed-top">
   <!-- BEGIN HEADER -->

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
<h4 style="margin-left: 20px;margin-bottom: 45px;text-transform: uppercase;"><?php echo $page_name; ?></h4>            
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
      <div class="panel-body">
       
         
         <div class="adv-table editable-table ">
                          <div class="clearfix" style="float: right;margin-right: 285px;top: 19px;position: relative;"> <a href="/teamster1932/add-member-discount/<?php echo $categoryID ?>"><button type="button" class="btn btn-primary"><i style="padding-right:5px;" class="fa fa-plus"></i> Add Discount <i class="icon-plus"></i> </button></a>
                          </div>
                        <div class="clearfix" style="float: right;margin-right: 10px;top: 19px;position: relative;"> <a href="/teamster1932/member-discount-type/<?php echo $categoryID; ?>"><button type="button" class="btn btn-primary"> View All Types <i class="icon-plus"></i> </button></a>
                          </div>
                        
                          <div class="space15"></div>
                <!-- BEGIN ADVANCED TABLE widget-->
                <?php
                if(isset($_GET['insert']) == 'true')
                {
                    echo"
                <div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>×</button>
                        <strong>Success!</strong> The Notification has been added.
                    </div>";
                }
            else if(isset($_GET['update']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been updated.
                </div>";
            }
            else if(isset($_GET['delete']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been Deleted.
                </div>";
            }
?>
             
                <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="editable-sample">
                                <thead>
                                     <tr>
                                    <th style="text-align:center;">S.No</th>
                                    <th>Title</th>
                                    <th>Website</th>
                                    <th>Short Description</th>
                                    <th>Type</th>
                                    <th>Locked</th>
                                    <th>Edited By</th>     
                                    <th>Last Edited</th>     
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    </tr>
                                   </thead>
<?php

					$query = "  SELECT member_discount.*,u.user_name , dis_type.name as member_type   FROM `member_discount` ,  member_discount_type dis_type, user u
                    where u.user_id = member_discount.last_edited_by and dis_type.id = member_discount.type and  category = $categoryID order by `order`";
                    
                    $sth = $dbh->prepare($query);
					$sth->execute();
					$count=0;
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{                    
					 $count++;
					$id = $row['id'];
					$order = $row['order'];
					$title_name = $row['title'];
					$website = $row['website'];
                    $member_type = $row['member_type'];    
					$description = $row['short_description'];
					$type = $row['type'];
                    $user_name = $row['user_name'];
                    $last_edited = $row['last_edited'];
                    $time = strtotime($last_edited .' UTC');    
                    $last_edited = date("Y-m-d h:i A", $time);						                        
					$public = $row['public'];
					 if($public == 1 ? $public = "<span id='published' class='label label-warning label-mini'>LOCKED</span>" : $public = "<span style='background:#21AF86' id='published' class='label label-success label-mini'>UNLOCKED</span>"); 	
					echo"
					<tr class=''> 
					<td style='text-align:center;'>{$count}</td>
					<td>{$title_name}</td>
                    <td>{$website}</td>
                    <td>{$description}</td>
                    <td>{$member_type}</td>
                    <td style='padding-top: 19px;'>{$public}</td>                    
                    <td>$user_name</td>
                    <td>".time_elapsed_string($last_edited)."</td>
					<td style='width:3%; text-align:center;'>
                    <a class='btn_icon' id='edit' href='/teamster1932/edit-member-discount/$categoryID/$id'>
                    <img style='width: 23px;' src='/theme/img/edit.svg'  /> </a></a>
                    </td>
                    <td style='width:3%;text-align:center;'>
                    
					<button class='btn delete' style='background:none;color:red;padding: 0px;padding-bottom: 9px;' type='button' id='$id'>                    
                    <img style='width: 16px;' src='/theme/img/delete.svg'  /> </a></a>
					</td>
						</tr>";
					}
					?>
					
          </table>
        </div>                                   
      </div>
    </section>
  </section>
</section>
<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
      <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
      <script src="/theme/assets/toastr-master/toastr.js"></script> 
	
      <!--script for this page only-->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>




      <!-- END JAVASCRIPTS -->
      <script>
		  $(document).ready(function() {
    $('#editable-sample').DataTable({

    "oLanguage": {
      "sLengthMenu": "_MENU_ Records Per Page"
    }
        
    });

              $('button.btn.btn-primary').show();
              
} );
		  
		  function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".delete", function (event) {
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                alert('Demo account cannot make any changes.');
            }
            else{              
            if (confirm("Are you sure you want to delete this Member?")) {
                  var ID = this.id;
                var $this = $(this);
                
            
            $.get("<?php echo $app_name ?>/backoffice/delete.php?discount_id="+ID, function (data, status) {
                        
                            var obj = jQuery.parseJSON(data);
                                if (obj.response == 1) {

                                    
                                 var table = $('#editable-sample').DataTable();  
                                  var row;

                                  if($this.closest('table').hasClass("collapsed")) {
                                    var child = $this.parents("tr.child");
                                    row = $(child).prevAll(".parent");
                                  } else {
                                    row = $this.parents('tr');
                                  }

                                  table.row(row).remove().draw();                
                                    
                                    
                                    showmessage('success','Memeber Discount Deleted Successfully','successfully');
                                } 
                          });  
                }
            }
            });
		  
		  
      </script>

</body>
