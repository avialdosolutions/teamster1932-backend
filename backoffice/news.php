<?php
	session_start();
	include("headers/connect.php");
    include '_user-details.php';
    include("header.php");
    $user_id = $_SESSION['user_id']; 
    $categoryID = $_GET['categoryID'];
    $title = @$_GET['title'];
    $PageTitle = strtoupper(str_replace('-',' ',$title));    

    $url = "";
    $redirect="";
    $name = "";
    $query = "SELECT (select c.name from categories where id='{$categoryID}') as category_name ,  w.name FROM `webservice_category` wc, `webservices` w , categories c  WHERE wc.`category` like '{$categoryID}' AND wc.webservice = w.id";
    $stmh = $dbh->prepare($query);
    $stmh->execute();
    while($row = $stmh->fetch(PDO::FETCH_ASSOC))
    {
        $fileName = $row['name'];
        $category_name = $row['category_name'];
    }

//    echo $fileName;

    if(isset($fileName))
    {        
        $PageTitleWithSlash = @$_GET['title'];
        if($fileName != "view.php" && $fileName != "news_category.php" && $fileName != "Report Immigiration Raid" && $fileName != "Find My Elected officials"){
            if($fileName == 'stay_connected.php'){
                $url = "$app_name/stay-connected/$categoryID";
                $redirect = 1;
            }   
            else if($fileName == 'Union Representative'){
                $url = "$app_name/union-representative/{$categoryID}";
                $redirect = 1;                
            }
            else if($fileName == 'Member Discount'){
                $url = "$app_name/member-discount/{$categoryID}";
                $redirect = 1;                
            } 
            else if ($fileName == 'Wufoo Form'){                
                $url = "$app_name/wufoo-form/$PageTitleWithSlash/{$categoryID}";
                $redirect = 1;                
            }
            else if($fileName == 'event'){                
                $url = "$app_name/event/{$categoryID}";
                $redirect = 1;                
            }            
            else{
            $url = "$app_name/default-news/$PageTitleWithSlash/{$categoryID}";
            $redirect = 1;
            }
            //header("Location: {$fileName}?categoryID={$categoryID}");
        }
    }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>'.$PageTitle.' | '.$title.'</title>';
    ?>
   <script>
   if(<?php echo $redirect;?> == 1){
			//alert('redirecting');
			window.location.href = '<?php echo $url; ?>';
   }
	</script>
<style>
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -29px;
    margin-bottom: 13px;
}    
.col-lg-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -42px;
    position: relative;
    right: -37px;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    display: none;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
a {
    color: #58595b !important;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -101px;
    margin-left: 26px;
    margin-right: 26px; 
    margin-bottom: -13px;
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.prev {
    border: none !important;
}
li.prev a{
    color: #919191 !important;
}
li.active {
        border: none !important;
}
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next {
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}    
</style>

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
        <?php        
        
            echo  '<h4 style="margin-left: 20px;margin-bottom: 45px;">'.strtolower($PageTitle).'</h4>';
        
        ?>
      
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
        
        <div class="panel-body">
       
        <?php 
		  		  $query = "select name from (select c.name ,c.id from `categories` c union select sc.name,sc.submenu_id from `subcategories` sc) `dd` where id ={$categoryID}";
				  $sth = $dbh->prepare($query);
				  $sth->execute();
				  $row = $sth->fetch(PDO::FETCH_ASSOC);
				  $name = $row['name'];
				?> 
                 <?php
			if(isset($_GET['insert']) == 'true')
			{
				echo"
			<div class='alert alert-success'>
					<button class='close' data-dismiss='alert'>×</button>
					<strong>Success!</strong> The news has been added.
				</div>";
			}
	 	else if(isset($_GET['update']) == 'true'){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The news has been updated.
            </div>";
		}
		else if(isset($_GET['delete']) == 'true'){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The news has been Deleted.
            </div>";
		}
?>
          
          
          
          
         
         <div class="adv-table editable-table ">
                            <?php 
                                if($fileName != 'Report Immigiration Raid' && $fileName != "Find My Elected officials") 
                                {?>
                          <div class="clearfix" style="float: right;margin-right: 250px;top: 19px;position: relative;"> <a href="/teamster1932/add-news/<?php echo $_GET['title'].'/'.$categoryID; ?>"><button type="button" class="btn btn-primary"><i style="padding-right:5px;" class="fa fa-plus"></i> Add News  </button></a>
                          </div>
                                <?php } ?>
                          <div class="space15"></div>
                <table  class="display table table-bordered table-striped" id="editable-sample">
                                <thead>
                                  <tr>
                				    <th style="text-align: center;">S No</th>
                                    <th>Title</th>
                                    <th>Edited By</th>
                                    <th>Last Edited</th>  
                                    <th style="text-align: center;">Private</th>  
                                    <th style="text-align: center;">Status</th>
                                    <th style="text-align: center;">Edit</th> 
                                    <th style="text-align: center;">Delete</th> 
                                    <th style="text-align: center;">View</th>      
                              </tr>
                              </thead>
      <?php

				$categoryID = $_GET['categoryID'];
			
					$query = "SELECT n.*,u.user_name from `news` n,user u where n.last_edited_by = u.user_id and n.category = {$categoryID} order by n.`order` asc";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$count = 0;
                                          
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
						$news_id = $row['news_id'];
						$count++;
                        $is_private = $row['is_private'];
                        $user_name = $row['user_name'];
                        $last_edited = $row['last_edited'];
                        $time = strtotime($last_edited .' UTC');    
                        $last_edited = date("Y-m-d h:i A", $time);						                     
                        if($is_private == 1 ? $is_private = 'Yes' : $is_private = 'No')
						$published = "<span id='published' class='label label-warning label-mini'>Unpublished</span>";
						$NewsTitle = strtolower(str_replace(' ','-',$row['title']));
                        
						
						if($row['published']==1)
							$published = "<span style='background:#21AF86' id='published' class='label label-success label-mini'>Published</span>";
                         
                            echo"
					               <tr class=''>
                                   <input type ='hidden' class='title' value='{$row['title']}'>
                                    <input type ='hidden' class='news_id' value='$news_id'>    
                                    <input type ='hidden' class='user_id' value='$user_id'>
								  <td style='text-align: center;width:7%'><a href='#'>{$count}</a></td>
								  <td  id='news_button' style='width:20%'><a href=''>{$row['title']}</a></td>
                                  <td><span style='text-transform:capitalize'>{$user_name}</span></td>
                                  <td>".time_elapsed_string($last_edited)."</td>
								  <td id='news_button' style='text-align: center;width:4%'><a href=''>{$is_private}</a></td>                                  
								  <td style='text-align: center;width:2%;padding-top: 16px;'>{$published}</span></td>
                                  
								  <td style='width:4%;text-align:center;'>
                                  <a class='btn_icon' id='edit' href='/teamster1932/".$_GET['title']."/$categoryID/{$news_id}/edit' 
								  >
                                  <img style='width: 21px;position:relative !important;' src='/theme/img/edit.svg' />
                                  </a>														
                                  </td>
                                 <td style='width: 2%;text-align: center;'>
                                 
                                 <button style='background:none;color:red' class='btn delete_news' type='button' id=$news_id>
                                            <img src='/theme/img/delete.svg' style='width: 17px;margin-top: -8px;' /></button>
                                 
                                 </td>
                                  <td style='width: 5%;text-align:center;padding-top: 7px;'>
								  <a target='_blank' href='/teamster1932/$NewsTitle/$categoryID/{$news_id}/{$appID}/view' class='btn_icon'>
								  <img style='width: 23px;' src='/theme/img/view.svg'  /> </a></td>
								  
								  </tr>";
					
					}
					?>	
			  
			
			  
          </table>

                      </div>
                                   
      </div>
    </section>
   
    <!-- page end--> 
  </section>
</section>


    
<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>

    
    <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
    <script src="/theme/assets/toastr-master/toastr.js"></script>
    
      <!--script for this page only-->
      <script src="/theme/js/editable-table.js"></script>

      <script type="text/javascript" src="/theme/assets/data-tables/jquery.dataTables.js"></script> 
     <script type="text/javascript" src="/theme/assets/data-tables/DT_bootstrap.js"></script> 
       
   
<!-- END JAVASCRIPTS -->
      <script>
       
   jQuery(document).ready(function() {
              EditableTable.init({
                      "oLanguage": {
                        "sLengthMenu": "_MENU_ Records Per Page"
                        }

              });
              $('button.btn.btn-primary').show();       
          });

      function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".delete_news", function (event) {
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                alert('Demo account cannot make any changes.');
            }
            else{              
            if (confirm("Are you sure you want to delete this News?")) {
                  var ID = this.id;
                    var $this = $(this);
                
                var title =  $(this).closest('tr').find('.title').val();
                var news_id =  $(this).closest('tr').find('.news_id').val();
                var user_ID =  $(this).closest('tr').find('.user_id').val();
                
                
            $.get("<?php echo $app_name ?>/backoffice/delete.php?mydelete_id="+ID+ "&title="+title+ "&news_id="+news_id+ "&user_ID="+user_ID+"&categoryID="+<?php echo $categoryID ?>, function (data, status) {
                            var obj = jQuery.parseJSON(data);
                                if (obj.response == 1) {

                                    
                                    
                                    
                                    var Row = $this.closest('tr');
                                    var tr = $this.parents('tr');
                                    var nRow = Row[0];
                                    $('#editable-sample').dataTable().fnDeleteRow(nRow)
                                    showmessage('success','News Deleted Successfully','successfully');
                                } 
                                else{
                                    alert('Something went wong please contact service provider.');
                                }
                          });  
                }
            }
            });
          
          
      </script>


