   <style type="text/css">
@font-face {
    font-family: Montserrat;
	src: url('/theme/fonts/Montserrat-Regular.eot');
	src: url('/theme/fonts/Montserrat-Regular.eot?#iefix') format('embedded-opentype'),
		url('/theme/fonts/Montserrat-Regular.woff2') format('woff2'),
		url('/theme/fonts/Montserrat-Regular.woff') format('woff'),
		url('/theme/fonts/Montserrat-Regular.ttf') format('truetype'),
		url('/theme/fonts/Montserrat-Regular.svg#Montserrat-Regular') format('svg');
	font-weight: normal;
	font-style: normal;
    
}
</style>       

<div style="background:white;border: 3px solid #0f2c3b;padding: 10px 0px 0px 0px;font-family: Montserrat;">
    <img src="/teamsters1932/backoffice/img/Home-logo-.svg" style="padding-left: 33px;width: 73%;padding-bottom: 14px;" />    
<div style="background:#ebebeb;color:#0f2c3b;padding: 42px;">
    <span style="font-size: 18px;">Welcome to <b>TEAMSTERS</b></span><br /><br />
    <span style="font-size: 13px;">Dear Member, Please use the following code <b>799902</b> to veriyfy your account</span><br /><br />
    <span style="font-size: 13px;"><b>Don't recognize this activity?</b></span><br />
    <span style="font-size: 13px;">Review your account now</span><br /><br /><br /><br /><br />
    <span style="color:#767676;font-size:13px;">This email can`t receive replies. To give us feedback on this alert. Click Here For more information, visit our website</span>    
</div>
    </div>