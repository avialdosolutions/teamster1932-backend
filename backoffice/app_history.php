<?php
    include("connect.php");
    include '_user-details.php';
    include("header.php");

$categoryID = $_GET['categoryID'];
$url = "";
$redirect="";
$name = "";

?>


<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <style>
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -23px;
    margin-bottom: 15px;
}    
.col-sm-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -4px;
    position: relative;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    display: none;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
a {
    color: #58595b !important;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -108px;
    margin-left: 26px;
    margin-right: 26px;    
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.previous a{
    color: #919191 !important;
        border: none !important;

}
li.active {
        border: none !important;
}
        .pagination>.active>a{
        background: none !important;    
        }
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next a{
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}    
table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
    left: 5px;
        }
        
table.dataTable{
        margin-top: 45px !important;
            
        }        
        
    </style>
   <script>
   if(<?php echo $redirect;?> == 1){
			//alert('redirecting');
			window.location.href = '<?php echo $url; ?>';
   }
	</script>
<script>
window.addEventListener("load", function(){
	var load_screen = document.getElementById("load_screen");
	document.body.removeChild(load_screen);
});
</script>
 </head>
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>App Users | '.$title.'</title>';
    ?>   
<body class="fixed-top">
   <!-- BEGIN HEADER -->

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
<h4 style="margin-left: 20px;margin-bottom: 45px;">App History</h4>      
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
      <div class="panel-body">
       
         
         <div class="adv-table editable-table ">
                          <div class="clearfix" style="float: right;margin-right: 285px;top: 55px;position: relative;"> <a href="/teamster1932/add-user"></a>
                          </div>
                          <div class="space15"></div>
                <!-- BEGIN ADVANCED TABLE widget-->
                <?php
                if(isset($_GET['insert']) == 'true')
                {
                    echo"
                <div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>×</button>
                        <strong>Success!</strong> The Notification has been added.
                    </div>";
                }
            else if(isset($_GET['update']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been updated.
                </div>";
            }
            else if(isset($_GET['delete']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been Deleted.
                </div>";
            }
?>
             
                <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="editable-sample">
                                <thead>
                                     <tr>
									<th>S.No</th>
									<th>Page Edited</th>
                                    <th>Action</th>
                                    <th>Edited By</th>
                                    <th>Date</th>     
                                    <th>Page Link</th>
                                   
								 </tr>
                                   </thead>
<?php
					$query = "SELECT app_his.`edit_type_name` ,app_his.`last_edited`  ,app_his.`page_url` ,app_his.`action` , u.user_name  FROM `app_history` app_his , user u WHERE u.user_id = app_his.edited_by order by app_his.id desc";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$count=0;
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
					 $count++;
					$edit_type_name = $row['edit_type_name'];
					$last_edited = $row['last_edited'];
					$page_url = $row['page_url'];
                    $user_name = $row['user_name'];    
					$action = $row['action'];    
                    $time = strtotime($last_edited .' UTC');    
                    $last_edited = date("Y-m-d h:i A", $time);						
                        
					echo"
					<tr class=''> 
					<td style='width:3%;text-align:center'>{$count}</td>
                    
					<td style='width:5%;'>{$edit_type_name}</td>
                      <td>$action</td>
                    <td>{$user_name}</td>
                   <td>".time_elapsed_string($last_edited)."</td>
					 <td><a target='_blank' href='$page_url'><u>$page_url</u></a></td>
                   
						</tr>";
					}
					?>
					
          </table>
        </div>                                   
      </div>
    </section>
  </section>
</section>

<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
      <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
      <script src="/theme/assets/toastr-master/toastr.js"></script> 
	
      <!--script for this page only-->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>




      <!-- END JAVASCRIPTS -->
      <script>
		  $(document).ready(function() {
    $('#editable-sample').DataTable({
        "aaSorting": []
    });
//  $('.dataTables_length label').html($('.dataTables_length label').html().replace('Show',''));
//  $('.dataTables_length label').html($('.dataTables_length label').html().replace('entries','Records Per Page'));
              $('button.btn.btn-primary').show();
} );
		        
		  
      </script>

</body>
