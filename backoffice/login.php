<?php 
session_start();
 include("headers/connect.php");
    if($_POST)
	{
		$email = $_POST['email'];
		$password = $_POST['password'];
        $date = gmdate("Y-m-d G:i:s");
		$query = "SELECT * from `user` where email = '{$email}' AND password like '{$password}' and is_delete = 0"
		or die('error2');
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $count = $stmh->rowCount();
		if($count >= 1)
		{
            // updating last login date and time
            $queryLogin = "UPDATE user set last_login = '$date' where user_id = ".$row['user_id'];
            $stm = $dbh->prepare($queryLogin);
            $stm->execute();
            
            // adding login session of user in to log table
            
            $queryLog = "INSERT INTO log (UserID,login_time) VALUE(".$row['user_id'].",'$date')";
            $stt = $dbh->prepare($queryLog);
            $stt->execute();
            $logId = $dbh->lastInsertId();
            $login_status = $row['login_status']; 
            $_SESSION['logId'] = $logId;
            $_SESSION['user_level'] = $row['user_level'];
            $_SESSION['user_id'] = $row['user_id'];
            $_SESSION['app_name'] = $app_name;
            if($login_status == 1){
		     echo "<script>window.location.href = $app_name/</script>";	            
            }
            else{
             header("Location: $app_name/change-password");
            }
		}
        else
        {
            $error =  '<div class="alert alert-danger fade in" style="width: 340px;margin-left: 20px;">
				Invalid  email or password
				</div>';
        }
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
     <link rel="icon" href="<?php echo $app_name ?>/backoffice/img/logo/icon.ico" type="image/x-icon">
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Login | '.$title.'</title>';
    ?>

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="">
<!--    <link rel="shortcut icon" href="img/favicon.png">-->
 <link href="/theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="/theme/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
<!--    <link href="/theme/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />-->
    <!-- Custom styles for this template -->
    <link href="/theme/app_css/style.css" rel="stylesheet">
    <link href="/theme/css/style-responsive.css" rel="stylesheet" />
    
    <link href="/theme/app_css/login.css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
	.img-box {
    padding-left: 10px;
    padding-top: 6px;
    padding-bottom: 7px;
    border-bottom: 12px solid #f1f2f7;
    background: #f1f2f7;
}
	</style>
</head>

<body>
 <div class="container">     
		<form id="loginform" class="form-signin" name="" action="login" method="post">
                   <?php
            if(isset($_GET['fail']) && $_GET['fail']=="true"){
                echo"<div class='alert alert-danger' role='alert'>
                <strong>Oh dear!</strong> Something went awry!
                It seems that the Delegation ID/Password you entered were not found in our database, please try again
                </div>";
            } else if(isset($_GET['register']) && $_GET['register']=="true"){

                  echo"<div class='alert alert-danger' role='alert'>
              <center>Kindly Check your email for the credentials</center></div>";
            }
            else if(isset($_GET['forget']) && $_GET['forget']=="true"){

                  echo"<div class='alert alert-danger' role='alert'>
            <center> Kindly Check your email for the credentials</center></div>";
            }
            else if(!empty($_GET['invalid-link'])){
                  echo"<div class='alert alert-danger' role='alert'>
            <center> Sorry! The requested Link is not valid </center></div>";
            }
            else if(!empty($_GET['reset']) =="true"){

                  echo"<div class='alert alert-success' role='alert'>
            <center> Success! Now you can login with your new password </center></div>";
            } 
            ?> 

				<input type="hidden" value="login" name="type">
                <img src="<?php echo $app_name ?>/backoffice/img/login-logo-.svg" style="width: 326px    ;margin-bottom: 11px;" />
<!--                <h2 class="login-heading"><strong>TEAMSTER</strong> LOCAL 1932</h2>-->
              <?php echo @$error; ?>
                <div class="login-wrap">
                <div class="iconic-input right">
                <img src="<?php echo $app_name ?>/backoffice/img/Username-icon-.png" />
				<input style="padding-left: 18px !important;" type="email" class="form-control" placeholder="Email" name="email" autofocus required maxlength="200">
                </div>
                <div class="iconic-input right">
                <img src="<?php echo $app_name ?>/backoffice/img/Password-.png" />
				<input style="padding-left: 18px !important;" type="password" class="form-control" placeholder="Password" name="password" required maxlength="">
                </div>    
				<input class="btn btn-lg btn-login btn-block" name="login" type="submit" value="Login">

            </div>

              
            </form>     
     
    </div>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="/theme/js/jquery.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
<!--    <script src="/theme/js/functions.js"></script>-->
    <script src="/theme/js/jstz.js"></script>
    <script type="text/javascript">
    //	var now = new Date();
    document.cookie = "cookieName="+jstz.determine().name();
    </script>
    
</body>
</html>
