<?php
	session_start();
	include("headers/connect.php");
    include '_user-details.php';
    include("header.php");
    $user_id = $_SESSION['user_id']; 
    $title = @$_GET['title'];
    $PageTitle = strtoupper(str_replace('-',' ',$title));    


//    echo $fileName;

    if(isset($fileName))
    {        
        $PageTitleWithSlash = @$_GET['title'];
        if($fileName != "view.php" && $fileName != "news_category.php" && $fileName != "Report Immigiration Raid" && $fileName != "Find My Elected officials"){
            if($fileName == 'stay_connected.php'){
                $url = "$app_name/stay-connected/$categoryID";
                $redirect = 1;
            }   
            else if($fileName == 'Union Representative'){
                $url = "$app_name/union-representative/{$categoryID}";
                $redirect = 1;                
            }
            else if($fileName == 'Member Discount'){
                $url = "$app_name/member-discount/{$categoryID}";
                $redirect = 1;                
            } 
            else if ($fileName == 'Wufoo Form'){                
                $url = "$app_name/wufoo-form/$PageTitleWithSlash/{$categoryID}";
                $redirect = 1;                
            }
            else{
            $url = "$app_name/default-news/$PageTitleWithSlash/{$categoryID}";
            $redirect = 1;
            }
            //header("Location: {$fileName}?categoryID={$categoryID}");
        }
    }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Add Icons | '.$title.'</title>';
    ?>
   <script>
   if(<?php echo $redirect;?> == 1){
			//alert('redirecting');
			window.location.href = '<?php echo $url; ?>';
   }
	</script>
<style>
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -29px;
    margin-bottom: 13px;
}    
.col-lg-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -42px;
    position: relative;
    right: -37px;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    display: none;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
a {
    color: #58595b !important;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -101px;
    margin-left: 26px;
    margin-right: 26px; 
    margin-bottom: -13px;
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.prev {
    border: none !important;
}
li.prev a{
    color: #919191 !important;
}
li.active {
        border: none !important;
}
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next {
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}  
 .submit_button,.add_iconn,.revert{
        background: #21AF86;
        color: white;
        border: none;
        padding: 10px;
        border-radius: 21px;
        padding-left: 35px;
        padding-right: 35px;
        font-size: 10px;
        box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
        font-family: Montserrat;
        
    }
    .submit_button:hover,.add_iconn:hover,.revert:hover{
        color: white;
    }
.modal-header {
    background: #425BA9 !important;
    padding: 3px;
    border-radius: 0px;
    min-height: 0px;
} 
button.close {
    position: absolute;
    top: -23px;
    right: -27px;
    cursor: pointer;
    font-size: 18px;
    padding-top: 0px !important;
    width: 58px;
    height: 46px;
    line-height: 24px;
    opacity: 0.9;
    font-weight: bold;
    color: white;
    padding-left: 2px !important;
    margin-top: -1px !important;
    }  
    input[type=file] {
        border: none !important;
        padding-top: 6px !important;
        padding-left: 0px !important;        
    }
    input{
    border-radius: 24px !important;
    padding-left: 24px !important;
    padding-top: 21px !important;
    padding-bottom: 20px !important;
    }
    select{

    border-radius: 24px !important;
    padding-left: 16px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;        
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 44px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }    
</style>

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
        <?php        
        
            echo  '<h4 style="margin-left: 20px;margin-bottom: 45px;">'.strtolower($PageTitle).'</h4>';
        
        ?>
      
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
        
        <div class="panel-body">
       
        <?php 
		  		  $query = "select name from (select c.name ,c.id from `categories` c union select sc.name,sc.submenu_id from `subcategories` sc) `dd` where id ={$categoryID}";
				  $sth = $dbh->prepare($query);
				  $sth->execute();
				  $row = $sth->fetch(PDO::FETCH_ASSOC);
				  $name = $row['name'];
				?> 
                 <?php
			if(isset($_GET['insert']) == 'true')
			{
				echo"
			<div class='alert alert-success'>
					<button class='close' data-dismiss='alert'>×</button>
					<strong>Success!</strong> The news has been added.
				</div>";
			}
	 	else if(isset($_GET['update']) == 'true'){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The news has been updated.
            </div>";
		}
		else if(isset($_GET['delete']) == 'true'){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The news has been Deleted.
            </div>";
		}
?>
          
       <!-- Modal of add icons -->
            <div class="modal fade" id="add_icon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/theme/img/close_btn.svg" style="width:20px;" /></button>
                  </div>
                  <div class="modal-body">
                  </div>
                </div>
              </div>
            </div>
   
          
          
         
         <div class="adv-table editable-table ">
                            <?php 
                                if($fileName != 'Report Immigiration Raid' && $fileName != "Find My Elected officials") 
                                {?>
                          <div class="clearfix" style="float: right;margin-right: 250px;top: 19px;position: relative;"> <button type="button" type="button" data-toggle= "modal" href="#add_icon"  class="btn btn-primary add_icon"><i style="padding-right:5px;" class="fa fa-plus"></i>Add Icon </button>
                          </div>
                                <?php } ?>
                          <div class="space15"></div>
                <table  class="display table table-bordered table-striped" id="editable-sample">
                                <thead>
                                  <tr>
                				    <th>S.No</th>
									<th>Name</th>
                                    <th>Icon</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                              </tr>
                              </thead>
            <?php
                    $query = "SELECT * FROM `app_icon`";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$count=0;
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
					 $count++;
                    $id = $row['id'];    
					$name = $row['name'];
					$image = $row['image'];
									
                        
					echo"
					<tr class=''> 
					<td style='width:3%;text-align:center'>{$count}</td>
                    
					<td style='width:70%;'>{$name}</td>";
                    if($name == null || !file_exists("img/navigation_icon/{$image}")) 
                    {
                        echo "<td style='width:10%;'><img src='$app_name/backoffice/img/navigation_icon/no-found.jpg' width='30' height='30'></td>";
                    }
                    else{
                        echo "<td style='width:10%;'><img src='$app_name/backoffice/img/navigation_icon/$image' width='30' height='30'></td>"; 
                    }
                        
                    echo "<td><a class='edit_icons' data-toggle= 'modal' href='#add_icon'>
                                  <img style='width: 21px;position:relative !important;' src='/theme/img/edit.svg'>
                                  </a></td>
                                 <input type='hidden' value=".$id." class='id' />
                                <input type='hidden' value=".$image." class='icon' />
                             <input type='hidden' value=".$name." class='name' />  
                        <td style='width:10%;'><button style='background:none;color:red' class='btn delete_icon' type='button' id=$id>
                    <img src='/theme/img/delete.svg' style='width: 17px;margin-top: -8px;' /></button></td>";
                    }
					
					
					
					?>	
			  
			
			  
          </table>

                      </div>
                                   
      </div>
    </section>
   
    <!-- page end--> 
  </section>
</section>


    
<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>

    
    <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
    <script src="/theme/assets/toastr-master/toastr.js"></script>
    
      <!--script for this page only-->
      <script src="/theme/js/editable-table.js"></script>

      <script type="text/javascript" src="/theme/assets/data-tables/jquery.dataTables.js"></script> 
     <script type="text/javascript" src="/theme/assets/data-tables/DT_bootstrap.js"></script> 
       
   
<!-- END JAVASCRIPTS -->
      <script>
       
   jQuery(document).ready(function() {
              EditableTable.init({
                      "oLanguage": {
                        "sLengthMenu": "_MENU_ Records Per Page"
                        }

              });
              $('button.btn.btn-primary').show();       
          });

      function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
   

          
$(document).on('click','.edit_icons', function(event){
		$(".icon_title").text("Edit Icons");
            var id = $(this).closest('tr').find('.id').val();
            var name = $(this).closest('tr').find('.name').val();
            var icon = $(this).closest('tr').find('.icon').val();
          
    $('#add_icon .modal-body').html('<form   method="post" id="form_id" class="form-horizontal add_icon_form" action="<?php echo $app_name ?>/backoffice/icon_code.php?id='+id+'" enctype="multipart/form-data"><div class="form-group">\
                         <label class="control-label col-md-4">Name</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control icon_name" name="icon_name" value="'+name+'" required value=""  size="16" type="text"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4">App Icon</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control icon_image" name="icon_image" value="'+icon+'"    size="16" type="file"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4">Highlighted Icon</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control highlighted_icon" name="highlighted_icon"    size="16" type="file"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4">Unhighlighted Icon</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control unhighlighted_icon" name="unhighlighted_icon"    size="16" type="file"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4"></label>\
                        <div class="col-md-8 col-xs-11">\
                          <button class="btn add_iconn"    size="16" type="submit">Submit</button>\</div></div></form>');
     });
          
          
$(document).on('click','.add_icon', function(event){
		$(".icon_title").text("Add Icons");
         
    $('#add_icon .modal-body').html('<form   method="post" id="form_id" class="form-horizontal add_icon_form" action="<?php echo $app_name ?>/backoffice/icon_code.php" enctype="multipart/form-data"><div class="form-group">\
                         <label class="control-label col-md-4">Name</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control icon_name" name="icon_name" required value=""  size="16" type="text"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4">App Icon</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control icon_image" name="icon_image"  required  size="16" type="file"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4">Highlighted Icon</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control highlighted_icon" name="highlighted_icon"  required  size="16" type="file"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4">Unhighlighted Icon</label>\
                        <div class="col-md-8 col-xs-11">\
                          <input class="form-control unhighlighted_icon" name="unhighlighted_icon"  required  size="16" type="file"/>\</div></div><div class="form-group">\
                        <label class="control-label col-md-4"></label>\
                        <div class="col-md-8 col-xs-11">\
                          <button class="btn add_iconn"    size="16" type="submit">Submit</button>\</div></div></form>');
     });
          
          
    
 $(document).on('submit','.add_icon_form', function(event){
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                alert('Demo account cannot make any changes.');
                                 return false;                   
            }
            else if(user_level == 2){
                
                alert("Sorry! you don't have permission to perform this action");                
                return false;
            }
     
            else{                          
            
                if(confirm('All your unsaved changes will be discarded. Do you want to continue?'))
                   {
                     return true;      
                   }
                   else {
                         return false;                   
                   }                                      
            }
 });          
          

$("table").on("click", ".delete_icon", function (event) {
                        
            if (confirm("Are you sure you want to delete this Icon?")) {
                  var ID = this.id;
                    var $this = $(this);
            $.get("<?php echo $app_name ?>/backoffice/delete.php?delete_icon="+ID, function (data, status) {
                            var obj = jQuery.parseJSON(data);
                                if (obj.response == 1) {
                                    var Row = $this.closest('tr');
                                    var tr = $this.parents('tr');
                                    var nRow = Row[0];
                                    $('#editable-sample').dataTable().fnDeleteRow(nRow)
                                    showmessage('success','Icon Deleted Successfully','successfully');
                                } 
                                else{
                                    alert('Something went wong please contact service provider.');
                                }
                          });  
                }
            
            });                 
          
          
      </script>


