<?php
    include("headers/connect.php");
    include '_user-details.php';
    include("header.php");

//$categoryID = $_GET['categoryID'];
$url = "";
$redirect="";
$name = "";
//$query = "SELECT w.name FROM `webservice_category` wc, `webservices` w WHERE wc.`category` like '{$categoryID}' AND wc.webservice = w.id";
//$result = mysqli_query($con,$query);
//$row = mysqli_fetch_array($result);
//$fileName = $row['name'];
//if(isset($fileName))
//{
//	if($fileName 	!= "view.php" && $fileName != "news_category.php"){
//		$url = "{$fileName}?categoryID={$categoryID}";
//		$redirect = 1;
//		//header("Location: {$fileName}?categoryID={$categoryID}");
//	}
//}

?>


<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <style>
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
table{
/*
    margin-top: -101px;
    margin-left: 26px;
    margin-right: 26px;
*/
}        
        
        
    </style>
 </head>
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Information | '.$title.'</title>';
    ?>   
<body class="fixed-top">
   <!-- BEGIN HEADER -->

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
<h4 style="margin-left: 20px;margin-bottom: 45px;">Information</h4>      
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
      <div class="panel-body">
       
         
         <div class="adv-table editable-table ">
<!--
                          <div class="clearfix"> <a href="insert_notification.php"><button type="button" class="btn btn-primary"> Add New <i class="icon-plus"></i> </button></a>
                          </div>
-->
                          <div class="space15"></div>
                <!-- BEGIN ADVANCED TABLE widget-->
                <?php
                if(isset($_GET['insert']) == 'true')
                {
                    echo"
                <div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>×</button>
                        <strong>Success!</strong> The Notification has been added.
                    </div>";
                }
            else if(isset($_GET['update']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been updated.
                </div>";
            }
            else if(isset($_GET['delete']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been Deleted.
                </div>";
            }
?>
             
                <table  class="table table-bordered table-striped table-condensed cf" id="">
                                <thead class="cf">
                                     <tr>
								    <th width="10">S.No</th>
                                    <th width="220px">Name</th>
									<th>Short Description</th>
									<th width='60'>Edit</th>
                                    <th width='60'>View</th>
                                    </tr>
                                   </thead>
                         <?php
					$query = "SELECT about,privacy,contact FROM `app`";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$row = $sth->fetch(PDO::FETCH_ASSOC);
                                          
                        $about = substr(strip_tags($row['about']),0, 120);
					    $contact = substr(strip_tags($row['contact']),0, 120);
                        $privacy = substr(strip_tags($row['privacy']),0, 120);
					
						if(strlen($about) == 120 ? $dot = '...' : $dot = '');
						if(strlen($contact) == 120 ? $dot1 = '...' : $dot1 = '');
						if(strlen($privacy) == 120 ? $dot2 = '...' : $dot2 = '');
					
					echo"
						<tr> 
						  <td>1</td>
						  <td>About Us</td>
						  <td>$about$dot</td>
						  <td style='text-align: center;vertical-align: middle;'><a href='/teamster1932/edit/about'><img style='width: 16px;' src='/theme/img/edit.svg'  /></a>
                          </td>
                          <td style='text-align: center;vertical-align: middle;'>
						  	  <a target='_blank' href='/teamster1932/about/view'><img style='width: 16px;' src='/theme/img/view.svg'  />
							  </a>
						  </td>
						 </tr>
						 <tr> 
						  <td>2</td>
						  <td>Contact $title</td>
						  <td>$contact$dot1</td>
						  <td style='text-align: center;vertical-align: middle;'><a href='/teamster1932/edit/contact'><img style='width: 16px;' src='/theme/img/edit.svg'  /></a>
                          </td>
                          <td style='text-align: center;vertical-align: middle;'>
						  	  <a target='_blank' href='/teamster1932/contact/view'><img style='width: 16px;' src='/theme/img/view.svg'  />
							  </a>
						  </td>
						 </tr>
						 <tr> 
						  <td>3</td>
						  <td>Terms and conditions / Privacy</td>
						  <td>$privacy$dot2</td>
						  <td style='text-align: center;vertical-align: middle;'><a href='/teamster1932/edit/privacy'><img style='width: 16px;' src='/theme/img/edit.svg'  /></a></td>
                          <td style='text-align: center;vertical-align: middle;'>
						  	  <a target='_blank' href='/teamster1932/privacy/view'><img style='width: 16px;' src='/theme/img/view.svg'  />
							  </a>
						  </td>
						 </tr>";
					
					?>	
          </table>
        </div>                                   
      </div>
    </section>
  </section>
</section>

<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
      <!-- script for this page only-->
    <script type="text/javascript" src="/theme/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/theme/assets/data-tables/DT_bootstrap.js"></script>
    <script src="/theme/js/respond.min.js" ></script>
    <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>

      <!--script for this page only-->
      <script src="/theme/js/editable-table.js"></script>

      <!-- END JAVASCRIPTS -->
      <script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
      </script>

</body>
