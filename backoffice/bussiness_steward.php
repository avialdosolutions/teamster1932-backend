<?php
    include("headers/connect.php");
    include '_user-details.php';
    include("header.php");

$categoryID = $_GET['categoryID'];
$url = "";
$redirect="";
$name = "";
//$query = "SELECT w.name FROM `webservice_category` wc, `webservices` w WHERE wc.`category` like '{$categoryID}' AND wc.webservice = w.id";
//$result = mysqli_query($con,$query);
//$row = mysqli_fetch_array($result);
//$fileName = $row['name'];
//if(isset($fileName))
//{
//	if($fileName 	!= "view.php" && $fileName != "news_category.php"){
//		$url = "{$fileName}?categoryID={$categoryID}";
//		$redirect = 1;
//		//header("Location: {$fileName}?categoryID={$categoryID}");
//	}
//}


?>


<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    
   <script>
   if(<?php echo $redirect;?> == 1){
			//alert('redirecting');
			window.location.href = '<?php echo $url; ?>';
   }
	</script>
<script>
window.addEventListener("load", function(){
	var load_screen = document.getElementById("load_screen");
	document.body.removeChild(load_screen);
});
</script>
 </head>
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Business Steward | '.$title.'</title>';
    ?>   
<body class="fixed-top">
   <!-- BEGIN HEADER -->

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
    <section class="panel" style="margin-left:39px;">
      <header class="panel-heading">Union Representative</header>
      <div class="panel-body">
       
         <a class="btn btn-success add_worksite"  href="/teamster1932/business-agent-steward/add">Add Union Representative </a>
         <div class="adv-table editable-table ">
             <div class="space15"></div>
                <!-- BEGIN ADVANCED TABLE widget-->
                <?php
                if(isset($_GET['insert']) == 'true')
                {
                    echo"
                <div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>×</button>
                        <strong>Success!</strong> The Notification has been added.
                    </div>";
                }
            else if(isset($_GET['update']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been updated.
                </div>";
            }
            else if(isset($_GET['delete']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been Deleted.
                </div>";
            }
?>
              <!-- Modal -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
             
                    </div>
                    
                    
                  </div>
                </div>
              </div>
     
            <!-- modal --> 
                <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="example">
                                <thead>
                                     <tr>
                                    <th>S No</th>     
                                    <th>Bussiness Name</th>
                                    <th>Bussiness Email</th>
									<th>Bussiness Cell</th>
                                    <th>Bussiness Ext</th>
									 <th>Steward Name</th>
                                    <th>Steward Email</th>
								    <th>Steward Cell</th>
                                    <th>Steward Ext</th>
									<th>Depat Name</th>
                                    <th>Worksite Name</th>
										 
                                    <th>Action</th>
								 
                                    </tr>
                                   </thead>
<?php
					$query = "SELECT  b.* , w.name as worksite_name , d.name as department_name   FROM  worksite w  , `department` d , bussines_agent b  WHERE d.id  = b.`depart_id` and b.`worksite_id` = w.`id` ";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$count=0;
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
					 $count++;
					$id = $row['id'];
                    $bussiness_name = $row['bussiness_name'];
					$bussiness_email = $row['bussiness_email'];
					$bussiness_cell = $row['bussiness_cell'];
					$bussiness_ext = $row['bussiness_ext'];
					$steward_name = $row['steward_name'];
					$steward_email = $row['steward_email'];
					$steward_cell = $row['steward_cell'];
					$steward_ext = $row['steward_ext'];
					$depart_id = $row['depart_id'];
					$worksite_id = $row['worksite_id'];
						
					$worksite_name = $row['worksite_name'];
					$department_name = $row['department_name'];	
                    
					echo"
					<tr class=''> 
					<td>{$count}</td>
					<td>{$bussiness_name}</td>
					<td>{$bussiness_email}</td>
					<td>{$bussiness_cell}</td>
					<td>{$bussiness_ext}</td>
					<td>{$steward_name}</td>
					<td>{$steward_email}</td>
					<td>{$steward_cell}</td>
					<td>{$steward_ext}</td>
					<td>{$department_name}</td>
                    <td>{$worksite_name}</td>
					<td style='width:3%; text-align:center;'>";
					echo '<a class="btn_icon" id="edit" href="/teamster1932/business-agent-steward/'.$id.'/edit">
                    <i style="position:relative !important; font-size:18px; color:green;margin:5px 3px 0 0;" class="fa fa-pencil"></i></a>';
					
                  echo 	"<button class='btn delete' style='background:none;color:red' type='button' id='$id'>         <i class='fa fa-trash-o'></i>
					</button>
					</td>
						</tr>";
					}
					?>
					
          </table>
        </div>                                   
      </div>
    </section>
  </section>
</section>

<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
      <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
      <script src="/theme/assets/toastr-master/toastr.js"></script> 
	
      <!--script for this page only-->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>




      <!-- END JAVASCRIPTS -->
      <script>
		  $(document).ready(function() {
    	   $('#example').DataTable();
		  });
		  
		  
		  
		  function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".delete", function (event) {
            if (confirm("Are you sure you want to delete this Union Representative?")) {
                  var ID = this.id;
                    var $this = $(this);
         
            $.get("<?php echo $app_name ?>/backoffice/delete.php?bussiness_id="+ID, function (data, status) {
                        
                            var obj = jQuery.parseJSON(data);
								
                                if (obj.response == 1) {

                                    var Row = $this.closest('tr');
                                    var nRow = Row[0];
                                    $('#example').dataTable().fnDeleteRow(nRow)
                                    showmessage('success','Union Representative Delete Successfully','successfully');
                                } 
                          });  
                }
            });
		  
		  
      </script>

</body>
