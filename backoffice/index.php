<?php
    //session_start();
    include("headers/connect.php");
    include '_user-details.php';
    include("header.php");
    $monthArray = array(
                array( 'Month' => 'JAN', 'total' => 0),
                array( 'Month' => 'FEB', 'total' => 0),
                array( 'Month' => 'MAR', 'total' => 0),
                array( 'Month' => 'APR', 'total' => 0),
                array( 'Month' => 'MAY', 'total' => 0),
                array( 'Month' => 'JUN', 'total' => 0),
                array( 'Month' => 'JUL', 'total' => 0),
                array( 'Month' => 'AUG', 'total' => 0),
                array( 'Month' => 'SEP', 'total' => 0),
                array( 'Month' => 'OCT', 'total' => 0),
                array( 'Month' => 'NOV', 'total' => 0),
                array( 'Month' => 'DEC', 'total' => 0)
                     );
    $AppInstalled = array();

$QueryCategory = "SELECT count(v.category_id) as count,(SELECT count(category_id) from visited_page) as totalCount,v.category_id from visited_page v group by v.category_id order by count(v.category_id) desc limit 4";
$stt = $dbh->prepare($QueryCategory);
$stt->execute();
$listSum = 0;
$categoryArray = array();
while($rowed = $stt->fetch(PDO::FETCH_ASSOC)){
    $count = $rowed['count'];
    $listSum += $count;
    $totalCount = $rowed['totalCount'];
    $category_id = $rowed['category_id'];    
    $queryCategories = "SELECT name from subcategories where id = '$category_id'";
    $sts = $dbh->prepare($queryCategories);
    $sts->execute();
    $rowcount = $sts->rowCount();
    if($rowcount >= 1){
        $rowcategory = $sts->fetch(PDO::FETCH_ASSOC);
        $name = $rowcategory['name'];                
    }
    else{
        $queryCategories = "SELECT name from categories where id = '$category_id'";
        $sts = $dbh->prepare($queryCategories);
        $sts->execute();
        $rowcount = $sts->rowCount();
        $rowcategory = $sts->fetch(PDO::FETCH_ASSOC);
        $name = $rowcategory['name'];                        
    }
        $rowed['name'] = $name;
        $categoryArray[] = $rowed;
    
}

$OthersCount = $totalCount - $listSum;



$queryApp = 'SELECT DATE_FORMAT(u.time_stamp, "%b") AS Month, count(u.id) as total
FROM `member_info` m , app_user u where u.id = m.app_user_id and ((u.name = "Anonymous" AND u.password = "Anonymous") OR (u.activate = 1)) GROUP BY DATE_FORMAT(u.time_stamp, "%m-%Y")';
$stm = $dbh->prepare($queryApp);
$stm->execute();
while($rows = $stm->fetch(PDO::FETCH_ASSOC)){
    $rows['Month'] = strtoupper($rows['Month']);
    $AppInstalled[] = $rows;
}
$commonIds = array_diff(array_column($monthArray, 'Month'), array_column($AppInstalled, 'Month'));
$commonIdsArray = array();
foreach($commonIds as $value){
        $commonIdsArray[] =  array( 'Month' => "$value", 'total' => 0);

}
$AppInstalled = array_merge($commonIdsArray,$AppInstalled);


//usort($AppInstalled,'monthCompare');
//function monthCompare($a, $b)
//{
//    $months = array('JAN' => 0, 'FEB' =>1,'MAR' => 2,'APR' => 3,'MAY' => 4,'JUN' => 5,'JUL' => 6,'AUG' => 7, 'SEP' => 8, 'OCT' => 9, 'NOV' => 10, 'DEC' => 11);
//    if($a == $b)
//    {
//        return 0;
//    }
//    return ($months[$a] > $months[$b]) ? 1 : -1;
//}

usort($AppInstalled, "compare_months");

function compare_months($a, $b) {
    $monthA = date_parse($a['Month']);
    $monthB = date_parse($b['Month']);

    return $monthA["month"] - $monthB["month"];
}


    $queryCount = "SELECT (SELECT count(news_id) from news where news.app_id = $appID ) as totalNews,
                          (SELECT count(u.id) FROM `member_info` m , app_user u where u.id = m.app_user_id and u.activate = 0) as UnregisteredUser,
                          (SELECT count(u.id) FROM `member_info` m , app_user u where u.id = m.app_user_id and u.activate = 1) as RegisteredUser,
                          (SELECT count(u.id) FROM `member_info` m , app_user u where u.id = m.app_user_id) as totalUser,
                          (SELECT count(id) from membership_feedback) as totalFeedback,
                          (SELECT count(id) from categories) as totalcategories from app";
    $sth = $dbh->prepare($queryCount);
    $sth->execute();
    $row = $sth->fetch(PDO::FETCH_ASSOC);
    $totlaNews = $row['totalNews'];
    $totalUser = $row['totalUser'];
    $UnregisteredUser = $row['UnregisteredUser'];
    $RegisteredUser = $row['RegisteredUser'];
    $totalFeedback = $row['totalFeedback'];
    $totalcategories = $row['totalcategories'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
   <link rel='shortcut icon' type='image/x-icon' href='img/icon.ico' />
    <link rel="stylesheet" type="text/css" href="/theme/app_css/style.css" />
    <style>
@font-face {
    font-family: Montserrat;
	src: url('/theme/fonts/Montserrat-Regular.eot');
	src: url('/theme/fonts/Montserrat-Regular.eot?#iefix') format('embedded-opentype'),
		url('/theme/fonts/Montserrat-Regular.woff2') format('woff2'),
		url('/theme/fonts/Montserrat-Regular.woff') format('woff'),
		url('/theme/fonts/Montserrat-Regular.ttf') format('truetype'),
		url('/theme/fonts/Montserrat-Regular.svg#Montserrat-Regular') format('svg');
	font-weight: normal;
	font-style: normal;
    
}   
        
.panel-heading {
    border: none !important; 
    font-size: 13px;
    font-weight: 300;
    padding-left: 35px;
    padding-top: 33px;
}
text {
    font-family: Montserrat;
}        
    </style>
      <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Home | '.$title.'</title>';
    ?>   
<section id="main-content">
  <section class="wrapper">
      
       <!-- page start-->
                <h4 style="margin-left: 14px;margin-bottom: 29px;margin-top: 0px;">Dashboard</h4>
                  <div class="row state-overview" style="margin-left: 16px;margin-right: 16px;background: white;box-shadow: 4px 7px 10px rgba(211,211,211,1);margin-bottom: 29px;">
                      <h4 style="margin-left: 55px;margin-top: 37px;">Current Statistics</h4>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="value">
                              <h1 class="count">
                                  0
                              </h1>
                              <p>Total Users</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="value">
                              <h1 class=" count5">
                                  0
                              </h1>
                              <p>Registered Users</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="value">
                              <h1 class=" count6">
                                  0
                              </h1>
                              <p>Unregistered Users</p>
                          </div>
                      </section>
                  </div>
                      
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="value">
                              <h1 class=" count2">
                                  0
                              </h1>
                              <p>Total News</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="value">
                              <h1 class=" count3">
                                  0
                              </h1>
                              <p>Total Feedback</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                      <section class="panel">
                          <div class="value">
                              <h1 class=" count4">
                                  0
                              </h1>
                              <p>Total News Categories</p>
                          </div>
                      </section>
                  </div>
              </div>		  
      
              <div id="morris">
                  <div class="row" style="margin-left: 2px;    margin-right: -18px;">
                      <div class="col-lg-6" style="margin-left: -2px;margin-right: -19px;">
                          <section class="panel">
                              <header class="panel-heading">
                                 Frequently Visited Pages
                              </header>
                              <div class="panel-body">
<!--                                <div id="piechart" style="width: 100%; height: 540px;"></div>-->
                                  <div id="piechart" style="width:100%; height: 330px;"></div>
                              </div>
                          </section>
                      </div>
                      <div class="col-lg-6" style="margin-right: -10px;">
                          <section class="panel">
                              <header class="panel-heading">
                                 Downloads Per Month - <?php echo date('Y') ?>
                              </header>
                              <div class="panel-body">
                                  <div id="columnchart_material" style="width: 100%; height: 330px;">
                                  </div>
<!--
                                  <div style="text-align:center;margin-top: 22px;">
                                  <span style="padding-left:38px;background: #425BA9;margin-right: 17px;"></span><span>Non Registered Users</span>
                                  <span style="padding-left:38px;background: #919191;margin-right: 17px;    margin-left: 17px;"></span><span>Registered Users</span>

                                  </div>
-->
                              </div>
                          </section>
                      </div>                      
                  </div>
              </div>

              <!-- page end-->
  </section>
</section>   
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>     
        <script src="/theme/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="/theme/js/jquery.scrollTo.min.js"></script>
        <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="/theme/js/respond.min.js" ></script>
        <script src="/theme/js/common-scripts.js"></script>

<!--
    <!-- script for this page only-->
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    

    <script type="text/javascript">
        
//        old pie chart 
//      google.charts.load("current", {packages:["corechart"]});
//      google.charts.setOnLoadCallback(drawChart);
//      function drawChart() {
//        var data = google.visualization.arrayToDataTable([
//          ['Registered Voters ', 'Unregistered Voters'],
//          ['Registered Voters',  80],
//          ['Unregistered Voters', 20]
//        ]);
//
//      var options = {
//        legend: 'none',
//        pieSliceText: 'label',
//        title: '',
//        colors: ['#425BA9', '#919191'],
//        pieStartAngle: 100,
//      };
//
//        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
//        chart.draw(data, options);
//      }
  
        
 google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', ''],
            <?php
            foreach($categoryArray as $keys => $values){
                $name = $categoryArray[$keys]['name'];
                $count = $categoryArray[$keys]['count'];
                echo "['$name', $count],";    
            }  
          ?>    
            
//          ['Find My Official', 15],
//          ['Contact My Union', 25],
//          ['Member Discounts', 40],
//          ['About', 13],
          ['Other', <?php echo $OthersCount ?>]
        ]);

        var options = {
          title: '',
          colors: ['#425ba9', '#5669a3','#6a769d','#7d8497','#919191']
            };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }        
        
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawCharts);

      function drawCharts() {
        var data = google.visualization.arrayToDataTable([

          ['', ''],
          <?php 
            $count = count($AppInstalled);
            foreach($AppInstalled as $key => $val){
                $month = $AppInstalled[$key]['Month'];
                $total = $AppInstalled[$key]['total'];
                if(($key + 1) == $count ){
                echo "['$month', $total]";    
                }
                else{
                echo "['$month', $total],";
                }
            }  
          ?>    
        ]);

        var options = {
//        width: 500,
//        height: 400,
        legend: { position: 'none' },
//        bar: { groupWidth: '75%' },            
          chart: {
            title: '',
            subtitle: ''
          },
        colors: ['#425BA9', '#919191']
            
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }        
        
        
$(window).resize(function(){
  drawChart();
  drawCharts();
});
        
        
    </script>
    
<script>
    
    
$('.count').each(function() {
  var $this = $(this),
      countTo = <?php echo $totalUser; ?>;
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 2000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  
  

});    
    
function countUp2(count)
{
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.count2'),
        run_count = 100,
        int_speed = 24;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

countUp2(<?php echo $totlaNews ?>);

function countUp3(count)
{
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.count3'),
        run_count = 100,
        int_speed = 24;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

countUp3(<?php echo $totalFeedback; ?>);

function countUp4(count)
{
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.count4'),
        run_count = 100,
        int_speed = 24;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

countUp4(<?php echo $totalcategories; ?>);

    
function countUp5(count)
{
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.count5'),
        run_count = 100,
        int_speed = 24;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

countUp5(<?php echo $RegisteredUser; ?>);


$('.count6').each(function() {
  var $this = $(this),
      countTo = <?php echo $UnregisteredUser; ?>;
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 2000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  
  

});        
    
</script>	    
    
    
    
    
