<?php
    include("headers/connect.php");
    include '_user-details.php';
    include("header.php");

//$categoryID = $_GET['categoryID'];
$url = "";
$redirect="";
$name = "";
//$query = "SELECT w.name FROM `webservice_category` wc, `webservices` w WHERE wc.`category` like '{$categoryID}' AND wc.webservice = w.id";
//$result = mysqli_query($con,$query);
//$row = mysqli_fetch_array($result);
//$fileName = $row['name'];
//if(isset($fileName))
//{
//	if($fileName 	!= "view.php" && $fileName != "news_category.php"){
//		$url = "{$fileName}?categoryID={$categoryID}";
//		$redirect = 1;
//		//header("Location: {$fileName}?categoryID={$categoryID}");
//	}
//}

?>


<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<style>
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -23px;
    margin-bottom: 15px;
}    
.col-lg-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -42px;
    position: relative;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
a {
    color: #58595b !important;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -66px;
    margin-left: 26px;
    margin-right: 26px;    
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.prev {
    border: none !important;
}
li.prev a{
    color: #919191 !important;
}
li.active {
        border: none !important;
}
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next {
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}    
    
</style>    
 </head>
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Membership | '.$title.'</title>';
    ?>   
<body class="fixed-top">
   <!-- BEGIN HEADER -->

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
      <h4 style="margin-left: 20px;margin-bottom: 45px;">Membership Feedback</h4>      
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
      <div class="panel-body">
       
         
         <div class="adv-table editable-table ">
<!--
                          <div class="clearfix"> <a href="insert_notification.php"><button type="button" class="btn btn-primary"> Add New <i class="icon-plus"></i> </button></a>
                          </div>
-->
                          <div class="space15"></div>
                <!-- BEGIN ADVANCED TABLE widget-->
                <?php
                if(isset($_GET['insert']) == 'true')
                {
                    echo"
                <div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>×</button>
                        <strong>Success!</strong> The Notification has been added.
                    </div>";
                }
            else if(isset($_GET['update']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been updated.
                </div>";
            }
            else if(isset($_GET['delete']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been Deleted.
                </div>";
            }
?>
             
                <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="editable-sample">
                <thead>
                    <tr>
                    <th width="5%">S.No</th>    
                    <th width="195px">Most useful features in union app</th>
                    <th width="224x">How easy is it to find information in app</th>	 
                    <th width="120px">Suggested Changes</th>
                  <th width="40px">Action</th>
                    </tr>
                   </thead>
					<?php
					$query = "SELECT * FROM `membership_feedback`";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$count = 0;
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
						$count++;
						$id = $row['id'];
						$features = $row['features'];
						$findInformation = $row['findInformation'];
						$recommend = $row['recommend'];
						$changes = $row['changes'];
						echo"
							<tr> 
                              <td>$count</td>
							  <td>$features</td>
							  <td>$findInformation</td>
							  <td>$changes</td>
							  <td><a class='btn_icon delete' id='$id' style='padding-bottom: 6px;width:47%;float:right;margin-right:15px;cursor:pointer'>
									<img style='width: 16px;' src='/theme/img/delete.svg'  /></a></td>
							 </tr>";						
					}
					

							?>
          </table>
        </div>                                   
      </div>
    </section>
  </section>
</section>

<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
  
    <script type="text/javascript" src="/theme/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/theme/assets/data-tables/DT_bootstrap.js"></script>
    <script src="/theme/js/respond.min.js" ></script>
    <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>

      <!--script for this page only-->
	      <script src="/theme/assets/toastr-master/toastr.js"></script> 
      <script src="/theme/js/editable-table.js"></script>

      <!-- END JAVASCRIPTS -->
      <script>
		  $(document).ready(function() {
              $('#editable-sample').DataTable();
              
          });
		  
		  
		  function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".delete", function (event) {
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                alert('Demo account cannot make any changes.');
            }
            else{
              
            if (confirm("Are you sure you want to delete this Feedback?")) {
                  var ID = this.id;
                    var $this = $(this);
            
            $.get("<?php echo $app_name ?>/backoffice/delete.php?member_id="+ID, function (data, status) {
                        
                            var obj = jQuery.parseJSON(data);
                                if (obj.response == 1) {

                                    var Row = $this.closest('tr');
                                    var nRow = Row[0];
                                    $('#editable-sample').dataTable().fnDeleteRow(nRow)
                                    showmessage('success','MemberShip Delete Successfully','successfully');
                                } 
                          });  
                }
            }
            });
		  
		 </script>

</body>
