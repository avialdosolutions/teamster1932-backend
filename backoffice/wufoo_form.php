<?php
	session_start();
	include("headers/connect.php");
    include '_user-details.php';
    include("header.php");

    $categoryID = $_GET['categoryID'];
    $title = @$_GET['title'];
    $PageTitle = strtoupper(str_replace('-',' ',$title));    

    $url = "";
    $redirect="";
    $name = "";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>'.$PageTitle.' | '.$title.'</title>';
    ?>
   <script>
   if(<?php echo $redirect;?> == 1){
			//alert('redirecting');
			window.location.href = '<?php echo $url; ?>';
   }
	</script>
<style>
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -23px;
    margin-bottom: 15px;
}    
.col-lg-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -42px;
    position: relative;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    box-shadow: 1px 3px 11px #888;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
}   
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -101px;
    margin-left: 26px;
    margin-right: 26px;    
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 1px 3px 11px #888;
    }    
</style>

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
        <?php        
        
            echo  '<h4 style="margin-left: 38px;margin-bottom: 45px;">'.$PageTitle.'</h4>';
        
        ?>
      
    <section class="panel" style="margin-left:34px;">
        
        <div class="panel-body">
       
        <?php 
		  		  $query = "select name from (select c.name ,c.id from `categories` c union select sc.name,sc.submenu_id from `subcategories` sc) `dd` where id ={$categoryID}";
				  $sth = $dbh->prepare($query);
				  $sth->execute();
				  $row = $sth->fetch(PDO::FETCH_ASSOC);
				  $name = $row['name'];
				?> 
                 <?php
			if(isset($_GET['insert']) == 'true')
			{
				echo"
			<div class='alert alert-success'>
					<button class='close' data-dismiss='alert'>×</button>
					<strong>Success!</strong> The form has been added.
				</div>";
			}
	 	else if(isset($_GET['update']) == 'true'){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The form has been updated.
            </div>";
		}
		else if(isset($_GET['delete']) == 'true'){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The form has been Deleted.
            </div>";
		}
?>
          
          
          
          
         
         <div class="adv-table editable-table ">
                            <?php 
                                if($fileName != 'Report Immigiration Raid' && $fileName != "Find My Elected officials") 
                                {?>
                          <div class="clearfix" style="float: right;margin-right: 285px;top: 19px;position: relative;"> <a class="add-button" style="visibility:hidden" href="/teamster1932/wufoo-form/<?php echo $_GET['title'].'/'.$categoryID; ?>/add"><button type="button" class="btn btn-primary"> Add Form <i class="icon-plus"></i> </button></a>
                          </div>
                                <?php } ?>
                          <div class="space15"></div>
                <table  class="display table table-bordered table-striped" id="editable-sample">
                                <thead>
                                  <tr>
                				    <th style="text-align: center;">S No</th>
                                    <th>Title</th>
                                    <th>Url</th>  
                                    <th style="text-align: center;">Edit</th> 
                                    <th style="text-align: center;">Delete</th> 
                                    <th style="text-align: center;">View</th>      
                              </tr>
                              </thead>
      <?php

				    $categoryID = $_GET['categoryID'];          
                    $title = @$_GET['title'];
                    $query = "SELECT * from `wufoo_form`  where categoryID = {$categoryID}";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$count = 0;
                    $counter = $sth->rowCount();
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
						$count++;
                        $id = $row['id'];
                        $Formtitle = $row['title'];
                        $url = $row['url'];                        
						$NewsTitle = strtolower(str_replace(' ','-',$row['title']));
                        
						
                        echo"
                           <tr class=''> 
                          <td style='text-align: center;width:7%'><a href='#'>{$count}</a></td>
                          <td  id='news_button' style='width:20%'>{$row['title']}</td>
                          <td  id='news_button' style='width:30%'>{$url}</td>
                          <td style='width:4%;text-align:center;'>
                          <a class='btn_icon' id='edit' href='/teamster1932/wufoo-form/$title/$categoryID/{$id}/edit' 
                          >
                          <img style='width: 21px;position:relative !important;' src='/theme/img/edit.svg' />
                          </a>														
                          </td>
                         <td style='width: 2%;text-align: center;'>

                         <button style='background:none;color:red' class='btn delete_news' type='button' id=$id>
                                    <img style='width: 17px;margin-top: -8px;' src='/theme/img/delete.svg' /></button>

                         </td>
                          <td style='width: 5%;text-align:center;padding-top: 7px;'>
                          <a target='_blank' href='$url' class='btn_icon'>
                          <img style='width: 23px;' src='/theme/img/view.svg' /> </a></td>

                          </tr>";
					
					}
					?>	
			  
			
			  
          </table>

                      </div>
                                   
      </div>
    </section>
   
    <!-- page end--> 
  </section>
</section>


    
<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>

    
    <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
    <script src="/theme/assets/toastr-master/toastr.js"></script>
    
      <!--script for this page only-->
      <script src="/theme/js/editable-table.js"></script>

      <script type="text/javascript" src="/theme/assets/data-tables/jquery.dataTables.js"></script> 
     <script type="text/javascript" src="/theme/assets/data-tables/DT_bootstrap.js"></script> 
       
   
<script>
    
    <?php 
        if($counter == 0){
            echo "$('.add-button').css('visibility','visible');";
        }
    
    ?>
    
</script>    
    
<!-- END JAVASCRIPTS -->
      <script>
       
   jQuery(document).ready(function() {
              EditableTable.init();
          });

      function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".delete_news", function (event) {
            if (confirm("Are you sure you want to delete this form?")) {
                  var ID = this.id;
                    var $this = $(this);
                   
            $.get("<?php echo $app_name ?>/backoffice/delete.php?form_id="+ID, function (data, status) {
                        
                            var obj = jQuery.parseJSON(data);
                                if (obj.response == 1) {

                                    var Row = $this.closest('tr');
                                    var nRow = Row[0];
                                    $('#editable-sample').dataTable().fnDeleteRow(nRow)
                                    showmessage('success','Form Deleted Successfully','successfully');
                                    $('.add-button').css('visibility','visible');
                                } 
                          });  
                }
            });
          
          
      </script>


