-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 09, 2017 at 02:06 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `UFCW1932`
--

-- --------------------------------------------------------

--
-- Table structure for table `app`
--

CREATE TABLE IF NOT EXISTS `app` (
  `app_id` int(3) NOT NULL,
  `about_us` varchar(5000) NOT NULL,
  `app_name` varchar(30) NOT NULL,
  `applicationID` varchar(40) NOT NULL,
  `clientKey` varchar(40) NOT NULL,
  `restKey` varchar(50) NOT NULL,
  `masterKey` varchar(50) NOT NULL,
  `version` decimal(10,1) NOT NULL,
  `about` longtext NOT NULL,
  `contact` longtext NOT NULL,
  `privacy` longtext NOT NULL,
  `pushCount` int(3) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app`
--

INSERT INTO `app` (`app_id`, `about_us`, `app_name`, `applicationID`, `clientKey`, `restKey`, `masterKey`, `version`, `about`, `contact`, `privacy`, `pushCount`) VALUES
(18, 'A Voice for working California', 'Teamsters Local 1932', '', '', '', '', '49.2', '<h4>TEAMSTERS LOCAL 1932 (FORMERLY SBPEA) MISSION STATEMENT:</h4>\r\n\r\n<p>It is the mission of Teamsters Local 1932 to provide the best possible service to all our members with integrity and equality; to advance the social, economic, and educational welfare of the membership; to promote professional working relationships and fair play between members and management alike; and to positively contribute to the communities we serve and live in.</p>\r\n\r\n<h1 style="text-align:center">&nbsp;</h1>\r\n\r\n<h1 style="text-align:center"><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/9WWbNFXELXM" width="390"></iframe></h1>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<h3><span style="font-family:georgia,serif"><strong>KNOW YOUR RIGHTS!</strong></span></h3>\r\n\r\n<h4><span style="font-family:georgia,serif">You may ask or hear some of these questions and more during the course of your employment:</span></h4>\r\n\r\n<ul>\r\n	<li><span style="font-family:georgia,serif">&ldquo;My manager just called me into a meeting. Am I entitled to have a union representative there?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can they just fire me?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How can I appeal a disciplinary action?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can I see a copy of my personnel file?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;What if my employer wants me to take a drug test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Do I have to take a polygraph (lie detector) test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How do I file a grievance?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Should I get overtime pay?&rdquo;</span></li>\r\n</ul>\r\n\r\n<h4><span style="font-family:georgia,serif">How do you get answers to these and other questions?</span></h4>\r\n\r\n<p><span style="font-family:georgia,serif">That&rsquo;s what Teamsters Local 1932 is all about.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">We will help you get the answers you </span><span style="font-family:georgia,serif">need,</span><span style="font-family:georgia,serif"> when you need them, and to make sure your rights are not violated.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Your job and your rights are precious to you and Teamsters Local 1932. We&rsquo;re committed to protecting them. Without a strong Union, your employer might take advantage of you, or intimidate you during investigations or meetings. Teamsters Local 1932 provides the balance so that you can deal with your situation and management on a level playing field.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Our expert knowledge of your contract, personnel </span><span style="font-family:georgia,serif">rules</span><span style="font-family:georgia,serif"> and current laws is the insurance you get by being a member of Teamsters Local 1932.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Critical information about your rights </span><span style="font-family:georgia,serif">change</span><span style="font-family:georgia,serif"> through new laws, contract language changes, and court decisions.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">You need Teamsters Local 1932 in your corner to make sure you get the respect and protection you deserve!</span></p>\r\n\r\n<h1>&nbsp;</h1>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif"><a href="http://www.teamsters1932.org/the-union/business-agents/">CONTACT YOUR BUSINESS REPRESENTATIVE</a></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<h2><span style="font-family:verdana,geneva,sans-serif">Weingarten Rights</span></h2>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">IF YOU ARE CALLED INTO A MEETING WITH ANY MANAGEMENT REPRESENTATIVE AND HAVE REASON TO BELIEVE THAT DISCIPLINARY ACTION MAY RESULT . . .</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Do the following:</span></p>\r\n\r\n<ul>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Ask your supervisor if you might be disciplined as a result of the interview. If he says &ldquo;NO&rdquo;, ask for a written statement to that effect. If he gives you such a statement, you must participate in the interview. If not, read him your Weingarten rights, remain for the meeting, take notes, and afterwards immediately contact your union representative.</span></li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">If he says you might be disciplined but will not allow you to have a union representative present, read him your Weingarten rights, stay in the room, take notes, and do not respond to any questions. Afterwards, contact your union representative immediately. If he allows your union representative to be present, you should participate in the interview.</span></li>\r\n</ul>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">YOUR WEINGARTEN RIGHTS . . .</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">&ldquo;If this discussion could in any way lead to my being disciplined or terminated, or affect my personal working conditions, I respectfully request that my union representative or steward be present at this meeting. If this discussion could lead to my being disciplined and you deny my request for representation, I choose not to answer any questions.&rdquo;</span></p>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">THE 1975 U.S. SUPREME COURT WEINGARTEN DECISION</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">In 1975 the U.S. Supreme Court ruled, in the Weingarten decision, that an employee is entitled to have a union representative present during any interview which may result in his or her discipline. It is up to you to insist on union representation. If you fail to do so, you may be waiving your rights!</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<h2><span style="font-family:verdana,geneva,sans-serif">Know Your Contract</span></h2>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">WHETHER YOU HAVE QUESTIONS ABOUT SICK LEAVE, HOLIDAYS, OVERTIME OR SENIORITY RIGHTS, THE FIRST PLACE TO LOOK FOR ANSWERS IS YOUR CONTRACT.</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Over the years, Teamster Local 1932 has bargained hundreds of contracts (also known as Memoranda of Understanding, or MOUs) covering thousands of public employees. Through the collective bargaining process, our members make their needs known to bargaining teams, who will then negotiate MOUs with the assistance of our professional staff.</span></p>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">These MOUs cover your rights and benefits on issues as varied as retirement benefits, health insurance premiums, maternity leave, educational incentives and dozens of other topics. No two MOUs are the same and we encourage all members to be familiar with what&rsquo;s in their contract, for two important reasons:</span></p>\r\n\r\n<ol>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">To ensure that your rights in the workplace are not being violated; and</span></li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">To build a base for improvements in wages, benefits and working conditions.</span></li>\r\n</ol>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Teamster Local 1932 wants our members to be well-informed and active participants in protecting the hard-fought gains of the many members who have come before them, and to ensure quality employment in the future for current and new employees alike.</span></p>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif"><a href="http://www.teamsters1932.org/contracts/">LOOKUP YOUR MEMORANDA OF UNDERSTANDING (MOU)</a></span></p>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Copies of your MOU should be provided to you by Human Resources during your &ldquo;New Employee Orientation.&rdquo;</span></p>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif"><input type="button" value="CONTACT YOUR BUSINESS REPRESENTATIVE" /></span></p>\r\n', '<p style="text-align:center"><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/Screen-Shot-2017-09-11-at-2.53.54-PM.png" style="height:30px; width:390px" /></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Teamsters Local 1932</strong><br />\r\n433 N. Sierra Way<br />\r\nSan Bernardino, CA 92410</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Phone:</strong>&nbsp;<a href="tel:909-889-8377">(909) 889-8377</a><br />\r\n<strong>Fax:</strong>&nbsp;(909) 888-7429</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Mailing Address:</strong><br />\r\nP.O. Box 432<br />\r\nSan Bernardino, CA 92402</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Office Hours:</strong><br />\r\nMonday- Friday, 7:30am &ndash; 5:30pm</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Media Inquiries:</strong><br />\r\nCommunications Coordinator Mario Vasquez<br />\r\n<a href="mailto:mvasquez@teamsters1932.org">mvasquez@teamsters1932.org</a></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h5><span style="font-family:verdana,geneva,sans-serif"><big>HAVE A QUESTION OR SUGGESTION? PLEASE FEEL FREE TO CONTACT US.</big></span></h5>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<form action="http://www.teamsters1932.org/contact/" enctype="multipart/form-data" id="gform_3" method="post">\r\n<ul>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Name</span>\r\n\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><input name="input_1" type="text" value="" /></span></p>\r\n	</li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Phone Number</span>\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><input name="input_2" type="tel" value="" /></span></p>\r\n	</li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Email*</span>\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><input name="input_3" type="email" value="" /></span></p>\r\n	</li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Message</span>\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><textarea cols="50" name="input_4" rows="10"></textarea></span></p>\r\n	</li>\r\n</ul>\r\n</form>\r\n', '<p><span style="font-family:verdana,geneva,sans-serif">Here is where we will add terms and conditions</span></p>\r\n', 30);

-- --------------------------------------------------------

--
-- Table structure for table `apps_plan`
--

CREATE TABLE IF NOT EXISTS `apps_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` int(5) NOT NULL,
  `plan_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `apps_plan`
--

INSERT INTO `apps_plan` (`id`, `app_id`, `plan_id`) VALUES
(1, 18, 3),
(2, 18, 4);

-- --------------------------------------------------------

--
-- Table structure for table `app_appearance`
--

CREATE TABLE IF NOT EXISTS `app_appearance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_version` varchar(10) NOT NULL,
  `app_logo` varchar(100) NOT NULL,
  `app_icon` varchar(100) NOT NULL,
  `splash_screen` varchar(100) NOT NULL,
  `register_button_color` varchar(100) NOT NULL,
  `language_button_color` varchar(100) NOT NULL,
  `local_button_color` varchar(100) NOT NULL,
  `state_button_color` varchar(100) NOT NULL,
  `national_button_color` varchar(100) NOT NULL,
  `logo_boder_color` varchar(100) NOT NULL,
  `tool_bar_arrow_color` varchar(100) NOT NULL,
  `toolbar_back_color` varchar(100) NOT NULL,
  `status_bar_background` varchar(20) NOT NULL,
  `menu_item_text_color` varchar(10) NOT NULL,
  `menu_navigation_color` varchar(10) NOT NULL,
  `menu_item_selected_color` varchar(50) NOT NULL,
  `info_button_color` varchar(10) NOT NULL,
  `login_button_text` varchar(10) NOT NULL,
  `language_button_enabled` varchar(10) NOT NULL,
  `status_bar_text_color` varchar(100) NOT NULL,
  `logo_background` varchar(100) NOT NULL,
  `logo_button_color` varchar(100) NOT NULL,
  `skip_button` varchar(100) NOT NULL,
  `primary_text_color` varchar(100) NOT NULL,
  `nav_bar_background` varchar(100) NOT NULL,
  `nav_bar_text` varchar(100) NOT NULL,
  `nav_arrow` varchar(100) NOT NULL,
  `menu_bar_color` varchar(100) NOT NULL,
  `second_logo_image` varchar(500) NOT NULL,
  `switch_thum_color` varchar(100) NOT NULL,
  `switch_thin_color` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `app_appearance`
--

INSERT INTO `app_appearance` (`id`, `app_version`, `app_logo`, `app_icon`, `splash_screen`, `register_button_color`, `language_button_color`, `local_button_color`, `state_button_color`, `national_button_color`, `logo_boder_color`, `tool_bar_arrow_color`, `toolbar_back_color`, `status_bar_background`, `menu_item_text_color`, `menu_navigation_color`, `menu_item_selected_color`, `info_button_color`, `login_button_text`, `language_button_enabled`, `status_bar_text_color`, `logo_background`, `logo_button_color`, `skip_button`, `primary_text_color`, `nav_bar_background`, `nav_bar_text`, `nav_arrow`, `menu_bar_color`, `second_logo_image`, `switch_thum_color`, `switch_thin_color`) VALUES
(1, '1.0-1.6', '611186.png', '809092.png', '538400.png', '#12547a', '#347fa5', '#555555', '#0b537b', '#555555', '#102c3b', '#ffffff', '#ab7171', '#12547a', '#112c3b', '#12547a', '#27b8cc', '#112c3b', 'Submit', 'false', 'white', '#12547a', '#112c3b     ', '#347fa5', '#112c3b', '#12547a', '#ffffff', '#ffffff', '#b58e8e', '713181.png', '#112c3b', '#12547a');

-- --------------------------------------------------------

--
-- Table structure for table `app_icon`
--

CREATE TABLE IF NOT EXISTS `app_icon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `app_icon`
--

INSERT INTO `app_icon` (`id`, `name`, `image`) VALUES
(1, 'About Teamster1932', 'info.png'),
(2, 'Member Discounts', 'save-icon.png'),
(3, 'Contracts', 'contract.png'),
(4, 'Member Resources', 'like-1.png'),
(5, 'Political Action', 'registertovote_img-1.png'),
(6, 'Negotiation Updates', 'News-1.png'),
(7, 'Immigration Guide', 'immigration-guide-1.png'),
(9, 'Member Services', 'memberservices.png'),
(10, 'Stay Informed', 'Megaphone-1.png'),
(17, 'Teamsters', '855454.png'),
(18, 'Team Adv', '758215.png'),
(19, 'Calendar', '772526.png'),
(20, 'No-Image', '399552.png'),
(21, 'Teamsters-icon', '989439.png');

-- --------------------------------------------------------

--
-- Table structure for table `app_relation`
--

CREATE TABLE IF NOT EXISTS `app_relation` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `parentID` int(6) NOT NULL,
  `catID` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `app_relation`
--

INSERT INTO `app_relation` (`id`, `parentID`, `catID`) VALUES
(1, 6, 24),
(2, 2, 24),
(3, 6, 39),
(4, 2, 39);

-- --------------------------------------------------------

--
-- Table structure for table `app_user`
--

CREATE TABLE IF NOT EXISTS `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `activate` int(11) NOT NULL,
  `verfiy_code` varchar(100) NOT NULL,
  `forget_token` varchar(100) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `app_user`
--

INSERT INTO `app_user` (`id`, `name`, `email`, `password`, `activate`, `verfiy_code`, `forget_token`, `time_stamp`) VALUES
(8, 'Henry Smith', 'henry.smith@mailinator.com', 'haider', 1, '895302', '920940', '2017-10-03 14:43:47'),
(9, 'Henry Smith', 'henry@mailinator.com', 'haider', 1, '215031', '351808', '2017-09-26 20:49:26'),
(7, 'Henry Smith', 'henrysmith@mailinator.com', 'henry', 1, '918132', '341023', '2017-09-26 19:15:45'),
(10, 'Zac', 'zac@avialdo.com', 'Hemani786!', 1, '803041', '', '2017-09-26 22:50:20'),
(11, 'Anonymous', '8C3AE65C-3543-4E53-8D0C-1F4F987090B6', 'Anonymous', 0, '172202', '', '2017-09-26 23:12:55'),
(12, 'Anonymous', 'A95C301D-7CCC-49BC-96D1-7DDE59AFA7B5', 'Anonymous', 0, '999474', '', '2017-09-26 23:14:53'),
(13, 'Steve Jones', 'steve@linkedunion.com', 'Tartans#44', 0, '149473', '', '2017-09-27 00:23:41'),
(14, 'Mario Vasquez', 'mvasquez@teamsters1932.org', 'marito182', 1, '983146', '', '2017-09-27 00:45:00'),
(15, 'Anonymous', 'B81CB007-F4E1-4873-9EE3-71DCAED0AD06', 'Anonymous', 0, '939448', '', '2017-09-27 03:24:56'),
(16, 'Anonymous', 'F437A751-5033-44E0-BA73-B2AD6F97BFD7', 'Anonymous', 0, '802587', '', '2017-09-27 06:56:54'),
(17, 'Justin Jones', 'justin@linkedunion.com', '2001062018jJ', 0, '558031', '', '2017-09-27 06:57:27'),
(18, 'Hammad', 'hamad.asghar@gmail.com', 'hammad10', 0, '180346', '', '2017-09-27 07:00:03'),
(19, 'Sarah', 'sarah@linkedunion.com', 'Hemani786!', 1, '387432', '', '2017-09-27 07:26:45'),
(20, 'Anonymous', '23A397CC-78A2-4084-81C3-223D61C82A4C', 'Anonymous', 0, '287873', '', '2017-09-28 09:46:47'),
(21, 'Anonymous', 'D7AC07B2-1E79-4DB4-8796-2A9913DBF593', 'Anonymous', 0, '440145', '', '2017-09-28 22:43:17'),
(23, 'John Doe', 'cottonclubclothing@gmail.com', 'haider', 0, '274037', '', '2017-09-29 10:21:59'),
(24, 'Steve Jones', 'jones436@gmail.com', 'LinkedUn10n', 1, '865828', '', '2017-10-03 21:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `bussines_agent`
--

CREATE TABLE IF NOT EXISTS `bussines_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bussiness_name` varchar(500) NOT NULL,
  `bussiness_email` varchar(500) NOT NULL,
  `bussiness_cell` varchar(500) NOT NULL,
  `bussiness_ext` varchar(500) NOT NULL,
  `steward_name` varchar(500) NOT NULL,
  `steward_email` varchar(500) NOT NULL,
  `steward_cell` varchar(500) NOT NULL,
  `steward_ext` varchar(500) NOT NULL,
  `depart_id` varchar(500) NOT NULL,
  `worksite_id` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bussines_agent`
--

INSERT INTO `bussines_agent` (`id`, `bussiness_name`, `bussiness_email`, `bussiness_cell`, `bussiness_ext`, `steward_name`, `steward_email`, `steward_cell`, `steward_ext`, `depart_id`, `worksite_id`) VALUES
(1, 'Tester', 'test@gmail.com', '32323232', 'ext', 'test', 'test@gmail.com', '56565656565', 'ext', '1', '4');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_id` int(5) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=737 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `app_id`, `icon`, `language`) VALUES
(664, 'Test', 18, '399552.png', 'spanish'),
(665, 'Testing', 18, 'info.png', 'spanish'),
(666, 'Main Menu 2', 18, 'registertovote_img-1.png', 'spanish'),
(667, 'Main Menu 3', 18, 'save-icon.png', 'spanish'),
(668, 'Main Menu 4', 18, '772526.png', 'spanish'),
(669, 'WEB VIEW', 18, 'info.png', 'spanish'),
(670, 'UNION REPRESENTATIVE', 18, 'info.png', 'spanish'),
(671, 'MEMBER DISCOUNTS', 18, 'info.png', 'spanish'),
(732, 'About ', 18, '989439.png', 'english'),
(733, 'members', 18, 'memberservices.png', 'english'),
(734, 'NEWS', 18, 'News-1.png', 'english'),
(735, 'POLITICAL ACTION', 18, 'registertovote_img-1.png', 'english'),
(736, 'Get Involved', 18, 'info.png', 'english');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_no1` varchar(20) DEFAULT NULL,
  `phone_no2` varchar(20) DEFAULT NULL,
  `fax_no` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `time_cone` varchar(15) DEFAULT NULL,
  `app_id` varchar(20) DEFAULT NULL,
  `order` int(2) NOT NULL,
  `public` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `website` varchar(250) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `name`, `designation`, `address`, `phone_no1`, `phone_no2`, `fax_no`, `email`, `time_cone`, `app_id`, `order`, `public`, `image`, `description`, `website`) VALUES
(1, 'SHERI ORELLANA', 'PRESIDENT', 'P.O. Box 432\r\nSan Bernardino, CA 92402																								', '(909) 889-8377', '(909) 889-8377', '(909) 888-7429', 'sorellana@teamsters1932.org', '2017-10-03 08:3', '18', 0, 1, '7981530.png', '<p>Details for Sheri Orellana will go here once provided from teamster group.</p>\r\n', 'www.myunionapp.com/web'),
(4, 'ROBERT NEVAREZ', 'VICE PRESIDENT', 'P.O. Box 432\r\nSan Bernardino, CA 92402			', '(909) 889-8377 ', '(909) 889-8377 ', '(909) 888-7429', 'info@teamsters1932.org', '2017-09-30 02:5', '18', 0, 0, '4678904.PNG', '', 'www.myunionapp.com/web'),
(5, 'REBECCA GALLEGOS', 'RECORDING SECRETARY', 'P.O. Box 432\r\nSan Bernardino, CA 92402					', '(909) 889-8377', '(909) 889-8377', '(909) 888-7429', 'info@teamsters1932.org', '2017-09-30 02:5', '18', 0, 0, '5846801.PNG', '', 'www.myunionapp.com/web'),
(6, 'RICH MCDOWELL', 'TRUSTEE', 'P.O. Box 432\r\nSan Bernardino, CA 92402				', '(909) 889-8377', '(909) 889-8377', '(909) 888-7429', 'info@teamsters1932.org', '2017-09-30 03:0', '18', 0, 0, '7161306.PNG', '', 'www.myunionapp.com/web'),
(7, 'KATHLEEN BRENNAN', 'TRUSTEE', 'P.O. Box 432\r\nSan Bernardino, CA 92402				', '(909) 889-8377', '(909) 889-8377', '(909) 888-7429', 'info@teamsters1932.org', '2017-09-30 03:0', '18', 0, 0, '5163683.PNG', '', 'www.myunionapp.com/web'),
(8, 'LAURA SHULTZ', 'TRUSTEE', 'P.O. Box 432\r\nSan Bernardino, CA 92402				', '(909) 889-8377', '(909) 889-8377', '(909) 888-7429', 'info@teamsters1932.org', '2017-09-30 03:0', '18', 0, 0, '2232384.PNG', '', 'www.myunionapp.com/web');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`) VALUES
(1, 'Sun Bernardio county'),
(2, 'Department');

-- --------------------------------------------------------

--
-- Table structure for table `get_started`
--

CREATE TABLE IF NOT EXISTS `get_started` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `office_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `office_title` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_no` varchar(15) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `time_cone` varchar(100) DEFAULT NULL,
  `app_id` varchar(20) DEFAULT NULL,
  `order` int(5) NOT NULL,
  PRIMARY KEY (`office_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`office_id`, `office_title`, `address`, `phone_no`, `website`, `time_cone`, `app_id`, `order`) VALUES
(37, 'UFCW Local 99 Tucson', '877 South Alvernon Way\nSuite 100\nTucson, Arizona 85711\n', '(520) 884-9716', 'ufcw99.org', '2015-04-01 20:10:24', '18', 1),
(38, 'UFCW Local 99 Phoenix', '2401 North Central Avenue\r\n2nd Floor\r\nPhoenix, Arizona 85004', '(602) 254-0099', 'ufcw99.org', '2015-04-01 20:07:59', '18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `maintainence_log`
--

CREATE TABLE IF NOT EXISTS `maintainence_log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `app_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `paymentDate` date NOT NULL,
  `validUpto` date NOT NULL,
  `transactionID` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `lastFour` int(4) NOT NULL,
  `customerID` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `exp_month` int(5) NOT NULL,
  `exp_year` int(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `country` varchar(10) NOT NULL,
  `plan_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(200) NOT NULL,
  `IsCheckpoint` int(11) NOT NULL,
  `intersection` varchar(1000) NOT NULL,
  `ethnicity` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `age` varchar(200) NOT NULL,
  `IsIdentity` int(11) NOT NULL,
  `IsForce` int(11) NOT NULL,
  `IsInsult` int(11) NOT NULL,
  `IsSexual` int(11) NOT NULL,
  `IsWeapon` int(11) NOT NULL,
  `OfficersName` varchar(300) NOT NULL,
  `badge` varchar(300) NOT NULL,
  `agency` varchar(300) NOT NULL,
  `IncidentDescription` varchar(3000) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `city`, `IsCheckpoint`, `intersection`, `ethnicity`, `gender`, `age`, `IsIdentity`, `IsForce`, `IsInsult`, `IsSexual`, `IsWeapon`, `OfficersName`, `badge`, `agency`, `IncidentDescription`, `timestamp`, `user_id`) VALUES
(7, 'Coty', 1, 'Intersecion', 'Middle Eastern', 'Male', 'Male', 1, 0, 0, 0, 1, 'Officer name', 'Officer badge', 'Police Sheriff', 'Incident', '2017-05-10 21:15:14', 0),
(8, 'Birmingham ', 1, 'Ok', 'Am. Indian or Alaska Native', 'Male', 'Male', 1, 1, 0, 1, 1, '', '', '', '', '2017-05-11 22:45:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `membership_feedback`
--

CREATE TABLE IF NOT EXISTS `membership_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `features` varchar(100) NOT NULL,
  `findInformation` varchar(100) NOT NULL,
  `recommend` varchar(100) NOT NULL,
  `changes` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `membership_feedback`
--

INSERT INTO `membership_feedback` (`id`, `features`, `findInformation`, `recommend`, `changes`) VALUES
(1, 'best features', 'very helpfull', 'yeah sure', 'not at all'),
(2, 'best features', 'very helpfull', 'yeah sure', 'not at all'),
(3, 'best features', 'very helpfull', 'yeah sure', 'not at all'),
(4, 'Other', 'Not at all easy', 'Not at all likely', 'Testing 2'),
(5, 'Member Resources', 'Slightly easy', 'Slightly likely', 'Testing 3'),
(6, 'Member Discounts', 'Very easy', 'Moderately likely', 'Testing 5'),
(10, 'Member Discounts', 'Very easy', 'Very likely', 'Nothing'),
(11, 'Shop Union', 'Extremely easy', 'Moderately likely', 'A'),
(12, 'Clock In / My Work Schedule', 'Extremely easy', 'Moderately likely', 'B'),
(13, 'Member Resources', 'Moderately easy', 'Extremely likely', 'App is really good.'),
(14, 'Contact my Business Agent/Steward', 'Moderately easy', 'Not at all likely', 'Enter Feedback..'),
(15, 'Contact my Business Agent/Steward', 'Extremely easy', '', 'Developer testing');

-- --------------------------------------------------------

--
-- Table structure for table `member_discount`
--

CREATE TABLE IF NOT EXISTS `member_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `website` varchar(200) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `short_description` varchar(250) NOT NULL,
  `type` varchar(50) NOT NULL,
  `order` varchar(100) NOT NULL,
  `public` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `member_discount`
--

INSERT INTO `member_discount` (`id`, `title`, `website`, `image`, `description`, `short_description`, `type`, `order`, `public`) VALUES
(13, 'INSURANCE', 'http://www.teamsters1932.org/', '3178142.PNG', '<p>Introducing the new Teamster Auto Insurance Program from MetLife Auto &amp; Home&reg;</p>\r\n\r\n<p>As a member of IBT you now have access to valuable features and benefits, including special group discounts on auto and home insurance offered through MetLife Auto &amp; Home &ndash; a leading provider of quality auto insurance coverage.</p>\r\n\r\n<p>This new program offers money-saving discounts not available through many other insurance programs, like:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Teamster member discount of up to 15%1</p>\r\n	</li>\r\n	<li>\r\n	<p>Length of membership discount of up to 20%2</p>\r\n	</li>\r\n	<li>\r\n	<p>Extra savings with automatic payment options</p>\r\n	</li>\r\n	<li>\r\n	<p>Multi-policy, multi-vehicle, good driver and anti-theft discounts</p>\r\n	</li>\r\n	<li>\r\n	<p>Special discounts for RV, condo or boat insurance</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>IT&rsquo;S VERY EASY TO SAVE MONEY. JUST CALL METLIFE AUTO &amp; HOME, MENTION THE TEAMSTERS DISCOUNT CODE: B6J, AND YOU COULD LOWER YOUR RATE IN MINUTES!</p>\r\n\r\n<p><a href="tel:1-877-491-5089">1-877-491-5089</a></p>\r\n', 'Discounted Teamster Auto Insurance Program Available     ', 'Lifestyle', '1', 1),
(17, 'LEGAL', 'http://www.teamsters1932.org/', '6800136.PNG', '<p>For a free case evaluation please call the offices of GEKLAW at&nbsp;<a href="tel:213-739-7000">(213) 739-7000</a>. For over 30 years, the law firm of Gordon Edelstein has established itself as a recognized leader in Workers&rsquo; Compensation and Personal Injury.</p>\r\n\r\n<p style="text-align: center;"><a href="http://www.geklaw.com/" target="_blank">www.geklaw.com</a></p>\r\n\r\n<p>For Pre-Paid Legal Services, Inc. is also available to our members for Legal and Identity Theft services. Call Denise Games at&nbsp;<a href="tel:951-837-0858">(951) 837-0858</a>.</p>\r\n', ' Free Case Evaluation Available', 'Lifestyle', '0', 1),
(18, ' LIBERTY MUTAL', 'http://www.teamsters1932.org/', '2793665.PNG', '<p>Teamsters Local 1932 members may find information on Home, Auto, and Life insurances offered by Liberty Mutual&nbsp;contact Tammy Gonzalez at&nbsp;<a href="tel:800-293-2518">1-800-293-2518</a>&nbsp;to find a plan that meets your needs or apply online:&nbsp;</p>\r\n\r\n<p style="text-align: center;"><a href="http://www.libertymutual.com/tammygonzalez" target="_blank">www.libertymutual.com/tammygonzalez</a></p>\r\n', ' Information Available On Home, Auto And Life Insurances ', 'Lifestyle', '0', 1),
(19, ' COLONIAL LIFE', 'http://www.teamsters1932.org/', '9473352.PNG', '<p>Teamsters Local 1932 members may contact Colonial Life about their insurance needs:</p>\r\n\r\n<h5>GARY L. DIMON, PUBLIC SECTOR MANAGER</h5>\r\n\r\n<ul>\r\n	<li><a href="tel:800-260-0783">(800) 260-0783</a>&nbsp;Toll-Free</li>\r\n	<li><a href="tel:909-763-2315">(909) 763-2315</a>&nbsp;Office</li>\r\n	<li><a href="mailto:gary.dimon@coloniallife.com">gary.dimon@coloniallife.com</a></li>\r\n</ul>\r\n\r\n<h5>JON RICHERT, PUBLIC SECTOR AREA MANAGER</h5>\r\n\r\n<ul>\r\n	<li><a href="tel:909-222-0659">(909) 222-0659</a>&nbsp;Cell</li>\r\n	<li><a href="tel:909-763-2315">(909) 763-2315</a>&nbsp;Office</li>\r\n	<li>(888) 809-5574 Fax</li>\r\n	<li><a href="mailto:jon.richert@coloniallife.com">jon.richert@coloniallife.com</a></li>\r\n</ul>\r\n\r\n<h5>CORINNE DUPREZ, CONTACT FOR GENERAL INFO/CLAIMS/BILLING:</h5>\r\n\r\n<ul>\r\n	<li><a href="tel:909-763-2315">(909) 763-2315</a>&nbsp;Office</li>\r\n	<li>(888) 809-5574&nbsp;Fax</li>\r\n	<li><a href="mailto:corinne.duprez@coloniallife.com">corinne.duprez@coloniallife.com</a></li>\r\n</ul>\r\n\r\n<p>Their office is at 425 North Sierra Way, San Bernardino CA 92410.<br />\r\nVisits are by appointment only.</p>\r\n\r\n<h5>THE FOLLOWING PLANS AVAILABLE TO TEAMSTERS LOCAL 1932 MEMBERS:</h5>\r\n\r\n<ul>\r\n	<li>Accident Insurance</li>\r\n	<li>Disability Coverage</li>\r\n	<li>Hospital-Medical Bridge Coverage</li>\r\n	<li>Cancer Insurance</li>\r\n	<li>Critical Illness Coverage</li>\r\n	<li>Life Insurance</li>\r\n</ul>\r\n', ' Catering To a Wide Range Of Insurance Needs', 'Entertainment', '0', 1),
(20, ' EYEMED', 'http://www.teamsters1932.org/', '2082278.PNG', '<p>Both you and your covered dependents will be eligible to enroll in one of three comprehensive EyeMed Premier plans to replace any current vision plan you are enrolled in through Teamster Local 1932.</p>\r\n\r\n<p>You may purchase one of these Premier plans via payroll deduction.&nbsp;<strong>Enrollment is optional.</strong></p>\r\n\r\n<p>You and your covered dependents may only cancel enrollment each year during Open Enrollment period in June. An exception may be made for a change in status by speaking with our Insurance Division at (909) 889-8377 ext. 234.</p>\r\n\r\n<p>A complete description of the three comprehensive Premier vision plans (EM 2, EM3, and Dependent Only) are part of the &ldquo;EyeMedVisionPlan.pdf&rdquo; below. To enroll in one of the optional comprehensive Premier vision plans, please do the following:</p>\r\n\r\n<ul>\r\n	<li>Select the plan that best suits your needs. EM 2 is designed for members who are currently enrolled in a medical plan that already covers an eye exam. EM3 is designed for members whose health plan does not cover exams or materials. Dependent Only is for members who are covered by Vision Insurance and wish to have their dependents covered with vision insurance. Compare the EyeMed benefit plans with any current Teamsters Local 1932 vision plan you are enrolled in for potential savings.</li>\r\n	<li>Download and complete the enrollment form below, and send to Teamsters Local 1932, P.O. box 432, San Bernardino, CA 92402. Payroll deductions will begin in the first pay period of the month following enrollment. Your Premier benefits will be available the first day of the month following payroll deduction.</li>\r\n	<li>Questions about your vision care plan should be directed to Teamsters Local 1932&rsquo;s Insurance Division at (909) 889-8377 ext. 234. To learn more about EyeMed or the EyeMed Provider Network, visit&nbsp;<a href="http://www.enrollwitheyemed.com/" target="_blank">www.enrollwitheyemed.com</a>&nbsp;or call EyeMed Member Services at 1-877-266-1115.</li>\r\n</ul>\r\n', ' Comprehensive EyeMed Premier Plans Available ', 'Health', '0', 1),
(21, 'RETIREES', 'http://www.teamsters1932.org/', '6919605.PNG', '<h3>DO YOU HAVE PLANS TO RETIRE SOON?</h3>\r\n\r\n<p>Are you thinking of retiring soon? Many of the Member benefits you enjoy now are included in your Retiree Membership with Teamsters Local 1932, as well as some that aren&rsquo;t &mdash; all possible for one low fee of $6.00 per year!</p>\r\n\r\n<h3>BENEFITS</h3>\r\n\r\n<ul>\r\n	<li>Enjoy discounted tickets to Movie Theater and Theme Parks</li>\r\n	<li>Discounts on See&rsquo;s Candy Orders during the Holiday Season</li>\r\n	<li>Thirty minutes of legal consultations</li>\r\n	<li>Safeguard Dental and Vision Insurance</li>\r\n	<li><strong>For more information or to receive your Retiree Packet contact the Teamsters Local 1932 Benefits Coordinator Paula Heredia at (909)889-8377 x 234 or visit us at 433 N. Sierra Way, San Bernardino, CA 92410</strong></li>\r\n</ul>\r\n', 'Retirement Benefits for Members', 'Health', '0', 1),
(22, ' NOTARY', 'http://www.teamsters1932.org/', '4709653.PNG', '<p>Notary Services are services rendered by a state commissioned notary public. These notary services include general notary acts such as acknowledgments, jurats, oaths, affirmations, protests, depositions, and more&hellip; The exact list of notary acts that a notary in your state is authorized to do varies from state to state. However, acknolwedgments, jurats, oaths, and affirmations are standard acts that are legal in almost every state.</p>\r\n\r\n<p>Notary services will be for Teamster Local 1932 members only by appointment.</p>\r\n\r\n<p>Appointments are scheduled&nbsp;on Tuesdays and Thursdays only. &nbsp;Please contact Anabel Palazuelos at (909) 889-8377 x217 or email&nbsp;<a href="mailto:apalazuelos@teamsters1932.org?subject=RE%3A%20Notary%20Services">apalazuelos@teamsters1932.org</a></p>\r\n', ' Notary Services Available Rendered by Notary Public', 'Lifestyle', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE IF NOT EXISTS `member_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberOfUnion` varchar(500) NOT NULL,
  `dateOfBirth` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(500) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `cell` varchar(100) NOT NULL,
  `cellPhoneProvider` varchar(100) NOT NULL,
  `sendMeTextMessages` varchar(500) NOT NULL,
  `registeredToVote` varchar(50) NOT NULL,
  `steward` varchar(50) NOT NULL,
  `getInvolvedInUnion` varchar(50) NOT NULL,
  `preferredCommunicationMethod` varchar(50) NOT NULL,
  `employeeID` varchar(50) NOT NULL,
  `countyEmployeeID` varchar(50) NOT NULL,
  `whereDoYouWork` varchar(100) NOT NULL,
  `consentToReceiveCommunication` varchar(500) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_topics`
--

CREATE TABLE IF NOT EXISTS `member_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `news_table` varchar(230) DEFAULT NULL,
  `description` varbinary(10000) NOT NULL,
  `file` varchar(100) NOT NULL,
  `time_cone` varchar(30) DEFAULT NULL,
  `category` varchar(20) NOT NULL,
  `app_id` int(11) NOT NULL,
  `new_image` varchar(500) NOT NULL,
  `is_private` int(11) NOT NULL,
  `facebook` varchar(20) DEFAULT NULL,
  `twitter` varchar(20) DEFAULT NULL,
  `google` varchar(20) DEFAULT NULL,
  `pinterest` varchar(20) DEFAULT NULL,
  `social` varchar(2) DEFAULT NULL,
  `order` int(100) DEFAULT NULL,
  `published` int(2) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1091 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `title`, `news_table`, `description`, `file`, `time_cone`, `category`, `app_id`, `new_image`, `is_private`, `facebook`, `twitter`, `google`, `pinterest`, `social`, `order`, `published`) VALUES
(1, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:40:19', '661', 18, '8078472.', 0, '', '', '', '', '', 0, 1),
(979, 'UFCW 135', 'Testing', '', '', '2017-08-01 04:59:58', '8', 18, '2455331.', 0, '', '', '', '', '', 0, 1),
(980, 'UFCW 135', 'Testing', '', '', '2017-08-03 01:20:43', '35', 18, '3765649.jpg', 0, '', '', '', '', '', 0, 1),
(981, 'UFCW 135', 'Sample', '', '', '2017-08-02 01:20:18', '36', 18, '7096422.jpg', 0, '', '', '', '', '', 0, 1),
(982, 'UFCW 135', 'Sample', '', '', '2017-08-02 01:20:45', '31', 18, '7628648.jpg', 0, '', '', '', '', '', 0, 1),
(985, 'UFCW 135', 'Sample', '', '', '2017-08-03 01:21:51', '37', 18, '3930597.jpg', 0, '', '', '', '', '', 0, 1),
(986, ' ', ' ', '', '', '2017-09-21 21:45:11', '808', 18, '2028257.jpg', 0, '', '', '', '', '', 0, 1),
(988, ' ', ' ', '<p style="text-align: center;"><strong>INSURANCE</strong></p>\r\n\r\n<p style="text-align: center;"><a href="http://www.teamsters1932.org/member-benefits/insurance/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/metlife.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Introducing the new Teamster Auto Insurance Program from MetLife Auto &amp; Home&reg; As a member of IBT you now have access to valuable features and benefits, including special group discounts on auto and home insurance offered through MetLife Auto &amp; Home &ndash; a leading provider of quality auto insurance coverage.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/insurance/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>AT&amp;T WIRELESS</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/att-wireless/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/att.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>15% Discount on wireless services from AT&amp;T As an IBT member, you can save 15%* on your monthly individual or family wireless plan from AT&amp;T, the nation&rsquo;s only union wireless company. Union families save an average of $110 a year on monthly service. Plus, the $36 activation fee is waived for union members.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/att-wireless/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>CREDIT CARDS</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/credit-cards/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/credit-card.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Apply Online:&nbsp;<a href="http://teamstercardnow.com/" target="_blank">teamstercardnow.com</a>&nbsp;SECURE. FAST. EASY. The Teamster Privilege Credit Card from Capital One&reg; Designed to meet the needs of hard-working Teamsters and their families. Choose from three card options,all with competitive rates, U.S.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/credit-cards/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>SCHOLARSHIPS</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/scholarships/"><img alt="scholarships" src="http://www.teamsters1932.org/app/uploads/2017/03/scholarships.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Scholarships for the 2016-2017 school year are now closed. Please, check back in the new school year, this fall, for information on the latest scholarship opportunity.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/scholarships/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>LEGAL</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/legal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/legal.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Legal Assistance: Complimentary and paid legal services are available to our members. To obtain information regarding a free 30-minute legal consultation, please call our office at (909) 889-8377. For Pre-Paid Legal Services, Inc. is also available to our members for Legal and Identity Theft services.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/legal/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>LIBERTY MUTAL</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/liberty-mutal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/liberty-mutual.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Teamsters Local 1932 members may also find information regarding Home, Auto, and Life insurances offered by Liberty Mutual contact Tammy Gonzalez at 1-800-293-2518 to find a plan that meets your needs or apply online: www.libertymutual.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/liberty-mutal/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>COLONIAL LIFE</strong></p>\r\n\r\n<p>Teamsters Local 1932 members may contact Colonial Life at (909) 889-8377 ext. 232 or Gary Diamond at (213) 308-7321 for information on the following plans available to Teamsters Local 1932 members.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/colonial-life/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>EYEMED</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/eyemed/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/eyemed.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Both you and your covered dependents will be eligible to enroll in one of three comprehensive EyeMed Premier plans to replace any current vision plan you are enrolled in through Teamster Local 1932.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/eyemed/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>RETIREES</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/retirees/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/retirement.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Do you have plans to retire soon? Are you thinking of retiring soon? If so, did you know you can take Teamsters Local 1932 with you?</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/retirees/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>NOTARY</strong></p>\r\n\r\n<p style="text-align: center;"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Notary services will be for members only, by appointment only on Tuesdays and Thursdays. Please contact Anabel Palazuelos.(909) 889-8377 x217.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/notary/">Learn More</a></p>\r\n', '', '2017-09-23 00:59:30', '821', 18, '1148743.jpg', 0, '', '', '', '', '', 0, 1),
(990, ' ', ' ', '<h1 style="text-align:center"><span style="font-family:georgia,serif">About Us</span></h1>\r\n\r\n<p><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/9WWbNFXELXM" width="390"></iframe></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<h3><span style="font-family:georgia,serif"><strong>Teamsters Local 1932 (formerly SBPEA) Mission Statement:</strong></span></h3>\r\n\r\n<p><span style="font-family:georgia,serif">It is the mission of Teamsters Local 1932 to provide the best possible service to all our members with integrity and equality; to advance the social, economic, and educational welfare of the membership; to promote professional working relationships and fair play between members and management alike; and to positively contribute to the communities we serve and live in.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif"><strong>Know Your Rights!</strong></span></p>\r\n\r\n<h4><span style="font-family:georgia,serif">You may ask or hear some of these questions and more during the course of your employment:</span></h4>\r\n\r\n<ul>\r\n	<li><span style="font-family:georgia,serif">&ldquo;My manager just called me into a meeting. Am I entitled to have a union representative there?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can they just fire me?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How can I appeal a disciplinary action?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can I see a copy of my personnel file?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;What if my employer wants me to take a drug test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Do I have to take a polygraph (lie detector) test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How do I file a grievance?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Should I get overtime pay?&rdquo;</span></li>\r\n</ul>\r\n\r\n<h4><span style="font-family:georgia,serif">How do you get answers to these and other questions?</span></h4>\r\n\r\n<p><span style="font-family:georgia,serif">That&rsquo;s what Teamsters Local 1932 is all about.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">We will help you get the answers you </span><span style="font-family:georgia,serif">need,</span><span style="font-family:georgia,serif"> when you need them, and to make sure your rights are not violated.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Your job and your rights are precious to you and Teamsters Local 1932. We&rsquo;re committed to protecting them. Without a strong Union, your employer might take advantage of you, or intimidate you during investigations or meetings. Teamsters Local 1932 provides the balance so that you can deal with your situation and management on a level playing field.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Our expert knowledge of your contract, personnel </span><span style="font-family:georgia,serif">rules</span><span style="font-family:georgia,serif"> and current laws is the insurance you get by being a member of Teamsters Local 1932.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Critical information about your rights </span><span style="font-family:georgia,serif">change</span><span style="font-family:georgia,serif"> through new laws, contract language changes, and court decisions.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">You need Teamsters Local 1932 in your corner to make sure you get the respect and protection you deserve!</span></p>\r\n', '', '2017-09-21 21:43:29', '820', 18, '4488246.jpg', 0, '', '', '', '', '', 0, 1),
(991, ' ', ' ', '<p style="text-align:center"><strong>BUSINESS AGENTS</strong></p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/SMatthews-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>STEVE MATTHEWS</strong></p>\r\n\r\n<p style="text-align:center"><strong>BUSINESS AGENT COORDINATOR</strong></p>\r\n\r\n<p><strong>Representing</strong>: City of San Bernadino Mid-Management, Chino (Prof, Tech and Clerical), Chino Valley Fire District</p>\r\n\r\n<p><strong>Supervisor: </strong>Randy Korgan<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:smatthews@teamsters1932.org">smatthews@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a>&nbsp; Ext. 238</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/sheri2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>SHERI ORELLANA</strong></p>\r\n\r\n<p><strong>Representing</strong>: City of San Bernadino Mid-Management, Chino (Prof, Tech and Clerical), Chino Valley Fire District</p>\r\n\r\n<p>e:&nbsp;<a href="mailto:sorellana@teamsters1932.org">sorellana@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377204">(909) 889-8377 Ext. 204</a>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/AWithers-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>ANTIONETTE MCDANIEL</strong></p>\r\n\r\n<p><strong>Representing:</strong> the City of Ontario, San Bernardino County Probation, San Bernardino County Public Defender, San Bernardino County District Attorney, and Human Services Personnel.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:awithers@teamsters1932.org">awithers@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377206">(909) 889-8377 Ext. 206</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/BethZen2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>BETH ZENDEJAS</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Airport, San Bernardino County Board of Supervisors,&nbsp;San Bernardino County Administrative Office, City of Colton Mid-Managers, San Bernardino County Counsel,&nbsp;San Bernardino County&nbsp;Human Resources, and&nbsp;San Bernardino County&nbsp;Sheriffs.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:bzendejas@teamsters1932.org">bzendejas@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377225">(909) 889-8377 Ext. 225</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/CarlosG2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>CARLOS GONZALES</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Workforce Development, Colton General, City of Redlands (GEAR), West Covina Non-Sworn Safety, City of Pomona (PCEA), and San Bernardino County Public Works.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:cgonzales@teamsters1932.org">cgonzales@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377261">(909) 889-8377 Ext. 261</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/DavidFarugia2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>DAVID FARUGIA</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County&nbsp;Agriculture Weights &amp; Measures, San Bernardino County&nbsp;Architecture &amp; Engineering, San Bernardino County&nbsp;Auditor/Controller/Tax Collector, City of Big Bear Lake, San Bernardino County&nbsp;Clerk of the Board,&nbsp;San Bernardino County&nbsp;Land Use Services,&nbsp;San Bernardino County&nbsp;Museum, San Bernardino County&nbsp;Parks, San Bernardino County&nbsp;Public Health &amp; Women, Infant, Children (WIC), San Bernardino County&nbsp;Register Voters, andSan Bernardino County&nbsp;Special Projects<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:dfarugia@teamsters1932.org">dfarugia@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377211">(909) 889-8377 Ext. 211</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/e-rommel.jpg" style="height:200px; width:200px" /></strong></p>\r\n\r\n<p style="text-align:center"><strong>E. ROMMEL MARTINEZ</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Assessor,&nbsp;San Bernardino County Children&rsquo;s Network,&nbsp;San Bernardino County Library,&nbsp;San Bernardino County Department of Aging &amp; Adult Services&nbsp;(DAAS),&nbsp;San Bernardino County Preschool Services,&nbsp;San Bernardino County Purchasing,&nbsp;San Bernardino County Risk Management,&nbsp;San Bernardino County Veteran&rsquo;s Affairs,&nbsp;Inland Counties Emergency Medical Agency (ICEMA) and Program Integrity Division (PID)<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:emartinez@teamsters1932.org">emartinez@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/jacqueline-palone.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>JACQUELINE PALONE</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;Arrowhead Regional Medical Center (ARMC), Behavioral Health at Arrowhead Regional Medical Center, and Superior Court of&nbsp;San Bernardino County.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:jpalone@teamsters1932.org">jpalone@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377228">(909) 889-8377 Ext. 228</a>&nbsp;</p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/juan-delgado.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>JUAN DELGADO</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Children and Family Services, San Bernardino County Child Support Services, San Bernardino County Information Services Department, &nbsp;San Bernardino County Facilities Management, and City of Coachella<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:jdelgado@teamsters1932.org">jdelgado@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/natalie-harts.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">NATALIE HARTS</h4>\r\n\r\n<p><strong>Representing:&nbsp;</strong>Transitional Assistance Department (TAD):TAD CSCs &ndash; ALL LOCATIONS: (Except SUP &amp; MGT), TAD Processing Ctrs: All (Except SUP &amp; MGT), TAD LOCATIONS: (Except SUP &amp; MGT) 01, 03, 04, 06, 07, 08, 10, 18, 19, 24, 48, 79; TAD LOCATIONS: (SUP &amp; MGT ONLY): 09, 15, 25, 39, 75/Employment Services Program (ESP)/Foster Care(FC),&nbsp;Air Quality Mojave Desert (AQMD) (Back-up to Fred)&nbsp;and City of Needles.</p>\r\n\r\n<p>e:&nbsp;<a href="mailto:nharts@teamsters1932.org">nharts@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377243">(909) 889-8377 Ext. 243</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/pete-sierra.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">PETE SIERRA</h4>\r\n\r\n<p><strong>Representing:</strong>&nbsp;&nbsp;Arrowhead Regional Medical Center (ARMC) and Behavioral Health at&nbsp;Arrowhead Regional Medical Center (ARMC).<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:psierra@teamsters1932.org">psierra@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/steve-cadena.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">STEVE CADENA</h4>\r\n\r\n<p><strong>Representing:&nbsp;</strong>City of Fontana (City Hall &amp; Yards), City of Rancho Cucamonga, City of Hesperia, City of Banning (Mid-Managers), City of Barstow, San Bernardino County&nbsp;Fleet Management and San Bernardino County Department of Behavioral Health.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:scadena@teamsters1932.org">scadena@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377218">(909) 889-8377 Ext. 218</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/susan-carl.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">SUSAN CARL</h4>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Schools (SBCSS),&nbsp;San Bernardino County&nbsp;Transitional Assistance Department (TAD):&nbsp;TAD CSCs &ndash; ALL LOCATIONS: (SUP/MGT Only), TAD Processing Ctrs &ndash; ALL, TAD LOCATIONS: (Except SUP &amp; MGT): 09, 15, 25, 39, 75 TAD LOCATIONS: (SUP &amp; MGT ONLY) 01, 02, 03, 04, 06, 07, 08, 10, 18, 19, 24, 48, 79,&nbsp;San Bernardino County&nbsp;Human Services Administration,&nbsp;San Bernardino County&nbsp;Performance, Education &amp; Resource Centers (PERC) and&nbsp;San Bernardino County&nbsp;Environmental Health Services.​​​​​​​<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:scarl@teamsters1932.org">scarl@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377216">(909) 889-8377 Ext. 216</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n', '', '2017-09-21 14:40:51', '807', 18, '3785300.jpg', 0, '', '', '', '', '', 0, 1),
(992, ' ', ' ', '<h1><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/news-1932.png" style="height:35px; width:500px" /></h1>\r\n\r\n<p><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/new-1932-selected.png" style="height:42px; width:500px" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/Screen-Shot-2017-09-11-at-3.17.34-PM.png" style="height:379px; width:500px" /></p>\r\n', '', '2017-09-11 14:45:04', '772', 18, '5089310.png', 0, '', '', '', '', '', 0, 1),
(1001, 'Sub 1', 'Sub Menu', '<p style="text-align: center;"><em><span style="color:#000080"><u><span style="font-size:48px"><span style="font-family:verdana,geneva,sans-serif"><big>Hello</big></span></span></u></span></em></p>\r\n', '', '2017-09-16 03:09:11', '280', 18, '6882106.', 0, '', '', '', '', '', 0, 1),
(1006, 'This is my first article.', 'Hurray!', '<p>PDF link:<br />\r\n<a href="http://goo.gl/5FYTRb">http://goo.gl/5FYTRb</a></p>\r\n\r\n<p><iframe frameborder="0" height="215" src="https://www.youtube.com/embed/aKiqln366lc" width="320"></iframe></p>\r\n', '', '2017-09-18 18:15:46', '303', 18, '6781961.', 0, '', '', '', '', '', 0, 1),
(1016, ' ', ' ', '<p style="text-align:center"><span style="font-size:14px"><strong>Teamsters Local 1932: Where Working People Stand Together</strong></span></p>\r\n\r\n<p><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/9WWbNFXELXM" width="390"></iframe></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><span style="font-size:14px"><strong>Teamster Advantage: Where Community Comes First</strong></span></p>\r\n\r\n<p><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/heKOzG9zG30" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><span style="font-size:14px"><strong>Reality vs. &quot;Right to Work&quot;</strong></span></p>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/YOoh5AjigxY" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align: center;"><span style="font-size:14px"><strong>The Ugly Truth of &quot;Right to Work&quot;</strong></span></p>\r\n<iframe width="390" height="230" src="https://www.youtube.com/embed/T3AtaP5gFTQ" frameborder="0" allowfullscreen></iframe>\r\n<p>&nbsp;</p>\r\n', '', '2017-09-21 13:12:58', '806', 18, '2181057.jpg', 0, '', '', '', '', '', 0, 1),
(1017, ' ', ' ', '<p style="text-align:center"><strong>Castle Park</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/insurance/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/metlife.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Promo Code (Add at top of home page): TL1932</p>\r\n\r\n<p style="text-align:center"><a href="http://www.castlepark.com/">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Wet &lsquo;n&rsquo; Wild Palm Springs</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/att-wireless/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/att.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Enter the following&nbsp;</p>\r\n\r\n<p style="text-align:center">Username: TEAMSTER</p>\r\n\r\n<p style="text-align:center">Password: 1932</p>\r\n\r\n<p style="text-align:center"><a href="http://prm-ps.secure.accesso.com/embed/login.php?m=159130&amp;emerchant_id=150077">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Splash Kingdom Waterpark</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/credit-cards/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/credit-card.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://bigairredlands.pfestore.com/retail/Teamsters/Default.aspx">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Costco</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/scholarships/"><img alt="scholarships" src="http://www.teamsters1932.org/app/uploads/2017/03/scholarships.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="https://costcomembershipoffer.com/purchase/purchase/Teamsters1932">Buy Costco Membership Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Legoland</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/legal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/legal.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="https://secure.legolandcaliforniaresort.com/LLC/shop/ViewItems.aspx?CG=8105&amp;C=8105">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Six Flags</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/liberty-mutal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/liberty-mutual.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Enter the following</p>\r\n\r\n<p style="text-align:center">Username: TEAM1932<br />\r\nPassword: SixFlags11</p>\r\n\r\n<p style="text-align:center"><a href="https://sf-la.secure.accesso.com/embed/login.php?m=34246&amp;emerchant_id=5011">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Universal Studios Hollywood</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/eyemed/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/eyemed.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://ushtix.com/1932teamsters">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Sea World</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/eyemed/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/eyemed.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Promo Code: SWCJC</p>\r\n\r\n<p style="text-align:center"><a href="http://seaworldparks.com/swcbusinesssales">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Knott&#39;s Berry Farm</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/retirees/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/retirement.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="https://ticketsatwork.com/tickets/KnottsBerryFarm/?&amp;company=KBF1932">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Aquarium of the Pacific</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://affiliatetickets.aquariumofpacific.org/affiliate.asp?ID=A9225F71-7B49-4929-8773-5D4B8A9E981A">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Davey&#39;s Locker</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://www.daveyslocker.com/fishing_full_day_49_offer.html">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Big Air Trampoline Park</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://bigairredlands.pfestore.com/retail/Teamsters/Default.aspx">Buy Tickets Here</a></p>\r\n', '', '2017-09-23 01:27:32', '822', 18, '5266274.jpg', 0, '', '', '', '', '', 0, 1),
(1019, ' ', ' ', '<h1 style="text-align:center">&ldquo;RIGHT TO WORK&rdquo;</h1>\r\n\r\n<h3 style="text-align:center">&ldquo;RIGHT TO WORK&rdquo; IS REDUCING THE WAGES. FIGHT BACK WITH US.</h3>\r\n\r\n<p>America&rsquo;s labor unions protect and seek to lift the standards for working families in this country. Deep-pocketed opponents of workers like you, however, are seeking to kill the labor movement through a variety of different forms. A legal challenge,&nbsp;<em>Janus v AFSCME</em>, that could result in the end of your constitutional right to participate in a union-funded by each member equally and fairly is fast approaching the U.S Supreme Court docket. This would impose across the country.&nbsp;what our opponents have termed &mdash; quite ridiculously &mdash; &nbsp;&ldquo;Right to Work&rdquo;. Move over, in February, anti-worker legislators in the House of Representatives introduced a bill into Congress that would result in the same outcome.</p>\r\n\r\n<p>The information below has been collected for you to read, share, and act on. Don&rsquo;t just pass the message on, stay involved so that you can be up to date on the best ways to fight back with our Union.</p>\r\n\r\n<p><strong>Watch our &ldquo;Right to Work&rdquo; explainer below to learn how &ldquo;Right to Work&rdquo; creates universal misery &ndash; both in the public and private sector:</strong></p>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/AFAFz2BhgYk?start=267&amp;feature=oembed" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>How does &ldquo;Right to Work&rdquo; reduce the wages? This one simple graph teaches you just how it&rsquo;s done.</strong></p>\r\n\r\n<p><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/05/RTWCycle-copy-e1495737407358.png" style="height:478px; width:390px" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Former U.S Secretary of Labor Robert Reich goes in depth on the subject in just 3 minutes:</strong></p>\r\n\r\n<p><iframe frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/lLV4P5Pq0_0?start=5&amp;feature=oembed" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>But what of the rotten and racist roots of &ldquo;Right to Work&rdquo; laws? While they currently remain unmentioned when talking about &ldquo;Right to Work&rdquo;, this can change by helping spread our findings on the matter. First, watch the Local 1932 presentation on the ugly truth behind &ldquo;right to work&rdquo; and start sharing. The information had been fully vetted through a variety of reputable sources, including an interview with a professor at the University of Arkansas, the state where &ldquo;Right to Work&rdquo; was truly born.</strong></p>\r\n\r\n<h6><strong>WATCH THE PRESENTATION, WITH NARRATION INCLUDED:</strong></h6>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/T3AtaP5gFTQ?feature=oembed" width="390"></iframe></p>\r\n\r\n<h6><strong>DOWNLOAD THE POWERPOINT AND RUN YOUR OWN PRESENTATION:&nbsp;<a href="http://www.teamsters1932.org/app/uploads/2017/05/RTW-5.3.17.ppsx">UGLY TRUTH OF RTW</a></strong></h6>\r\n\r\n<p><strong>And so, what effects does &ldquo;Right to Work&rdquo; have on the public at large once enacted? The numbers don&rsquo;t lie &ndash; &ldquo;Right to Work&rdquo; is WRONG and TOXIC.</strong></p>\r\n\r\n<p><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/05/RTWInfographic.png" style="height:419px; width:390px" /></p>\r\n\r\n<p><em>&nbsp;</em></p>\r\n\r\n<p><em><strong>Help stop &ldquo;Right to Work&rdquo; by either&nbsp;<a href="http://salsa4.salsalabs.com/o/50740/p/dia/action3/common/public/?action_KEY=20867" target="_blank">emailing</a>&nbsp;your elected representative in Congress or&nbsp;<a href="http://www.teamsters1932.org/political-action/">calling them</a>. Get their contact information by clicking on the &ldquo;POLITICAL ACTION&rdquo; tab at the top of this page, followed by the appropriate city selection.</strong></em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Why does it matter? Because working people standing together for their right to organize a union in their workplace has made our country stronger.&nbsp;</strong></p>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/STv4KWHbelM?feature=oembed" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>STAY UP TO DATE ON THE LATEST RTW NEWS AT <a href="http://www.weareonebigunion.com/press" target="_blank">WWW.WEAREONEBIGUNION.COM/PRESS</a></h3>\r\n', '', '2017-09-21 22:29:10', '841', 18, '7625100.jpg', 0, '', '', '', '', '', 0, 1),
(1020, ' ', ' ', '<p>&nbsp;</p>\r\n\r\n<p style="text-align: center;"><span style="font-size:28px">Click </span><span style="font-size:36px"><strong><a href="http://www.teamsters1932.org/publications-the-voice/">HERE</a></strong></span><span style="font-size:28px"> to view current publications of The Voice.</span></p>\r\n', '', '2017-09-21 22:35:36', '842', 18, '4776071.jpg', 0, '', '', '', '', '', 0, 1),
(1021, ' ', ' ', '<p><a href="http://www.teamsters1932.org/political-action/adelanto/"><img alt="adelanto" src="http://www.teamsters1932.org/app/uploads/2017/03/Adelanto-California.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<p>ADELANTO</p>\r\n\r\n<h5>Zip Code(s): 92301</h5>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/apple-valley/"><img alt="apple-valley" src="http://www.teamsters1932.org/app/uploads/2017/03/apple-valley.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>APPLE VALLEY</h5>\r\n\r\n<p>Zip Code(s): 92307, 92308</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/banning/"><img alt="banning" src="http://www.teamsters1932.org/app/uploads/2017/04/banning.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>BANNING</h5>\r\n\r\n<p>Zip Code(s): 92220</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/barstow/"><img alt="barstow" src="http://www.teamsters1932.org/app/uploads/2017/03/barstow.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>BARSTOW</h5>\r\n\r\n<p>Zip Code(s): 92311, 92312</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/chino/"><img alt="chino" src="http://www.teamsters1932.org/app/uploads/2017/03/chino-1.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>CHINO</h5>\r\n\r\n<p>Zip Code(s): 91708</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/chino-hills/"><img alt="chino-hills" src="http://www.teamsters1932.org/app/uploads/2017/03/chino.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>CHINO HILLS</h5>\r\n\r\n<p>Zip Code(s): 91709</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/coachella/"><img alt="coachella" src="http://www.teamsters1932.org/app/uploads/2017/04/coachella.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>COACHELLA</h5>\r\n\r\n<p>Zip Code(s): 92236</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/colton/"><img alt="coltan" src="http://www.teamsters1932.org/app/uploads/2017/03/colton.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>COLTON</h5>\r\n\r\n<p>Zip Code(s): 92324</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/fontana/"><img alt="fontana" src="http://www.teamsters1932.org/app/uploads/2017/04/fontana.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>FONTANA</h5>\r\n\r\n<p>Zip Code(s): 92335</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/grand-terrace/"><img alt="grand-terrace" src="http://www.teamsters1932.org/app/uploads/2017/03/grandterrace.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>GRAND TERRACE</h5>\r\n\r\n<p>Zip Code(s): 92313</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/hesperia/"><img alt="hesperia" src="http://www.teamsters1932.org/app/uploads/2017/04/hesperia.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>HESPERIA</h5>\r\n\r\n<p>Zip Code(s): 92340, 92344, 92345</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/highland/"><img alt="highland" src="http://www.teamsters1932.org/app/uploads/2017/04/highland.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>HIGHLAND</h5>\r\n\r\n<p>Zip Code(s): 92346</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/loma-linda/"><img alt="loma-linda" src="http://www.teamsters1932.org/app/uploads/2017/04/lomalinda.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>LOMA LINDA</h5>\r\n\r\n<p>Zip Code(s): 92354, 92350, 92357</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/montclair/"><img alt="montclair" src="http://www.teamsters1932.org/app/uploads/2017/04/montclair.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>MONTCLAIR</h5>\r\n\r\n<p>Zip Code(s): 91763</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/needles/"><img alt="needles" src="http://www.teamsters1932.org/app/uploads/2017/04/needles.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>NEEDLES</h5>\r\n\r\n<p>Zip Code(s): 92363</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/ontario/"><img alt="ontario" src="http://www.teamsters1932.org/app/uploads/2017/04/ontario.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>ONTARIO</h5>\r\n\r\n<p>Zip Code(s): 91758, 91761, 91762, 91764</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/pomona/"><img alt="pomona" src="http://www.teamsters1932.org/app/uploads/2017/04/pomona.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>POMONA</h5>\r\n\r\n<p>Zip Code(s): 91766, 91767, 91768, 91769</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/rancho-cucamonga/"><img alt="rancho-cucamonga" src="http://www.teamsters1932.org/app/uploads/2017/04/rancho-cucamonga.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>RANCHO CUCAMONGA</h5>\r\n\r\n<p>Zip Code(s): 91730, 91701, 91729, 91737, 91739</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/redlands/"><img alt="Redlands-ca" src="http://www.teamsters1932.org/app/uploads/2017/04/redlands.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>REDLANDS</h5>\r\n\r\n<p>Zip Code(s): 92375, 92373, 92374</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/rialto/"><img alt="Rialto" src="http://www.teamsters1932.org/app/uploads/2017/04/Rialto.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>RIALTO</h5>\r\n\r\n<p>Zip Code(s): 92376, 92377</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/san-bernardino/"><img alt="san-bernardino" src="http://www.teamsters1932.org/app/uploads/2017/04/sanbernardino.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>SAN BERNARDINO</h5>\r\n\r\n<p>Zip Code(s): 92401, 92403, 92404, 92405, 92407, 92408, 92410, 92411</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/twentynine-palms/"><img alt="twentynine-palms" src="http://www.teamsters1932.org/app/uploads/2017/04/twentyninepalms.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>TWENTYNINE PALMS</h5>\r\n\r\n<p>Zip Code(s): 92277, 92278</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/upland/"><img alt="upland" src="http://www.teamsters1932.org/app/uploads/2017/04/upland.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>UPLAND</h5>\r\n\r\n<p>Zip Code(s): 91785, 91786, 91784</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/victorville/"><img alt="victorville" src="http://www.teamsters1932.org/app/uploads/2017/04/victorville.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>VICTORVILLE</h5>\r\n\r\n<p>Zip Code(s): 92394, 92395, 92392, 92393</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/west-covina/"><img alt="west-covina" src="http://www.teamsters1932.org/app/uploads/2017/04/westcovina.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>WEST COVINA</h5>\r\n\r\n<p>Zip Code(s): 91791, 91792, 91793, 91790</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/yucaipa/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/04/yucaipa.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>YUCAIPA</h5>\r\n\r\n<p>Zip Code(s): 92399</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/yucca-valley/"><img alt="yucca-valley" src="http://www.teamsters1932.org/app/uploads/2017/04/yucca.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>YUCCA VALLEY</h5>\r\n\r\n<p>Zip Code(s): 92284, 92286</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/additional-cities/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/cities.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>ADDITIONAL CITIES</h5>\r\n', '', '2017-09-21 22:41:12', '843', 18, '8781416.jpg', 0, '', '', '', '', '', 0, 1),
(1023, 'Teamster Advantage', 'Teamster Advantage', '<p><iframe frameborder="0" height="315" src="https://www.youtube.com/embed/heKOzG9zG30" width="260"></iframe></p>\r\n\r\n<h5>Shopping locally is a vital&nbsp;component of a healthy economy. Rather than being shipped off to corporate headquarters when shoppers choose big business, by shopping locally, tax revenue is much more likely to recycle within the local economy.</h5>\r\n\r\n<h5>With Teamster Advantage, Teamsters&nbsp;across the Inland region like you are rewarded with discount offers by program partners in recognition of your hard work. Teamster Advantage allows you to shop locally, with big savings, so we can keep shopping locally.</h5>\r\n\r\n<h5>To qualify for Teamster Advantage discounts, you must present your Teamster Advantage Member ID to program partners.</h5>\r\n\r\n<h5>Don&#39;t have a Member ID? &nbsp;<u>Go to Teamsters Local 1932 Union Hall</u>&nbsp;and sign up, or call your Business Agent for more information. &nbsp;You must be a dues-paying member, in good standing, of Teamsters Local 1932 or Local 63. &nbsp;No sign-up fee for Teamster Advantage program participation.</h5>\r\n', '', '2017-09-23 01:47:38', '823', 18, '8002363.', 0, '', '', '', '', '', 0, 1);
INSERT INTO `news` (`news_id`, `title`, `news_table`, `description`, `file`, `time_cone`, `category`, `app_id`, `new_image`, `is_private`, `facebook`, `twitter`, `google`, `pinterest`, `social`, `order`, `published`) VALUES
(1024, 'Contracts', 'Contracts', '<h3>You can download the current Contracts by clicking on the links below:</h3>\r\n\r\n<h5>Chino Valley Fire District</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/CVIFD_2016_2018_1_.pdf" rel="noopener" target="_blank">Chino Valley Fire District (2016-2018)</a> </strong>Memorandum of Understanding Chino Valley Independent Fire District and Teamsters, Local 1932 Effective: July 16, 2016 through June 30, 2018 Adopted: June 8, 2016</li>\r\n</ul>\r\n\r\n<h5>City of Banning</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Resolution_No__2016_95.pdf" rel="noopener" target="_blank">Banning MOU, 2016-2017</a> </strong></li>\r\n</ul>\r\n\r\n<h5>City of Barstow</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Barstow_Unit_1___7_1_15_thru_6_30_18.pdf" rel="noopener" target="_blank">Barstow Unit 1 - 7-1-15 thru 6-30-18</a> </strong>Barstow Unit 1 - 7-1-15 thru 6-30-18</li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Barstow_Unit_2___7_1_15_thru_6_30_18.pdf" rel="noopener" target="_blank">Barstow Unit 2 - 7-1-15 thru 6-30-18</a> </strong>Barstow Unit 2 - 7-1-15 thru 6-30-18</li>\r\n</ul>\r\n\r\n<h5>City of Big Bear</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/BBL_MOU_7_1_12_to_6_30_15.pdf" rel="noopener" target="_blank"><strong>Big Bear Lake MOU, 2012-2015</strong></a></li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/08/Big-Bear-Lake-MOU-for-website.pdf">Big Bear Lake MOU, 2015-2018</a></strong></li>\r\n</ul>\r\n\r\n<h5>City of Chino</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/CHINO_MOU_PTC_Unit_2015_2019_For_Print_1_Reduced_Updated_10_2016_1_.pdf" rel="noopener" target="_blank"><strong>CHINO MOU PTC Unit_2015-2019_For Print-1 Reduced Updated 10-2016</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Coachella</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/FINALJuly2014June2017Confi.pdf" rel="noopener" target="_blank"><strong>2014-2017 City of Coachella Mid-Management Employees</strong></a> MEMORANDUM OF UNDERSTANDING BETWEEN THE CITY OF COACHELLA AND SAN BERNARDINO PUBLIC EMPLOYEES ASSOCIATION CONFIDENTIAL MID-MANAGEMENT EMPLOYEES JULY 1, 2014 TO JUNE 30, 2017</li>\r\n</ul>\r\n\r\n<h5>City of Colton</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Colton_General_Unit_MOU_2014_2016.pdf" rel="noopener" target="_blank"><strong>Colton General Unit MOU, 2014-2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Colton_Mid_Mgrs_MOU_3_1_14_to_12_31_16.pdf" rel="noopener" target="_blank"><strong>Colton Mid Managers MOU 2014-2016</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Fontana</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Fontana_City_Hall_MOU_2014_2017.pdf" rel="noopener" target="_blank"><strong>Fontana City Hall MOU 2014-2017</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Yard_MOU_2016_2017_Ratified.pdf" rel="noopener" target="_blank"><strong>Yard MOU 2016-2017 Ratified</strong></a> Yard MOU 2016-2017 Ratified</li>\r\n</ul>\r\n\r\n<h5>City of Hesperia</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/The_City_of_Hesperia_MOU.pdf" rel="noopener" target="_blank"><strong>Hesperia MOU 2016</strong></a> EXHIBIT A To Resolutions: Resolution No. 2016-003 and HWD 2016-02 MEMORANDUM OF UNDERSTANDING Made and Entered Into Between The City of Hesperia and the Teamsters Local 1932 Effective January 1, 2016 through December 31, 2016</li>\r\n</ul>\r\n\r\n<h5>City of Montclair</h5>\r\n\r\n<ul>\r\n	<li>No Files Found</li>\r\n</ul>\r\n\r\n<h5>City of Needles</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Needles_MOU___JUL_1_2015_to_JUN_30_2018.pdf" rel="noopener" target="_blank"><strong>Needles MOU 2015-2018</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Ontario</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Teamsters_Local_1932_Amendment_and_Extension_2013_2017.pdf" rel="noopener" target="_blank"><strong>Ontario MOU, 2013-2017</strong></a> Memorandum of Understanding Between Teamsters Local 1932 and City of Ontario July 1, 2013 though June 30, 2017</li>\r\n</ul>\r\n\r\n<h5>City of Pomona</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Pomona_PCEA_MOU_2014_2016.pdf" rel="noopener" target="_blank"><strong>Pomona MOU 2014-2016</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Rancho Cucamonga</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Rancho_MOU_2014_2017.pdf" rel="noopener" target="_blank"><strong>Rancho MOU, 2014-2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Redlands</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/MOU_GEAR_FY_2015_2017.pdf" rel="noopener" target="_blank"><strong>Redlands MOU 2015-2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of San Bernardino</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Middle_Management_Employees_MOU_City_of_San_Bernardino.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> MOU for City of San Bernardino Middle Management Employees</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2009.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2009</li>\r\n	<li><strong>Middle Management Employees</strong> (358.57 KB) Side Letter 2010</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2013.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2013</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2014.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2014</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2015a.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2015 part 1</li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2015b.pdf" rel="noopener" target="_blank">Middle Management Employees</a> </strong>Side Letter 2015 part 2</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2015c.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2015 part 3</li>\r\n</ul>\r\n\r\n<h5>County of San Bernardino</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBPEA_Teamsters_Local_1932_MOU_2015_2019_2_23_16.pdf" rel="noopener" target="_blank"><strong>2015-2019 County of San Bernardino MOU for Admin. Services, Clerical, Craft, Labor &amp; Trades, Management, Supervisory, Supervisory Nurses, and Technical &amp; Inspection Units</strong></a> San Bernardino County: Consolidated Memorandum of Understanding 2015-2019 Administrative Services; Clerical; Craft, Labor &amp; Trades; Management; Supervisory; Supervisory Nurses; and Technical &amp; Inspection Units.</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/County___2014_2017___ADM_CLK_CLT_MGT_SUP_SRN_TI_ONLY.pdf" rel="noopener" target="_blank"><strong>SB County Consolidated MOU, 2014-2017</strong></a> Administrative Services; Clerical; Craft, Labor &amp; Trades; Management; Supervisory; Supervisory Nurses; and Technical &amp; Inspection Units</li>\r\n</ul>\r\n\r\n<h5>East Valley Water District</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/EVWD_MOU_7_1_2014_to_6_30_2017.pdf" rel="noopener" target="_blank"><strong>EVWD MOU, 2014-2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>Mojave Desert Air Quality Management District</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/MOU_Adopted_9_26_2016_fnl.pdf" rel="noopener" target="_blank"><strong>Mojave Desert AQMD MOU 2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_Agreement_Rule_14.pdf" rel="noopener" target="_blank"><strong>Side Letter 2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>San Bernardino County Schools</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Health___Welfare_Contract_2014_15_School_YR.pdf" rel="noopener" target="_blank"><strong>SB County Schools Health &amp; Welfare Contract 2014-2015</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Health___Welfare_Cost_Sheet_2015_16_School_YR.pdf" rel="noopener" target="_blank"><strong>SB County Schools Health &amp; Welfare Cost Sheet 2015-2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Co_Schools_MOU_2014_2017.pdf" rel="noopener" target="_blank"><strong>SB County Schools MOU, 2014-2017</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Co_Schools_MOU_Evaluation_Procedures_7_6_2015.pdf" rel="noopener" target="_blank"><strong>SB County Schools MOU, Evaluation Procedures</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Side_Letter___Salary_Bonus_2015_16_School_Yr.pdf" rel="noopener" target="_blank"><strong>SB County Schools Side Letter - Salary Bonus 2015-2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Side_Letter___Leave_Provision_3_26_2015.pdf" rel="noopener" target="_blank"><strong>SB County Schools Side Letter- Leave Provision</strong></a></li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/08/SB-COUNTY-SCHOOLS-CONTRACT-2017-2020.pdf">SB County Schools Collective Bargaining Agreement, 2017-2020</a></strong></li>\r\n</ul>\r\n\r\n<h5>Superior Court of California, County of San Bernardino</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Superior_Court_of_Califonia_2015_2019.pdf" rel="noopener" target="_blank">2015 - 2019 MEMORANDUM OF UNDERSTANDING BETWEEN THE SUPERIOR ', '', '2017-09-23 01:49:36', '824', 18, '4717291.', 0, '', '', '', '', '', 0, 1),
(1025, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:41:14', '640', 18, '3063359.', 0, '', '', '', '', '', 0, 1),
(1026, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:41:31', '637', 18, '8309472.', 0, '', '', '', '', '', 0, 1),
(1027, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:41:41', '638', 18, '4511590.', 0, '', '', '', '', '', 0, 1),
(1028, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:41:52', '639', 18, '8657655.', 0, '', '', '', '', '', 0, 1),
(1029, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:42:23', '658', 18, '9547080.png', 0, '', '', '', '', '', 0, 1),
(1030, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:42:39', '659', 18, '7297093.png', 0, '', '', '', '', '', 0, 1),
(1031, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:42:51', '659', 18, '6522078.png', 0, '', '', '', '', '', 0, 1),
(1032, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:47:06', '660', 18, '1231315.', 0, '', '', '', '', '', 0, 1),
(1033, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-27 04:43:10', '661', 18, '6997407.', 0, '', '', '', '', '', 0, 1),
(1034, 'Teamsters Local 1932', 'Teamsters Local 1932', '', '', '2017-09-27 04:43:20', '662', 18, '1973694.', 0, '', '', '', '', '', 0, 1),
(1035, 'Teamsters Local 1932', 'Teamsters Local 1932', '<p>Teamsters Local 1932</p>\r\n', '', '2017-09-29 01:33:36', '659', 18, '8401408.', 0, '', '', '', '', '', 0, 1),
(1036, 'TOP 10 REASONS TO SUPPORT LOCALLY OWNED BUSINESSES', 'TOP 10 REASONS TO SUPPORT LOCALLY OWNED BUSINESSES', '<p><span style="font-size:14px">Teamster Advantage is the way our Union supports local business &mdash; helping to create a sustainable, healthy, and resilient economy. Download the official Teamster Advantage app on your phone&rsquo;s app store or visit&nbsp;<a href="http://teamsters1932.org/Advantage" target="_blank">Teamsters1932.org/Advantage</a>&nbsp;to&nbsp;start supporting local business with us.</span></p>\r\n\r\n<p><span style="font-size:14px"><strong>Why should you support locally owned businesses?</strong>&nbsp;The following list comes from the Institute for Local Self-Reliance (<a href="http://www.ilsr.org/" target="_blank">www.ilsr.org</a>), a national nonprofit organization working to strengthen independent businesses and local economies.&nbsp;</span></p>\r\n\r\n<h3><span style="font-size:14px">1.&nbsp; LOCAL CHARACTER AND PROSPERITY</span></h3>\r\n\r\n<p><span style="font-size:14px">In an increasingly homogenized world, communities that preserve their one-of-a-kind businesses and distinctive character have an economic advantage.</span></p>\r\n\r\n<h3><span style="font-size:14px">2.&nbsp; COMMUNITY WELL-BEING</span></h3>\r\n\r\n<p><span style="font-size:14px">Locally owned businesses build strong communities by sustaining vibrant town centers, linking neighbors in a web of economic and social relationships, and contributing to local causes.</span></p>\r\n\r\n<h3><span style="font-size:14px">3. LOCAL DECISION-MAKING</span></h3>\r\n\r\n<p><span style="font-size:14px">Local ownership ensures that important decisions are made locally by people who live in the community and who will feel the impacts of those decisions.</span></p>\r\n\r\n<h3><span style="font-size:14px">4.&nbsp; KEEPING DOLLARS IN THE LOCAL ECONOMY</span></h3>\r\n\r\n<p><span style="font-size:14px">Compared to chain stores, locally owned businesses recycle a much larger share of their revenue back into the local economy, enriching the whole community.</span></p>\r\n\r\n<h3><span style="font-size:14px">5.&nbsp; JOB AND WAGES</span></h3>\r\n\r\n<p><span style="font-size:14px">Locally owned businesses create more jobs locally and, in some sectors, provide better wages and benefits than chains do.</span></p>\r\n\r\n<h3><span style="font-size:14px">6.&nbsp; ENTREPRENEURSHIP</span></h3>\r\n\r\n<p><span style="font-size:14px">Entrepreneurship fuels America&rsquo;s economic innovation and prosperity and serves as a key means for families to move out of low-wage jobs and into the middle class.</span></p>\r\n\r\n<h3><span style="font-size:14px">7.&nbsp; PUBLIC BENEFITS AND COSTS</span></h3>\r\n\r\n<p><span style="font-size:14px">Local stores in town centers require comparatively little infrastructure and make more efficient use of public services relative to big box stores and strip shopping malls.</span></p>\r\n\r\n<h3><span style="font-size:14px">8.&nbsp; ENVIRONMENTAL SUSTAINABILITY</span></h3>\r\n\r\n<p><span style="font-size:14px">Local stores help to sustain vibrant, compact, walkable town centers-which, in turn, are essential to reducing sprawl, automobile use, habitat loss, and air and water pollution.</span></p>\r\n\r\n<h3><span style="font-size:14px">9.&nbsp; COMPETITION</span></h3>\r\n\r\n<p><span style="font-size:14px">A marketplace of tens of thousands of small businesses is the best way to ensure innovation and low prices over the long-term.</span></p>\r\n\r\n<h3><span style="font-size:14px">10.&nbsp; PRODUCT DIVERSITY</span></h3>\r\n\r\n<p><span style="font-size:14px">A multitude of small businesses, each selecting products based, not on a national sales plan, but on their own interests and the needs of their local customers, guarantees a much broader range of product choices.</span></p>\r\n', '', '2017-10-01 07:35:43', '839', 18, '5731164.', 0, '', '', '', '', '', 0, 1),
(1037, 'TOP 10 REASONS TO SUPPORT LOCALLY OWNED BUSINESSES', 'TOP 10 REASONS TO SUPPORT LOCALLY OWNED BUSINESSES', '<p><span style="font-size:14px">Teamster Advantage is the way our Union supports local business &mdash; helping to create a sustainable, healthy, and resilient economy. Download the official Teamster Advantage app on your phone&rsquo;s app store or visit&nbsp;<a href="http://teamsters1932.org/Advantage" target="_blank">Teamsters1932.org/Advantage</a>&nbsp;to&nbsp;start supporting local business with us.</span></p>\r\n\r\n<p><span style="font-size:14px"><strong>Why should you support locally owned businesses?</strong>&nbsp;The following list comes from the Institute for Local Self-Reliance (<a href="http://www.ilsr.org/" target="_blank">www.ilsr.org</a>), a national nonprofit organization working to strengthen independent businesses and local economies.&nbsp;</span></p>\r\n\r\n<h3><span style="font-size:14px">1.&nbsp; LOCAL CHARACTER AND PROSPERITY</span></h3>\r\n\r\n<p><span style="font-size:14px">In an increasingly homogenized world, communities that preserve their one-of-a-kind businesses and distinctive character have an economic advantage.</span></p>\r\n\r\n<h3><span style="font-size:14px">2.&nbsp; COMMUNITY WELL-BEING</span></h3>\r\n\r\n<p><span style="font-size:14px">Locally owned businesses build strong communities by sustaining vibrant town centers, linking neighbors in a web of economic and social relationships, and contributing to local causes.</span></p>\r\n\r\n<h3><span style="font-size:14px">3. LOCAL DECISION-MAKING</span></h3>\r\n\r\n<p><span style="font-size:14px">Local ownership ensures that important decisions are made locally by people who live in the community and who will feel the impacts of those decisions.</span></p>\r\n\r\n<h3><span style="font-size:14px">4.&nbsp; KEEPING DOLLARS IN THE LOCAL ECONOMY</span></h3>\r\n\r\n<p><span style="font-size:14px">Compared to chain stores, locally owned businesses recycle a much larger share of their revenue back into the local economy, enriching the whole community.</span></p>\r\n\r\n<h3><span style="font-size:14px">5.&nbsp; JOB AND WAGES</span></h3>\r\n\r\n<p><span style="font-size:14px">Locally owned businesses create more jobs locally and, in some sectors, provide better wages and benefits than chains do.</span></p>\r\n\r\n<h3><span style="font-size:14px">6.&nbsp; ENTREPRENEURSHIP</span></h3>\r\n\r\n<p><span style="font-size:14px">Entrepreneurship fuels America&rsquo;s economic innovation and prosperity and serves as a key means for families to move out of low-wage jobs and into the middle class.</span></p>\r\n\r\n<h3><span style="font-size:14px">7.&nbsp; PUBLIC BENEFITS AND COSTS</span></h3>\r\n\r\n<p><span style="font-size:14px">Local stores in town centers require comparatively little infrastructure and make more efficient use of public services relative to big box stores and strip shopping malls.</span></p>\r\n\r\n<h3><span style="font-size:14px">8.&nbsp; ENVIRONMENTAL SUSTAINABILITY</span></h3>\r\n\r\n<p><span style="font-size:14px">Local stores help to sustain vibrant, compact, walkable town centers-which, in turn, are essential to reducing sprawl, automobile use, habitat loss, and air and water pollution.</span></p>\r\n\r\n<h3><span style="font-size:14px">9.&nbsp; COMPETITION</span></h3>\r\n\r\n<p><span style="font-size:14px">A marketplace of tens of thousands of small businesses is the best way to ensure innovation and low prices over the long-term.</span></p>\r\n\r\n<h3><span style="font-size:14px">10.&nbsp; PRODUCT DIVERSITY</span></h3>\r\n\r\n<p><span style="font-size:14px">A multitude of small businesses, each selecting products based, not on a national sales plan, but on their own interests and the needs of their local customers, guarantees a much broader range of product choices.</span></p>\r\n', '', '2017-10-02 04:02:00', '974', 18, '7824100.', 0, '', '', '', '', '', 0, 1),
(1038, 'NOV. 4-5: ARBITRATION TRAINING AT THE UNION HALL', 'NOV. 4-5: ARBITRATION TRAINING AT THE UNION HALL', '<p><span style="font-size:14px">Space is limited! RSVP for this important training on Arbitration by emailing Natalie Harts at&nbsp;<a href="mailto:nharts@teamsters1932.org">nharts@teamsters1932.org</a></span></p>\r\n\r\n<p><span style="font-size:14px">Deadline to register is October 20th.</span></p>\r\n', '', '2017-10-01 07:45:49', '976', 18, '1525239.png', 0, '', '', '', '', '', 0, 1),
(1039, 'OCTOBER 21ST: TEAMSTERS LOCAL 1932 FAMILY DAY ï»¿AT GLEN HELEN REGIONAL PARK', 'OCTOBER 21ST: TEAMSTERS LOCAL 1932 FAMILY DAY ï»¿AT GLEN HELEN REGIONAL PARK', '<p><span style="font-size:14px">A day of fun and family with fellow Teamsters from across the region! Join us between 11 am &ndash; 5 pm on October 21st with picnic games and activities for all ages. Food will be provided!</span></p>\r\n\r\n<p><span style="font-size:14px">Will you take part in the fun with your family? RSVP&nbsp;<a href="http://www.teamsters1932.org/family-day-rsvp/" rel="noopener" target="_blank">here</a></span></p>\r\n', '', '2017-10-01 07:47:54', '974', 18, '9694958.jpg', 0, '', '', '', '', '', 0, 1),
(1040, 'ABOUT US', 'ABOUT US', '<p><strong>TEAMSTERS LOCAL 1932 (FORMERLY SBPEA) MISSION STATEMENT:</strong></p>\r\n\r\n<p>It is the mission of Teamsters Local 1932 to provide the best possible service to all our members with integrity and equality; to advance the social, economic, and educational welfare of the membership; to promote professional working relationships and fair play between members and management alike; and to positively contribute to the communities we serve and live in.</p>\r\n\r\n<p><iframe frameborder="0" height="275" src="https://www.youtube.com/embed/9WWbNFXELXM" width="560"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>KNOW YOUR RIGHTS!</strong></p>\r\n\r\n<p>YOU MAY ASK OR HEAR SOME OF THESE QUESTIONS AND MORE DURING THE COURSE OF YOUR EMPLOYMENT:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>&ldquo;My manager just called me into a meeting. Am I entitled to have a union representative there?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Can they just fire me?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;How can I appeal a disciplinary action?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Can I see a copy of my personnel file?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;What if my employer wants me to take a drug test?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Do I have to take a polygraph (lie detector) test?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;How do I file a grievance?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Should I get overtime pay?&rdquo;</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>HOW DO YOU GET ANSWERS TO THESE AND OTHER QUESTIONS?</p>\r\n\r\n<p>That&rsquo;s what Teamsters Local 1932 is all about.</p>\r\n\r\n<p>We will help you get the answers you need, when you need them, and to make sure your rights are not violated.</p>\r\n\r\n<p>Your job and your rights are precious to you and Teamsters Local 1932. We&rsquo;re committed to protecting them. Without a strong Union, your employer might take advantage of you, or intimidate you during investigations or meetings. Teamsters Local 1932 provides the balance so that you can deal with your situation and management on a level playing field.</p>\r\n\r\n<p>Our expert knowledge of your contract, personnel rules and current laws is the insurance you get by being a member of Teamsters Local 1932.</p>\r\n\r\n<p>Critical information about your rights change through new laws, contract language changes, and court decisions.</p>\r\n\r\n<p>You need Teamsters Local 1932 in your corner to make sure you get the respect and protection you deserve!</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/the-union/business-agents/">http://www.teamsters1932.org/the-union/business-agents/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="font-size:18px"><strong>Know Your Contract</strong></span></p>\r\n\r\n<p>WHETHER YOU HAVE QUESTIONS ABOUT SICK LEAVE, HOLIDAYS, OVERTIME OR SENIORITY RIGHTS, THE FIRST PLACE TO LOOK FOR ANSWERS IS YOUR CONTRACT.</p>\r\n\r\n<p>Over the years, Teamster Local 1932 has bargained hundreds of contracts (also known as Memoranda of Understanding, or MOUs) covering thousands of public employees. Through the collective bargaining process, our members make their needs known to bargaining teams, who will then negotiate MOUs with the assistance of our professional staff.</p>\r\n\r\n<p>These MOUs cover your rights and benefits on issues as varied as retirement benefits, health insurance premiums, maternity leave, educational incentives and dozens of other topics. No two MOUs are the same and we encourage all members to be familiar with what&rsquo;s in their contract, for two important reasons:</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>To ensure that your rights in the workplace are not being violated; and</p>\r\n	</li>\r\n	<li>\r\n	<p>To build a base for improvements in wages, benefits and working conditions.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Teamster Local 1932 wants our members to be well-informed and active participants in protecting the hard-fought gains of the many members who have come before them, and to ensure quality employment in the future for current and new employees alike.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/contracts/">http://www.teamsters1932.org/contracts/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '2017-10-02 12:39:34', '970', 18, '6050976.', 0, '', '', '', '', '', 0, 1),
(1041, 'VIDEOS', 'VIDEOS', '<p>TEAMSTERS LOCAL 1932: WHERE WORKING PEOPLE STAND TOGETHER</p>\r\n\r\n<p><iframe frameborder="0" height="250" src="https://www.youtube.com/embed/9WWbNFXELXM" width="560"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>TEAMSTER ADVANTAGE: WHERE COMMUNITY COMES FIRST!</p>\r\n\r\n<p><iframe frameborder="0" height="250" src="https://www.youtube.com/embed/heKOzG9zG30" width="560"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>REALITY VS &ldquo;RIGHT TO WORK&rdquo;</p>\r\n\r\n<p><iframe frameborder="0" height="250" src="https://www.youtube.com/embed/YOoh5AjigxY" width="560"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>THE UGLY TRUTH OF &ldquo;RIGHT TO WORK&rdquo;</p>\r\n\r\n<p><iframe frameborder="0" height="250" src="https://www.youtube.com/embed/T3AtaP5gFTQ" width="560"></iframe></p>\r\n', '', '2017-10-02 12:53:11', '971', 18, '3373345.', 0, '', '', '', '', '', 0, 1),
(1042, 'We Are Teamsters', 'We are Teamsters', '<p>Description of where we are in Teamster structure (IBT + JC 42), history</p>\r\n', '', '2017-10-02 13:04:29', '973', 18, '6012009.', 0, '', '', '', '', '', 0, 1),
(1043, 'ABOUT US', 'ABOUT US', '<p><strong>TEAMSTERS LOCAL 1932 (FORMERLY SBPEA) MISSION STATEMENT:</strong></p>\r\n\r\n<p>It is the mission of Teamsters Local 1932 to provide the best possible service to all our members with integrity and equality; to advance the social, economic, and educational welfare of the membership; to promote professional working relationships and fair play between members and management alike; and to positively contribute to the communities we serve and live in.</p>\r\n\r\n<p><iframe frameborder="0" height="240" src="https://www.youtube.com/embed/9WWbNFXELXM" width="560"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '2017-10-07 22:38:16', '1296', 18, '1674507.', 0, '', '', '', '', '', 0, 1),
(1044, 'VIDEOS', 'VIDEOS', '<p>TEAMSTERS LOCAL 1932: WHERE WORKING PEOPLE STAND TOGETHER</p>\r\n\r\n<p><iframe frameborder="0" height="240" src="https://www.youtube.com/embed/9WWbNFXELXM" width="560"></iframe></p>\r\n\r\n<p>TEAMSTER ADVANTAGE: WHERE COMMUNITY COMES FIRST!</p>\r\n\r\n<p><iframe frameborder="0" height="240" src="https://www.youtube.com/embed/heKOzG9zG30" width="560"></iframe></p>\r\n\r\n<p>REALITY VS &ldquo;RIGHT TO WORK&rdquo;</p>\r\n\r\n<p><iframe frameborder="0" height="240" src="https://www.youtube.com/embed/YOoh5AjigxY" width="560"></iframe></p>\r\n\r\n<p>THE UGLY TRUTH OF &ldquo;RIGHT TO WORK&rdquo;</p>\r\n\r\n<p><iframe frameborder="0" height="240" src="https://www.youtube.com/embed/T3AtaP5gFTQ" width="560"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '2017-10-02 13:43:53', '1297', 18, '8120580.', 0, '', '', '', '', '', 0, 1),
(1045, 'Teamster Advantage', 'Teamster Advantage', '<p><span style="font-family:arial,helvetica,sans-serif">Start saving today with Teamster Advantage &mdash; a <strong>members-only program</strong>, founded in November 2016, to stop the flow of taxpayer funds from leaving our communities when shoppers instead choose big business.</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif">By shopping locally, we keep our home economy prosperous, sustainable, and resilient, creating a stable tax base that funds the popular services provided for residents by hard-working public employees.</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif">Through partnerships with hundreds of local businesses, <strong>Teamster Advantage helps members shop locally while saving money</strong>, too, thanks to discounts offered for members only.</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif">Download the official Teamster Advantage app on your phone&rsquo;s app store to find our partners:</span>&nbsp;<span style="font-family:arial,helvetica,sans-serif"><a href="https://itunes.apple.com/us/app/teamster-advantage/id1260905482?mt=8">Apple</a> or <a href="https://play.google.com/store/apps/details?id=com.app.teamsteradvantage&amp;hl=en">Android</a></span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif">No cost for participation because with Teamster Advantage <em>community comes first</em>.</span></p>\r\n\r\n<hr />\r\n<p><span style="font-family:arial,helvetica,sans-serif"><strong>Don&rsquo;t have a Teamster Advantage ID card needed to qualify for our discounts? Reach out to your Local 1932 or Local 63 Business Agent today to start the application process.</strong></span></p>\r\n', '', '2017-10-07 21:29:01', '1304', 18, '6969776.PNG', 0, '', '', '', '', '', 0, 1),
(1046, 'Links to Vote', 'Links to Vote', '<p>links to CA Registrar of Voters will go here.</p>\r\n', '', '2017-10-02 13:57:26', '1311', 18, '6983287.', 0, '', '', '', '', '', 0, 1),
(1047, 'Endorsement', 'Endorsement ', '<p>Description of process/ upcoming endorsement meetings/ list of who we have endorsed will go here.</p>\r\n', '', '2017-10-02 13:58:26', '1312', 18, '9206656.', 1, '', '', '', '', '', 0, 1),
(1048, 'Join Power Network', 'Join Power Network', '<p>&nbsp;Description of new team for members who want to get involved beyond Steward status + App, will go here.</p>\r\n', '', '2017-10-02 14:00:12', '1313', 18, '2471268.', 0, '', '', '', '', '', 0, 1),
(1049, 'Political Activism', 'Political Activism', '<p>Description of how to get involved (endorsement process, door to door volunteer, phone bank volunteering) will go here.</p>\r\n', '', '2017-10-02 14:00:52', '1314', 18, '1803502.', 0, '', '', '', '', '', 0, 1),
(1050, 'We are teamsters', 'We are teamsters', '<p>Single page description of where we are in Teamster structure (IBT + JC 42), history, will go here.&nbsp;</p>\r\n', '', '2017-10-02 14:02:34', '1299', 18, '4621479.', 0, '', '', '', '', '', 0, 1),
(1051, ' ', ' Our union is coming together Tuesday, October 24th for our next Monthly General Membership Meeting.', '<h1>OCT. 24: GENERAL MEMBERSHIP MEETING</h1>\r\n\r\n<p><em>October 3, 2017</em></p>\r\n\r\n<p>Our union is coming together Tuesday, October 24th for our next Monthly General Membership Meeting.</p>\r\n\r\n<p>If you haven&rsquo;t attended a General Membership Meeting in the past, join us! Teamsters from across the region each month to learn about our Union&rsquo;s latest projects, events, and what each and every one of us can do to transform the Inland Empire.</p>\r\n\r\n<p>The meeting will start at 5:30 PM, with food provided after the meeting. See you there Teamsters!</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '2017-10-03 16:49:01', '1316', 18, '5918716.png', 0, '', '', '', '', '', 0, 1),
(1052, 'Castle Park', 'Castle Park', '<p style="text-align:center"><a href="https://www.castlepark.com/"><span style="font-family:arial,helvetica,sans-serif">Tap Here for Discount Tickets</span></a></p>\r\n\r\n<p style="text-align: center;"><span style="font-family:arial,helvetica,sans-serif">Promo Code (Add at top of home page): TL1932</span></p>\r\n', '', '2017-10-08 11:44:18', '1302', 18, '7524041.jpg', 0, '', '', '', '', '', 0, 1),
(1053, 'Fiesta Village', 'Fiesta Village', '<p style="text-align:center"><a href="http://www.teamsters1932.org/app/uploads/2017/09/company-flyer-NEW-0917-Teamsters.pdf">Tap here for Discount Flyer</a>.</p>\r\n\r\n<p style="text-align:center">Must be presented for discount.</p>\r\n', '', '2017-10-07 21:43:12', '1302', 18, '1914718.png', 0, '', '', '', '', '', 0, 1),
(1054, 'Wet â€˜nâ€™ Wild Palm Springs', 'Wet â€˜nâ€™ Wild Palm Springs', '<p style="text-align:center"><span style="font-family:arial,helvetica,sans-serif"><a href="https://prm-ps.secure.accesso.com/embed/login.php?m=159130&amp;emerchant_id=150077">Tap here for Discount Tickets</a></span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:arial,helvetica,sans-serif">Username: TEAMSTER<br />\r\nPassword: 1932</span></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n', '', '2017-10-07 21:43:17', '1302', 18, '3968359.png', 0, '', '', '', '', '', 0, 1),
(1055, 'Splash Kingdom Waterpark', 'Splash Kingdom Waterpark', '<p style="text-align: center;"><span style="font-family:arial,helvetica,sans-serif"><a href="http://bigairredlands.pfestore.com/retail/Teamsters/Default.aspx">Tap here for Discount Tickets</a></span></p>\r\n', '', '2017-10-07 21:46:08', '1302', 18, '4423202.png', 0, '', '', '', '', '', 0, 1),
(1056, 'Legoland', 'Legoland', '<p style="text-align: center;"><a href="https://secure.legolandcaliforniaresort.com/LLC/shop/ViewItems.aspx?CG=8105&amp;C=8105"><span style="font-family:arial,helvetica,sans-serif">Tap here for Discount Tickets</span></a></p>\r\n', '', '2017-10-07 21:46:58', '1302', 18, '2663952.png', 0, '', '', '', '', '', 0, 1),
(1057, 'Six Flags', 'Six Flags', '<p style="text-align: center;"><span style="font-family:arial,helvetica,sans-serif"><a href="https://sf-la.secure.accesso.com/embed/login.php?m=34246&amp;emerchant_id=5011">Tap here for Discount tickets</a></span></p>\r\n\r\n<p style="text-align: center;"><span style="font-family:arial,helvetica,sans-serif">Username: TEAM1932<br />\r\nPassword: SixFlags11</span></p>\r\n', '', '2017-10-07 21:48:11', '1302', 18, '7393593.png', 0, '', '', '', '', '', 0, 1),
(1058, 'Universal Studios Hollywood', 'Universal Studios Hollywood', '<p style="text-align: center;"><a href="http://ushtix.com/1932teamsters"><span style="font-family:arial,helvetica,sans-serif">Tap here for Discount tickets</span></a></p>\r\n', '', '2017-10-07 21:48:56', '1302', 18, '4558269.png', 0, '', '', '', '', '', 0, 1),
(1059, 'Sea World', 'Sea World', '<p style="text-align: center;"><span style="font-family:arial,helvetica,sans-serif"><strong>Promo Code: SWCJC</strong></span></p>\r\n', '', '2017-10-07 21:51:17', '1302', 18, '8862823.png', 0, '', '', '', '', '', 0, 1),
(1060, 'Chino Valley Fire District', 'Chino Valley Fire District', '<p style="text-align: center;"><span style="font-family:arial,helvetica,sans-serif"><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/CVIFD_2016_2018_1_.pdf" rel="noopener" target="_blank">Chino Valley Fire District (2016-2018)</a></strong></span></p>\r\n', '', '2017-10-07 22:09:28', '1305', 18, '6612961.jpg', 0, '', '', '', '', '', 0, 1),
(1061, 'City of Banning', 'City of Banning', '', '', '2017-10-07 21:56:57', '1305', 18, '3569336.', 0, '', '', '', '', '', 0, 1),
(1062, 'City of Barstow', 'City of Barstow', '', '', '2017-10-07 21:57:14', '1305', 18, '4279997.', 0, '', '', '', '', '', 0, 1),
(1063, 'City of Big Bear', 'City of Big Bear', '', '', '2017-10-07 21:57:19', '1305', 18, '9430697.', 0, '', '', '', '', '', 0, 1),
(1064, 'City of Chino', 'City of Chino', '', '', '2017-10-07 21:58:12', '1305', 18, '4403344.', 0, '', '', '', '', '', 0, 1),
(1065, 'City of Coachella', 'City of Coachella', '', '', '2017-10-07 21:58:28', '1305', 18, '9005855.', 0, '', '', '', '', '', 0, 1),
(1067, 'City of Colton', 'City of Colton', '', '', '2017-10-07 21:58:56', '1305', 18, '9639330.', 0, '', '', '', '', '', 0, 1),
(1068, 'City of Fontana', 'City of Fontana', '', '', '2017-10-07 21:59:07', '1305', 18, '9411738.', 0, '', '', '', '', '', 0, 1),
(1069, 'City of Hesperia', 'City of Hesperia', '', '', '2017-10-07 21:59:34', '1305', 18, '7978569.', 0, '', '', '', '', '', 0, 1),
(1071, 'City of Needles', 'City of Needles', '', '', '2017-10-07 22:00:16', '1305', 18, '3868825.', 0, '', '', '', '', '', 0, 1),
(1072, 'City of Ontario', 'City of Ontario', '', '', '2017-10-07 22:00:37', '1305', 18, '1022236.', 0, '', '', '', '', '', 0, 1),
(1073, 'City of Pomona', 'City of Pomona', '', '', '2017-10-07 22:03:34', '1305', 18, '7560702.', 0, '', '', '', '', '', 0, 1),
(1074, 'City of Rancho Cucamonga', 'City of Rancho Cucamonga', '', '', '2017-10-07 22:03:45', '1305', 18, '7234247.', 0, '', '', '', '', '', 0, 1),
(1075, 'City of Redlands', 'City of Redlands', '', '', '2017-10-07 22:03:55', '1305', 18, '5613590.', 0, '', '', '', '', '', 0, 1),
(1076, 'City of San Bernardino', 'City of San Bernardino', '', '', '2017-10-07 22:04:07', '1305', 18, '1261813.', 0, '', '', '', '', '', 0, 1),
(1077, 'County of San Bernardino', 'County of San Bernardino', '', '', '2017-10-07 22:04:32', '1305', 18, '8197741.', 0, '', '', '', '', '', 0, 1),
(1078, 'East Valley Water District', 'East Valley Water District', '', '', '2017-10-07 22:04:43', '1305', 18, '2202345.', 0, '', '', '', '', '', 0, 1),
(1079, 'Mojave Desert Air Quality Management District', 'Mojave Desert Air Quality Management District', '', '', '2017-10-07 22:04:55', '1305', 18, '9058210.', 0, '', '', '', '', '', 0, 1),
(1080, 'San Bernardino County Schools', 'San Bernardino County Schools', '', '', '2017-10-07 22:05:04', '1305', 18, '5461901.', 0, '', '', '', '', '', 0, 1),
(1081, 'Superior Court of California, County of San Bernardino', 'Superior Court of California, County of San Bernardino', '', '', '2017-10-07 22:05:19', '1305', 18, '8579767.', 0, '', '', '', '', '', 0, 1),
(1082, 'Right to Work', 'Right to Work', '<p style="text-align:left">America&#39;s labor unions protect and seek to lift the standards for working families in this country. Deep-pocketed opponents of workers like you, however, are seeking to kill the labor movement through a variety of different forms. A legal challenge, <em>Janus v AFSCME</em>, that could result in the end of your constitutional right to participate in a union-funded by each member equally and fairly is fast approaching the U.S Supreme Court docket. This would impose across the country.&nbsp;what our opponents have termed -- quite ridiculously -- &nbsp;&quot;Right to Work&quot;. Move over, in February, anti-worker legislators in the House of Representatives introduced a bill into Congress that would result in the same outcome.</p>\r\n\r\n<p style="text-align:left">The information below has been collected for you to read, share, and act on. Don&#39;t just pass the message on, stay involved so that you can be up to date on the best ways to fight back with our Union.</p>\r\n\r\n<p style="text-align:left"><strong>Watch our &quot;Right to Work&quot; explainer below to learn how &quot;Right to Work&quot; creates universal misery - both in the public and private sector:</strong></p>\r\n\r\n<div>[embed]https://www.youtube.com/watch?v=AFAFz2BhgYk&amp;t=267s[/embed]</div>\r\n\r\n<p style="text-align:left"><strong>How does &quot;Right to Work&quot; reduce the wages? This one simple graph teaches you just how it&#39;s done.</strong></p>\r\n\r\n<p style="text-align:left"><img alt="" class="aligncenter size-full wp-image-1460" src="http://www.teamsters1932.org/app/uploads/2017/05/RTWCycle-copy-e1495737407358.png" style="height:980px; width:800px" /></p>\r\n\r\n<p style="text-align:left"><strong>Former U.S Secretary of Labor Robert Reich goes in depth on the subject in just 3 minutes:</strong></p>\r\n\r\n<div>[embed]https://www.youtube.com/watch?v=lLV4P5Pq0_0&amp;t=5s[/embed]</div>\r\n\r\n<p style="text-align:left"><strong>But what of the rotten and racist roots of &quot;Right to Work&quot; laws? While they currently remain unmentioned when talking about &quot;Right to Work&quot;, this can change by helping spread our findings on the matter. First, watch the Local 1932 presentation on the ugly truth behind &quot;right to work&quot; and start sharing. The information had been fully vetted through a variety of reputable sources, including an interview with a professor at the University of Arkansas, the state where &quot;Right to Work&quot; was truly born.</strong></p>\r\n\r\n<h6 style="text-align:center"><strong>Watch the presentation, with narration included:</strong></h6>\r\n\r\n<div>[embed]https://www.youtube.com/watch?v=T3AtaP5gFTQ[/embed]</div>\r\n\r\n<h6 style="text-align:center"><strong>Download the PowerPoint and run your own presentation: <a href="http://www.teamsters1932.org/app/uploads/2017/05/RTW-5.3.17.ppsx">Ugly Truth of RTW</a></strong></h6>\r\n\r\n<p style="text-align:center"><strong>And so, what effects does &quot;Right to Work&quot; have on the public at large once enacted? The numbers don&#39;t lie - &quot;Right to Work&quot; is WRONG and TOXIC.</strong></p>\r\n\r\n<p style="text-align:center"><img alt="" class="alignnone size-full wp-image-1461" src="http://www.teamsters1932.org/app/uploads/2017/05/RTWInfographic.png" style="height:980px; width:912px" /></p>\r\n\r\n<p style="text-align:left"><em>&nbsp;</em></p>\r\n\r\n<p style="text-align:left"><em><strong>Help stop &quot;Right to Work&quot; by either <a href="http://salsa4.salsalabs.com/o/50740/p/dia/action3/common/public/?action_KEY=20867">emailing</a> your elected representative in Congress or <a href="http://www.teamsters1932.org/political-action/">calling them</a>. Get their contact information by clicking on the &quot;POLITICAL ACTION&quot; tab at the top of this page, followed by the appropriate city selection.</strong></em></p>\r\n\r\n<p><strong>Why does it matter? Because working people standing together for their right to organize a union in their workplace has made our country stronger.&nbsp;</strong> &nbsp;</p>\r\n\r\n<p>[embed]https://www.youtube.com/watch?v=STv4KWHbelM[/embed] &nbsp;</p>\r\n\r\n<h3>&nbsp;</h3>\r\n', '', '2017-10-07 22:14:29', '1308', 18, '4640911.', 0, '', '', '', '', '', 0, 1),
(1083, 'Fall 2015', 'Fall 2015', '<p style="text-align: center;"><a href="http://www.teamsters1932.org/app/uploads/2017/03/The-Voice-Newsletter-Fall-2015-1.pdf">Tap here to read our Fall 2015 issue</a></p>\r\n', '', '2017-10-07 22:24:07', '1309', 18, '8396201.', 0, '', '', '', '', '', 0, 1),
(1084, 'Winter 2016', 'Winter 2016', '<p style="text-align: center;"><a href="http://www.teamsters1932.org/app/uploads/2017/03/The-Voice-Newsletter-1-8-16.pdf">Tap here to read our Winter 2016 issue</a></p>\r\n', '', '2017-10-07 22:25:21', '1309', 18, '4486599.', 0, '', '', '', '', '', 0, 1),
(1085, 'Fall 2016', 'Fall 2016', '<p style="text-align: center;"><a href="http://www.teamsters1932.org/app/uploads/2017/03/TheVoiceFALL.pdf">Tap here to read our Fall 2016 issue</a></p>\r\n', '', '2017-10-07 22:26:50', '1309', 18, '9796414.', 0, '', '', '', '', '', 0, 1),
(1086, 'Winter 2017', 'Winter 2017', '<p style="text-align: center;"><a href="http://www.teamsters1932.org/app/uploads/2017/03/TheVoiceWinter2017-2.pdf">Tap here to read our Winter 2017 issue</a></p>\r\n', '', '2017-10-07 22:28:10', '1309', 18, '7025677.', 0, '', '', '', '', '', 0, 1),
(1087, 'Spring 2017', 'Spring 2017', '<p style="text-align: center;"><a href="http://www.teamsters1932.org/app/uploads/2017/05/VoiceSpring17.pdf">Tap here to read our Spring 2017 issue</a></p>\r\n', '', '2017-10-07 22:32:24', '1309', 18, '8233283.', 0, '', '', '', '', '', 0, 1),
(1088, 'Summer 2017', 'Summer 2017', '<p style="text-align: center;"><a href="http://www.teamsters1932.org/app/uploads/2017/08/1932_Summer2017.pdf">Tap here to read our Summer 2017 issue</a></p>\r\n', '', '2017-10-07 22:33:22', '1309', 18, '2562498.', 0, '', '', '', '', '', 0, 1),
(1089, 'KNOW YOUR RIGHTS!', 'KNOW YOUR RIGHTS!', '<p><strong>KNOW YOUR RIGHTS!</strong></p>\r\n\r\n<p>YOU MAY ASK OR HEAR SOME OF THESE QUESTIONS AND MORE DURING THE COURSE OF YOUR EMPLOYMENT:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>&ldquo;My manager just called me into a meeting. Am I entitled to have a union representative there?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Can they just fire me?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;How can I appeal a disciplinary action?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Can I see a copy of my personnel file?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;What if my employer wants me to take a drug test?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Do I have to take a polygraph (lie detector) test?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;How do I file a grievance?&rdquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>&ldquo;Should I get overtime pay?&rdquo;</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>HOW DO YOU GET ANSWERS TO THESE AND OTHER QUESTIONS?</p>\r\n\r\n<p>That&rsquo;s what Teamsters Local 1932 is all about.</p>\r\n\r\n<p>We will help you get the answers you need, when you need them, and to make sure your rights are not violated.</p>\r\n\r\n<p>Your job and your rights are precious to you and Teamsters Local 1932. We&rsquo;re committed to protecting them. Without a strong Union, your employer might take advantage of you, or intimidate you during investigations or meetings. Teamsters Local 1932 provides the balance so that you can deal with your situation and management on a level playing field.</p>\r\n\r\n<p>Our expert knowledge of your contract, personnel rules and current laws is the insurance you get by being a member of Teamsters Local 1932.</p>\r\n\r\n<p>Critical information about your rights change through new laws, contract language changes, and court decisions.</p>\r\n\r\n<p>You need Teamsters Local 1932 in your corner to make sure you get the respect and protection you deserve!</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/the-union/business-agents/">http://www.teamsters1932.org/the-union/business-agents/</a></p>\r\n', '', '2017-10-07 22:36:10', '1301', 18, '6977162.', 0, '', '', '', '', '', 0, 1),
(1090, 'Know Your Contract', 'Know Your Contract', '<p><strong>Know Your Contract</strong></p>\r\n\r\n<p>WHETHER YOU HAVE QUESTIONS ABOUT SICK LEAVE, HOLIDAYS, OVERTIME OR SENIORITY RIGHTS, THE FIRST PLACE TO LOOK FOR ANSWERS IS YOUR CONTRACT.</p>\r\n\r\n<p>Over the years, Teamster Local 1932 has bargained hundreds of contracts (also known as Memoranda of Understanding, or MOUs) covering thousands of public employees. Through the collective bargaining process, our members make their needs known to bargaining teams, who will then negotiate MOUs with the assistance of our professional staff.</p>\r\n\r\n<p>These MOUs cover your rights and benefits on issues as varied as retirement benefits, health insurance premiums, maternity leave, educational incentives and dozens of other topics. No two MOUs are the same and we encourage all members to be familiar with what&rsquo;s in their contract, for two important reasons:</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>To ensure that your rights in the workplace are not being violated; and</p>\r\n	</li>\r\n	<li>\r\n	<p>To build a base for improvements in wages, benefits and working conditions.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Teamster Local 1932 wants our members to be well-informed and active participants in protecting the hard-fought gains of the many members who have come before them, and to ensure quality employment in the future for current and new employees alike.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/contracts/">http://www.teamsters1932.org/contracts/</a></p>\r\n', '', '2017-10-07 22:36:38', '1300', 18, '2209721.', 0, '', '', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `petition_people`
--

CREATE TABLE IF NOT EXISTS `petition_people` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `app_id` int(5) NOT NULL COMMENT 'APP ID',
  `zipcode` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=245 ;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE IF NOT EXISTS `plans` (
  `plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `amount` int(10) NOT NULL COMMENT 'Dollars',
  `months` int(5) NOT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`plan_id`, `amount`, `months`) VALUES
(1, 100, 1),
(2, 200, 2),
(3, 200, 1),
(4, 400, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pushbridge`
--

CREATE TABLE IF NOT EXISTS `pushbridge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pushID` int(5) NOT NULL,
  `appID` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `pushbridge`
--

INSERT INTO `pushbridge` (`id`, `pushID`, `appID`) VALUES
(1, 203, 18),
(17, 228, 18),
(16, 227, 18),
(15, 226, 18),
(14, 225, 18),
(13, 224, 18),
(12, 223, 18),
(11, 222, 18),
(10, 221, 18),
(18, 229, 18);

-- --------------------------------------------------------

--
-- Table structure for table `pushmessage`
--

CREATE TABLE IF NOT EXISTS `pushmessage` (
  `pushID` int(3) NOT NULL AUTO_INCREMENT COMMENT 'AutoIncrement.',
  `title` varchar(100) NOT NULL,
  `pushMessage` varchar(200) NOT NULL COMMENT 'NotificationMsg',
  `topics` varchar(100) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Author App ID',
  PRIMARY KEY (`pushID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=244 ;

--
-- Dumping data for table `pushmessage`
--

INSERT INTO `pushmessage` (`pushID`, `title`, `pushMessage`, `topics`, `TimeStamp`) VALUES
(242, 'Hurray', 'Beep Beep!', 'allUsers', '2017-09-19 01:28:01'),
(243, 'Special Message from your Union (**TEST PUSH**)', 'Our union is coming together Tuesday, October 24th for our next Monthly General Membership Meeting. See you there.\r\n\r\n ', 'allUsers', '2017-10-04 00:45:23');

-- --------------------------------------------------------

--
-- Table structure for table `push_notification`
--

CREATE TABLE IF NOT EXISTS `push_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UDID` varchar(250) NOT NULL,
  `token` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE IF NOT EXISTS `register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FrstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `EmployeeID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `BirthDate` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `requestunion`
--

CREATE TABLE IF NOT EXISTS `requestunion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `employer` varchar(100) NOT NULL,
  `peoplework` varchar(100) NOT NULL,
  `postal` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `issues` varchar(2000) NOT NULL,
  `app_user_id` varchar(100) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Table structure for table `shopunion`
--

CREATE TABLE IF NOT EXISTS `shopunion` (
  `shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(100) DEFAULT NULL,
  `employe_name` varchar(50) DEFAULT NULL,
  `store_number` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `latitude` varchar(30) NOT NULL COMMENT 'radians',
  `longitude` varchar(30) NOT NULL COMMENT 'radians',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `shopunion`
--

INSERT INTO `shopunion` (`shop_id`, `area`, `employe_name`, `store_number`, `address`, `city`, `state`, `zip`, `phone`, `app_id`, `latitude`, `longitude`) VALUES
(3, 'Birmingham', 'Subway', '1', '1614 15th Avenue South Apartment H', 'Birmingham', 'Alabama', '35205', '6615938417', 18, '0.43400269', '1.1697976285'),
(4, 'Jefferson', 'Wings', '2', '1614 15th Avenue South Apartment H', 'Birmingham', 'Alabama', '35205', '6615938417', 18, '0.43400269', '1.1697976285'),
(5, 'hgfhgfh', 'fghgf', 'gfhgfhgf', 'hgfh', 'hgfhfgh', 'ghgfh', 'ghfgh', 'ghgfhf', 18, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `stayconected`
--

CREATE TABLE IF NOT EXISTS `stayconected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `order` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `stayconected`
--

INSERT INTO `stayconected` (`id`, `name`, `link`, `app_id`, `order`) VALUES
(50, 'Instagram', 'https://www.instagram.com/teamsters1932/', 18, 3),
(56, 'Facebook', 'https://www.facebook.com/teamsterslocal1932/', 18, 1),
(61, 'Twitter', 'https://twitter.com/1932teamsters?lang=en', 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(50) NOT NULL,
  `submenu_id` varchar(10) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1318 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `menu_id`, `submenu_id`, `icon`, `name`) VALUES
(1047, 664, '664.1', 'info.png', 'Sub Menu'),
(1048, 664, '664.2', 'info.png', 'Sub Menu'),
(1049, 664, '664.3', 'info.png', 'Sub Menu'),
(1050, 664, '664.4', 'info.png', 'Sub Menu'),
(1051, 665, '665.1', 'info.png', 'Sub Menu'),
(1296, 732, '732.1', 'info.png', 'About US'),
(1297, 732, '732.2', 'info.png', 'VIDEOS'),
(1298, 732, '732.3', 'info.png', 'Staff List'),
(1299, 732, '732.4', 'info.png', 'WE ARE TEAMSTERS'),
(1300, 732, '732.5', 'info.png', 'Know Your contract'),
(1301, 732, '732.6', 'info.png', 'KNow your rights'),
(1302, 733, '733.1', 'info.png', 'MEMBER Discounts'),
(1303, 733, '733.2', 'info.png', 'Member benefits'),
(1304, 733, '733.3', 'info.png', 'Teamster Advantage '),
(1305, 733, '733.4', 'info.png', 'contracts'),
(1306, 734, '734.1', 'info.png', 'NEWS'),
(1307, 734, '734.2', 'info.png', 'Event Calendar'),
(1308, 734, '734.3', 'info.png', 'RTW'),
(1309, 734, '734.4', 'info.png', 'The Voice'),
(1310, 735, '735.1', 'info.png', 'FIND MY ELECTED OFFICIALs'),
(1311, 735, '735.2', 'info.png', 'Register to Vote'),
(1312, 735, '735.3', 'info.png', 'Endorsement News'),
(1313, 736, '736.1', 'info.png', 'join our power network'),
(1314, 736, '736.2', 'info.png', 'Political Activism'),
(1315, 736, '736.3', 'info.png', 'Organize a union'),
(1316, 736, '736.4', 'info.png', 'Attend a Training'),
(1317, 736, '736.5', 'info.png', 'Become a Steward');

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE IF NOT EXISTS `super_admin` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `super_admin`
--

INSERT INTO `super_admin` (`user_id`, `name`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `union_hall_contact`
--

CREATE TABLE IF NOT EXISTS `union_hall_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(5000) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `mailing_address` varchar(500) NOT NULL,
  `office_hour` varchar(500) NOT NULL,
  `media_inquiries` varchar(2500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `union_hall_contact`
--

INSERT INTO `union_hall_contact` (`id`, `address`, `phone`, `fax`, `mailing_address`, `office_hour`, `media_inquiries`) VALUES
(1, 'Teamster local1932   433.Sierra Way San Bernardino , CA 92410  ', '(909)  889-8377 ', '(909) 888 7429', 'P,O BOX 432 SAN BERNARDINO ,CA 92402', 'MONDAY- FRIDAY 7:30AM 5:30PM', 'Communications coordinator  mario vasquez ');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(2) NOT NULL,
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `forgot_password` varchar(100) DEFAULT NULL,
  `cookie` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT 'user.jpg',
  `email` varchar(25) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `time_cone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `app_id`, `user_name`, `password`, `forgot_password`, `cookie`, `image`, `email`, `logo`, `cover`, `time_cone`) VALUES
(7, 18, 'teamster1932', 'ufcw_teamster1932', NULL, NULL, 'user.jpg', 'steve@linkedunion.com', 'ufcw324-logo.png', 'ufcw324-cover.jpg', '2017-03-1 05:17:24');

-- --------------------------------------------------------

--
-- Table structure for table `users_location`
--

CREATE TABLE IF NOT EXISTS `users_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=232 ;

--
-- Dumping data for table `users_location`
--

INSERT INTO `users_location` (`id`, `UserID`, `latitude`, `longitude`, `address`, `country`, `state`, `city`, `timestamp`) VALUES
(207, 8, '33.9826108888108', '-117.368638850858', 'Continuation School, 3600 Lime St, Riverside, CA  92501, United States', 'United States', 'CA', 'Riverside', '2017-10-03 21:22:26'),
(206, 8, '33.9826024230886', '-117.368646646028', 'Continuation School, 3600 Lime St, Riverside, CA  92501, United States', 'United States', 'CA', 'Riverside', '2017-10-03 21:18:10'),
(205, 14, '34.0482421126534', '-117.305322876309', '1241 E Washington St, Colton, CA  92324, United States', 'United States', 'CA', 'Colton', '2017-10-03 20:42:55'),
(204, 8, '33.9633534662742', '-117.852595942205', 'CA-57 S, Diamond Bar, CA  91765, United States', 'United States', 'CA', 'Diamond Bar', '2017-10-03 20:09:48'),
(203, 8, '33.9631151268575', '-117.852740865311', 'CA-57 S, Walnut, CA  91789, United States', 'United States', 'CA', 'Diamond Bar', '2017-10-03 20:09:48'),
(202, 14, '34.0482522547562', '-117.305208463331', '1241 E Washington St, Colton, CA  92324, United States', 'United States', 'CA', 'Colton', '2017-10-03 20:00:27'),
(201, 8, '34.0608824836407', '-117.803087476751', 'DeVry Inst of Technology, 971 Corporate Center Dr, Pomona, CA  91768, United States', 'United States', 'CA', 'Pomona', '2017-10-03 16:34:22'),
(200, 8, '34.0614620922451', '-117.802870385459', 'DeVry Inst of Technology, 971 Corporate Center Dr, Pomona, CA  91768, United States', 'United States', 'CA', 'Pomona', '2017-10-03 16:14:46'),
(199, 8, '33.494209838131', '-86.7999058776412', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-10-01 01:30:21'),
(198, 8, '33.494209838131', '-86.7999058776412', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-10-01 01:30:21'),
(197, 8, '33.494209838131', '-86.7999058776412', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-10-01 01:30:21'),
(196, 8, '33.4941816749363', '-86.7998269201133', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-10-01 01:28:59'),
(195, 8, '33.9510246087182', '-117.026678826769', '36242 Clearwater Ct, Beaumont, CA  92223, United States', 'United States', 'CA', 'Beaumont', '2017-09-30 22:36:01'),
(194, 8, '33.9510246087182', '-117.026678826769', '36242 Clearwater Ct, Beaumont, CA  92223, United States', 'United States', 'CA', 'Beaumont', '2017-09-30 22:36:01'),
(193, 14, '33.9569504466224', '-117.3935380486', '6280 Magnolia Ave, Riverside, CA  92506, United States', 'United States', 'CA', 'Riverside', '2017-09-30 20:59:22'),
(26, 100, '34.1457135044356', '-117.858137637487', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 00:57:58'),
(27, 100, '34.1457254067381', '-117.858000258094', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 01:00:04'),
(28, 100, '34.1469473927916', '-117.855140178228', '676â€“698 E Sierra Madre Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 01:01:37'),
(29, 101, '34.1456151428019', '-117.858053566998', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 01:17:57'),
(192, 14, '33.9569504466224', '-117.3935380486', '6280 Magnolia Ave, Riverside, CA  92506, United States', 'United States', 'CA', 'Riverside', '2017-09-30 20:59:22'),
(191, 14, '33.9569519134555', '-117.39353897061', '6280 Magnolia Ave, Riverside, CA  92506, United States', 'United States', 'CA', 'Riverside', '2017-09-30 20:59:00'),
(190, 8, '43.681806628633', '-116.359837493036', '436 W River Trail Ct, Eagle, ID  83616, United States', 'United States', 'ID', 'Eagle', '2017-09-30 16:34:03'),
(189, 8, '33.5044887336283', '-86.807762486941', 'University of Alabama at Birmingham, 1501 15th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 06:24:01'),
(188, 8, '33.5044887336283', '-86.807762486941', 'University of Alabama at Birmingham, 1501 15th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 06:24:01'),
(187, 8, '43.681826368015', '-116.359827602391', '436 W River Trail Ct, Eagle, ID  83616, United States', 'United States', 'ID', 'Eagle', '2017-09-30 06:15:34'),
(186, 8, '33.4942637337684', '-86.7997099925641', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 05:46:42'),
(185, 8, '33.4943109657927', '-86.7998060491744', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 05:42:25'),
(184, 8, '33.4947294173892', '-86.7993936840813', '1406 17th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 05:42:25'),
(183, 8, '33.4947294173892', '-86.7993936840813', '1406 17th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 05:42:25'),
(182, 8, '33.9759476110656', '-117.370455209275', '4066â€“4148 Vine St, Riverside, CA  92507, United States', 'United States', 'CA', 'Riverside', '2017-09-30 01:34:06'),
(181, 8, '33.4988486565731', '-86.8053949248078', 'University of Alabama at Birmingham, 801 14th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 01:30:19'),
(180, 8, '33.5007965827904', '-86.8084189625518', 'University of Alabama at Birmingham, 617 13th St S, Birmingham, AL  35233, United States', 'United States', 'AL', 'Birmingham', '2017-09-30 00:44:44'),
(179, 8, '46.0755720595327', '-116.342651322592', '2642 Highway 95 N, Cottonwood, ID  83522, United States', 'United States', 'ID', 'Cottonwood', '2017-09-30 00:39:13'),
(178, 14, '34.1072974400912', '-117.284652683993', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-30 00:12:08'),
(177, 8, '46.4132806566045', '-116.999192638432', '701 21st St, Lewiston, ID  83501, United States', 'United States', 'ID', 'Lewiston', '2017-09-29 23:10:07'),
(176, 8, '33.95258436', '-117.01947263', '36981 Dunhill Ct, Beaumont, CA  92223, United States', 'United States', 'CA', 'Beaumont', '2017-09-29 05:44:16'),
(175, 13, '43.6924064555103', '-116.340022124205', '1125 E State St, Eagle, ID  83616, United States', 'United States', 'ID', 'Eagle', '2017-09-28 22:38:29'),
(174, 10, '33.5004060133324', '-86.8067663371198', 'University of Alabama at Birmingham, 801 14th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-28 22:36:49'),
(173, 14, '34.0661347937156', '-117.269378425663', '701â€“769 Harriman Pl, San Bernardino, CA  92408, United States', 'United States', 'CA', 'San Bernardino', '2017-09-28 22:34:20'),
(172, 14, '34.0662297606785', '-117.269339114537', '700â€“768 Harriman Pl, San Bernardino, CA  92408, United States', 'United States', 'CA', 'San Bernardino', '2017-09-28 22:33:52'),
(171, 110, '34.0945581229756', '-117.253826165524', '1477 E Harry Shepard Blvd, San Bernardino, CA  92408, United States', 'United States', 'CA', 'San Bernardino', '2017-09-28 20:12:52'),
(66, 101, '34.1456612432694', '-117.858115760719', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 13:59:11'),
(67, 103, '34.1456612432694', '-117.858115760719', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 13:59:53'),
(68, 104, '34.1456612432694', '-117.858115760719', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 13:59:53'),
(69, 105, '34.145661536636', '-117.858011908939', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:14:42'),
(70, 106, '34.145859936284', '-117.85813906241', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:26'),
(71, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(72, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(73, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(74, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(75, 106, '34.1457732674052', '-117.858040742686', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:41'),
(76, 106, '34.1457732674052', '-117.858040742686', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:41'),
(77, 106, '34.1457732674052', '-117.858040742686', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:41'),
(78, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(79, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(80, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(81, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(82, 106, '34.1457820684036', '-117.858028421288', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:56'),
(83, 106, '34.1457820684036', '-117.858028421288', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:56'),
(85, 108, '33.4942094190358', '-86.799833960912', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-19 16:32:50'),
(86, 106, '33.7160208542214', '-117.884776499232', '2303 S Bristol St, Santa Ana, CA  92704, United States', 'United States', 'CA', 'Santa Ana', '2017-09-19 22:39:24'),
(87, 106, '33.6579067726002', '-117.866488695254', '1000 Bristol St N, Newport Beach, CA  92660, United States', 'United States', 'CA', 'Newport Beach', '2017-09-20 01:15:23'),
(88, 108, '33.4981452757025', '-86.8066825181263', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:35:03'),
(89, 108, '33.4981781239168', '-86.8066612743605', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:37:59'),
(90, 108, '33.4981805806449', '-86.8066572278539', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:38:48'),
(91, 108, '33.4981300345801', '-86.8066915960576', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:39:35'),
(92, 108, '33.4981290516732', '-86.8066920137677', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:39:53'),
(170, 10, '33.4993901895671', '-86.8096781709109', 'University of Alabama at Birmingham, 1201 University Blvd, Birmingham, AL  35233, United States', 'United States', 'AL', 'Birmingham', '2017-09-27 23:55:35'),
(169, 10, '33.4994890122055', '-86.8097042386297', 'University of Alabama at Birmingham, 1201 University Blvd, Birmingham, AL  35233, United States', 'United States', 'AL', 'Birmingham', '2017-09-27 23:51:08'),
(168, 10, '33.4994890122055', '-86.8097042386297', 'University of Alabama at Birmingham, 1201 University Blvd, Birmingham, AL  35233, United States', 'United States', 'AL', 'Birmingham', '2017-09-27 23:51:08'),
(97, 108, '33.4943289030655', '-86.7997590266976', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 20:20:15'),
(98, 110, '33.4356536865234', '-112.001998901367', 'Phoenix Sky Harbor International Airport, 3701 E Sky Harbor Blvd, Phoenix, AZ  85034, United States', 'United States', 'AZ', 'Phoenix', '2017-09-20 20:22:38'),
(99, 106, '34.104463006007', '-117.817408561843', '565 W Arrow Hwy, San Dimas, CA  91773, United States', 'United States', 'CA', 'San Dimas', '2017-09-20 21:06:41'),
(100, 111, '32.7338971663571', '-115.538029102716', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-09-20 21:49:47'),
(167, 10, '33.4995015012412', '-86.8096934259746', 'University of Alabama at Birmingham, 1201 University Blvd, Birmingham, AL  35233, United States', 'United States', 'AL', 'Birmingham', '2017-09-27 23:50:48'),
(103, 110, '34.1073011281286', '-117.284656372031', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 16:46:54'),
(104, 111, '34.1071999166478', '-117.284377087017', '400â€“512 N Lugo Ave, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 17:51:39'),
(105, 106, '37.5921703867566', '-122.370534377492', '1511 Rollins Rd, Burlingame, CA  94010, United States', 'United States', 'CA', 'Burlingame', '2017-09-21 17:59:59'),
(106, 111, '34.1072866274362', '-117.284518657361', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:25:05'),
(107, 111, '34.1073990706672', '-117.285229023655', '400â€“492 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:27:39'),
(108, 111, '34.1073395172452', '-117.28450750943', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:00'),
(109, 111, '34.1073161317353', '-117.284547155832', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:22'),
(110, 111, '34.1073161317353', '-117.284547155832', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:22'),
(111, 111, '34.1073161317353', '-117.284547155832', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:22'),
(112, 106, '37.5922472146012', '-122.371213423516', '1529 Rollins Rd, Burlingame, CA  94010, United States', 'United States', 'CA', 'Burlingame', '2017-09-21 18:49:44'),
(113, 106, '37.5920551593149', '-122.371069604044', '1519 Rollins Rd, Burlingame, CA  94010, United States', 'United States', 'CA', 'Burlingame', '2017-09-21 18:51:24'),
(114, 111, '34.1072854958792', '-117.28456635039', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 21:43:38'),
(115, 106, '37.7090315882722', '-122.212882257544', 'Oakland International Airport, Oakland International Airport, Oakland, CA  94621, United States', 'United States', 'CA', 'Oakland', '2017-09-22 04:27:31'),
(116, 111, '34.0297563700322', '-117.316078618278', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-22 16:35:58'),
(166, 10, '33.4995015012412', '-86.8096934259746', 'University of Alabama at Birmingham, 1201 University Blvd, Birmingham, AL  35233, United States', 'United States', 'AL', 'Birmingham', '2017-09-27 23:50:48'),
(165, 10, '33.4981803419882', '-86.8065714322226', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-27 21:12:20'),
(164, 18, '24.8709216267104', '67.0835616405486', 'Syedna Yousuf Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-27 08:49:35'),
(163, 14, '34.0299587510843', '-117.315982645487', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-27 07:23:28'),
(162, 113, '24.8709242073569', '67.0835597537523', 'Syedna Yousuf Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-27 06:48:18'),
(161, 108, '33.4981522608356', '-86.8066725949087', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-26 22:48:11'),
(160, 108, '33.4981527722596', '-86.8065868033512', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-26 22:45:07'),
(128, 111, '34.0297445096392', '-117.315920451765', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 07:44:22'),
(129, 111, '34.0298731299434', '-117.316051880007', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 07:49:32'),
(130, 111, '34.0297615249027', '-117.316068308537', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 07:50:59'),
(131, 111, '34.0297246026192', '-117.315958757063', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 08:48:10'),
(132, 111, '34.0297361277361', '-117.316026063745', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 08:51:28'),
(133, 106, '43.6924053447534', '-116.340018405582', '1125 E State St, Eagle, ID  83616, United States', 'United States', 'ID', 'Eagle', '2017-09-25 18:50:11'),
(134, 111, '34.1072341986318', '-117.284730468055', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-25 19:17:55'),
(135, 111, '34.1072936682348', '-117.284624101703', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-25 19:18:10'),
(136, 111, '34.1072936682348', '-117.284624101703', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-25 19:18:10'),
(137, 110, '34.1073295427804', '-117.284673806389', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-25 21:19:53'),
(138, 108, '33.4982408630042', '-86.8062594440643', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-25 22:20:42'),
(139, 108, '33.4982408630042', '-86.8062594440643', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-25 22:21:45'),
(140, 108, '33.4982408630042', '-86.8062594440643', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-25 22:21:45'),
(141, 108, '33.4982408630042', '-86.8062594440643', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-25 22:21:45'),
(142, 108, '33.4981640752926', '-86.8062797266698', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-25 22:28:23'),
(143, 108, '33.4981975285648', '-86.8064184487674', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-25 22:28:39'),
(144, 108, '33.4941950859814', '-86.7998624593828', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-26 06:39:04'),
(145, 108, '33.4942530049323', '-86.799929011694', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-26 06:42:17'),
(159, 106, '43.6711643357237', '-116.415945533771', '7355 N Linder Rd, Meridian, ID  83646, United States', 'United States', 'ID', 'Meridian', '2017-09-26 22:38:54'),
(158, 110, '34.1074286587854', '-117.284865249058', '463 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-26 20:07:59'),
(208, 24, '34.1455671564063', '-117.858002772665', '533 E Virginia Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-10-03 23:32:05'),
(209, 24, '34.0448512137253', '-117.802547598367', '3177 W Temple Ave, Pomona, CA  91768, United States', 'United States', 'CA', 'Pomona', '2017-10-04 16:56:18'),
(210, 14, '34.1026260797252', '-117.297920063247', '699 W 2nd St, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-10-04 18:35:27'),
(211, 14, '34.1071393573974', '-117.284483956282', '400â€“512 N Lugo Ave, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-10-04 20:21:21'),
(212, 14, '34.1072132857834', '-117.284562159439', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-10-04 20:26:59'),
(213, 24, '34.14569062184', '-117.857946697732', '542 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-10-05 12:03:02'),
(214, 24, '40.2567426022517', '-111.664895396784', '449 W 1720 N, Provo, UT  84604, United States', 'United States', 'UT', 'Provo', '2017-10-06 22:05:51'),
(215, 14, '32.7339014411277', '-115.538086183477', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:29:14'),
(216, 14, '32.7338763792373', '-115.538183581192', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:31:30'),
(217, 14, '32.7339226473428', '-115.537977889288', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:36:24'),
(218, 14, '32.7338553825698', '-115.538065396357', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:42:13'),
(219, 14, '32.7338553825698', '-115.538065396357', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:42:25'),
(220, 14, '32.7338553825698', '-115.538065396357', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:42:25'),
(221, 14, '32.7338553825698', '-115.538065396357', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:42:25'),
(222, 14, '32.7338439831815', '-115.538087356943', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:43:28'),
(223, 14, '32.7338834200359', '-115.538111329186', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 04:53:27'),
(224, 14, '32.7339392854206', '-115.538054835159', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:18:53'),
(225, 14, '32.7338168677247', '-115.537988450486', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:22:50'),
(226, 14, '32.7338168677247', '-115.537988450486', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:23:06'),
(227, 14, '32.7338168677247', '-115.537988450486', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:23:06'),
(228, 14, '32.7339114994115', '-115.538120716918', '41 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:33:39'),
(229, 14, '32.7338573523171', '-115.538048213455', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:36:51'),
(230, 14, '32.7338462882049', '-115.538025749955', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:37:33'),
(231, 14, '32.7338497666947', '-115.537981074411', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-10-08 05:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `webservices`
--

CREATE TABLE IF NOT EXISTS `webservices` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `webservices`
--

INSERT INTO `webservices` (`id`, `name`) VALUES
(1, 'news_category.php'),
(2, 'Sub Menu'),
(3, 'stay_connected.php'),
(4, 'default.php'),
(5, 'view.php'),
(6, 'default.php'),
(7, 'Member Discount'),
(8, 'Union Representative');

-- --------------------------------------------------------

--
-- Table structure for table `webservice_category`
--

CREATE TABLE IF NOT EXISTS `webservice_category` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `webservice` int(2) NOT NULL,
  `category` varchar(10) NOT NULL,
  `language` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2010 ;

--
-- Dumping data for table `webservice_category`
--

INSERT INTO `webservice_category` (`id`, `webservice`, `category`, `language`) VALUES
(1666, 8, '1047', 'spanish'),
(1667, 7, '1048', 'spanish'),
(1668, 5, '1049', 'spanish'),
(1669, 5, '1050', 'spanish'),
(1670, 2, '664', 'spanish'),
(1671, 5, '1051', 'spanish'),
(1672, 2, '665', 'spanish'),
(1673, 1, '666', 'spanish'),
(1674, 1, '667', 'spanish'),
(1675, 1, '668', 'spanish'),
(1676, 5, '669', 'spanish'),
(1677, 8, '670', 'spanish'),
(1678, 7, '671', 'spanish'),
(1983, 5, '1296', 'english'),
(1984, 5, '1297', 'english'),
(1985, 8, '1298', 'english'),
(1986, 5, '1299', 'english'),
(1987, 5, '1300', 'english'),
(1988, 5, '1301', 'english'),
(1989, 2, '732', 'english'),
(1990, 7, '1302', 'english'),
(1991, 7, '1303', 'english'),
(1992, 5, '1304', 'english'),
(1993, 5, '1305', 'english'),
(1994, 2, '733', 'english'),
(1995, 5, '1306', 'english'),
(1996, 5, '1307', 'english'),
(1997, 5, '1308', 'english'),
(1998, 5, '1309', 'english'),
(1999, 2, '734', 'english'),
(2000, 4, '1310', 'english'),
(2001, 5, '1311', 'english'),
(2002, 5, '1312', 'english'),
(2003, 2, '735', 'english'),
(2004, 5, '1313', 'english'),
(2005, 5, '1314', 'english'),
(2006, 5, '1315', 'english'),
(2007, 5, '1316', 'english'),
(2008, 5, '1317', 'english'),
(2009, 2, '736', 'english');

-- --------------------------------------------------------

--
-- Table structure for table `worksite`
--

CREATE TABLE IF NOT EXISTS `worksite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `dep_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `worksite`
--

INSERT INTO `worksite` (`id`, `name`, `dep_id`) VALUES
(4, 'Testing', '1'),
(6, 'Tester', '2');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
