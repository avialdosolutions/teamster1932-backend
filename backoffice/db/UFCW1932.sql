-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 25, 2017 at 07:54 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `UFCW1932`
--

-- --------------------------------------------------------

--
-- Table structure for table `app`
--

CREATE TABLE IF NOT EXISTS `app` (
  `app_id` int(3) NOT NULL,
  `about_us` varchar(5000) NOT NULL,
  `app_name` varchar(30) NOT NULL,
  `applicationID` varchar(40) NOT NULL,
  `clientKey` varchar(40) NOT NULL,
  `restKey` varchar(50) NOT NULL,
  `masterKey` varchar(50) NOT NULL,
  `version` decimal(10,1) NOT NULL,
  `about` longtext NOT NULL,
  `contact` longtext NOT NULL,
  `privacy` longtext NOT NULL,
  `pushCount` int(3) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app`
--

INSERT INTO `app` (`app_id`, `about_us`, `app_name`, `applicationID`, `clientKey`, `restKey`, `masterKey`, `version`, `about`, `contact`, `privacy`, `pushCount`) VALUES
(18, 'A Voice for working California', 'Teamster Local 1932', '', '', '', '', '10.2', '<h1 style="text-align:center"><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/9WWbNFXELXM" width="390"></iframe></h1>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<h3><span style="font-family:georgia,serif"><strong>Teamsters Local 1932 (formerly SBPEA) Mission Statement:</strong></span></h3>\r\n\r\n<p><span style="font-family:georgia,serif">It is the mission of Teamsters Local 1932 to provide the best possible service to all our members with integrity and equality; to advance the social, economic, and educational welfare of the membership; to promote professional working relationships and fair play between members and management alike; and to positively contribute to the communities we serve and live in.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif"><strong>Know Your Rights!</strong></span></p>\r\n\r\n<h4><span style="font-family:georgia,serif">You may ask or hear some of these questions and more during the course of your employment:</span></h4>\r\n\r\n<ul>\r\n	<li><span style="font-family:georgia,serif">&ldquo;My manager just called me into a meeting. Am I entitled to have a union representative there?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can they just fire me?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How can I appeal a disciplinary action?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can I see a copy of my personnel file?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;What if my employer wants me to take a drug test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Do I have to take a polygraph (lie detector) test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How do I file a grievance?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Should I get overtime pay?&rdquo;</span></li>\r\n</ul>\r\n\r\n<h4><span style="font-family:georgia,serif">How do you get answers to these and other questions?</span></h4>\r\n\r\n<p><span style="font-family:georgia,serif">That&rsquo;s what Teamsters Local 1932 is all about.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">We will help you get the answers you </span><span style="font-family:georgia,serif">need,</span><span style="font-family:georgia,serif"> when you need them, and to make sure your rights are not violated.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Your job and your rights are precious to you and Teamsters Local 1932. We&rsquo;re committed to protecting them. Without a strong Union, your employer might take advantage of you, or intimidate you during investigations or meetings. Teamsters Local 1932 provides the balance so that you can deal with your situation and management on a level playing field.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Our expert knowledge of your contract, personnel </span><span style="font-family:georgia,serif">rules</span><span style="font-family:georgia,serif"> and current laws is the insurance you get by being a member of Teamsters Local 1932.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Critical information about your rights </span><span style="font-family:georgia,serif">change</span><span style="font-family:georgia,serif"> through new laws, contract language changes, and court decisions.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">You need Teamsters Local 1932 in your corner to make sure you get the respect and protection you deserve!</span></p>\r\n\r\n<h1>&nbsp;</h1>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif"><a href="http://www.teamsters1932.org/the-union/business-agents/">CONTACT YOUR BUSINESS REPRESENTATIVE</a></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<h2><span style="font-family:verdana,geneva,sans-serif">Weingarten Rights</span></h2>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">IF YOU ARE CALLED INTO A MEETING WITH ANY MANAGEMENT REPRESENTATIVE AND HAVE REASON TO BELIEVE THAT DISCIPLINARY ACTION MAY RESULT . . .</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Do the following:</span></p>\r\n\r\n<ul>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Ask your supervisor if you might be disciplined as a result of the interview. If he says &ldquo;NO&rdquo;, ask for a written statement to that effect. If he gives you such a statement, you must participate in the interview. If not, read him your Weingarten rights, remain for the meeting, take notes, and afterwards immediately contact your union representative.</span></li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">If he says you might be disciplined but will not allow you to have a union representative present, read him your Weingarten rights, stay in the room, take notes, and do not respond to any questions. Afterwards, contact your union representative immediately. If he allows your union representative to be present, you should participate in the interview.</span></li>\r\n</ul>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">YOUR WEINGARTEN RIGHTS . . .</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">&ldquo;If this discussion could in any way lead to my being disciplined or terminated, or affect my personal working conditions, I respectfully request that my union representative or steward be present at this meeting. If this discussion could lead to my being disciplined and you deny my request for representation, I choose not to answer any questions.&rdquo;</span></p>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">THE 1975 U.S. SUPREME COURT WEINGARTEN DECISION</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">In 1975 the U.S. Supreme Court ruled, in the Weingarten decision, that an employee is entitled to have a union representative present during any interview which may result in his or her discipline. It is up to you to insist on union representation. If you fail to do so, you may be waiving your rights!</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<h2><span style="font-family:verdana,geneva,sans-serif">Know Your Contract</span></h2>\r\n\r\n<h4><span style="font-family:verdana,geneva,sans-serif">WHETHER YOU HAVE QUESTIONS ABOUT SICK LEAVE, HOLIDAYS, OVERTIME OR SENIORITY RIGHTS, THE FIRST PLACE TO LOOK FOR ANSWERS IS YOUR CONTRACT.</span></h4>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Over the years, Teamster Local 1932 has bargained hundreds of contracts (also known as Memoranda of Understanding, or MOUs) covering thousands of public employees. Through the collective bargaining process, our members make their needs known to bargaining teams, who will then negotiate MOUs with the assistance of our professional staff.</span></p>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">These MOUs cover your rights and benefits on issues as varied as retirement benefits, health insurance premiums, maternity leave, educational incentives and dozens of other topics. No two MOUs are the same and we encourage all members to be familiar with what&rsquo;s in their contract, for two important reasons:</span></p>\r\n\r\n<ol>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">To ensure that your rights in the workplace are not being violated; and</span></li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">To build a base for improvements in wages, benefits and working conditions.</span></li>\r\n</ol>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Teamster Local 1932 wants our members to be well-informed and active participants in protecting the hard-fought gains of the many members who have come before them, and to ensure quality employment in the future for current and new employees alike.</span></p>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif"><a href="http://www.teamsters1932.org/contracts/">LOOKUP YOUR MEMORANDA OF UNDERSTANDING (MOU)</a></span></p>\r\n\r\n<p><span style="font-family:verdana,geneva,sans-serif">Copies of your MOU should be provided to you by Human Resources during your &ldquo;New Employee Orientation.&rdquo;</span></p>\r\n', '<p style="text-align:center"><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/Screen-Shot-2017-09-11-at-2.53.54-PM.png" style="height:30px; width:390px" /></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Teamsters Local 1932</strong><br />\r\n433 N. Sierra Way<br />\r\nSan Bernardino, CA 92410</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Phone:</strong>&nbsp;<a href="tel:909-889-8377">(909) 889-8377</a><br />\r\n<strong>Fax:</strong>&nbsp;(909) 888-7429</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Mailing Address:</strong><br />\r\nP.O. Box 432<br />\r\nSan Bernardino, CA 92402</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Office Hours:</strong><br />\r\nMonday- Friday, 7:30am &ndash; 5:30pm</span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong>Media Inquiries:</strong><br />\r\nCommunications Coordinator Mario Vasquez<br />\r\n<a href="mailto:mvasquez@teamsters1932.org">mvasquez@teamsters1932.org</a></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h5><span style="font-family:verdana,geneva,sans-serif"><big>HAVE A QUESTION OR SUGGESTION? PLEASE FEEL FREE TO CONTACT US.</big></span></h5>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<form action="http://www.teamsters1932.org/contact/" enctype="multipart/form-data" id="gform_3" method="post">\r\n<ul>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Name</span>\r\n\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><input name="input_1" type="text" value="" /></span></p>\r\n	</li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Phone Number</span>\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><input name="input_2" type="tel" value="" /></span></p>\r\n	</li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Email*</span>\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><input name="input_3" type="email" value="" /></span></p>\r\n	</li>\r\n	<li><span style="font-family:verdana,geneva,sans-serif">Message</span>\r\n	<p><span style="font-family:verdana,geneva,sans-serif"><textarea cols="50" name="input_4" rows="10"></textarea></span></p>\r\n	</li>\r\n</ul>\r\n</form>\r\n', '<p><span style="font-family:verdana,geneva,sans-serif">Here is where we will add terms and conditions</span></p>\r\n', 30);

-- --------------------------------------------------------

--
-- Table structure for table `apps_plan`
--

CREATE TABLE IF NOT EXISTS `apps_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` int(5) NOT NULL,
  `plan_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `apps_plan`
--

INSERT INTO `apps_plan` (`id`, `app_id`, `plan_id`) VALUES
(1, 18, 3),
(2, 18, 4);

-- --------------------------------------------------------

--
-- Table structure for table `app_appearance`
--

CREATE TABLE IF NOT EXISTS `app_appearance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_version` varchar(10) NOT NULL,
  `app_logo` varchar(100) NOT NULL,
  `app_icon` varchar(100) NOT NULL,
  `splash_screen` varchar(100) NOT NULL,
  `register_button_color` varchar(100) NOT NULL,
  `language_button_color` varchar(100) NOT NULL,
  `local_button_color` varchar(100) NOT NULL,
  `state_button_color` varchar(100) NOT NULL,
  `national_button_color` varchar(100) NOT NULL,
  `logo_boder_color` varchar(100) NOT NULL,
  `toolbar_text_color` varchar(100) NOT NULL,
  `tool_bar_arrow_color` varchar(100) NOT NULL,
  `toolbar_back_color` varchar(100) NOT NULL,
  `status_bar_color` varchar(10) NOT NULL,
  `status_bar_style` varchar(10) NOT NULL,
  `status_bar_background` varchar(20) NOT NULL,
  `navigation_bar_color` varchar(10) NOT NULL,
  `menu_item_text_color` varchar(10) NOT NULL,
  `menu_navigation_color` varchar(10) NOT NULL,
  `info_button_color` varchar(10) NOT NULL,
  `login_button_text` varchar(10) NOT NULL,
  `language_button_enabled` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `app_appearance`
--

INSERT INTO `app_appearance` (`id`, `app_version`, `app_logo`, `app_icon`, `splash_screen`, `register_button_color`, `language_button_color`, `local_button_color`, `state_button_color`, `national_button_color`, `logo_boder_color`, `toolbar_text_color`, `tool_bar_arrow_color`, `toolbar_back_color`, `status_bar_color`, `status_bar_style`, `status_bar_background`, `navigation_bar_color`, `menu_item_text_color`, `menu_navigation_color`, `info_button_color`, `login_button_text`, `language_button_enabled`) VALUES
(1, '1.0-1.4', '611186.png', '809092.png', '538400.png', '#555555', '#0b537b', '#555555', '#0b537b', '#555555', '#13255c', '#ffffff', '#ffffff', '', '#00527c', 'light', '#00527c', '#00527c', '#000000', '#000000', '#000000', 'REGISTER', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `app_icon`
--

CREATE TABLE IF NOT EXISTS `app_icon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `app_icon`
--

INSERT INTO `app_icon` (`id`, `name`, `image`) VALUES
(1, 'About UFCW 135', 'info.png'),
(2, 'Member Discounts', 'save-icon.png'),
(3, 'Contracts', 'contract.png'),
(4, 'Member Resources', 'like-1.png'),
(5, 'Political Action', 'registertovote_img-1.png'),
(6, 'Negotiation Updates', 'News-1.png'),
(7, 'Immigration Guide', 'immigration-guide-1.png'),
(9, 'Member Services', 'memberservices.png'),
(10, 'Stay Informed', 'Megaphone-1.png'),
(17, 'Teamsters', '855454.png'),
(18, 'Team Adv', '758215.png'),
(19, 'Calendar', '772526.png');

-- --------------------------------------------------------

--
-- Table structure for table `app_relation`
--

CREATE TABLE IF NOT EXISTS `app_relation` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `parentID` int(6) NOT NULL,
  `catID` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `app_relation`
--

INSERT INTO `app_relation` (`id`, `parentID`, `catID`) VALUES
(1, 6, 24),
(2, 2, 24),
(3, 6, 39),
(4, 2, 39);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_id` int(5) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=436 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `app_id`, `icon`, `language`) VALUES
(314, 'Test', 18, 'info.png', 'spanish'),
(315, 'Testing', 18, 'info.png', 'spanish'),
(316, 'Main Menu 2', 18, 'registertovote_img-1.png', 'spanish'),
(317, 'Main Menu 3', 18, 'save-icon.png', 'spanish'),
(318, 'Main Menu 4', 18, '772526.png', 'spanish'),
(430, 'ABOUT', 18, '', 'english'),
(431, 'MEMBER RESOURCES', 18, '', 'english'),
(432, 'NEWS', 18, '', 'english'),
(433, 'POLITICAL ACTION', 18, '', 'english'),
(434, 'GET INVOLVED', 18, '', 'english'),
(435, 'CALENDAR', 18, '', 'english');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_no1` varchar(20) DEFAULT NULL,
  `phone_no2` varchar(20) DEFAULT NULL,
  `fax_no` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `time_cone` varchar(15) DEFAULT NULL,
  `app_id` varchar(20) DEFAULT NULL,
  `order` int(2) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `name`, `designation`, `address`, `phone_no1`, `phone_no2`, `fax_no`, `email`, `time_cone`, `app_id`, `order`) VALUES
(81, 'BRIDGET ADINOLFI', 'Union Representative', 'Office: (714) 995-4601 x228\r\nCell: (714) 920-3667\r\nEmail: bridget@ufcw324.org\r\nTuesday in the office', '(714)9954601', '(714)9203667', '7145551212', 'bridget@ufcw324.org', '2017-04-07 08:3', '18', 1),
(82, 'Jo', 'Founder', 'jo@gmail.com', '3523525', '32523525', '2352352', 'jo@gmail.com', '2017-03-21 23:2', '18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `get_started`
--

CREATE TABLE IF NOT EXISTS `get_started` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `office_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `office_title` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_no` varchar(15) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `time_cone` varchar(100) DEFAULT NULL,
  `app_id` varchar(20) DEFAULT NULL,
  `order` int(5) NOT NULL,
  PRIMARY KEY (`office_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`office_id`, `office_title`, `address`, `phone_no`, `website`, `time_cone`, `app_id`, `order`) VALUES
(37, 'UFCW Local 99 Tucson', '877 South Alvernon Way\r\nSuite 100\r\nTucson, Arizona 85711\r\n', '(520) 884-9716', 'ufcw99.org', '2015-04-01 20:10:24', '18', 1),
(38, 'UFCW Local 99 Phoenix', '2401 North Central Avenue\r\n2nd Floor\r\nPhoenix, Arizona 85004', '(602) 254-0099', 'ufcw99.org', '2015-04-01 20:07:59', '18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `maintainence_log`
--

CREATE TABLE IF NOT EXISTS `maintainence_log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `app_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `paymentDate` date NOT NULL,
  `validUpto` date NOT NULL,
  `transactionID` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `lastFour` int(4) NOT NULL,
  `customerID` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `exp_month` int(5) NOT NULL,
  `exp_year` int(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `country` varchar(10) NOT NULL,
  `plan_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(200) NOT NULL,
  `IsCheckpoint` int(11) NOT NULL,
  `intersection` varchar(1000) NOT NULL,
  `ethnicity` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `age` varchar(200) NOT NULL,
  `IsIdentity` int(11) NOT NULL,
  `IsForce` int(11) NOT NULL,
  `IsInsult` int(11) NOT NULL,
  `IsSexual` int(11) NOT NULL,
  `IsWeapon` int(11) NOT NULL,
  `OfficersName` varchar(300) NOT NULL,
  `badge` varchar(300) NOT NULL,
  `agency` varchar(300) NOT NULL,
  `IncidentDescription` varchar(3000) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `city`, `IsCheckpoint`, `intersection`, `ethnicity`, `gender`, `age`, `IsIdentity`, `IsForce`, `IsInsult`, `IsSexual`, `IsWeapon`, `OfficersName`, `badge`, `agency`, `IncidentDescription`, `timestamp`, `user_id`) VALUES
(7, 'Coty', 1, 'Intersecion', 'Middle Eastern', 'Male', 'Male', 1, 0, 0, 0, 1, 'Officer name', 'Officer badge', 'Police Sheriff', 'Incident', '2017-05-10 21:15:14', 0),
(8, 'Birmingham ', 1, 'Ok', 'Am. Indian or Alaska Native', 'Male', 'Male', 1, 1, 0, 1, 1, '', '', '', '', '2017-05-11 22:45:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `membership_feedback`
--

CREATE TABLE IF NOT EXISTS `membership_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `features` varchar(100) NOT NULL,
  `findInformation` varchar(100) NOT NULL,
  `recommend` varchar(100) NOT NULL,
  `changes` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `membership_feedback`
--

INSERT INTO `membership_feedback` (`id`, `features`, `findInformation`, `recommend`, `changes`) VALUES
(1, 'best features', 'very helpfull', 'yeah sure', 'not at all'),
(2, 'best features', 'very helpfull', 'yeah sure', 'not at all'),
(3, 'best features', 'very helpfull', 'yeah sure', 'not at all'),
(4, 'Other', 'Not at all easy', 'Not at all likely', 'Testing 2'),
(5, 'Member Resources', 'Slightly easy', 'Slightly likely', 'Testing 3'),
(6, 'Member Discounts', 'Very easy', 'Moderately likely', 'Testing 5');

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE IF NOT EXISTS `member_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `belong_to_myunion` varchar(500) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zip` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `cell` varchar(50) NOT NULL,
  `work_island` varchar(500) NOT NULL,
  `way_of_myunion` varchar(1000) NOT NULL,
  `employer_do_you_work` varchar(500) NOT NULL,
  `myunion_currently` varchar(500) NOT NULL,
  `myunion_share_info` varchar(500) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `member_info`
--

INSERT INTO `member_info` (`id`, `belong_to_myunion`, `name`, `address`, `city`, `state`, `zip`, `country`, `email`, `cell`, `work_island`, `way_of_myunion`, `employer_do_you_work`, `myunion_currently`, `myunion_share_info`, `time_stamp`) VALUES
(94, 'No', 'Henry Smith', 'Alabama, Texas.', 'Alabama', 'Texas', '123', 'United States', 'henry.smith01@outlook.com', '123456789', 'American Textile Mills (Kansas City, MO)', 'Push Notification', 'Securitas', 'No', 'True', '2017-08-03 18:42:45'),
(97, '0', 'Anonymous', 'Barih Street, Karachi, Pakistan', 'Karachi', 'Sindh', '', 'Pakistan', '8FA64D0F-E79F-422D-8C75-FF0752056E6D', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-15 09:02:55'),
(98, '0', 'Anonymous', 'Barih Street, Karachi, Pakistan', 'Karachi', 'Sindh', '', 'Pakistan', '8FA64D0F-E79F-422D-8C75-FF0752056E6D', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-15 09:36:00'),
(99, '0', 'Anonymous', 'Barih Street, Karachi, Pakistan', 'Karachi', 'Sindh', '', 'Pakistan', '8FA64D0F-E79F-422D-8C75-FF0752056E6D', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-15 09:39:07'),
(100, '0', 'Anonymous', '534 E Northridge Ave, Glendora, CA  91741, United States', 'Glendora', 'CA', '', 'United States', '8C3AE65C-3543-4E53-8D0C-1F4F987090B6', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 00:57:58'),
(101, '0', 'Anonymous', '534 E Northridge Ave, Glendora, CA  91741, United States', 'Glendora', 'CA', '', 'United States', '8C3AE65C-3543-4E53-8D0C-1F4F987090B6', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 01:17:57'),
(102, '0', 'Anonymous', 'Barih Street, Karachi, Pakistan', 'Karachi', 'Sindh', '', 'Pakistan', '8FA64D0F-E79F-422D-8C75-FF0752056E6D', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 09:31:54'),
(103, '0', 'Anonymous', '534 E Northridge Ave, Glendora, CA  91741, United States', 'Glendora', 'CA', '', 'United States', '8C3AE65C-3543-4E53-8D0C-1F4F987090B6', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 13:59:53'),
(104, '0', 'Anonymous', '534 E Northridge Ave, Glendora, CA  91741, United States', 'Glendora', 'CA', '', 'United States', '8C3AE65C-3543-4E53-8D0C-1F4F987090B6', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 13:59:53'),
(105, '0', 'Anonymous', '534 E Northridge Ave, Glendora, CA  91741, United States', 'Glendora', 'CA', '', 'United States', '8C3AE65C-3543-4E53-8D0C-1F4F987090B6', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 14:14:42'),
(106, '0', 'Anonymous', '534 E Northridge Ave, Glendora, CA  91741, United States', 'Glendora', 'CA', '', 'United States', '8C3AE65C-3543-4E53-8D0C-1F4F987090B6', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 14:17:26'),
(107, '0', 'Anonymous', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Karachi', 'Sindh', '', 'Pakistan', '54D1F5F9-9C3B-4737-BD05-4645E7E2901F', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 16:06:14'),
(108, '0', 'Anonymous', '1612 15th Ave S, Birmingham, AL  35205, United States', 'Birmingham', 'AL', '', 'United States', 'D7AC07B2-1E79-4DB4-8796-2A9913DBF593', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-19 16:32:49'),
(109, '0', 'Anonymous', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Karachi', 'Sindh', '', 'Pakistan', '8FA64D0F-E79F-422D-8C75-FF0752056E6D', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-20 19:24:15'),
(110, '0', 'Anonymous', 'Phoenix Sky Harbor International Airport, 3701 E Sky Harbor Blvd, Phoenix, AZ  85034, United States', 'Phoenix', 'AZ', '', 'United States', '345A3CE2-F95E-49F0-95CA-76529499B79F', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-20 20:22:38'),
(111, '0', 'Anonymous', '39 W Pheasant St, Heber, CA  92249, United States', 'Heber', 'CA', '', 'United States', 'E31324FE-13F1-45F6-AB77-3A0441DC3A8B', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-20 21:49:47'),
(112, '0', 'Anonymous', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Karachi', 'Sindh', '', 'Pakistan', '97F5B8D4-6E03-4B84-BC40-60B086E58EC5', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '0', '2017-09-21 08:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `member_topics`
--

CREATE TABLE IF NOT EXISTS `member_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `news_table` varchar(230) DEFAULT NULL,
  `description` varbinary(10000) NOT NULL,
  `file` varchar(100) NOT NULL,
  `time_cone` varchar(30) DEFAULT NULL,
  `category` varchar(20) NOT NULL,
  `app_id` int(11) NOT NULL,
  `new_image` varchar(500) NOT NULL,
  `facebook` varchar(20) DEFAULT NULL,
  `twitter` varchar(20) DEFAULT NULL,
  `google` varchar(20) DEFAULT NULL,
  `pinterest` varchar(20) DEFAULT NULL,
  `social` varchar(2) DEFAULT NULL,
  `order` int(100) DEFAULT NULL,
  `published` int(2) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1025 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `title`, `news_table`, `description`, `file`, `time_cone`, `category`, `app_id`, `new_image`, `facebook`, `twitter`, `google`, `pinterest`, `social`, `order`, `published`) VALUES
(979, 'UFCW 135', 'Testing', '', '', '2017-08-01 04:59:58', '8', 18, '2455331.', '', '', '', '', '', 0, 1),
(980, 'UFCW 135', 'Testing', '', '', '2017-08-03 01:20:43', '35', 18, '3765649.jpg', '', '', '', '', '', 0, 1),
(981, 'UFCW 135', 'Sample', '', '', '2017-08-02 01:20:18', '36', 18, '7096422.jpg', '', '', '', '', '', 0, 1),
(982, 'UFCW 135', 'Sample', '', '', '2017-08-02 01:20:45', '31', 18, '7628648.jpg', '', '', '', '', '', 0, 1),
(985, 'UFCW 135', 'Sample', '', '', '2017-08-03 01:21:51', '37', 18, '3930597.jpg', '', '', '', '', '', 0, 1),
(986, ' ', ' ', '', '', '2017-09-21 21:45:11', '568', 18, '2028257.jpg', '', '', '', '', '', 0, 1),
(988, ' ', ' ', '<p style="text-align: center;"><strong>INSURANCE</strong></p>\r\n\r\n<p style="text-align: center;"><a href="http://www.teamsters1932.org/member-benefits/insurance/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/metlife.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Introducing the new Teamster Auto Insurance Program from MetLife Auto &amp; Home&reg; As a member of IBT you now have access to valuable features and benefits, including special group discounts on auto and home insurance offered through MetLife Auto &amp; Home &ndash; a leading provider of quality auto insurance coverage.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/insurance/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>AT&amp;T WIRELESS</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/att-wireless/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/att.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>15% Discount on wireless services from AT&amp;T As an IBT member, you can save 15%* on your monthly individual or family wireless plan from AT&amp;T, the nation&rsquo;s only union wireless company. Union families save an average of $110 a year on monthly service. Plus, the $36 activation fee is waived for union members.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/att-wireless/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>CREDIT CARDS</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/credit-cards/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/credit-card.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Apply Online:&nbsp;<a href="http://teamstercardnow.com/" target="_blank">teamstercardnow.com</a>&nbsp;SECURE. FAST. EASY. The Teamster Privilege Credit Card from Capital One&reg; Designed to meet the needs of hard-working Teamsters and their families. Choose from three card options,all with competitive rates, U.S.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/credit-cards/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>SCHOLARSHIPS</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/scholarships/"><img alt="scholarships" src="http://www.teamsters1932.org/app/uploads/2017/03/scholarships.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Scholarships for the 2016-2017 school year are now closed. Please, check back in the new school year, this fall, for information on the latest scholarship opportunity.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/scholarships/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>LEGAL</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/legal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/legal.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Legal Assistance: Complimentary and paid legal services are available to our members. To obtain information regarding a free 30-minute legal consultation, please call our office at (909) 889-8377. For Pre-Paid Legal Services, Inc. is also available to our members for Legal and Identity Theft services.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/legal/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>LIBERTY MUTAL</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/liberty-mutal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/liberty-mutual.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Teamsters Local 1932 members may also find information regarding Home, Auto, and Life insurances offered by Liberty Mutual contact Tammy Gonzalez at 1-800-293-2518 to find a plan that meets your needs or apply online: www.libertymutual.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/liberty-mutal/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>COLONIAL LIFE</strong></p>\r\n\r\n<p>Teamsters Local 1932 members may contact Colonial Life at (909) 889-8377 ext. 232 or Gary Diamond at (213) 308-7321 for information on the following plans available to Teamsters Local 1932 members.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/colonial-life/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>EYEMED</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/eyemed/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/eyemed.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Both you and your covered dependents will be eligible to enroll in one of three comprehensive EyeMed Premier plans to replace any current vision plan you are enrolled in through Teamster Local 1932.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/eyemed/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>RETIREES</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/retirees/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/retirement.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Do you have plans to retire soon? Are you thinking of retiring soon? If so, did you know you can take Teamsters Local 1932 with you?</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/retirees/">Learn More</a></p>\r\n\r\n<p style="text-align:center"><strong>NOTARY</strong></p>\r\n\r\n<p style="text-align: center;"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p>Notary services will be for members only, by appointment only on Tuesdays and Thursdays. Please contact Anabel Palazuelos.(909) 889-8377 x217.</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/member-benefits/notary/">Learn More</a></p>\r\n', '', '2017-09-23 00:59:30', '569', 18, '1148743.jpg', '', '', '', '', '', 0, 1),
(990, ' ', ' ', '<h1 style="text-align:center"><span style="font-family:georgia,serif">About Us</span></h1>\r\n\r\n<p><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/9WWbNFXELXM" width="390"></iframe></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<h3><span style="font-family:georgia,serif"><strong>Teamsters Local 1932 (formerly SBPEA) Mission Statement:</strong></span></h3>\r\n\r\n<p><span style="font-family:georgia,serif">It is the mission of Teamsters Local 1932 to provide the best possible service to all our members with integrity and equality; to advance the social, economic, and educational welfare of the membership; to promote professional working relationships and fair play between members and management alike; and to positively contribute to the communities we serve and live in.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif"><strong>Know Your Rights!</strong></span></p>\r\n\r\n<h4><span style="font-family:georgia,serif">You may ask or hear some of these questions and more during the course of your employment:</span></h4>\r\n\r\n<ul>\r\n	<li><span style="font-family:georgia,serif">&ldquo;My manager just called me into a meeting. Am I entitled to have a union representative there?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can they just fire me?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How can I appeal a disciplinary action?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Can I see a copy of my personnel file?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;What if my employer wants me to take a drug test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Do I have to take a polygraph (lie detector) test?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;How do I file a grievance?&rdquo;</span></li>\r\n	<li><span style="font-family:georgia,serif">&ldquo;Should I get overtime pay?&rdquo;</span></li>\r\n</ul>\r\n\r\n<h4><span style="font-family:georgia,serif">How do you get answers to these and other questions?</span></h4>\r\n\r\n<p><span style="font-family:georgia,serif">That&rsquo;s what Teamsters Local 1932 is all about.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">We will help you get the answers you </span><span style="font-family:georgia,serif">need,</span><span style="font-family:georgia,serif"> when you need them, and to make sure your rights are not violated.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Your job and your rights are precious to you and Teamsters Local 1932. We&rsquo;re committed to protecting them. Without a strong Union, your employer might take advantage of you, or intimidate you during investigations or meetings. Teamsters Local 1932 provides the balance so that you can deal with your situation and management on a level playing field.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Our expert knowledge of your contract, personnel </span><span style="font-family:georgia,serif">rules</span><span style="font-family:georgia,serif"> and current laws is the insurance you get by being a member of Teamsters Local 1932.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">Critical information about your rights </span><span style="font-family:georgia,serif">change</span><span style="font-family:georgia,serif"> through new laws, contract language changes, and court decisions.</span></p>\r\n\r\n<p><span style="font-family:georgia,serif">You need Teamsters Local 1932 in your corner to make sure you get the respect and protection you deserve!</span></p>\r\n', '', '2017-09-21 21:43:29', '565', 18, '4488246.jpg', '', '', '', '', '', 0, 1),
(991, ' ', ' ', '<p style="text-align:center"><strong>BUSINESS AGENTS</strong></p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/SMatthews-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>STEVE MATTHEWS</strong></p>\r\n\r\n<p style="text-align:center"><strong>BUSINESS AGENT COORDINATOR</strong></p>\r\n\r\n<p><strong>Representing</strong>: City of San Bernadino Mid-Management, Chino (Prof, Tech and Clerical), Chino Valley Fire District</p>\r\n\r\n<p><strong>Supervisor: </strong>Randy Korgan<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:smatthews@teamsters1932.org">smatthews@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a>&nbsp; Ext. 238</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/sheri2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>SHERI ORELLANA</strong></p>\r\n\r\n<p><strong>Representing</strong>: City of San Bernadino Mid-Management, Chino (Prof, Tech and Clerical), Chino Valley Fire District</p>\r\n\r\n<p>e:&nbsp;<a href="mailto:sorellana@teamsters1932.org">sorellana@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377204">(909) 889-8377 Ext. 204</a>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/AWithers-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>ANTIONETTE MCDANIEL</strong></p>\r\n\r\n<p><strong>Representing:</strong> the City of Ontario, San Bernardino County Probation, San Bernardino County Public Defender, San Bernardino County District Attorney, and Human Services Personnel.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:awithers@teamsters1932.org">awithers@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377206">(909) 889-8377 Ext. 206</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/BethZen2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>BETH ZENDEJAS</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Airport, San Bernardino County Board of Supervisors,&nbsp;San Bernardino County Administrative Office, City of Colton Mid-Managers, San Bernardino County Counsel,&nbsp;San Bernardino County&nbsp;Human Resources, and&nbsp;San Bernardino County&nbsp;Sheriffs.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:bzendejas@teamsters1932.org">bzendejas@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377225">(909) 889-8377 Ext. 225</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/CarlosG2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>CARLOS GONZALES</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Workforce Development, Colton General, City of Redlands (GEAR), West Covina Non-Sworn Safety, City of Pomona (PCEA), and San Bernardino County Public Works.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:cgonzales@teamsters1932.org">cgonzales@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377261">(909) 889-8377 Ext. 261</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/DavidFarugia2-792x792.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>DAVID FARUGIA</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County&nbsp;Agriculture Weights &amp; Measures, San Bernardino County&nbsp;Architecture &amp; Engineering, San Bernardino County&nbsp;Auditor/Controller/Tax Collector, City of Big Bear Lake, San Bernardino County&nbsp;Clerk of the Board,&nbsp;San Bernardino County&nbsp;Land Use Services,&nbsp;San Bernardino County&nbsp;Museum, San Bernardino County&nbsp;Parks, San Bernardino County&nbsp;Public Health &amp; Women, Infant, Children (WIC), San Bernardino County&nbsp;Register Voters, andSan Bernardino County&nbsp;Special Projects<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:dfarugia@teamsters1932.org">dfarugia@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377211">(909) 889-8377 Ext. 211</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/e-rommel.jpg" style="height:200px; width:200px" /></strong></p>\r\n\r\n<p style="text-align:center"><strong>E. ROMMEL MARTINEZ</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Assessor,&nbsp;San Bernardino County Children&rsquo;s Network,&nbsp;San Bernardino County Library,&nbsp;San Bernardino County Department of Aging &amp; Adult Services&nbsp;(DAAS),&nbsp;San Bernardino County Preschool Services,&nbsp;San Bernardino County Purchasing,&nbsp;San Bernardino County Risk Management,&nbsp;San Bernardino County Veteran&rsquo;s Affairs,&nbsp;Inland Counties Emergency Medical Agency (ICEMA) and Program Integrity Division (PID)<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:emartinez@teamsters1932.org">emartinez@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/jacqueline-palone.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>JACQUELINE PALONE</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;Arrowhead Regional Medical Center (ARMC), Behavioral Health at Arrowhead Regional Medical Center, and Superior Court of&nbsp;San Bernardino County.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:jpalone@teamsters1932.org">jpalone@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377228">(909) 889-8377 Ext. 228</a>&nbsp;</p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/juan-delgado.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<p style="text-align:center"><strong>JUAN DELGADO</strong></p>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Children and Family Services, San Bernardino County Child Support Services, San Bernardino County Information Services Department, &nbsp;San Bernardino County Facilities Management, and City of Coachella<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:jdelgado@teamsters1932.org">jdelgado@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/natalie-harts.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">NATALIE HARTS</h4>\r\n\r\n<p><strong>Representing:&nbsp;</strong>Transitional Assistance Department (TAD):TAD CSCs &ndash; ALL LOCATIONS: (Except SUP &amp; MGT), TAD Processing Ctrs: All (Except SUP &amp; MGT), TAD LOCATIONS: (Except SUP &amp; MGT) 01, 03, 04, 06, 07, 08, 10, 18, 19, 24, 48, 79; TAD LOCATIONS: (SUP &amp; MGT ONLY): 09, 15, 25, 39, 75/Employment Services Program (ESP)/Foster Care(FC),&nbsp;Air Quality Mojave Desert (AQMD) (Back-up to Fred)&nbsp;and City of Needles.</p>\r\n\r\n<p>e:&nbsp;<a href="mailto:nharts@teamsters1932.org">nharts@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377243">(909) 889-8377 Ext. 243</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/pete-sierra.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">PETE SIERRA</h4>\r\n\r\n<p><strong>Representing:</strong>&nbsp;&nbsp;Arrowhead Regional Medical Center (ARMC) and Behavioral Health at&nbsp;Arrowhead Regional Medical Center (ARMC).<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:psierra@teamsters1932.org">psierra@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377">(909) 889-8377</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/steve-cadena.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">STEVE CADENA</h4>\r\n\r\n<p><strong>Representing:&nbsp;</strong>City of Fontana (City Hall &amp; Yards), City of Rancho Cucamonga, City of Hesperia, City of Banning (Mid-Managers), City of Barstow, San Bernardino County&nbsp;Fleet Management and San Bernardino County Department of Behavioral Health.<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:scadena@teamsters1932.org">scadena@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377218">(909) 889-8377 Ext. 218</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/susan-carl.jpg" style="height:200px; width:200px" /></p>\r\n\r\n<h4 style="text-align:center">SUSAN CARL</h4>\r\n\r\n<p><strong>Representing:</strong>&nbsp;San Bernardino County Schools (SBCSS),&nbsp;San Bernardino County&nbsp;Transitional Assistance Department (TAD):&nbsp;TAD CSCs &ndash; ALL LOCATIONS: (SUP/MGT Only), TAD Processing Ctrs &ndash; ALL, TAD LOCATIONS: (Except SUP &amp; MGT): 09, 15, 25, 39, 75 TAD LOCATIONS: (SUP &amp; MGT ONLY) 01, 02, 03, 04, 06, 07, 08, 10, 18, 19, 24, 48, 79,&nbsp;San Bernardino County&nbsp;Human Services Administration,&nbsp;San Bernardino County&nbsp;Performance, Education &amp; Resource Centers (PERC) and&nbsp;San Bernardino County&nbsp;Environmental Health Services.​​​​​​​<br />\r\n<br />\r\ne:&nbsp;<a href="mailto:scarl@teamsters1932.org">scarl@teamsters1932.org</a>&nbsp;<br />\r\n<br />\r\no:&nbsp;<a href="tel:9098898377216">(909) 889-8377 Ext. 216</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n', '', '2017-09-21 14:40:51', '567', 18, '3785300.jpg', '', '', '', '', '', 0, 1),
(992, ' ', ' ', '<h1><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/news-1932.png" style="height:35px; width:500px" /></h1>\r\n\r\n<p><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/new-1932-selected.png" style="height:42px; width:500px" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="http://ufcwunitedlatinos.org/wp-content/uploads/2017/09/Screen-Shot-2017-09-11-at-3.17.34-PM.png" style="height:379px; width:500px" /></p>\r\n', '', '2017-09-11 14:45:04', '582', 18, '5089310.png', '', '', '', '', '', 0, 1),
(1001, 'Sub 1', 'Sub Menu', '<p style="text-align: center;"><em><span style="color:#000080"><u><span style="font-size:48px"><span style="font-family:verdana,geneva,sans-serif"><big>Hello</big></span></span></u></span></em></p>\r\n', '', '2017-09-16 03:09:11', '280', 18, '6882106.', '', '', '', '', '', 0, 1),
(1006, 'This is my first article.', 'Hurray!', '<p>PDF link:<br />\r\n<a href="http://goo.gl/5FYTRb">http://goo.gl/5FYTRb</a></p>\r\n\r\n<p><iframe frameborder="0" height="215" src="https://www.youtube.com/embed/aKiqln366lc" width="320"></iframe></p>\r\n', '', '2017-09-18 18:15:46', '303', 18, '6781961.', '', '', '', '', '', 0, 1),
(1016, ' ', ' ', '<p style="text-align:center"><span style="font-size:14px"><strong>Teamsters Local 1932: Where Working People Stand Together</strong></span></p>\r\n\r\n<p><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/9WWbNFXELXM" width="390"></iframe></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><span style="font-size:14px"><strong>Teamster Advantage: Where Community Comes First</strong></span></p>\r\n\r\n<p><iframe align="middle" frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/heKOzG9zG30" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><span style="font-size:14px"><strong>Reality vs. &quot;Right to Work&quot;</strong></span></p>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/YOoh5AjigxY" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align: center;"><span style="font-size:14px"><strong>The Ugly Truth of &quot;Right to Work&quot;</strong></span></p>\r\n<iframe width="390" height="230" src="https://www.youtube.com/embed/T3AtaP5gFTQ" frameborder="0" allowfullscreen></iframe>\r\n<p>&nbsp;</p>\r\n', '', '2017-09-21 13:12:58', '566', 18, '2181057.jpg', '', '', '', '', '', 0, 1),
(1017, ' ', ' ', '<p style="text-align:center"><strong>Castle Park</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/insurance/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/metlife.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Promo Code (Add at top of home page): TL1932</p>\r\n\r\n<p style="text-align:center"><a href="http://www.castlepark.com/">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Wet &lsquo;n&rsquo; Wild Palm Springs</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/att-wireless/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/att.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Enter the following&nbsp;</p>\r\n\r\n<p style="text-align:center">Username: TEAMSTER</p>\r\n\r\n<p style="text-align:center">Password: 1932</p>\r\n\r\n<p style="text-align:center"><a href="http://prm-ps.secure.accesso.com/embed/login.php?m=159130&amp;emerchant_id=150077">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Splash Kingdom Waterpark</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/credit-cards/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/credit-card.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://bigairredlands.pfestore.com/retail/Teamsters/Default.aspx">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Costco</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/scholarships/"><img alt="scholarships" src="http://www.teamsters1932.org/app/uploads/2017/03/scholarships.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="https://costcomembershipoffer.com/purchase/purchase/Teamsters1932">Buy Costco Membership Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Legoland</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/legal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/legal.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="https://secure.legolandcaliforniaresort.com/LLC/shop/ViewItems.aspx?CG=8105&amp;C=8105">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Six Flags</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/liberty-mutal/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/liberty-mutual.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Enter the following</p>\r\n\r\n<p style="text-align:center">Username: TEAM1932<br />\r\nPassword: SixFlags11</p>\r\n\r\n<p style="text-align:center"><a href="https://sf-la.secure.accesso.com/embed/login.php?m=34246&amp;emerchant_id=5011">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Universal Studios Hollywood</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/eyemed/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/eyemed.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://ushtix.com/1932teamsters">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Sea World</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/eyemed/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/eyemed.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center">Promo Code: SWCJC</p>\r\n\r\n<p style="text-align:center"><a href="http://seaworldparks.com/swcbusinesssales">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Knott&#39;s Berry Farm</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/retirees/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/retirement.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="https://ticketsatwork.com/tickets/KnottsBerryFarm/?&amp;company=KBF1932">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Aquarium of the Pacific</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://affiliatetickets.aquariumofpacific.org/affiliate.asp?ID=A9225F71-7B49-4929-8773-5D4B8A9E981A">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Davey&#39;s Locker</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://www.daveyslocker.com/fishing_full_day_49_offer.html">Buy Tickets Here</a></p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p style="text-align:center"><strong>Big Air Trampoline Park</strong></p>\r\n\r\n<p style="text-align:center"><a href="http://www.teamsters1932.org/member-benefits/notary/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/notary.jpg" style="height:202px; width:290px" /></a></p>\r\n\r\n<p style="text-align:center"><a href="http://bigairredlands.pfestore.com/retail/Teamsters/Default.aspx">Buy Tickets Here</a></p>\r\n', '', '2017-09-23 01:27:32', '570', 18, '5266274.jpg', '', '', '', '', '', 0, 1),
(1018, ' ', ' ', '<p><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/teamsters-local-1932-06.jpg" style="height:390px; width:390px" /></p>\r\n\r\n<p style="text-align:center">Click <strong><a href="http://www.teamsters1932.org/local-1932-news/">HERE</a></strong> to view Teamsters 1932 current news.</p>\r\n', '', '2017-09-21 22:20:49', '573', 18, '8488086.jpg', '', '', '', '', '', 0, 1),
(1019, ' ', ' ', '<h1 style="text-align:center">&ldquo;RIGHT TO WORK&rdquo;</h1>\r\n\r\n<h3 style="text-align:center">&ldquo;RIGHT TO WORK&rdquo; IS REDUCING THE WAGES. FIGHT BACK WITH US.</h3>\r\n\r\n<p>America&rsquo;s labor unions protect and seek to lift the standards for working families in this country. Deep-pocketed opponents of workers like you, however, are seeking to kill the labor movement through a variety of different forms. A legal challenge,&nbsp;<em>Janus v AFSCME</em>, that could result in the end of your constitutional right to participate in a union-funded by each member equally and fairly is fast approaching the U.S Supreme Court docket. This would impose across the country.&nbsp;what our opponents have termed &mdash; quite ridiculously &mdash; &nbsp;&ldquo;Right to Work&rdquo;. Move over, in February, anti-worker legislators in the House of Representatives introduced a bill into Congress that would result in the same outcome.</p>\r\n\r\n<p>The information below has been collected for you to read, share, and act on. Don&rsquo;t just pass the message on, stay involved so that you can be up to date on the best ways to fight back with our Union.</p>\r\n\r\n<p><strong>Watch our &ldquo;Right to Work&rdquo; explainer below to learn how &ldquo;Right to Work&rdquo; creates universal misery &ndash; both in the public and private sector:</strong></p>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/AFAFz2BhgYk?start=267&amp;feature=oembed" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>How does &ldquo;Right to Work&rdquo; reduce the wages? This one simple graph teaches you just how it&rsquo;s done.</strong></p>\r\n\r\n<p><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/05/RTWCycle-copy-e1495737407358.png" style="height:478px; width:390px" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Former U.S Secretary of Labor Robert Reich goes in depth on the subject in just 3 minutes:</strong></p>\r\n\r\n<p><iframe frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/lLV4P5Pq0_0?start=5&amp;feature=oembed" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>But what of the rotten and racist roots of &ldquo;Right to Work&rdquo; laws? While they currently remain unmentioned when talking about &ldquo;Right to Work&rdquo;, this can change by helping spread our findings on the matter. First, watch the Local 1932 presentation on the ugly truth behind &ldquo;right to work&rdquo; and start sharing. The information had been fully vetted through a variety of reputable sources, including an interview with a professor at the University of Arkansas, the state where &ldquo;Right to Work&rdquo; was truly born.</strong></p>\r\n\r\n<h6><strong>WATCH THE PRESENTATION, WITH NARRATION INCLUDED:</strong></h6>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/T3AtaP5gFTQ?feature=oembed" width="390"></iframe></p>\r\n\r\n<h6><strong>DOWNLOAD THE POWERPOINT AND RUN YOUR OWN PRESENTATION:&nbsp;<a href="http://www.teamsters1932.org/app/uploads/2017/05/RTW-5.3.17.ppsx">UGLY TRUTH OF RTW</a></strong></h6>\r\n\r\n<p><strong>And so, what effects does &ldquo;Right to Work&rdquo; have on the public at large once enacted? The numbers don&rsquo;t lie &ndash; &ldquo;Right to Work&rdquo; is WRONG and TOXIC.</strong></p>\r\n\r\n<p><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/05/RTWInfographic.png" style="height:419px; width:390px" /></p>\r\n\r\n<p><em>&nbsp;</em></p>\r\n\r\n<p><em><strong>Help stop &ldquo;Right to Work&rdquo; by either&nbsp;<a href="http://salsa4.salsalabs.com/o/50740/p/dia/action3/common/public/?action_KEY=20867" target="_blank">emailing</a>&nbsp;your elected representative in Congress or&nbsp;<a href="http://www.teamsters1932.org/political-action/">calling them</a>. Get their contact information by clicking on the &ldquo;POLITICAL ACTION&rdquo; tab at the top of this page, followed by the appropriate city selection.</strong></em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Why does it matter? Because working people standing together for their right to organize a union in their workplace has made our country stronger.&nbsp;</strong></p>\r\n\r\n<p><iframe frameborder="0" height="230" src="https://www.youtube.com/embed/STv4KWHbelM?feature=oembed" width="390"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>STAY UP TO DATE ON THE LATEST RTW NEWS AT <a href="http://www.weareonebigunion.com/press" target="_blank">WWW.WEAREONEBIGUNION.COM/PRESS</a></h3>\r\n', '', '2017-09-21 22:29:10', '575', 18, '7625100.jpg', '', '', '', '', '', 0, 1),
(1020, ' ', ' ', '<p>&nbsp;</p>\r\n\r\n<p style="text-align: center;"><span style="font-size:28px">Click </span><span style="font-size:36px"><strong><a href="http://www.teamsters1932.org/publications-the-voice/">HERE</a></strong></span><span style="font-size:28px"> to view current publications of The Voice.</span></p>\r\n', '', '2017-09-21 22:35:36', '576', 18, '4776071.jpg', '', '', '', '', '', 0, 1),
(1021, ' ', ' ', '<p><a href="http://www.teamsters1932.org/political-action/adelanto/"><img alt="adelanto" src="http://www.teamsters1932.org/app/uploads/2017/03/Adelanto-California.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<p>ADELANTO</p>\r\n\r\n<h5>Zip Code(s): 92301</h5>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/apple-valley/"><img alt="apple-valley" src="http://www.teamsters1932.org/app/uploads/2017/03/apple-valley.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>APPLE VALLEY</h5>\r\n\r\n<p>Zip Code(s): 92307, 92308</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/banning/"><img alt="banning" src="http://www.teamsters1932.org/app/uploads/2017/04/banning.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>BANNING</h5>\r\n\r\n<p>Zip Code(s): 92220</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/barstow/"><img alt="barstow" src="http://www.teamsters1932.org/app/uploads/2017/03/barstow.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>BARSTOW</h5>\r\n\r\n<p>Zip Code(s): 92311, 92312</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/chino/"><img alt="chino" src="http://www.teamsters1932.org/app/uploads/2017/03/chino-1.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>CHINO</h5>\r\n\r\n<p>Zip Code(s): 91708</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/chino-hills/"><img alt="chino-hills" src="http://www.teamsters1932.org/app/uploads/2017/03/chino.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>CHINO HILLS</h5>\r\n\r\n<p>Zip Code(s): 91709</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/coachella/"><img alt="coachella" src="http://www.teamsters1932.org/app/uploads/2017/04/coachella.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>COACHELLA</h5>\r\n\r\n<p>Zip Code(s): 92236</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/colton/"><img alt="coltan" src="http://www.teamsters1932.org/app/uploads/2017/03/colton.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>COLTON</h5>\r\n\r\n<p>Zip Code(s): 92324</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/fontana/"><img alt="fontana" src="http://www.teamsters1932.org/app/uploads/2017/04/fontana.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>FONTANA</h5>\r\n\r\n<p>Zip Code(s): 92335</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/grand-terrace/"><img alt="grand-terrace" src="http://www.teamsters1932.org/app/uploads/2017/03/grandterrace.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>GRAND TERRACE</h5>\r\n\r\n<p>Zip Code(s): 92313</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/hesperia/"><img alt="hesperia" src="http://www.teamsters1932.org/app/uploads/2017/04/hesperia.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>HESPERIA</h5>\r\n\r\n<p>Zip Code(s): 92340, 92344, 92345</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/highland/"><img alt="highland" src="http://www.teamsters1932.org/app/uploads/2017/04/highland.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>HIGHLAND</h5>\r\n\r\n<p>Zip Code(s): 92346</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/loma-linda/"><img alt="loma-linda" src="http://www.teamsters1932.org/app/uploads/2017/04/lomalinda.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>LOMA LINDA</h5>\r\n\r\n<p>Zip Code(s): 92354, 92350, 92357</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/montclair/"><img alt="montclair" src="http://www.teamsters1932.org/app/uploads/2017/04/montclair.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>MONTCLAIR</h5>\r\n\r\n<p>Zip Code(s): 91763</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/needles/"><img alt="needles" src="http://www.teamsters1932.org/app/uploads/2017/04/needles.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>NEEDLES</h5>\r\n\r\n<p>Zip Code(s): 92363</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/ontario/"><img alt="ontario" src="http://www.teamsters1932.org/app/uploads/2017/04/ontario.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>ONTARIO</h5>\r\n\r\n<p>Zip Code(s): 91758, 91761, 91762, 91764</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/pomona/"><img alt="pomona" src="http://www.teamsters1932.org/app/uploads/2017/04/pomona.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>POMONA</h5>\r\n\r\n<p>Zip Code(s): 91766, 91767, 91768, 91769</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/rancho-cucamonga/"><img alt="rancho-cucamonga" src="http://www.teamsters1932.org/app/uploads/2017/04/rancho-cucamonga.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>RANCHO CUCAMONGA</h5>\r\n\r\n<p>Zip Code(s): 91730, 91701, 91729, 91737, 91739</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/redlands/"><img alt="Redlands-ca" src="http://www.teamsters1932.org/app/uploads/2017/04/redlands.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>REDLANDS</h5>\r\n\r\n<p>Zip Code(s): 92375, 92373, 92374</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/rialto/"><img alt="Rialto" src="http://www.teamsters1932.org/app/uploads/2017/04/Rialto.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>RIALTO</h5>\r\n\r\n<p>Zip Code(s): 92376, 92377</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/san-bernardino/"><img alt="san-bernardino" src="http://www.teamsters1932.org/app/uploads/2017/04/sanbernardino.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>SAN BERNARDINO</h5>\r\n\r\n<p>Zip Code(s): 92401, 92403, 92404, 92405, 92407, 92408, 92410, 92411</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/twentynine-palms/"><img alt="twentynine-palms" src="http://www.teamsters1932.org/app/uploads/2017/04/twentyninepalms.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>TWENTYNINE PALMS</h5>\r\n\r\n<p>Zip Code(s): 92277, 92278</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/upland/"><img alt="upland" src="http://www.teamsters1932.org/app/uploads/2017/04/upland.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>UPLAND</h5>\r\n\r\n<p>Zip Code(s): 91785, 91786, 91784</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/victorville/"><img alt="victorville" src="http://www.teamsters1932.org/app/uploads/2017/04/victorville.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>VICTORVILLE</h5>\r\n\r\n<p>Zip Code(s): 92394, 92395, 92392, 92393</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/west-covina/"><img alt="west-covina" src="http://www.teamsters1932.org/app/uploads/2017/04/westcovina.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>WEST COVINA</h5>\r\n\r\n<p>Zip Code(s): 91791, 91792, 91793, 91790</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/yucaipa/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/04/yucaipa.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>YUCAIPA</h5>\r\n\r\n<p>Zip Code(s): 92399</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/yucca-valley/"><img alt="yucca-valley" src="http://www.teamsters1932.org/app/uploads/2017/04/yucca.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>YUCCA VALLEY</h5>\r\n\r\n<p>Zip Code(s): 92284, 92286</p>\r\n\r\n<p><a href="http://www.teamsters1932.org/political-action/additional-cities/"><img alt="" src="http://www.teamsters1932.org/app/uploads/2017/03/cities.jpg" style="height:281px; width:390px" /></a></p>\r\n\r\n<h5>ADDITIONAL CITIES</h5>\r\n', '', '2017-09-21 22:41:12', '577', 18, '8781416.jpg', '', '', '', '', '', 0, 1),
(1023, 'Teamster Advantage', 'Teamster Advantage', '<p><iframe frameborder="0" height="315" src="https://www.youtube.com/embed/heKOzG9zG30" width="260"></iframe></p>\r\n\r\n<h5>Shopping locally is a vital&nbsp;component of a healthy economy. Rather than being shipped off to corporate headquarters when shoppers choose big business, by shopping locally, tax revenue is much more likely to recycle within the local economy.</h5>\r\n\r\n<h5>With Teamster Advantage, Teamsters&nbsp;across the Inland region like you are rewarded with discount offers by program partners in recognition of your hard work. Teamster Advantage allows you to shop locally, with big savings, so we can keep shopping locally.</h5>\r\n\r\n<h5>To qualify for Teamster Advantage discounts, you must present your Teamster Advantage Member ID to program partners.</h5>\r\n\r\n<h5>Don&#39;t have a Member ID? &nbsp;<u>Go to Teamsters Local 1932 Union Hall</u>&nbsp;and sign up, or call your Business Agent for more information. &nbsp;You must be a dues-paying member, in good standing, of Teamsters Local 1932 or Local 63. &nbsp;No sign-up fee for Teamster Advantage program participation.</h5>\r\n', '', '2017-09-23 01:47:38', '571', 18, '8002363.', '', '', '', '', '', 0, 1);
INSERT INTO `news` (`news_id`, `title`, `news_table`, `description`, `file`, `time_cone`, `category`, `app_id`, `new_image`, `facebook`, `twitter`, `google`, `pinterest`, `social`, `order`, `published`) VALUES
(1024, 'Contracts', 'Contracts', '<h3>You can download the current Contracts by clicking on the links below:</h3>\r\n\r\n<h5>Chino Valley Fire District</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/CVIFD_2016_2018_1_.pdf" rel="noopener" target="_blank">Chino Valley Fire District (2016-2018)</a> </strong>Memorandum of Understanding Chino Valley Independent Fire District and Teamsters, Local 1932 Effective: July 16, 2016 through June 30, 2018 Adopted: June 8, 2016</li>\r\n</ul>\r\n\r\n<h5>City of Banning</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Resolution_No__2016_95.pdf" rel="noopener" target="_blank">Banning MOU, 2016-2017</a> </strong></li>\r\n</ul>\r\n\r\n<h5>City of Barstow</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Barstow_Unit_1___7_1_15_thru_6_30_18.pdf" rel="noopener" target="_blank">Barstow Unit 1 - 7-1-15 thru 6-30-18</a> </strong>Barstow Unit 1 - 7-1-15 thru 6-30-18</li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Barstow_Unit_2___7_1_15_thru_6_30_18.pdf" rel="noopener" target="_blank">Barstow Unit 2 - 7-1-15 thru 6-30-18</a> </strong>Barstow Unit 2 - 7-1-15 thru 6-30-18</li>\r\n</ul>\r\n\r\n<h5>City of Big Bear</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/BBL_MOU_7_1_12_to_6_30_15.pdf" rel="noopener" target="_blank"><strong>Big Bear Lake MOU, 2012-2015</strong></a></li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/08/Big-Bear-Lake-MOU-for-website.pdf">Big Bear Lake MOU, 2015-2018</a></strong></li>\r\n</ul>\r\n\r\n<h5>City of Chino</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/CHINO_MOU_PTC_Unit_2015_2019_For_Print_1_Reduced_Updated_10_2016_1_.pdf" rel="noopener" target="_blank"><strong>CHINO MOU PTC Unit_2015-2019_For Print-1 Reduced Updated 10-2016</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Coachella</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/FINALJuly2014June2017Confi.pdf" rel="noopener" target="_blank"><strong>2014-2017 City of Coachella Mid-Management Employees</strong></a> MEMORANDUM OF UNDERSTANDING BETWEEN THE CITY OF COACHELLA AND SAN BERNARDINO PUBLIC EMPLOYEES ASSOCIATION CONFIDENTIAL MID-MANAGEMENT EMPLOYEES JULY 1, 2014 TO JUNE 30, 2017</li>\r\n</ul>\r\n\r\n<h5>City of Colton</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Colton_General_Unit_MOU_2014_2016.pdf" rel="noopener" target="_blank"><strong>Colton General Unit MOU, 2014-2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Colton_Mid_Mgrs_MOU_3_1_14_to_12_31_16.pdf" rel="noopener" target="_blank"><strong>Colton Mid Managers MOU 2014-2016</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Fontana</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Fontana_City_Hall_MOU_2014_2017.pdf" rel="noopener" target="_blank"><strong>Fontana City Hall MOU 2014-2017</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Yard_MOU_2016_2017_Ratified.pdf" rel="noopener" target="_blank"><strong>Yard MOU 2016-2017 Ratified</strong></a> Yard MOU 2016-2017 Ratified</li>\r\n</ul>\r\n\r\n<h5>City of Hesperia</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/The_City_of_Hesperia_MOU.pdf" rel="noopener" target="_blank"><strong>Hesperia MOU 2016</strong></a> EXHIBIT A To Resolutions: Resolution No. 2016-003 and HWD 2016-02 MEMORANDUM OF UNDERSTANDING Made and Entered Into Between The City of Hesperia and the Teamsters Local 1932 Effective January 1, 2016 through December 31, 2016</li>\r\n</ul>\r\n\r\n<h5>City of Montclair</h5>\r\n\r\n<ul>\r\n	<li>No Files Found</li>\r\n</ul>\r\n\r\n<h5>City of Needles</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Needles_MOU___JUL_1_2015_to_JUN_30_2018.pdf" rel="noopener" target="_blank"><strong>Needles MOU 2015-2018</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Ontario</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Teamsters_Local_1932_Amendment_and_Extension_2013_2017.pdf" rel="noopener" target="_blank"><strong>Ontario MOU, 2013-2017</strong></a> Memorandum of Understanding Between Teamsters Local 1932 and City of Ontario July 1, 2013 though June 30, 2017</li>\r\n</ul>\r\n\r\n<h5>City of Pomona</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Pomona_PCEA_MOU_2014_2016.pdf" rel="noopener" target="_blank"><strong>Pomona MOU 2014-2016</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Rancho Cucamonga</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Rancho_MOU_2014_2017.pdf" rel="noopener" target="_blank"><strong>Rancho MOU, 2014-2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of Redlands</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/MOU_GEAR_FY_2015_2017.pdf" rel="noopener" target="_blank"><strong>Redlands MOU 2015-2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>City of San Bernardino</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Middle_Management_Employees_MOU_City_of_San_Bernardino.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> MOU for City of San Bernardino Middle Management Employees</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2009.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2009</li>\r\n	<li><strong>Middle Management Employees</strong> (358.57 KB) Side Letter 2010</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2013.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2013</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2014.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2014</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2015a.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2015 part 1</li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2015b.pdf" rel="noopener" target="_blank">Middle Management Employees</a> </strong>Side Letter 2015 part 2</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_2015c.pdf" rel="noopener" target="_blank"><strong>Middle Management Employees</strong></a> Side Letter 2015 part 3</li>\r\n</ul>\r\n\r\n<h5>County of San Bernardino</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBPEA_Teamsters_Local_1932_MOU_2015_2019_2_23_16.pdf" rel="noopener" target="_blank"><strong>2015-2019 County of San Bernardino MOU for Admin. Services, Clerical, Craft, Labor &amp; Trades, Management, Supervisory, Supervisory Nurses, and Technical &amp; Inspection Units</strong></a> San Bernardino County: Consolidated Memorandum of Understanding 2015-2019 Administrative Services; Clerical; Craft, Labor &amp; Trades; Management; Supervisory; Supervisory Nurses; and Technical &amp; Inspection Units.</li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/County___2014_2017___ADM_CLK_CLT_MGT_SUP_SRN_TI_ONLY.pdf" rel="noopener" target="_blank"><strong>SB County Consolidated MOU, 2014-2017</strong></a> Administrative Services; Clerical; Craft, Labor &amp; Trades; Management; Supervisory; Supervisory Nurses; and Technical &amp; Inspection Units</li>\r\n</ul>\r\n\r\n<h5>East Valley Water District</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/EVWD_MOU_7_1_2014_to_6_30_2017.pdf" rel="noopener" target="_blank"><strong>EVWD MOU, 2014-2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>Mojave Desert Air Quality Management District</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/MOU_Adopted_9_26_2016_fnl.pdf" rel="noopener" target="_blank"><strong>Mojave Desert AQMD MOU 2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Side_Letter_Agreement_Rule_14.pdf" rel="noopener" target="_blank"><strong>Side Letter 2017</strong></a></li>\r\n</ul>\r\n\r\n<h5>San Bernardino County Schools</h5>\r\n\r\n<ul>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Health___Welfare_Contract_2014_15_School_YR.pdf" rel="noopener" target="_blank"><strong>SB County Schools Health &amp; Welfare Contract 2014-2015</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Health___Welfare_Cost_Sheet_2015_16_School_YR.pdf" rel="noopener" target="_blank"><strong>SB County Schools Health &amp; Welfare Cost Sheet 2015-2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Co_Schools_MOU_2014_2017.pdf" rel="noopener" target="_blank"><strong>SB County Schools MOU, 2014-2017</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/Co_Schools_MOU_Evaluation_Procedures_7_6_2015.pdf" rel="noopener" target="_blank"><strong>SB County Schools MOU, Evaluation Procedures</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Side_Letter___Salary_Bonus_2015_16_School_Yr.pdf" rel="noopener" target="_blank"><strong>SB County Schools Side Letter - Salary Bonus 2015-2016</strong></a></li>\r\n	<li><a href="http://www.teamsters1932.org/app/uploads/2017/03/SBCSS_Side_Letter___Leave_Provision_3_26_2015.pdf" rel="noopener" target="_blank"><strong>SB County Schools Side Letter- Leave Provision</strong></a></li>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/08/SB-COUNTY-SCHOOLS-CONTRACT-2017-2020.pdf">SB County Schools Collective Bargaining Agreement, 2017-2020</a></strong></li>\r\n</ul>\r\n\r\n<h5>Superior Court of California, County of San Bernardino</h5>\r\n\r\n<ul>\r\n	<li><strong><a href="http://www.teamsters1932.org/app/uploads/2017/03/Superior_Court_of_Califonia_2015_2019.pdf" rel="noopener" target="_blank">2015 - 2019 MEMORANDUM OF UNDERSTANDING BETWEEN THE SUPERIOR ', '', '2017-09-23 01:49:36', '572', 18, '4717291.', '', '', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `petition_people`
--

CREATE TABLE IF NOT EXISTS `petition_people` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `app_id` int(5) NOT NULL COMMENT 'APP ID',
  `zipcode` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=245 ;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE IF NOT EXISTS `plans` (
  `plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `amount` int(10) NOT NULL COMMENT 'Dollars',
  `months` int(5) NOT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`plan_id`, `amount`, `months`) VALUES
(1, 100, 1),
(2, 200, 2),
(3, 200, 1),
(4, 400, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pushbridge`
--

CREATE TABLE IF NOT EXISTS `pushbridge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pushID` int(5) NOT NULL,
  `appID` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `pushbridge`
--

INSERT INTO `pushbridge` (`id`, `pushID`, `appID`) VALUES
(1, 203, 18),
(17, 228, 18),
(16, 227, 18),
(15, 226, 18),
(14, 225, 18),
(13, 224, 18),
(12, 223, 18),
(11, 222, 18),
(10, 221, 18),
(18, 229, 18);

-- --------------------------------------------------------

--
-- Table structure for table `pushmessage`
--

CREATE TABLE IF NOT EXISTS `pushmessage` (
  `pushID` int(3) NOT NULL AUTO_INCREMENT COMMENT 'AutoIncrement.',
  `title` varchar(100) NOT NULL,
  `pushMessage` varchar(200) NOT NULL COMMENT 'NotificationMsg',
  `topics` varchar(100) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Author App ID',
  PRIMARY KEY (`pushID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=243 ;

--
-- Dumping data for table `pushmessage`
--

INSERT INTO `pushmessage` (`pushID`, `title`, `pushMessage`, `topics`, `TimeStamp`) VALUES
(242, 'Hurray', 'Beep Beep!', 'allUsers', '2017-09-19 01:28:01'),
(241, 'Hurray', 'Beep Beep!', 'United States', '2017-09-19 01:27:27'),
(240, 'Hi', 'Testing Push', 'allUsers', '2017-09-16 12:58:51'),
(239, 'Hi', 'Testing Notification', 'allUsers', '2017-09-16 10:37:18');

-- --------------------------------------------------------

--
-- Table structure for table `push_notification`
--

CREATE TABLE IF NOT EXISTS `push_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UDID` varchar(250) NOT NULL,
  `token` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE IF NOT EXISTS `register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FrstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `EmployeeID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `BirthDate` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `requestunion`
--

CREATE TABLE IF NOT EXISTS `requestunion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(50) NOT NULL,
  `forget_token` varchar(50) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `employer` varchar(100) NOT NULL,
  `peoplework` varchar(100) NOT NULL,
  `postal` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `issues` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `requestunion`
--

INSERT INTO `requestunion` (`id`, `fname`, `lname`, `email`, `password`, `forget_token`, `phone`, `country`, `state`, `city`, `occupation`, `employer`, `peoplework`, `postal`, `address`, `issues`) VALUES
(12, 'arbish', 'palla', 'arbishpalla@gmail.com', '558709', '', '03152554562', 'pakistan', 'sindh', 'karachi', 'Web Developer', '2', '5', '26500', '966/2 fb area karachi', 'nothing'),
(13, 'arbish', 'palla', 'arbishpalla@gmail.com', '195112', '', '03152554562', 'pakistan', 'sindh', 'karachi', 'Web Developer', '2', '5', '26500', '966/2 fb area karachi', 'nothing'),
(14, 'arbish', 'palla', 'arbishpalla@gmail.com', '803532', '', '03152554562', 'pakistan', 'sindh', 'karachi', 'Web Developer', '2', '5', '26500', '966/2 fb area karachi', 'nothing'),
(15, 'arbish', 'palla', 'arbishpalla@gmail.com', '950539', '', '03152554562', 'pakistan', 'sindh', 'karachi', 'Web Developer', '2', '5', '26500', '966/2 fb area karachi', 'nothing'),
(16, 'arbish', 'palla', 'arbishpalla@gmail.com', '981541', '', '03152554562', 'pakistan', 'sindh', 'karachi', 'Web Developer', '2', '5', '26500', '966/2 fb area karachi', 'nothing'),
(28, 'faizan', 'khan', 'faizan_developer@hotmail.com', '123', '165661', '00022200241', 'pakistan', 'sin', 'city', 'deve', 'avil', 'yes', '7566', 'karachi', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `shopunion`
--

CREATE TABLE IF NOT EXISTS `shopunion` (
  `shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(100) DEFAULT NULL,
  `employe_name` varchar(50) DEFAULT NULL,
  `store_number` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `latitude` varchar(30) NOT NULL COMMENT 'radians',
  `longitude` varchar(30) NOT NULL COMMENT 'radians',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `shopunion`
--

INSERT INTO `shopunion` (`shop_id`, `area`, `employe_name`, `store_number`, `address`, `city`, `state`, `zip`, `phone`, `app_id`, `latitude`, `longitude`) VALUES
(3, 'Birmingham', 'Subway', '1', '1614 15th Avenue South Apartment H', 'Birmingham', 'Alabama', '35205', '6615938417', 18, '0.43400269', '1.1697976285'),
(4, 'Jefferson', 'Wings', '2', '1614 15th Avenue South Apartment H', 'Birmingham', 'Alabama', '35205', '6615938417', 18, '0.43400269', '1.1697976285'),
(5, 'hgfhgfh', 'fghgf', 'gfhgfhgf', 'hgfh', 'hgfhfgh', 'ghgfh', 'ghfgh', 'ghgfhf', 18, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `stayconected`
--

CREATE TABLE IF NOT EXISTS `stayconected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `order` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `stayconected`
--

INSERT INTO `stayconected` (`id`, `name`, `link`, `app_id`, `order`) VALUES
(50, 'Instagram', 'https://www.instagram.com/teamsters1932/', 18, 3),
(56, 'Facebook', 'https://www.facebook.com/teamsterslocal1932/', 18, 1),
(61, 'Twitter', 'https://twitter.com/1932teamsters?lang=en', 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(50) NOT NULL,
  `submenu_id` varchar(10) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=585 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `menu_id`, `submenu_id`, `icon`, `name`) VALUES
(280, 315, '315.1', 'info.png', 'Sub Menu'),
(281, 315, '315.2', 'info.png', 'Find my elected officials'),
(282, 315, '315.3', 'info.png', 'Sub Menu 2'),
(565, 430, '430.1', '', 'About Us'),
(566, 430, '430.2', '', 'Videos'),
(567, 430, '430.3', '', 'Staff List'),
(568, 430, '430.4', '', 'We are Teamsters'),
(569, 431, '431.1', '', 'Benefits'),
(570, 431, '431.2', '', 'Discounts'),
(571, 431, '431.3', '', 'Teamster Advantage'),
(572, 431, '431.4', '', 'Contracts'),
(573, 432, '432.1', '', 'News'),
(574, 432, '432.2', '', 'Event Calendar'),
(575, 432, '432.3', '', 'RTW'),
(576, 432, '432.4', '', 'The Voice'),
(577, 433, '433.1', '', 'Zip-code entry'),
(578, 433, '433.2', '', 'Register to Vote'),
(579, 433, '433.3', '', 'Endorsement News'),
(580, 434, '434.1', '', 'Join Power Network'),
(581, 434, '434.2', '', 'Political Activism'),
(582, 434, '434.3', '', 'Organizing Tips'),
(583, 434, '434.4', '', 'Trainings'),
(584, 434, '434.5', '', 'Stewards');

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE IF NOT EXISTS `super_admin` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `super_admin`
--

INSERT INTO `super_admin` (`user_id`, `name`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(2) NOT NULL,
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `forgot_password` varchar(100) DEFAULT NULL,
  `cookie` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT 'user.jpg',
  `email` varchar(25) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `time_cone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `app_id`, `user_name`, `password`, `forgot_password`, `cookie`, `image`, `email`, `logo`, `cover`, `time_cone`) VALUES
(7, 18, 'teamster1932', 'teamster1932', NULL, NULL, 'user.jpg', 'steve@linkedunion.com', 'ufcw324-logo.png', 'ufcw324-cover.jpg', '2017-03-1 05:17:24');

-- --------------------------------------------------------

--
-- Table structure for table `users_location`
--

CREATE TABLE IF NOT EXISTS `users_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=133 ;

--
-- Dumping data for table `users_location`
--

INSERT INTO `users_location` (`id`, `UserID`, `latitude`, `longitude`, `address`, `country`, `state`, `city`, `timestamp`) VALUES
(3, 97, '24.8798569476204', '67.0353233051857', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 09:04:34'),
(4, 98, '24.879881240531', '67.0353578695011', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 09:36:00'),
(5, 99, '24.8799425829434', '67.035264121431', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 09:39:08'),
(6, 99, '24.879815974296', '67.0355078671752', 'Misquit Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 09:40:43'),
(7, 99, '24.8798333525059', '67.035366298593', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 10:28:51'),
(8, 99, '24.879902014532', '67.0353326015799', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 10:58:31'),
(9, 99, '24.8797815246739', '67.0352994092433', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 11:14:19'),
(10, 99, '24.8799987416946', '67.0354274847238', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 11:15:21'),
(11, 99, '24.8799051996552', '67.0353919454543', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 11:16:08'),
(12, 99, '24.8799051996552', '67.0353919454543', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 11:16:09'),
(13, 99, '24.8799051996552', '67.0353919454543', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 11:16:09'),
(14, 99, '24.8799036070936', '67.0353502873956', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-15 11:20:06'),
(15, 99, '24.8736934865172', '67.0525647587651', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-16 09:53:31'),
(16, 99, '24.873586711505', '67.052777530779', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-16 10:10:03'),
(17, 99, '24.8736410532086', '67.0526323308642', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-16 10:15:39'),
(18, 99, '24.8736413708385', '67.0526378036371', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-16 10:19:31'),
(19, 99, '24.8736400829479', '67.0526828549254', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-16 10:20:57'),
(20, 99, '24.8736624556502', '67.052749424654', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-16 10:29:47'),
(21, 99, '24.879950922937', '67.0355145726977', 'Misquit Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-18 07:29:22'),
(22, 99, '24.87994287631', '67.0354845654844', 'Misquit Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-18 07:56:18'),
(23, 99, '24.879846358695', '67.0352989901481', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-18 09:05:49'),
(24, 99, '24.8736861725792', '67.0525737379717', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-18 20:04:46'),
(25, 99, '24.8736865255546', '67.0525462944097', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-18 20:06:43'),
(26, 100, '34.1457135044356', '-117.858137637487', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 00:57:58'),
(27, 100, '34.1457254067381', '-117.858000258094', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 01:00:04'),
(28, 100, '34.1469473927916', '-117.855140178228', '676â€“698 E Sierra Madre Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 01:01:37'),
(29, 101, '34.1456151428019', '-117.858053566998', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 01:17:57'),
(30, 99, '24.8800297547363', '67.0354060270516', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 08:19:45'),
(31, 99, '24.879943043948', '67.0353859943031', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 08:29:56'),
(32, 99, '24.879943043948', '67.0353859943031', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 08:29:57'),
(33, 99, '24.879943043948', '67.0353859943031', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 08:29:57'),
(34, 99, '24.8798413421106', '67.0353272834598', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 08:33:17'),
(35, 99, '24.8799924133577', '67.0353643689929', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 08:38:07'),
(36, 99, '24.8798871785634', '67.0353562385468', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 08:39:35'),
(37, 99, '24.8798582609975', '67.0354118943839', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:01:12'),
(38, 99, '24.8798446404048', '67.0353449229775', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:01:19'),
(39, 99, '24.8798446404048', '67.0353449229775', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:01:20'),
(40, 99, '24.8798446404048', '67.0353449229775', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:01:33'),
(41, 99, '24.8798446404048', '67.0353449229775', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:01:33'),
(42, 99, '24.8798446404048', '67.0353449229775', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:01:34'),
(43, 99, '24.8799090553307', '67.0355677139638', 'Misquit Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:22:41'),
(44, 99, '24.8798361746826', '67.0353696495919', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:23:47'),
(45, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:27'),
(46, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:27'),
(47, 99, '24.8798891902202', '67.0353685599445', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:27'),
(48, 99, '24.8798891902202', '67.0353685599445', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:27'),
(49, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:27'),
(50, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:27'),
(51, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:27'),
(52, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(53, 99, '24.8798821494215', '67.0354323462276', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(54, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(55, 99, '24.8798821494215', '67.0354323462276', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(56, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(57, 99, '24.8797854222589', '67.0354290772854', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(58, 99, '24.8798891902202', '67.0353685599445', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(59, 99, '24.8797854222589', '67.0354290772854', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(60, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(61, 99, '24.8799017630749', '67.0354195219158', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:25:28'),
(62, 99, '24.8799009667941', '67.0353511255859', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:26:49'),
(63, 102, '24.8798896512248', '67.0353670512019', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:31:54'),
(64, 102, '24.8796657239083', '67.0354029908328', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:33:12'),
(65, 102, '24.8799116537207', '67.035388425055', 'Barih Street, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 09:33:37'),
(66, 101, '34.1456612432694', '-117.858115760719', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 13:59:11'),
(67, 103, '34.1456612432694', '-117.858115760719', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 13:59:53'),
(68, 104, '34.1456612432694', '-117.858115760719', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 13:59:53'),
(69, 105, '34.145661536636', '-117.858011908939', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:14:42'),
(70, 106, '34.145859936284', '-117.85813906241', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:26'),
(71, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(72, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(73, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(74, 106, '34.1458187811395', '-117.85808449622', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:32'),
(75, 106, '34.1457732674052', '-117.858040742686', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:41'),
(76, 106, '34.1457732674052', '-117.858040742686', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:41'),
(77, 106, '34.1457732674052', '-117.858040742686', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:41'),
(78, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(79, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(80, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(81, 106, '34.1457673581635', '-117.858036635553', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:49'),
(82, 106, '34.1457820684036', '-117.858028421288', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:56'),
(83, 106, '34.1457820684036', '-117.858028421288', '534 E Northridge Ave, Glendora, CA  91741, United States', 'United States', 'CA', 'Glendora', '2017-09-19 14:17:56'),
(84, 107, '24.8736884526065', '67.0525412479579', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-19 16:06:14'),
(85, 108, '33.4942094190358', '-86.799833960912', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-19 16:32:50'),
(86, 106, '33.7160208542214', '-117.884776499232', '2303 S Bristol St, Santa Ana, CA  92704, United States', 'United States', 'CA', 'Santa Ana', '2017-09-19 22:39:24'),
(87, 106, '33.6579067726002', '-117.866488695254', '1000 Bristol St N, Newport Beach, CA  92660, United States', 'United States', 'CA', 'Newport Beach', '2017-09-20 01:15:23'),
(88, 108, '33.4981452757025', '-86.8066825181263', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:35:03'),
(89, 108, '33.4981781239168', '-86.8066612743605', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:37:59'),
(90, 108, '33.4981805806449', '-86.8066572278539', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:38:48'),
(91, 108, '33.4981300345801', '-86.8066915960576', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:39:35'),
(92, 108, '33.4981290516732', '-86.8066920137677', 'University of Alabama at Birmingham, 917 13th St S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 06:39:53'),
(93, 107, '24.8801328102358', '67.0357256290196', 'Misquit Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-20 08:18:16'),
(94, 109, '24.8737007967698', '67.0525437194975', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-20 19:24:15'),
(95, 109, '24.873682538544', '67.052551236874', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-20 19:31:59'),
(96, 109, '24.8736768915467', '67.0525235949925', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-20 19:36:08'),
(97, 108, '33.4943289030655', '-86.7997590266976', '1612 15th Ave S, Birmingham, AL  35205, United States', 'United States', 'AL', 'Birmingham', '2017-09-20 20:20:15'),
(98, 110, '33.4356536865234', '-112.001998901367', 'Phoenix Sky Harbor International Airport, 3701 E Sky Harbor Blvd, Phoenix, AZ  85034, United States', 'United States', 'AZ', 'Phoenix', '2017-09-20 20:22:38'),
(99, 106, '34.104463006007', '-117.817408561843', '565 W Arrow Hwy, San Dimas, CA  91773, United States', 'United States', 'CA', 'San Dimas', '2017-09-20 21:06:41'),
(100, 111, '32.7338971663571', '-115.538029102716', '39 W Pheasant St, Heber, CA  92249, United States', 'United States', 'CA', 'Heber', '2017-09-20 21:49:47'),
(101, 112, '24.8737094115195', '67.0525788789098', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-21 08:32:24'),
(102, 112, '24.873664775987', '67.0525938127399', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-21 08:43:01'),
(103, 110, '34.1073011281286', '-117.284656372031', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 16:46:54'),
(104, 111, '34.1071999166478', '-117.284377087017', '400â€“512 N Lugo Ave, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 17:51:39'),
(105, 106, '37.5921703867566', '-122.370534377492', '1511 Rollins Rd, Burlingame, CA  94010, United States', 'United States', 'CA', 'Burlingame', '2017-09-21 17:59:59'),
(106, 111, '34.1072866274362', '-117.284518657361', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:25:05'),
(107, 111, '34.1073990706672', '-117.285229023655', '400â€“492 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:27:39'),
(108, 111, '34.1073395172452', '-117.28450750943', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:00'),
(109, 111, '34.1073161317353', '-117.284547155832', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:22'),
(110, 111, '34.1073161317353', '-117.284547155832', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:22'),
(111, 111, '34.1073161317353', '-117.284547155832', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 18:28:22'),
(112, 106, '37.5922472146012', '-122.371213423516', '1529 Rollins Rd, Burlingame, CA  94010, United States', 'United States', 'CA', 'Burlingame', '2017-09-21 18:49:44'),
(113, 106, '37.5920551593149', '-122.371069604044', '1519 Rollins Rd, Burlingame, CA  94010, United States', 'United States', 'CA', 'Burlingame', '2017-09-21 18:51:24'),
(114, 111, '34.1072854958792', '-117.28456635039', '433 N Sierra Way, San Bernardino, CA  92410, United States', 'United States', 'CA', 'San Bernardino', '2017-09-21 21:43:38'),
(115, 106, '37.7090315882722', '-122.212882257544', 'Oakland International Airport, Oakland International Airport, Oakland, CA  94621, United States', 'United States', 'CA', 'Oakland', '2017-09-22 04:27:31'),
(116, 111, '34.0297563700322', '-117.316078618278', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-22 16:35:58'),
(117, 112, '24.8736849574091', '67.0525720347591', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:13:31'),
(118, 112, '24.8736910072227', '67.0525721532776', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:15:44'),
(119, 112, '24.8736900442994', '67.0525488865447', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:17:21'),
(120, 112, '24.8736877711196', '67.0525306667498', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:18:51'),
(121, 112, '24.8736872192288', '67.0525315078494', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:21:37'),
(122, 112, '24.873687351245', '67.0525296503229', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:24:27'),
(123, 112, '24.8736785800953', '67.0525375360324', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:26:05'),
(124, 112, '24.873686193921', '67.052540156961', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:28:50'),
(125, 112, '24.8736939974192', '67.0525686532544', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:35:15'),
(126, 112, '24.873675989123', '67.0525341231072', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:38:17'),
(127, 112, '24.8736900615257', '67.0525726948752', 'Hamid Hussain Farooqi Road, Karachi, Pakistan', 'Pakistan', 'Sindh', 'Karachi', '2017-09-22 18:39:34'),
(128, 111, '34.0297445096392', '-117.315920451765', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 07:44:22'),
(129, 111, '34.0298731299434', '-117.316051880007', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 07:49:32'),
(130, 111, '34.0297615249027', '-117.316068308537', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 07:50:59'),
(131, 111, '34.0297246026192', '-117.315958757063', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 08:48:10'),
(132, 111, '34.0297361277361', '-117.316026063745', '22491 De Berry St, Grand Terrace, CA  92313, United States', 'United States', 'CA', 'Grand Terrace', '2017-09-23 08:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `webservices`
--

CREATE TABLE IF NOT EXISTS `webservices` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `webservices`
--

INSERT INTO `webservices` (`id`, `name`) VALUES
(1, 'news_category.php'),
(2, 'Sub Menu'),
(3, 'stay_connected.php'),
(4, 'default.php'),
(5, 'view.php'),
(6, 'default.php');

-- --------------------------------------------------------

--
-- Table structure for table `webservice_category`
--

CREATE TABLE IF NOT EXISTS `webservice_category` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `webservice` int(2) NOT NULL,
  `category` varchar(10) NOT NULL,
  `language` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=995 ;

--
-- Dumping data for table `webservice_category`
--

INSERT INTO `webservice_category` (`id`, `webservice`, `category`, `language`) VALUES
(568, 1, '314', 'spanish'),
(569, 5, '280', 'spanish'),
(570, 4, '281', 'spanish'),
(571, 5, '282', 'spanish'),
(572, 2, '315', 'spanish'),
(573, 3, '316', 'spanish'),
(574, 4, '317', 'spanish'),
(575, 5, '318', 'spanish'),
(969, 0, '565', 'english'),
(970, 0, '566', 'english'),
(971, 0, '567', 'english'),
(972, 0, '568', 'english'),
(973, 0, '430', 'english'),
(974, 0, '569', 'english'),
(975, 0, '570', 'english'),
(976, 0, '571', 'english'),
(977, 0, '572', 'english'),
(978, 0, '431', 'english'),
(979, 0, '573', 'english'),
(980, 0, '574', 'english'),
(981, 0, '575', 'english'),
(982, 0, '576', 'english'),
(983, 0, '432', 'english'),
(984, 0, '577', 'english'),
(985, 0, '578', 'english'),
(986, 0, '579', 'english'),
(987, 0, '433', 'english'),
(988, 0, '580', 'english'),
(989, 0, '581', 'english'),
(990, 0, '582', 'english'),
(991, 0, '583', 'english'),
(992, 0, '584', 'english'),
(993, 0, '434', 'english'),
(994, 0, '435', 'english');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
