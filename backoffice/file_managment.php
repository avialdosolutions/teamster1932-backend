<?php

//	session_start();

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

	include("headers/connect.php");
    include("header.php");

    $title = @$_GET['title'];
    $PageTitle = strtoupper(str_replace('-',' ',$title));    
    

    echo date('Y-m-d h:i');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>File Managment | '.$title.'</title>';
    ?>
<link href="/theme/file_upload/fine-uploader-new.css" rel="stylesheet">    
<style>
.highlight td{
    background-color: #426fd9 !important;
    color: white !important;
}    
    
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -29px;
    margin-bottom: 13px;
}    
.col-lg-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -42px;
    position: relative;
    right: -37px;
}    
/*
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    display: none;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
*/
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -101px;
    margin-left: 26px;
    margin-right: 26px; 
    margin-bottom: -13px;
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.prev {
    border: none !important;
}
li.prev a{
    color: #919191 !important;
}
li.active {
        border: none !important;
}
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next {
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}  
 .submit_button,.add_iconn,.revert{
        background: #21AF86;
        color: white;
        border: none;
        padding: 10px;
        border-radius: 21px;
        padding-left: 35px;
        padding-right: 35px;
        font-size: 10px;
        box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
        font-family: Montserrat;
        
    }
    .submit_button:hover,.add_iconn:hover,.revert:hover{
        color: white;
    }
.modal-header {
    background: #425BA9 !important;
    padding: 3px;
    border-radius: 0px;
    min-height: 0px;
} 
button.close {
    position: absolute;
    top: -23px;
    right: -27px;
    cursor: pointer;
    font-size: 18px;
    padding-top: 0px !important;
    width: 58px;
    height: 46px;
    line-height: 24px;
    opacity: 0.9;
    font-weight: bold;
    color: white;
    padding-left: 2px !important;
    margin-top: -1px !important;
    }  
    input[type=file] {
        border: none !important;
        padding-top: 6px !important;
        padding-left: 0px !important;        
    }
    input{
/*
    border-radius: 24px !important;
    padding-left: 24px !important;
    padding-top: 21px !important;
    padding-bottom: 20px !important;
*/
    }
select{
    border-radius: 0px !important;
    padding-left: 16px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;        
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 97% 46%;
    background-repeat: no-repeat;
    padding-right: 44px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }    
.bootstrap-tagsinput {
    width: 100%;
/*    height: 39px;*/
}    
.top_bar ul li {
    display:  inline-block;
    margin-right: 37px;
}    
.top_bar a{
        color: #3282e6;
}    
.top_bar>tbody>tr>td{
    color: black !important;
}    
.disabled {
    color: #999 !important;
    cursor: not-allowed;
}    
#trigger-upload {
    color: white;
    background-color: #00ABC7;
    font-size: 14px;
    padding: 7px 20px;
    background-image: none;
}

#fine-uploader-manual-trigger .qq-upload-button {
    margin-right: 15px;
}

#fine-uploader-manual-trigger .buttons {
    width: 36%;
}

#fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
    width: 60%;
} 

element.style {
}
.qq-uploader {
    margin-bottom: 12px;
}    
.progress {
  height: 2.1em;
    }
    .progress-bar{
            background-color: #70ac00;
    }    
.progress-bar:before {
  content: attr(data-label);
  font-size: 12px;
  position: absolute;
  text-align: center;
/*  top: 5px;*/
/*  left: 0;*/
  right: 0;
margin-right: 64px;
color: black;
font-weight: bold;   
        margin-top: 3px;
}  
button#trigger-cancel {
    color: white;
    background-color: #00ABC7;
    font-size: 14px;
    padding: 7px 20px;
    background-image: none;
    margin-left: 12px;
}    
</style>
<link rel="stylesheet" href="/theme/jBox/jBox.css">
<link rel="stylesheet" href="/theme/jBox/plugins/Notice/jBox.Notice.css">
<link rel="stylesheet" href="/theme/jBox/themes/NoticeFancy.css">
    
<link rel="stylesheet" type="text/css" href="/theme/css/bootstrap-tagsinput.css"     
<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
        <?php        
        
            echo  '<h4 style="margin-left: 20px;margin-bottom: 45px;">'.strtolower($PageTitle).'</h4>';
        
        ?>
      
    <section class="panel" style="margin-left: 233px;margin-right: 26px;">
        
        <div class="panel-body">
<?php       
			if(isset($_GET['key']) == 'success')
			{
//				echo"
//			<div class='alert alert-success'>
//					<button class='close' data-dismiss='alert'>×</button>
//					<strong>Success!</strong> Message send successfully.
//				</div>";
			}
	?>	  		  
       <!-- Modal of add icons -->
            <div class="modal fade" id="folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/theme/img/close_btn.svg" style="width:20px;" /></button>
                  </div>
                  <div class="modal-body">
                        <form method="post" id="form_id" class="form-group " action="#" enctype="multipart/form-data">                            
                        <div class="form-group">
                         <label class="">New Folder Name:</label>
                          <input class="form-control folder_name" name="" required value=""  type="text"/>                              
                          </div>
                        <div class="form-group">
                         <label class="">New Folder will be created in:</label>
                            <div class="input-group m-bot15">
                              <span class="input-group-btn">
                                <button type="button" class="btn btn-white" style="    height: 34px;"><i class="fa fa-home"></i></button>
                              </span>
                              <input type="text" value="/home/" class="form-control folder_path">
                          </div> 
                            </div>    
                        <div class="form-group">
                        <label class=""></label>

                          <button class="btn add_iconn create_folder"    size="16" type="button">Create New Folder</button></div>
                              
                          </form>                      
                  </div>
                </div>
              </div>
            </div>
            
            <div class="modal fade" id="UrlLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/theme/img/close_btn.svg" style="width:20px;" /></button>
                  </div>
                  <div class="modal-body">
                        <form method="post" id="form_id" class="form-group " action="#" enctype="multipart/form-data">                            
                        <div class="form-group">
                         <label class="">Embed codes:</label>
                          <select class="form-control link_type" >
                              <option value="1">Viewer Links</option>
                              <option value="2">HTML Linked</option>
                              <option value="3">HTML Img Linked</option>
                            </select>
                            <br />
                          <input type="text" class="form-control link" id="linkCopy" />    
                          </div>
                        <div class="form-group">
                        <label class=""></label>    
                          <button data-clipboard-target="#linkCopy" class="btn add_iconn copy_url"  size="16" type="button">Copy To Clipboard</button></div>
                              
                          </form>                      
                  </div>
                </div>
              </div>
            </div>            
   
          
            <div class="progress progress-xs" >
              <div class="progress-bar" data-label="0 MB / 100 MB" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  <span class="sr-only">0 MB / 100 MB</span>
              </div>
            </div>          
            
            <div class="adv-table editable-table ">

                          <div class="space15"></div>
             <div class="top_bar">
                 <ul>
                 <li><a href="javascript:void(0)" class="ca-home"><i class="fa fa-home"></i> Home</a></li>
                 <li><a href="javascript:void(0)" class="ca-back disabled"><i class="fa fa-arrow-left"></i> Back</a></li>     
                 <li><a href="javascript:void(0)" class="ca-refresh"><i class="fa fa-refresh"></i> Reload</a></li>     
                 <li><a href="javascript:void(0)" class="ca-select"><i class="fa fa-check-square"></i> Select All</a></li>     
                 <li><a href="javascript:avoid(0)" class="ca-unselect disabled"><i class="fa fa-square"></i> Unselect All</a> </li>   
                 <li><a href="javascipt:void(0)" class="ca-delete disabled"><i class="fa fa-trash-o"></i> Delete</a></li>     
                 <li><a href="javascript:void(0)" class="ca-folder"><i class="fa fa-plus"></i> Folder</a></li>
                 <li><a href="javasciprt:void(0)" class="ca-upload"><i class="fa fa-plus"></i>  Upload</a></li>
                 <li><a href="javasciprt:void(0)" class="ca-link disabled"><i class="fa fa-link"></i>  File Link</a></li>     
                 </ul>

             </div>

<div id="fine-uploader-manual-trigger"></div>                    
                               <div class="space15"></div>           
                <table  class="display table table-bordered table-striped" id="main">
                                <thead>
                                  <tr>
                				    <th></th>
									<th>Name</th>
                                    <th>Size</th>
                                    <th>Last Modified</th>  
                                    <th>Type</th>  
                                  </tr>
                              </thead>
			                 <tbody>
			                     
			                 </tbody>
			
			  
          </table>

                      </div>                               
      </div>
    </section>
   
    <!-- page end--> 
  </section>
</section>
    
<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>

    
    <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
    <script src="/theme/assets/toastr-master/toastr.js"></script>
    
      <!--script for this page only-->
      <script src="/theme/js/editable-table.js"></script>

      <script type="text/javascript" src="/theme/assets/data-tables/jquery.dataTables.js"></script> 
     <script type="text/javascript" src="/theme/assets/data-tables/DT_bootstrap.js"></script> 
<script type="text/javascript" src="/theme/js/bootstrap-tagsinput.js"></script>
   
<script src="/theme/jBox/jBox.js"></script>
<script src="/theme/jBox/plugins/Notice/jBox.Notice.js"></script>
    <script src="/theme/jBox/Demo.js"></script>

<script>
//    new jBox('Tooltip', {
//  attach: '#copy',
//  trigger: 'click',
//  autoClose: 5000,
//  position: {
//    x: 'right',
//    y: 'bottom'
//  },        
//  overlay: false,        
//  onClose: function() {
//    
//  }
//});
    
function notice(text){
new jBox('Notice', {
    content: text+' has been copied to your clipboard!',
    color: 'red',
    attributes: {
      x: 'right',
      y: 'bottom'
    },
  autoClose: 5000,
    overlay: false
 });

}
    
</script>

<!-- END JAVASCRIPTS -->

    <script src="/theme/file_upload/fine-uploader.js"></script>
    <script type="text/template" id="qq-template-manual-trigger">
        <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>Select files</div>
                </div>
                <button type="button" id="trigger-upload" class="btn btn-primary">
                    <i class="icon-upload icon-white"></i> Upload
                </button>
                <button type="button" id="trigger-cancel" class="btn btn-primary">
                    <i class="icon-upload icon-white"></i> Cancel
                </button>                
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>
    <script>
        var TotalSpace = 100000000; // In byte (100 MB)
        var MainSize = 0;
        $('.ca-upload').click(function(){
            $('#fine-uploader-manual-trigger').empty();
        var defaultParam = {
                id: 123
            }
        
        var manualUploader = new qq.FineUploader({
            element: document.getElementById('fine-uploader-manual-trigger'),
            template: 'qq-template-manual-trigger',
            request: {
                endpoint: 'https://linkedunion.com<?php echo $app_name ?>/backoffice/image_upload.php'
            },
            callbacks: {
            onAllComplete: function(id) {
//                    console.log(qq.status.UPLOAD_SUCCESSFUL)
                    if(qq.status.UPLOAD_SUCCESSFUL == 'upload successful'){
                        directories(current_path);   
                    }
                    else{
                        alert('Failed to upload this file please retry');
                    }
                }, 
    onValidate: function(data) {
        if (MainSize + data.size > TotalSpace) {
            alert('Server space not available. Please contact LinkedUnion at support@linkedunion.com for more space or delete some items and try again');
            return false;
        }
        MainSize += data.size;
    },                
        onSubmit: function (id, fileName) {
            var imageParams = {
                current_path: current_path
            },
                finalParams = defaultParam;
            
                qq.extend(finalParams, imageParams);
                this.setParams(finalParams);
            }
            },            
            thumbnails: {
                placeholders: {
                    waitingPath: '/theme/file_upload/placeholders/waiting-generic.png',
                    notAvailablePath: '/theme/file_upload/placeholders/not_available-generic.png'
                }
            },
            validation: {
                sizeLimit: 10000000 // 50 kB = 50 * 1024 bytes
            },            
            autoUpload: false,
            debug: true
            
        });            
        qq(document.getElementById("trigger-upload")).attach("click", function() {
            manualUploader.uploadStoredFiles();
        });
            
        });
        

    $('body').on('click','#trigger-cancel',function(){
        $('#fine-uploader-manual-trigger').empty();
    })
        
        
    </script>



      <script>
          var current_path = '/home/';
          var previous_path = '';
          var path_array = [];
          var longUrl = '';  
          var shortUrl = '';
          function directories(folder){
              $('#main tbody').html('<tr><td colspan="5"><i class="fa fa-spinner fa-spin" style="font-size:15px"></i> Fetching directory contents ...</td></tr>');
              $.ajax({
                  url: "<?php echo $app_name ?>/backoffice/directories.php?folder_path="+folder,
                  type: "get",
                  dataType: "json",
                  success: function(response){
                      $('#fine-uploader-manual-trigger').empty();
                      $('#main tbody').html('');       
                      
                      MainSize = response['MainSize'];
                      var MainSizeMb = response['MainSizeMb'];
                      if(MainSizeMb >= '80'){
                          $('.progress-bar').css('background','red');
                      }
                      else{
                          $('.progress-bar').css('background','#70ac00');                          
                      }
                      $('.progress-bar').attr('data-label',MainSizeMb + ' MB / 100 MB');
                      $('.progress-bar').css('width',MainSizeMb+'%');
                      if(response['count'] == 0){
                        $('#main tbody').html('<tr><td colspan="5">This directory is empty.</td>');                 
                      }
                    $('.ca-unselect').addClass('disabled');
                    $('.ca-link').addClass('disabled');
                    $('.ca-delete').addClass('disabled'); 
                    
                    $.each(response, function(key, value) {
                        var name = value['name'];
                        var folder_path = value['path'];
                        var timestamp = value['timestamp'];
                        var size = value['size'];
                        var type = value['type'];
                        var icon = value['icon'];                   
                        var fullUrl = value['fullUrl'];
                        if(timestamp != null){
                        $('#main tbody').append('<tr data-path="'+folder_path + name +'/" data-name="'+name+'" data-type="'+type+'" data-url="'+fullUrl+'">\
                            <td>'+icon+'</td>\
                            <td>'+name+'</td>\
                            <td>'+size+'</td>\
                            <td>'+timestamp+'</td>\
                            <td>'+type+'</td>\
                          </tr>')
                        }
                    })
                  }
              })
              
          }
          directories(current_path);
          
      </script>
<script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
    <script>
        $('.create_folder').click(function(){
            var folder_path = $('.folder_path').val();
            var folder_name = $('.folder_name').val();
            if(folder_name == ''){
                alert('Error: No name specified');                
            }
            else{
                $.ajax({
                    url: "<?php echo $app_name ?>/backoffice/create_folder.php",
                    type: "post",
                    data: {folder_path:folder_path,folder_name:folder_name},
                    success: function(response){
                        response = $.trim(response);
                        $('#folder').modal('hide');
                        if(response == '-1'){
                            alert('Error: Colud not create directory "'+folder_name+'" in '+folder_path+': Directory '+folder_path+' does not exits');
                        }
                        if(response == '-2'){
                            alert('Error: Colud not create directory "'+folder_name+'" in '+folder_path+': File exits');
                        }
                        if(response == '1'){
                            directories(current_path);
                        }
                        $('.folder_path').val('/home/');
                        $('.folder_name').val('');
                    }
                })
            }
        })                
      
$("body").on('click','#main tbody tr',function() {
    var selected = $(this).hasClass("highlight");
    var file_path = $(this).attr('data-type')
    $("#main tbody tr").removeClass("highlight");
    $('.ca-link').addClass('disabled');
    if(!selected){
        $(this).addClass("highlight");
        $('.ca-delete').removeClass('disabled');
        if(file_path != 'directory'){
        var url = $(this).attr('data-url');
        longUrl = url;     
        $('.ca-link').removeClass('disabled');
        }
    }
    else{
        $('.ca-delete').addClass('disabled');
        $('.ca-link').addClass('disabled');
    }
});        
$("body").on('dblclick','#main tbody tr',function() {
    var path = $(this).attr('data-path');
    var file_type = $(this).attr('data-type');
    console.log(file_type);
    if(file_type == 'directory'){
    directories(path);
    previous_path = current_path;
    path_array.push(current_path);
    console.log(path_array);
    current_path = path;
    $('.ca-back').removeClass('disabled');
    }
    else{
        // download will come here
    }
})        
$('.ca-folder').click(function(){
    $('.folder_path').val(current_path);
    $('#folder').modal('show');
});
$('.ca-home').click(function(){
    directories('/home/');
    current_path = '/home/';
    $('.ca-back').addClass('disabled');
});
$('.ca-refresh').click(function(){
    directories(current_path);        
})            
$('.ca-select').click(function(){
    $('#main tbody tr').addClass('highlight');
    $('.ca-unselect').removeClass('disabled');
    $('.ca-delete').removeClass('disabled');
    $('.ca-link').addClass('disabled');
})
$('.ca-unselect').click(function(){
    $('#main tbody tr').removeClass('highlight');
    $('.ca-unselect').addClass('disabled');    
    $('.ca-delete').addClass('disabled');
    $('.ca-link').addClass('disabled');
})

$('.ca-back').click(function(){
    var lastEl = path_array[path_array.length-1];
    if($(this).hasClass("disabled")){        
    }
    else{
        if(lastEl == '/home/'){
         $(this).addClass('disabled');
         current_path = '/home/';
         path_array = []
        }
        path_array = jQuery.grep(path_array, function(value) {
          return value != lastEl;
        }); 
        current_path = lastEl;
        directories(lastEl);
    }
});
$('.ca-delete').click(function(){
    
    var path_value = [];
    var path_name = [];
    $('#main tbody tr').each(function(){
        if($(this).hasClass("highlight")){
         var data_path = $(this).attr('data-path');
         var data_name = $(this).attr('data-name');    
         path_value.push(data_path);  
         path_name.push(""+data_name+"");    
        }
    });
    if(confirm('Are you sure you want to delete these files')){
        $.ajax({
            url: "<?php echo $app_name ?>/backoffice/delete_folder.php",
            type: "post",
            "data": {path_value:path_value,path_name:path_name},
            success: function(response){
                //console.log(response);
                $('.ca-delete').addClass('disabled');
                directories(current_path);
            }
            
        })
    }
});
        
$('.ca-link').click(function(){
    
    $.ajax({
            url: 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyBV82tqCcrrKT5c77x9NcZ0ISjNy__Ew5Q',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: '{ longUrl: "' + longUrl +'"}',
            success: function(response) {
                $('#UrlLink').modal('show');
                $('.link_type').val('1');
                $('.link').val(response.id);        
                shortUrl = response.id;
            }
         });        
})        

new ClipboardJS('.copy_url', {
    container: document.getElementById('modal')
});
    
var clipboard = new ClipboardJS('.copy_url');

clipboard.on('success', function(e) {
    e.clearSelection();
    notice(shortUrl);
});    


$('.link_type').change(function(){
    var value = $(this).val();
    var UrlText = longUrl;
    if(value == 1){
        $('.link').val(shortUrl);
    }
    if(value == 2){
        $('.link').val('<a href="'+shortUrl+'">'+shortUrl+'</a>');
    }
    if(value == 3){
        $('.link').val('<img src="'+shortUrl+'" />');
    }
})
  
    </script>
