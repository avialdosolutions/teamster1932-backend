<?php
include("headers/connect.php");
include ('dumper.php');
session_start();
$user_id = $_SESSION['user_id'];
$todayDate = gmdate('d-m-Y g:i:s');
$date = gmdate("Y-m-d G:i:s");  
$page_url = "https://linkedunion.com$app_name/settings";  

// exporting old db before reseting
try {
    $world_dumper = Shuttle_Dumper::create(array(
        'host' => $dbhost,
        'username' => $dbuser,
        'password' => $dbpass,
        'db_name' => $dbname,
        'include_tables' => array('app_icon','categories','contact','department','member_discount','member_discount_type','member_info','news','requestunion','stayconected','subcategories','webservice_category','webservices','worksite','wufoo_form'),
    ));

    // dump the database to plain text file
    $world_dumper->dump("db/$todayDate.sql");

    $queryDump = "INSERT INTO db_history (filename) VALUES ('$todayDate')";
    $st = $dbh->prepare($queryDump);
    $st->execute();
    
    $query_history = "insert into app_history (edit_type_name,last_edited,edited_by,type_id,category_id,page_url)
                      VALUES('Setting Revert DB','$date',$user_id,'0','0','$page_url')";
    $sth = $dbh->prepare($query_history);
    $sth->execute();

    
}
catch(Shuttle_Exception $e) {
     echo "Couldn't dump database: " . $e->getMessage();
}


$filename = $_POST['filename'];
$filename = "db/$filename.sql"; 
$op_data = '';
$lines = file($filename);
foreach ($lines as $line)
{
    if (substr($line, 0, 2) == '--' || $line == '')//This IF Remove Comment Inside SQL FILE
    {
        continue;
    }
    $op_data .= $line;
    if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY
    {
        $conn->query($op_data);
        $op_data = '';
    }
}

$columnArray = array();
$query = "SELECT `COLUMN_NAME` 
FROM `INFORMATION_SCHEMA`.`COLUMNS` 
WHERE `TABLE_SCHEMA`='myunionapp_development' 
    AND `TABLE_NAME`='categories';";    
$stm = $dbh->prepare($query);
$stm->execute();
while($rows = $stm->fetch(PDO::FETCH_ASSOC)){
    $columnArray[] = $rows['COLUMN_NAME'];
}
if (!in_array("order", $columnArray)) {
    $queryCategories = "ALTER table categories add `order` int";
    $st = $dbh->prepare($queryCategories);
    $st->execute();
    
    $querySubcategories = "ALTER table subcategories add `order` int";
    $stt = $dbh->prepare($querySubcategories);
    $stt->execute();
}

header("Location: $app_name/settings?key=success");
?>