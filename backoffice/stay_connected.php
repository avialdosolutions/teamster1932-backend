<?php
	 include("headers/connect.php");
    include '_user-details.php';
    include("header.php");
    $categoryID = $_GET['categoryID'];

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<!-- Mirrored from thevectorlab.net/adminlab/editable_table.html by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 04 Nov 2014 07:58:54 GMT -->
<head>
    
    <style>
   div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -29px;
    margin-bottom: 13px;
}    
.col-lg-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -42px;
    position: relative;
    right: -37px;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    display: none;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
a {
    color: #58595b !important;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -101px;
    margin-left: 26px;
    margin-right: 26px; 
    margin-bottom: -13px;
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.prev {
    border: none !important;
}
li.prev a{
    color: #919191 !important;
}
li.active {
        border: none !important;
}
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next {
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}        
    </style>
   <meta charset="utf-8" />
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Stay Connected | '.$title.'</title>';
    ?>
</head>
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<section id="main-content">
  <section class="wrapper"> 
       <?php
        $querys = "SELECT name from `categories` WHERE `id` = '$categoryID'";
        $st = $dbh->prepare($querys);
        $st->execute();
        $row = $st->fetch(PDO::FETCH_ASSOC);
        $count = $st->rowCount();
        $category_name = $row['name'];
         if($count >= 1)
            {
                echo '<h4 style="margin-left: 20px;margin-bottom: 45px;"> '.$category_name.'  </h4>';        
            }
        else
            {
                $querys = "SELECT `name` FROM `subcategories` WHERE `submenu_id` ='$categoryID'";
                $st = $dbh->prepare($querys);
                $st->execute();
                $row = $st->fetch(PDO::FETCH_ASSOC);
                $sub_name = $row['name'];
                 echo '<h4 style="margin-left: 20px;margin-bottom: 45px;"> '.$sub_name.'  </h4>';        
            }
        
      ?>
      
      <!-- page start-->
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
      <div class="panel-body">
    
    <?php
			if(isset($_GET['insert']) == true)
			{
				echo"
			<div class='alert alert-success'>
					<button class='close' data-dismiss='alert'>×</button>
					<strong>Success!</strong> The Stay Conected has been added.
				</div>";
			}
	 	else if(isset($_GET['update']) == true){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The Stay Conected has been updated.
            </div>";
		}
		else if(isset($_GET['delete']) == true){
      echo"
	    <div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>×</button>
                <strong>Success!</strong> The Stay Conected has been Deleted.
            </div>";
		}
?>

                             
                    <div class="adv-table editable-table ">
                          <div style="float: right;margin-right: 250px;top: 19px;position: relative;" class="clearfix"> <a href="/teamster1932/add-connection/<?php echo $categoryID ?>"><button type="button" class="btn btn-primary"><i style="padding-right:5px;" class="fa fa-plus"></i> Add New  </button></a>
                          </div>
                          <div class="space15"></div>
                <table  class="display table table-bordered table-striped" id="editable-sample">
                        <thead>
                                <tr>
                                    <th style='width:8px;'>Order</th>
                                    <th>Name</th>
                                    <th>Link</th>   
                                    <th>Edited By</th>
                                    <th>Last Edited</th>
                                    <th>Edit</th> 
                                    <th>Delete</th> 
                                </tr>
                     
                        </thead>
                        <tbody>

				        <?php
						
						
						$query_stay = "SELECT s.*,u.user_name FROM stayconected s,user u WHERE u.user_id = s.last_edited_by and s.app_id = '$appID'";
						$result_stay = mysqli_query($conn,$query_stay);
						$count = 0;
						while($row = mysqli_fetch_array($result_stay))
						{
							$id = $row['id'];
							$order = $row['order'];
							$name = $row['name'];
                            $user_name = $row['user_name'];
                            $last_edited = $row['last_edited'];
                            $time = strtotime($last_edited .' UTC');    
                            $last_edited = date("Y-m-d h:i A", $time);						               
							$link = $row['link'];
							$count++;
						  echo" 
								<tr class=''>
									 <td style='text-align:center;'>{$order}</td>
									<td>{$name}</td>
									<td><a href='$link' target='_blank'>{$link}</a></td>
							        <td>$user_name</td> 
                                    <td>$last_edited</td> 
									<td style='width:4%; background: white !important; border-color:#eeeff1;text-align:center;'>
                                    <a class='btn_icon' id='edit' href='/teamster1932/edit-connection/$categoryID/$id' id='update_button'>
                                    <img style='width: 18px;' src='/theme/img/edit.svg'  />
									</a>
                                    </td>
                                    <td style='padding-bottom: 14px;width:3%; background: white !important; border-color:#eeeff1;text-align:center;'>
                                    <a class='deleted' id='$id' style='cursor:pointer'>
                                    <img style='width: 16px;' src='/theme/img/delete.svg'  />                                    
									</a>
			                         </td>
								</tr>";
						}
						?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
      </section></section></section>
    	      
    
      </body>

<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>

    
    <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
    <script src="/theme/assets/toastr-master/toastr.js"></script>
    
      <!--script for this page only-->
      <script src="/theme/js/editable-table.js"></script>

      <script type="text/javascript" src="/theme/assets/data-tables/jquery.dataTables.js"></script> 
     <script type="text/javascript" src="/theme/assets/data-tables/DT_bootstrap.js"></script> 
       
   
<!-- END JAVASCRIPTS -->
      <script>
       
   jQuery(document).ready(function() {
              EditableTable.init();
              $('button.btn.btn-primary').show();       
          });

      function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".deleted", function (event) {
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                event.preventDefault();
                alert('Demo account cannot make any changes.');
            }
            else{
              
            if (confirm("Are you sure you want to delete this link?")) {
                  var ID = this.id;
                    var $this = $(this);
              
            $.get("<?php echo $app_name ?>/backoffice/delete.php?stay_delete="+ID, function (data, status) {
                        
                            var obj = jQuery.parseJSON(data);
                                if (obj.response == 1) {

                                    var Row = $this.closest('tr');
                                    var nRow = Row[0];
                                    $('#editable-sample').dataTable().fnDeleteRow(nRow)
                                    showmessage('success','News Delete Successfully','successfully');
                                } 
                          });  
                }
            }
            });
		  
		  
      </script>
    

</html>
