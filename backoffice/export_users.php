<?php 
$dbh = new PDO("mysql:host=localhost;dbname=teamsters1932", "linkedunion","Hemani786!");
$todayDate = gmdate('d-m-Y g:i:s');
$column = implode(',',$_POST['column']);
$sql = "SELECT $column from member_info m, app_user a where a.id = m.app_user_id";
$statement = $dbh->prepare($sql);
$statement->execute();
$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
$columnNames = array();
if(!empty($rows)){
    $firstRow = $rows[0];
    foreach($firstRow as $colName => $val){
        if($colName == 'activate'){
            $colName = 'status';
        }
        $columnNames[] = $colName;
    }
}
$fileName = "members($todayDate).csv";
header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="' . $fileName . '"');
$fp = fopen('php://output', 'w');
fputcsv($fp, $columnNames,',');
foreach ($rows as $row) {
    if($row['activate'] == '1'){
       $row['activate'] = 'registered'; 
    }
    else{
        $row['activate'] = 'unregistered';
    }
    fputcsv($fp, $row);
}
fclose($fp);
?>