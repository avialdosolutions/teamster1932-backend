<?php 

    include("headers/connect.php");
    include '_user-details.php';
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
	if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
    $returnArray = array();
    $user_id = $_GET['user_id'];
    $query = "SELECT * FROM users_location where UserID = '$user_id' order by timestamp desc";
    $sth = $dbh->prepare($query);
    $sth->execute();
    while($row = $sth->fetch(PDO::FETCH_ASSOC)){
        
        $time = strtotime($row['timestamp'] .' UTC');
        $timestamp = date('m/d/Y h:i A',$time);        
        $row['timestamp'] = time_elapsed_string($timestamp);        
        $returnArray[] = $row;
    }

    echo json_encode($returnArray);

?>