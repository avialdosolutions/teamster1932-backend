    <?php

    include 'headers/connect.php';
    include '_user-details.php';
    include("header.php");
if($languageBoolean == "false"){
    $languageVisibility = "style='display:none'";
}
if($_POST)
{
    $app_logo_image = $_POST['logo_image'];
	$status_bar_background = $_POST['status_bar_background'];
	$status_bar_text_color =$_POST['status_bar_text_color'];
    $logo_background = $_POST['logo_background'];
	$logo_button_color = $_POST['logo_button_color'];
	$register_color = $_POST['register_color'];
	$skip_button = $_POST['skip_button'];
	$primary_text_color = $_POST['primary_text_color'];
	$switch_color = $_POST['switch_color'];
	$lang_color = $_POST['lang_color'];
	$info_button_color = $_POST['info_button_color'];
	$logo_boder = $_POST['logo_boder'];
	$nav_bar_background = $_POST['nav_bar_background'];
	$nav_bar_text = $_POST['nav_bar_text'];
	$nav_arrow = $_POST['nav_arrow'];
	$menu_item_text_color = $_POST['menu_item_text_color'];
	$menu_navigation_color = $_POST['menu_navigation_color'];
	$menu_bar_color = $_POST['menu_bar_color'];
	$language_button_enabled = $_POST['language_button_enabled'];
    $menu_item_selected_color = $_POST['menu_item_selected_color'];
	$local_color = $_POST['local_color'];
	$state_color = $_POST['state_color'];
    $nat_color = $_POST['nat_color'];

	$login_button_text = $_POST['login_button_text'];

	$new_version = $_POST['new_version'];    
   
    
    $navigation_bar_color = $_POST['navigation_bar_color'];
     $status_bar_background = $_POST['status_bar_background'];
    
	  $switch_thum_color = $_POST['switch_thum_color'];
     $switch_thin_color = $_POST['switch_thin_color'];    
	$secondary_logo_hidden = $_POST['secondary_logo_hidden'];
    $primary_logo_hidden = $_POST['primary_logo_hidden'];
	
	
	for($i=0; $i<count($_FILES['walkthrough']['name']); $i++)
				{
					$file_extn = strtolower(end(explode('.', $_FILES['walkthrough']['name'][$i])));
					if($file_extn != null){
					$file_name = substr(md5(time()), 0, 10) . '.' . $file_extn;					
					move_uploaded_file($_FILES['walkthrough']['tmp_name'][$i], ''.$rootFolder.''.$app_name.'/backoffice/img/walkthrough/'.$file_name);

					$query = "INSERT into get_started (name) VALUES ('$file_name')";
					$sth = $dbh->prepare($query);
					$sth->execute();
					}
				}
        
	
   if(!empty($_FILES['logo_image']['name']))
        {
				$type=$_FILES['logo_image']['type'];
				if($type=='image/jpeg' || $type=='image/png' || $type=='image/gif'|| $type=='image/pjpeg')     {
					$primary_logo= mt_rand(100000, 999999).'.png';	
					$up = move_uploaded_file($_FILES['logo_image']['tmp_name'],''.$rootFolder.''.$app_name.'/backoffice/img/logo/'.$primary_logo);
					$Images = $primary_logo;		
				    }	 	 	
			    
            $image = ",`app_logo` ='$Images'";
        }   
        else
        {
            $image = '';
            $primary_logo = $primary_logo_hidden;    
            
        }

    if(!empty($_FILES['sec_logo_image']['name']))
        {
                $file_extn = strtolower(end(explode('.', $_FILES['sec_logo_image']['name'])));
				$type=$_FILES['sec_logo_image']['type'];
				if($type=='image/jpeg' || $type=='image/png' || $type=='image/gif'|| $type=='image/pjpeg')     {
					$secondary_logo= mt_rand(100000, 999999).'.'.$file_extn;	
					$up = move_uploaded_file($_FILES['sec_logo_image']['tmp_name'],  ''.$rootFolder.''.$app_name.'/backoffice/img/logo/'.$secondary_logo);
						$SecondImages = $secondary_logo;		
                    }	 	 	
			    
            $Secondimage = ",`second_logo_image` ='$SecondImages'";
        }   
        else
        {
            $Secondimage = '';
            $secondary_logo = $secondary_logo_hidden;    
        }	
	
        $date = gmdate("Y-m-d G:i:s");  	
		$update_version = "UPDATE `app` SET  `version` =  `version` + .1  WHERE `app_id` ='$appID'";
        $st_ver = $dbh->prepare($update_version);
            $st_ver->execute();     
    	
		$query = "UPDATE `app_appearance` SET  `register_button_color`='$register_color',`language_button_color`='$lang_color',`local_button_color`='$local_color',`state_button_color`='$state_color',`national_button_color`='$nat_color',`logo_boder_color`='$logo_boder', `menu_item_text_color` = '$menu_item_text_color',`menu_navigation_color` = '$menu_navigation_color',`info_button_color` = '$info_button_color',`login_button_text` = '$login_button_text',`language_button_enabled` = '$language_button_enabled',`status_bar_background` = '$status_bar_background',`status_bar_text_color`='$status_bar_text_color',`logo_background`='$logo_background',`logo_button_color`='$logo_button_color ',`skip_button`='$skip_button',`primary_text_color`='$primary_text_color',`nav_bar_background`='$nav_bar_background',`nav_bar_text`='$nav_bar_text',`nav_arrow`='$nav_arrow',`menu_bar_color`='$menu_bar_color' ,`menu_item_selected_color` = '$menu_item_selected_color',`switch_thum_color`='$switch_thum_color' ,`switch_thin_color`='$switch_thin_color',last_edited = '$date',last_edited_by='$user_id' $image $Secondimage";
	
		
        $stm = $dbh->prepare($query);
        $stm->execute();
        
    
        $insert_query = "INSERT INTO `app_appearance_history`(`app_logo`,  `register_button_color`, `language_button_color`, `local_button_color`, `state_button_color`, `national_button_color`, `logo_boder_color`, `status_bar_background`, `menu_item_text_color`, `menu_navigation_color`, `menu_item_selected_color`, `info_button_color`, `login_button_text`, `language_button_enabled`, `status_bar_text_color`, `logo_background`, `logo_button_color`, `skip_button`, `primary_text_color`, `nav_bar_background`, `nav_bar_text`, `nav_arrow`, `menu_bar_color`, `second_logo_image`, `switch_thum_color`, `switch_thin_color`, `time_stamp`) VALUES ('$primary_logo','$register_color','$lang_color','$local_color','$state_color','$nat_color','$logo_boder','$status_bar_background','$menu_item_text_color','$menu_navigation_color','$menu_item_selected_color','$info_button_color','$login_button_text','$language_button_enabled','$status_bar_text_color','$logo_background','$logo_button_color','$skip_button','$primary_text_color','$nav_bar_background','$nav_bar_text','$nav_arrow','$menu_bar_color','$secondary_logo','$switch_thum_color','$switch_thin_color',now())";
	
		
        $stmt = $dbh->prepare($insert_query);
        $stmt->execute();
        
        $page_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";  
        $query_history = "insert into app_history (edit_type_name,last_edited,edited_by,type_id,category_id,page_url)
                          VALUES('App Appearance','$date',$user_id,'0','0','$page_url')";
        $sth = $dbh->prepare($query_history);
        $sth->execute();
    
    
         echo "<script>
    			window.location.href = 'app-appearance?key=success';
    		   </script>";
        	
}

        else
        {
                    $fetch_query ="SELECT * from `app_appearance` ";
					$sth = $dbh->prepare($fetch_query);
					$sth->execute();
					   
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
							$app_logo = $row['app_logo'];
                            $app_icon = $row['app_icon'];
                            $splash_screen = $row['splash_screen'];
                            $register_button_color = $row['register_button_color'];
                            $language_button_color = $row['language_button_color'];
                            $local_button_color = $row['local_button_color'];
                            $state_button_color = $row['state_button_color'];
                            $national_button_color = $row['national_button_color'];
                            $logo_boder_color = $row['logo_boder_color'];

                            $menu_item_text_color = $row['menu_item_text_color'];
                            $menu_navigation_color = $row['menu_navigation_color'];
                            $info_button_color = $row['info_button_color'];
                            $login_button_text = $row['login_button_text'];
                            $status_bar_background = $row['status_bar_background'];
                            $language_button_enabled = $row['language_button_enabled'];	
                            $menu_item_selected_color = $row['menu_item_selected_color'];
							$status_bar_text_color= $row['status_bar_text_color'];
                            $logo_background = $row['logo_background'];
                            $skip_button = $row['skip_button'];
                            $primary_text_color = $row['primary_text_color'];
                            $nav_bar_background = $row['nav_bar_background'];
                            $nav_bar_text = $row['nav_bar_text'];
                            $nav_arrow = $row['nav_arrow'];
							$menu_bar_color = $row['menu_bar_color'];
							$logo_button_color =$row['logo_button_color'];
							$second_logo_image = $row['second_logo_image'];
						
							$switch_thum_color =$row['switch_thum_color'];
							$switch_thin_color = $row['switch_thin_color'];
						
						
						
                            if($language_button_enabled == 'true') {
                                $languageStyle = '';
                                $checked = 'checked';
                            }
                            else{
                                $checked = '';                                
                                $languageStyle = 'display:none;';
                            }   
                    }
    
}


?>

<!doctype html>
<html>
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>App Appearance | '.$title.'</title>';
    ?>
	<link rel="stylesheet" href="/theme/app_css/web_view.css" />
	<style>
		.image-preview
		{
			display: inline-block;
			    margin-right: 18px;
			    text-align: center;
		}
.main-navigation ul li, .sub-navigation li {
        padding: 7px 0px 3px 0px;
        font-size: 9px !important;
        font-weight: 600;
        font-family: Myrid-Regular;
        text-transform: uppercase;
        list-style: none;
        margin-top: 1px;
        margin-bottom: 16px;
        background: white;
        border-radius: 4px;
        box-shadow: -1px 2px 2px #888;
}
.arrow {
        float: right;
        margin-right: 15px;
        margin-top: 3px;
        font-size: 16px;
        font-weight: bolder;
        color: <?php echo $menu_navigation_color ?>;
}		
.status_bar_background{
    background: <?php echo $status_bar_background ?>
}        
    .primary
{
    font-size: 12px;
        }
        .menu_item_text_color{
            color: <?php echo $menu_item_text_color; ?>
        }        
span.btn.btn-white.btn-file {
    background: #425BA9;
    color: white;
    border-radius: 21px;
    font-size: 10px;
    padding-left: 16px;
    padding-right: 16px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    font-family: Montserrat !importnat;
    
}
a.btn.btn-danger.fileupload-exists {
    border-radius: 21px;
    font-size: 10px;
    background: #E11F26;
}      
.submit_button {
    background: #21AF86;
    float: right;
    color: white;
    border: none;
    padding: 10px;
    border-radius: 21px;
    padding-left: 35px;
    padding-right: 35px;
    font-size: 10px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    font-family: Montserrat !importnat;
}        
.main_center {
    width: 289px !important;
}        
input {
    border-radius: 24px !important;
    padding-left: 40px !important;
    padding-top: 21px !important;
    padding-bottom: 20px !important;
}

element.style {
}
.form-control {
    border: 1px solid #e2e2e4;
    box-shadow: none;
    color: #c2c2c2;
}
.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #b3b4b5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
        label.control-label.col-md-5{
            border: none !important;
        }
.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #b3b4b5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
select {
    border-radius: 24px !important;
    padding-left: 16px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer; 
    }       
label.control-label.col-md-5 {
    color: #a5a5a5;
    font-weight: 100;
}        
.col-md-2 {
    margin-left: -16px;
}
i.fa.fa-question-circle.info {
    font-size: 26px;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 11123123123; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
/*
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 46%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}
*/

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
        opacity: 1;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: white;
    color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}
#myBtn{
    cursor: pointer;
    width: 30px;
}
        
.add_icon{
    border: none;
    background: #888787;
    color: white;
    font-size: 10px;
    padding: 7px;
    border-radius: 31px;
    height: 33px;
    padding-left: 20px;
    padding-right: 20px;
    margin-right: 11px;
/*    margin-top: 8px;*/
    float: right;

} 
 .add_icon:hover{
        color: white;
    }        
	img#uploadPreview1 {
    width: 100px;
    height: 100px;
    }
button#btnGet {
    background: #21AF86;
    border-radius: 21px;
    font-size: 13px;
    padding-left: 26px;
    padding-right: 26px;
    padding-top: 11px;
    padding-bottom: 11px;
        box-shadow: 5px 3px 12px rgba(207,206,206,0.7) !important;
} 
.yes{
    background: #21AF86;
    border-radius: 21px;
    font-size: 13px;
    padding-left: 26px;
    padding-right: 26px;
    padding-top: 11px;
    padding-bottom: 11px;
    box-shadow: 5px 3px 12px rgba(207,206,206,0.7) !important;
    border: none;
    color: white;
    margin-right: 10px;
    margin-bottom: 20px;
}
.modal-header {
    background: #425BA9 !important;
    padding: 3px;
    border-radius: 0px;
    min-height: 0px;
}    
       
.no{
    background: #E11F26;
    border-radius: 21px;
    font-size: 13px;
    padding-left: 26px;
    padding-right: 26px;
    padding-top: 11px;
    padding-bottom: 11px;
    box-shadow: 5px 3px 12px rgba(207,206,206,0.7) !important;
    border: none;
    color: white;
    margin-bottom: 20px;
}               
span.last_edited {
    float: right;
}
img.user_image {
    width: 45px;
    height: 47px;
    border-radius: 40px;
}
span.user_name {
    top: -2px;
    position: relative;
    text-transform: capitalize;
    left: 10px;
}
span.edit_date {
    left: 56px;
    top: -17px;
    font-size: 10px;
    margin-left: 10px;
}
span.last_edited {
    float: right;
    margin-top: 5px;
    color: #21AF86;
}        
#profilePic
    {
    width: 45px;
    height: 47px;
    border-radius: 50%;
    background: #2a3542;
    font-size: 21px;
    color: #fff;
    text-align: center;
    line-height: 45px;
    float: left;
    margin-top: -7px;
    margin-bottom: 6px;
    text-transform: capitalize;
}
.image_box{
    float: left;
    margin-top: -7px;
    margin-bottom: 6px;
}        
button.close {
    position: absolute;
    top: -23px;
    right: -27px;
    cursor: pointer;
    font-size: 18px;
    padding-top: 0px !important;
    width: 58px;
    height: 46px;
    line-height: 24px;
    opacity: 0.9;
    font-weight: bold;
    color: white;
    padding-left: 2px !important;
    margin-top: -1px !important;
    }   
        
	</style>
</head>

<body>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
<section id="main-content">
  <section class="wrapper"> 
      

<div id="myModal" class="modal">
              <div class="modal-dialog">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/theme/img/close_btn.svg" style="width:20px;" /></button>        
    </div>
    <div class="modal-body">
        <img src="/theme/img/hints/1-Primary Logo.png" class="hint_image" />
    </div>
  </div>
    </div>
</div>

    <div class="modal fade" id="publish_dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body">
          <center>
              <h4 style="line-height: 27px;margin-bottom: 31px;font-size: 15px;">App Appearance will be reset to default theme settings. Do you wish to continue?</h4>
              <button type="button" class="yes">Yes</button>
              <button type="button" class="no">No</button>
          </center>
      </div>
    </div>
  </div>
</div>  
      
            <!-- BEGIN PAGE HEADER-->   
                    <div class="row">
	  				 <div class="col-lg-8">
                         
                         
                <form method="post" action="/teamster1932/reset-appearance-default" id="resend_default">
                                 
                    </form>
                         
                    <form action="/teamster1932/app-appearance" style="" method="post" class="form-horizontal" enctype="multipart/form-data"> 
                    
                    <button class="submit_button" type="submit">Save</button>
                    <button class="btn btn-xs add_icon finish" type="button" style="margin-left:15px;">Reset To Default</button>
                    <h4 style="margin-left: 19px;margin-bottom: 40px;">App Appearance</h4>	  <div class="alert alert-success message-wait" style="margin-left: 38px;background: #21AF86;color: white;display:none"> 
              <strong>Updating!</strong> Please Wait...
                </div>						 
                    <section class="panel" style="margin-left: 20px;">
                      <div class="panel-body" id="" style="">
                        <div style="margin-left: 44px;margin-top: 43px;"> 
                          
                             <div class="form-group">                            
                            <label class="control-label col-md-5">Primary Logo</label>
                            <div class="col-md-4">
                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-new thumbnail" style="text-align:right;" >
                                      <img style="max-width: 200px; max-height: 150px;" src="<?php if($app_logo == null) {echo "https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";}
                                        else{echo "https://linkedunion.com$app_name/backoffice/img/logo/$app_logo" ;}?>" alt="" />         
                                  <span style='top: 0px;margin-left: 19px;position: absolute;background-image: url("/theme/img/question_mark.svg");background-repeat: no-repeat;background-size: 34px 46px;background-position: 0px -5px;width: 50px;height: 50px;' class="info" data="1-Primary Logo.png" id="myBtn" ></span>                                      
                                  </div>                     
                                                               
                                  <div class="fileupload-preview primary_thumbnail fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                  <div>
                                   <span class="btn btn-white btn-file">
                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                   <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                   <input type="file" name="logo_image" class="default main_logo" />
                                    <input type="hidden" value="<?php echo $app_logo ?>" name="primary_logo_hidden">   
                                   </span>
                                      <a href="#" class="btn btn-danger fileupload-exists fileupload-exists_primary" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                  </div>
                              </div>                                
                                    </div>
                                 

                                </div>
							<div class="form-group">                            
                            <label class="control-label col-md-5">Secondary Logo</label>
                            <div class="col-md-3">
                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-new thumbnail" style="text-align:right;">
                                      <img style="max-width: 200px; max-height: 150px;"  src="<?php if($second_logo_image == null || !file_exists($_SERVER['DOCUMENT_ROOT']."$app_name/backoffice/img/logo/$second_logo_image") ) {echo "https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";}
                                        else{echo "$app_name/backoffice/img/logo/{$second_logo_image}" ;}?>" alt="" /> 
                                  <span style='top: 0px;margin-left: 19px;position: absolute;background-image: url("/theme/img/question_mark.svg");background-repeat: no-repeat;background-size: 34px 46px;background-position: 0px -5px;width: 50px;height: 50px;' class="info" data="2-Secondary Logo.png" id="myBtn" ></span>                                                                       
                                  </div>
                                  <div class="fileupload-preview fileupload-exists thumbnail secondary_thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                  <div>
                                   <span class="btn btn-white btn-file">
                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                   <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                   <input type="file" name="sec_logo_image" class="secondary_logo"  />
                                   </span>
                                    <input type="hidden" value="<?php echo $second_logo_image ?>" name="secondary_logo_hidden">    
                                      <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                  </div>
                              </div>                                
                                    </div>
                                </div> 
						 <div style="display:none;" class="form-group">
                              <label class="control-label col-md-5">WalkThrough</label>
                              <div class="col-md-3">
                                 <input type="file" class="form-control" multiple name="walkthrough[]">
                             	
									<?php 
										$queryImage = "SELECT * FROM get_started";
										$sth = $dbh->prepare($queryImage);
										$sth->execute();
										while($row = $sth->fetch(PDO::FETCH_ASSOC))
										 {
											 $id = $row['id'];
											 $name = $row['name'];
											 echo "<div class='image-preview'>
											 <div style='width: 100px;margin-bottom: 5px;margin-top: 9px;' class='thumbnail'>
												<img src='img/walkthrough/$name'  /> 
											</div>
											<button id='$id' class='btn btn-danger remove' type='button'>Remove</button></div>  ";
									     }
									?>

								
							 </div>
                             </div>
                            
                           
<!--
                             <div class="form-group">
                            <label class="control-label col-md-4">App Icon</label>
                            <div class="col-md-8">
                          <div class="fileupload fileupload-new" data-provides="fileupload">
                              <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                  <img src="<?php if($app_icon == null) {echo "https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";}
                                        else{echo "img/logo/$app_icon" ;}?>" alt="" />
                              </div>
                              <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                              <div>
                               <span class="btn btn-white btn-file">
                               <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                               <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                               <input type="file" name="app_icon" class="default" />
                               </span>
                                  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                              </div>
                          </div>                                
                            </div>
                        </div> 
-->
                            
<!--
                             <div class="form-group">
                            <label class="control-label col-md-4">Splash Screen</label>
                            <div class="col-md-8">
                                
                          <div class="fileupload fileupload-new" data-provides="fileupload">
                              <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                  <img src="<?php if($splash_screen == null) {echo "https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";}
                                        else{echo "/$app_name/backoffice/img/logo/$splash_screen" ;}?>" alt="" />
                              </div>
                              <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                              <div>
                               <span class="btn btn-white btn-file">
                               <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                               <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                               <input type="file" name="splash_image" class="default" />
                               </span>
                                  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                              </div>
                          </div>
                                    </div>
                                </div> 
-->	
							
							  <div class="form-group">
                              <label class="control-label col-md-5">Status Bar Background Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$status_bar_background; ?>" class="colorpicker-default form-control" id="title" name="status_bar_background" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="3-Status Bar Background Color.png" class="info" id="myBtn" /> 
                                </div>
                                  
                             </div> 
							
							
							 <div class="form-group">
                              <label class="control-label col-md-5">Status Bar Text Color</label>
                              <div class="col-md-3">
                                 <select style="padding-left: 41px !important;" class="form-control" id="status_text_color" name="status_bar_text_color" required>
									 <option value="black">Black</option>
									 <option value="white">White</option>
								  </select>
								  </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="4-Status Bar Text Color.png" class="info" id="myBtn" />
                                </div>
                                 
                             </div>
								<?php 
								
									echo "<script>
										$('#status_text_color').val('$status_bar_text_color');
									</script>";
							?>
							
							<div class="form-group">
                              <label class="control-label col-md-5">Logo/Top View Background Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$logo_background; ?>" class="colorpicker-default form-control" id="title" name="logo_background" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="5-LogoTop View Background Color.png" class="info" id="myBtn" />
                                </div>
                                
                             </div> 
							
							
							
							 <div class="form-group">
                              <label class="control-label col-md-5">Login Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$logo_button_color; ?>" class="colorpicker-default form-control" id="title" name="logo_button_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="6-Login Button Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
							
							 <div class="form-group">
                              <label class="control-label col-md-5">Register Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" required class="colorpicker-default form-control " value="<?php echo @$register_button_color; ?>" name="register_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="7-Register Button Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
							<div class="form-group">
                              <label class="control-label col-md-5">Skip Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" required class="colorpicker-default form-control " value="<?php echo @$skip_button; ?>" name="skip_button" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="8-Skip Button Color.png" class="info" id="myBtn" />
                                </div>                                
                             </div>
							
							<div class="form-group">
                              <label class="control-label col-md-5">Primary Text Color</label>
                              <div class="col-md-3">
                                 <input type="text" required class="colorpicker-default form-control " value="<?php echo @$primary_text_color; ?>" name="primary_text_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="9-Primary-Text-Color.png" class="info" id="myBtn" />
                                </div>                                
                             </div>
							
							<div class="form-group" style="display:none;">
                              <label class="control-label col-md-5">Switch Thumb Color</label>
                              <div class="col-md-3">
                                 <input type="text" required class="colorpicker-default form-control " value="<?php echo @$switch_thum_color; ?>" name="switch_thum_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="" class="info" id="myBtn" />
                                </div>                                
                             </div>
							
							<div class="form-group" style="display:none;">
                              <label class="control-label col-md-5">Switch Tint Color</label>
                              <div class="col-md-3">
                                 <input type="text" required class="colorpicker-default form-control " value="<?php echo @$switch_thin_color; ?>" name="switch_thin_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="" class="info" id="myBtn" />
                                </div>                                
                             </div>
							
                             <div class="form-group" <?php echo $languageVisibility ?>>
                              <label class="control-label col-md-5">Language Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$language_button_color; ?>" class="colorpicker-default form-control" id="title" name="lang_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="12-Language Button Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
							<div class="form-group">
                              <label class="control-label col-md-5">Info Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$info_button_color; ?>" class="colorpicker-default form-control" id="title" name="info_button_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="13-Info Button Color.png" class="info" id="myBtn" />
                                </div>                                
                             </div>
							
							 <div style="disply:none;" class="form-group">
                              <label class="control-label col-md-5">Logo Border Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$logo_boder_color; ?>" class="colorpicker-default form-control" id="title" name="logo_boder" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="14-Logo Border Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
							 <div class="form-group">
                              <label class="control-label col-md-5">Navigation Bar Background Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$nav_bar_background; ?>" class="colorpicker-default form-control" id="title" name="nav_bar_background" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="15-Navigation Bar Background Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
							<div class="form-group">
                              <label class="control-label col-md-5">Navigation Bar Text Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$nav_bar_text; ?>" class="colorpicker-default form-control" id="title" name="nav_bar_text" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="16-Navigation Bar Text Color.png" class="info" id="myBtn" />
                                </div>                                
                             </div>
                            
							<div class="form-group">
                              <label class="control-label col-md-5">Navigation Bar Arrow Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$nav_arrow; ?>" class="colorpicker-default form-control"  id="title" name="nav_arrow" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="17-Navigation Bar Arrow Color.png" class="info" id="myBtn" />
                                </div>                                
                             </div>
							
							 <div class="form-group">
                              <label class="control-label col-md-5">Menu Text Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$menu_item_text_color; ?>" class="colorpicker-default form-control" id="title" 
                                        name="menu_item_text_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="18-Menu Text Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
                             <div class="form-group">
                              <label class="control-label col-md-5">Menu Arrow Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$menu_navigation_color; ?>" class="colorpicker-default form-control" id="title" name="menu_navigation_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="19-Menu Arrow Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
							 <div class="form-group">
                              <label class="control-label col-md-5">Menu Item Selected Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$menu_item_selected_color; ?>" class="colorpicker-default form-control" id="title" name="menu_item_selected_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="20-Menu Item Selected Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
							
                             <div class="form-group" style="display:none;">
                              <label class="control-label col-md-5">Menu Border Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$menu_bar_color; ?>" class="colorpicker-default form-control" id="title" name="menu_bar_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="" class="info" id="myBtn" />
                                </div>                                 
                             </div>
							 <div class="form-group">
                              <label class="control-label col-md-5">Local Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$local_button_color; ?>"  class="colorpicker-default form-control" id="title" name="local_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="22-Local Button Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
                             <div class="form-group">
                              <label class="control-label col-md-5">State Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$state_button_color; ?>" class="colorpicker-default form-control" id="title" name="state_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="23-State Button Color.png" class="info" id="myBtn" />
                                </div>                                 
                             </div>
                            
							  <div class="form-group">
                              <label class="control-label col-md-5">National Button Color</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$national_button_color; ?>" class="colorpicker-default form-control" data="24-National Button Color.png" id="title" name="nat_color" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" class="info" id="myBtn" />
                                </div>                                  
                             </div>
                            							
                            
                            
                                                        
                             <div style="display:none" class="form-group">
                              <label class="control-label col-md-5">Register Button Text</label>
                              <div class="col-md-3">
                                 <input type="text" value="<?php echo @$login_button_text; ?>" class="form-control" maxlength="15" id="title" style="text-transform: uppercase;" name="login_button_text" required>
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="25-Register Btn Text.png" class="info" id="myBtn" />
                                </div>                                 
             </div>                                                                                      
                        <div class="form-group" <?php echo $languageVisibility ?>>
                              <label class="control-label col-md-5">Language Button</label>
                              <div class="col-md-3" style="text-align:center">
                                  <input type="hidden" value="false" name="language_button_enabled" />
                                 <input type="checkbox" class="" <?php echo $checked ?> name="language_button_enabled" value="true">
                               </div>
                                <div class="col-md-2">
                                    <img src="/theme/img/question_mark.svg" data="26-Language-Checkbox.png" class="info" id="myBtn" />
                                </div>                                
                             </div> 
                                                          
                            
                            
                          </div>
                        </div></section>
                          </form>
						 </div>
				    <?php
        
                        $query = "SELECT u.user_name,u.image,a.last_edited from app_history a , user u where a.edit_type_name = 'App Appearance' and a.edited_by = u.user_id order by a.id desc limit 1";  
                        $sth = $dbh->prepare($query);
                        $sth->execute();
                        $count = $sth->rowCount();                                        
                        $rows = $sth->fetch(PDO::FETCH_ASSOC);
                        $user_name = $rows['user_name'];
                        $last_edited = $rows['last_edited'];
                        $time = strtotime($last_edited .' UTC');
                        $last_edited = date('d F Y h:i A',$time);              
                        $image = $rows['image'];
                        if($image == 'user.jpg'){
                            $image = '<div alt="" id="profilePic"></div>';
                        }                
                        else{
                            $image = '<div class="image_box"><img class="user_image" src="'.$app_name.'/backoffice/img/user/'.$image.'" /></div>';
                        }                
    
                    if($count >= 1){
                    ?>		
                        
	  				 <div class="col-lg-4">
                    <section class="panel" style="margin-top: -15px;">
				<div class="panel-body" style="padding-bottom: 4px;">
                    <span>
                    <span class="last_edited">Last Edited</span>    
				    <?php echo $image ?>
                    <span class="user_name"><?php echo $user_name; ?></span> <br />   
                    <span class="edit_date"><?php echo time_elapsed_string($last_edited); ?></span>    
                    </span>
				</div>	
                         </section>
                        <?php  } 
                         
                         else{
                             echo '<div style="margin-top:71px" class="col-lg-4">';
                         }
                         ?> 
                         
                        <section class="panel android-tab" style="position: relative;top: -3px;">
                      <div class="panel-body" id="" style="">
						  
<div class="tab-pane fade in  active" id="preview-android">
                            
    <h4 align="center">Live Preview</h4>                            

                                    
<div class="wrapper main_center">
<div class="main">  
        <div class="splash-screen" style="display: none;">
            <img src="img/front-image2.png">
            <div class="app-icon">
                <div class="app-icon-img">
                <img src="img/app-icon/app-icon.png">
                </div>                
                <span class="app-name">My App</span>
            </div>
        </div>
                 <div class="wrappers" style="">
                     <div class="top-bar status_bar_background">
                         <img src="/teamsters1932/backoffice/img/iphone6-portrait-bar-light.png">
                     </div>
                <div id="logo" style="margin: 0px;">                    
                <div class="back-button" style="background: rgb(166, 77, 121); display: none;">
                    <span class="back" display=".main-navigation" hide=".detail" sub="false"><i class="fa fa-angle-left top_arrow"></i></span>
                    <span class="top_text" data="ABOUT UNITED LATINOS">ABOUT UNITED LATINOS</span>
                </div>    
                <img style="width: 236px;border-radius: 0px;height: 73px;" src="<?php echo $app_name ?>/backoffice/img/logo/<?php echo $app_logo ?>">                   
                </div>
                    <div class="panel" style="margin-top: 14px;margin-left: 14px;margin-right: 14px;box-shadow: 0px 0px !important;border: none;">
                            <div class="main-navigation" style="display: block;">                            
                                <ul>
								<?php
									$language = $_SESSION['lang'];
									$query = "SELECT * from categories 	where language = '$language' ";
					 				$stm = $dbh->prepare($query);
					 				$stm->execute();
					 				while($row = $stm->fetch(PDO::FETCH_ASSOC))
									{
										$id = $row['id'];
										$name = $row['name'];
										$icon = $row['icon'];
										
                                    if (strlen($name) > 20)
                                    {
                                        $inline_li_css = "style='padding: 8px 0px 9px 0px;line-height: 16px;'";
                                        $inline_arrow_css = "style='margin-top: 2px;'";
                                        $inline_icon_css = "style='margin-top: 2px;'";
                                        $inline_text_css = "style='width: 9em;'";
                                        
                                    }
                                    else
                                    {
                                        $inline_li_css = "style='    line-height: 24px;'";
                                        $inline_arrow_css = "";
                                        $inline_icon_css = "";
                                        $inline_text_css = "";
                                    }					 
										
										
                                     echo " <li $inline_li_css class='session navigation_bar_color main_navigation_$id' id='navigation_$id' sub='false'>                                    
                                    <img $inline_icon_css class='icon navigation_icon_$id' src='/teamster1932/backoffice/img/navigation_icon/$icon' />                   
                                    <span $inline_text_css class='navigation_text menu_item_text_color'> $name </span>   
                                        <i $inline_arrow_css class='fa fa-angle-right arrow menu_navigation_color'></i>       
                                    </li> ";									}
								?>	
                                </ul>
                                <div class="button">
                                    <button style="text-transform: uppercase;background:<?php echo $register_button_color ?>" class="primary register_color"><?php echo $login_button_text ?></button>
									<button style="<?php echo $languageStyle ?>background:<?php echo $language_button_color ?>" class="primary lang_color">ENGLISH</button>
                                    <button style="padding-top: 11px;padding-bottom: 9px;background:<?php echo $info_button_color ?>" class="primary info_button_color"><i class="fa fa-info" id="myBtn"></i></button>                                    
<!--                                    <button class="default">EN ESPANOL</button>-->
                                </div>
                            </div>
                        
<!--                        sub navigation start here-->
                                
                        <span class="sub_navigation_box"></span>                    

    
<!--                        stay connected start here -->
                        
                        <span class="stay_connected_box"></span>
                        
<!--                    web view start here    -->
                            
                            <div class="detail" style="margin-top: -15px; display: none;">
                                <img alt="" src="" style="height:403px; width:730px" title="Latinos in Action 2013">    
                            <div class="description">    
                            <h2><strong>Mission Statement</strong></h2>
                            <p><strong>The United Latinos of the United Food and Commercial Workers Union (UFCW) is an organization of men and women who have joined together to promote the issues and pursue interests important to Latino workers.</strong></p>
                            <p><strong>“Working Toward Latino Empowerment and Building Latino Pride!” We have a simple but powerful purpose of empowering Latino men and women within the UFCW and within our communities. We are also building Latino pride which will help others better understand our cultural differences. Our diversity will help make our organization and our society stronger. The United Latinos will help teach others that through UFCW membership, there is a better way of life that includes wages, health benefits, pensions, job security and dignity.</strong></p>
                            </div>
                            </div>
                        
                        </div>
                </div>
		        
      </div>
	                                        
                                        </div>
                                    </div>						  
						  
                        </div></section>
						 </div>						
					<div>
					
					</div>	
	  </div>
  </section></section>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php  

	?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->

    
    
    <script src="/theme/js/jquery.js"></script>
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
$('body').on('click','.info',function(){
    var image = $(this).attr('data');
    if(image != ""){
    $('.hint_image').attr('src','/theme/img/hints/'+image);
    modal.style.display = "block";
    }
})

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
<script src="/theme/js/jscolor.js"></script>
    
<script>
    $(document).ready(function(){
  var intials = $('.user_name').text().charAt(0);
  var profileImage = $('#profilePic').text(intials);
});
    </script>
    
    
        <script>
        var boolean = false
        $('.form-horizontal').submit(function(e){
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                e.preventDefault();
                alert('Demo account cannot make any changes.');
            }
            else if(user_level == 2){
                e.preventDefault();
                alert("Sorry! you don't have permission to perform this action");                
            }
            else{
          
        if(boolean == false){
                $('.submit_button').attr('disabled','dsiabled');
                $('.message-wait').show();

                e.preventDefault();
            
            var data = {
              "app_id": "<?php echo $api_key;  ?>",
               "included_segments": ["All"],
              "data": {"version_number": "0"},
              "content_available": "1"            
            };
    
    $.ajax({
                type : 'POST',
                url : "https://onesignal.com/api/v1/notifications",
                headers : {
                    Authorization : '<?php echo $Authorization; ?>'
                },
                contentType : 'application/json',
                data : JSON.stringify(data),
                success : function(response) {
                    console.log(response);                 
                    boolean = true;
                    $('.message-wait').hide();
                    $('form').submit();
                },
                error : function(xhr, status, error) {
                   // console.log(xhr.error);                  
                }
            });
            }    
            
            }
        })
    </script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
  
    <!--this page plugins-->

  <script type="text/javascript" src="/theme/assets/fuelux/js/spinner.min.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="/theme/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="/theme/assets/jquery-multi-select/js/jquery.multi-select.js"></script>
  <script type="text/javascript" src="/theme/assets/jquery-multi-select/js/jquery.quicksearch.js"></script>


  <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
    <!--this page  script only-->
    <script src="/theme/js/advanced-form-components.js"></script>
	<script>
        
 $('body').on('change','#status_text_color' ,function(){
     var color = $(this).val();
     if(color == 'black'){
         $('.status_bar_background img').attr('src','/theme/img/iphone6-portrait-bar-black.png');
     }
     else{
         $('.status_bar_background img').attr('src','/theme/img/iphone6-portrait-bar-light.png');         
     }
 });

        $("body").on("click", ".remove", function (event) {
            
            if (confirm("Are you sure you want to delete this image?")) 
            {
                var ID = this.id;
                // your deletion code				
                 $.get("delete.php?id="+ID, function(data, status){
					 if(data == 1)
                    {
						  $('#'+ID).closest('.image-preview').remove();
                   		  var shortCutFunction = 'success';
						  var msg = 'Carrier Deleted SuccessFully';
						  var title = 'Delete Carrier';
					}
					else{
						  var shortCutFunction = 'danger';
						  var msg = 'Failed to delete Carrier';
						  var title = 'Delete Carrier';
					}
                });
										
            }

         });

        
 $(function()
    {
      $('[name="language_button_enabled"]').change(function()
      {
        if ($(this).is(':checked')) {
           // Do something...
           $('.lang_color').show();
        }
        else{
            $('.lang_color').hide();
        }  
      });
    });        
       
$(window).scroll(function(e){ 
  var $el = $('.android-tab'); 
  var isPositionFixed = ($el.css('position') == 'fixed');
  if ($(this).scrollTop() > 230 && !isPositionFixed){ 
    $('.android-tab').css({'position': 'fixed', 'top': '10px'}); 
  }
  if ($(this).scrollTop() < 230 && isPositionFixed)
  {
    $('.android-tab').css({'position': 'static', 'top': '0px'}); 
  } 
});       
                
	$("input[name='login_button_text']").on("input propertychange", function(event) {
	       
            if($(this).val().length == 15)
            {
                $('.primary').css('fontSize','11px');
            }
            else
            {
                $('.primary').css('fontSize','12px');
            }
        
        $('.register_color').text($(this).val());
	});      
        
// setting value of dropdown
        


$('.colorpicker-default').colorpicker().on('hide', function(ev){

     	var Attrname = $(this).attr('name')
		var color = ev.color.toHex();
		if(Attrname == 'register_color' || Attrname == 'lang_color' || Attrname == 'info_button_color' || Attrname == 'status_bar_background')
		{
			$('.'+Attrname).css('background',color);
		}
        else if (Attrname == 'navigation_bar_color'){
            $('.'+Attrname).css('borderBottom','3px solid '+color);
        }
		else
		{
			$('.'+Attrname).css('color',color);	
		}
		
	
});		
		    
        
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#logo img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        function icon(){    
        $('<span style="top: 0px;margin-left: 19px;position: absolute;background-image:\ url(/theme/img/question_mark.svg);background-repeat: no-repeat;background-size: 34px\ 46px;background-position: 0px -5px;width: 50px;height: 50px;" class="info" \
          data="1-Primary Logo.png" id="myBtn"></span>').insertAfter('.primary_thumbnail img');
        }
        window.setTimeout(icon, 500);            
    }
}

$(".main_logo").change(function(){
    readURL(this);
});
$('.secondary_logo').change(function(){         
        function icon(){    
        $('<span style="top: 0px;margin-left: 19px;position: absolute;background-image:\ url(/theme/img/question_mark.svg);background-repeat: no-repeat;background-size: 34px\ 46px;background-position: 0px -5px;width: 50px;height: 50px;" class="info" data="2-Secondary Logo.png" id="myBtn"></span>').insertAfter('.secondary_thumbnail img');
        }
        window.setTimeout(icon, 500);            
        
});
		
$('.fileupload-exists_primary').click(function(){
	$('#logo img').attr('src', '/teamsters1932/backoffice/img/logo/<?php echo $app_logo ?>');
})		
		

$('.colorpicker-default').on('keyup', function() {
    $(this).colorpicker('setValue', $(this).val());
	
});
        
 
 $('.finish').click(function(e){  
      
    e.preventDefault();
    $('#publish_dialog').modal('show');
             
    })        
         
$('.yes').click(function(){
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                alert('Demo account cannot make any changes.');
            }   
            else if(user_level == 2){
                alert("Sorry! you don't have permission to perform this action");                
            }    
            else{
    
                 $('.submit_button').attr('disabled','dsiabled');
                $('.message-wait').show();

                $('#publish_dialog').modal('hide');
                $('#resend_default').submit();
            }
    })
    $('.no').click(function(){
        $('#publish_dialog').modal('hide');
    })        
        
	</script>

<!--    dynamic script of this page stat here-->
    
</body>
</html>

