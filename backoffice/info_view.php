<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
 <title>VIEW PAGE</title></head>

  <script>
//window.addEventListener("load", function(){
//	var load_screen = document.getElementById("load_screen");
//	document.body.removeChild(load_screen);
//});
</script>    
<style>
@font-face {
    font-family: Montserrat;
	src: url('/theme/fonts/Montserrat-Regular.eot');
	src: url('/theme/fonts/Montserrat-Regular.eot?#iefix') format('embedded-opentype'),
		url('/theme/fonts/Montserrat-Regular.woff2') format('woff2'),
		url('/theme/fonts/Montserrat-Regular.woff') format('woff'),
		url('/theme/fonts/Montserrat-Regular.ttf') format('truetype'),
		url('/theme/fonts/Montserrat-Regular.svg#Montserrat-Regular') format('svg');
	font-weight: normal;
	font-style: normal;
    
}   

    body{
        background: #EBECED;
        font-family: Montserrat !important;
    }
        
    span,strong,div,p,a{
        font-family: Montserrat !important;
        letter-spacing: -1px !important;
        word-spacing: 1px !important;

    }
    
    .title{
        margin-left: 30px;
        padding-top: 38px;
        color: #152C3B;
        font-weight: bold;
        text-transform: uppercase;
        font-size: 25px;
        padding-left: 30px;
    } 
    h1,h2,h3,h4,h5{
        color: #112C3B;
        letter-spacing: -1px !important;
        word-spacing: 1px !important;
        
    }
    
.description {
    text-align: justify;
    margin-left: 22px;
    margin-right: 22px;
    background: white;
    padding: 12px;
    padding-top: 11px;
    border-radius: 12px;
    font-family: Montserrat !important;
    padding-right: 26px;
    background: white;
    box-shadow: 2px 2px 5px #888888;
    letter-spacing: -1px !important;
    word-spacing: 1px !important;
    margin-bottom: 34px;
}    
.panel {
    background: #EBECED;
    margin-top: -25px;
    margin-left: -8px;
    margin-right: -8px;
    margin-bottom: 10px;
    
}    
input[type="button"] {
    background: #112C3B;
    color: white;
    padding: 10px;
    border: none;
    border-radius: 9px;
    cursor: pointer;
    font-size: 12px;
    font-family: Montserrat;
    width: 298px !important;
}    
iframe {
    width: 300px;
    padding-left: 4px;
}    
    img{
        width: 306px !important;
        height: auto;
    }  
    textarea,input{
        width: 250px !important;
    }
    
</style>
<?php

	include("headers/connect.php");
     $type = $_GET['type'];
	
		// this is called from the sub menu which directly opens webview
		$query = "SELECT $type from `app` ";
		$result = mysqli_query($conn,$query);
		$row=mysqli_fetch_assoc($result);
		$param = $row[$type];
		if($type == 'about') $type = 'About Us';
		if($type == 'contact') $type = 'Contact '.$application_name.'';
		if($type == 'privacy') $type = 'Terms and condition / Privacy';  
        echo "<div class='panel'>";
		echo "<h2 class='title' style='	margin-left:10px;'>{$type}</h2>";
//        echo "<p class='time'>$time_stamp</p>";
        
        echo"<div class='description'>{$param}</div>";
        echo "</div>";
