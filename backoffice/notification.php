<?php

    include("headers/connect.php");
    include '_user-details.php';
    include("header.php");

?>
<!DOCTYPE html>
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Notification | '.$title.'</title>';
    ?>
    <head>
    <style>
    div#editable-sample_filter {
    display: none;
}
div#editable-sample_info {
    position: relative;
    top: 22px;
    left: 22px;
    color: #919191;
}
.dataTables_paginate.paging_bootstrap.pagination {
    float: right;
    margin-top: -23px;
    margin-bottom: 15px;
}    
.col-sm-6 {
    width: 100%;
}
div#editable-sample_length {
    float: right;
    top: -43px;
    position: relative;
}    
button.btn.btn-primary {
background: #21AF86;
    border-radius: 31px;
    font-size: 12px;
    padding-left: 32px;
    padding-right: 32px;
    padding-top: 13px;
    padding-bottom: 13px;
    position: relative;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    display: none;
    }    
    select{

    border-radius: 24px !important;
    padding-left: 51px !important;
    height: 45px !important;
    -webkit-appearance: none;
    -webkit-border-radius: 24px;
    background-image: url(/theme/img/Down-arrow-.svg) !important;
    background-position: 86% 46%;
    background-repeat: no-repeat;
    padding-right: 28px !important;
    background-size: 13px 18px;
    cursor: pointer;
    }
    table th{
        background: #eaeaea !important;
        color: #919191;
        font-weight: normal;
        
    }
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background-color: white !important;
    color: #919191;
    font-weight: normal;    
    padding-bottom: 5px;    
}   
label {
    color: #919191;
    text-transform: capitalize;
}    
a {
    color: #58595b !important;
}    
table.dataTable thead .sorting:after {
    opacity: 0.7 !important;
    font-size: 14px !important;
    color: #929291 !important;
}    
div#editable-sample_wrapper {
    margin-top: -108px;
    margin-left: 26px;
    margin-right: 26px;    
}    
div.dataTables_wrapper div.dataTables_length select {
    width: 121px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    }    
li.previous a{
    color: #919191 !important;
        border: none !important;

}
li.active {
        border: none !important;
}
        .pagination>.active>a{
        background: none !important;    
        }
li.active a{
        background: none !important;
        color: #919191 !important;
}
li.next a{
    border: none !important;
}    
li. next a{
    color: #919191 !important;
}    
table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
    left: 5px;
        }        
    </style>
</head>
    
<script>
window.addEventListener("load", function(){
	var load_screen = document.getElementById("load_screen");
	document.body.removeChild(load_screen);
});
</script>    
<body class="fixed-top">
   <!-- BEGIN HEADER -->

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
    <h4 style="margin-left: 20px;margin-bottom: 45px;">Notifications</h4>   
    <section class="panel" style="margin-left: 19px;margin-right: 26px;">
     
      
      <div class="panel-body">
       
         
         <div class="adv-table editable-table ">
                          <div class="clearfix" style="float: right;margin-right: 285px;top: 16px;position: relative;"> <a href="send-notification"><button type="button" class="btn btn-primary"><i style="padding-right:5px;" class="fa fa-plus"></i>Send Push</button></a>
                          </div>
                          <div class="space15"></div>
                <!-- BEGIN ADVANCED TABLE widget-->
                <?php
                if(isset($_GET['insert']) == 'true')
                {
                    echo"
                <div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>×</button>
                        <strong>Success!</strong> The Notification has been added.
                    </div>";
                }
            else if(isset($_GET['update']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been updated.
                </div>";
            }
            else if(isset($_GET['delete']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been Deleted.
                </div>";
            }
?>
             
                   <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="editable-sample">
                                <thead>
                                   <tr>
								 <th style="width:31px;">S No</th>
                                    <th>Title</th>
                                    <th>Target Audience</th>
                                    <th>Time</th>                                       
                                   	<th>Body</th>
									<th>Action</th>
                                    </tr>
                     
                              </thead>
                            <?php
//					$query = "select pm.*,pb.*,ap.* from `pushmessage` pm,`pushbridge`pb,`app` ap where pm.authorAppID = {$appID} and pm.pushID = pb.pushID AND pb.appID = ap.app_id order by pm.pushID desc";
                    
                    $query = "select * from `pushmessage` order by  `TimeStamp` desc";
                    
					$result = mysqli_query($conn,$query);
					$count=1;
					$s_no = 1;
				while($row = mysqli_fetch_array($result))
                {
					$tempID=$row['pushID'];
					$title = $row['title'];
					$notification = $row['pushMessage'];
					$timeStamp = $row['TimeStamp'];
                    $timeStamp = date("d-M-Y h:i a", strtotime($timeStamp));
                    
//                    26-Sep-2017 3:38 pm
                    $topics = $row['topics'];
					
					echo"<tr class=''> 
											<td id='table_width' style='width:5%'>{$s_no}</a></td>
											<td style='width:12%'>{$title}</a></td>
                                            <td style='width:6%'>{$topics}</a></td>
										      <td style='width:5%'>{$timeStamp}</a></td>
											<td style='width:26%'>{$notification}</a></td>
										<td>
										<button class='btn delete' style='padding-left: 0px;background:none;color:red' type='button' id='$tempID'>      <img style='width: 16px;margin-left: 18px;' src='/theme/img/delete.svg'  />
						</button>
                                    </td>
											  </tr>";
					$s_no++;
                }

					?>	
          </table>
        </div>                                   
      </div>
    </section>
  </section>
</section>

 <script src="/theme/js/jquery.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
      <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
      <script src="/theme/assets/toastr-master/toastr.js"></script> 
	
      <!--script for this page only-->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    
    
    
     
    <script>
      		  $(document).ready(function() {
                $('#editable-sample').DataTable();
//              $('.dataTables_length label').html($('.dataTables_length label').html().replace('Show',''));
//              $('.dataTables_length label').html($('.dataTables_length label').html().replace('entries','Records Per Page'));
              $('button.btn.btn-primary').show();
});
   </script>
     <script>
          
		 function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".delete", function (event) {
            var user_level = <?php echo $user_level ?>;
            if(user_level == 3){
                alert('Demo account cannot make any changes.');
            }
            else{
              
            if (confirm("Are you sure you want to delete this Notification?")) {
                  var ID = this.id;
                    var $this = $(this);
            
            $.get("<?php echo $app_name ?>/backoffice/delete.php?notifi_id="+ID, function (data, status) {
                        
                            var obj = jQuery.parseJSON(data);
	                                if (obj.response == 1) {

                                 var table = $('#editable-sample').DataTable();  
                                  var row;

                                  if($this.closest('table').hasClass("collapsed")) {
                                    var child = $this.parents("tr.child");
                                    row = $(child).prevAll(".parent");
                                  } else {
                                    row = $this.parents('tr');
                                  }

                                  table.row(row).remove().draw();                
                                    showmessage('success','Notification Delete Successfully','successfully');
                                } 
                          });  
                }
            }
            });
		 
		 
    </script>
    
  
        <?php
        $key = @$_GET['key'];
        if($key == 'success')
        {
             echo "<script>showmessage('success','Notification Send','Successfully');</script>";
        }
    ?>

</body>