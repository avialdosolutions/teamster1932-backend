<?php 
    include("headers/connect.php");
    
    $query = "SELECT a.`app_name`,ap.app_logo  FROM `app` a ,app_appearance ap ";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        $app_logo = $row['app_logo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Information View | <?php echo $title ?> </title>
     <link rel="icon" href="<?php echo $app_name ?>/backoffice/img/logo/icon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/assets//theme/app_css/style.css" />
        <style>
            body
            {
                background:  #1d2029!important
            }
           .main_center
            {
                margin: 0 auto;
                width: 417px;
                display: block;
                
            }
            .main
            {
                margin-top: 5%;
                /* text-align: center; */
                background-image: url(/theme/img/iphone6-portrait-white.png);
                background-repeat: no-repeat;
                width: 100% !IMPORTANT;
                height: 800px;
                padding-top: 88px;
                padding-left: 22px;
                overflow: hidden;
            }
            .content
            {
/*
                margin-left: -20px;
                margin-top: -9px;
                width: 379px !important;
*/
            }
            .panel
            {
                width: 365px !important;
                margin-top: 0px;
                margin-left: -2px;
                text-align: left;
            }
            .description
            {
                text-align: justify;
                margin-left: 15px;
            }
            .wrappers {
                height: 672px;
                overflow-y: hidden;
                overflow-x: hidden;
                margin-left: -5px;
                width: 379px !important;
                margin-top: -7px;            
            }
            
            
            .wrappers:hover {
                overflow-y:overlay; 
                cursor: pointer;
            }
.wrappers::-webkit-scrollbar {
    width: 12px;
    height: 18px;
}
.wrappers::-webkit-scrollbar-thumb {
    height: 6px;
    border: 4px solid rgba(0, 0, 0, 0);
    margin-right: 40px;
    background-clip: padding-box;
    -webkit-border-radius: 7px;
    background-color: rgba(0, 0, 0, 0.15);
/*    -webkit-box-shadow: inset -1px -1px 0px rgba(0, 0, 0, 0.05);*/
}
.wrappers::-webkit-scrollbar-button {
    width: 0;
    height: 0;
/*    background: none;*/
    display: none;
}
.wrappers::-webkit-scrollbar-corner {
    background-color: none;            
            }
            
input[type="button"] {
    background: #112C3B;
    color: white;
    padding: 10px;
    border: none;
    border-radius: 9px;
    cursor: pointer;
    font-size: 12px;
    font-family: Montserrat;
    width: 298px !important;
}            
    </style>
</head>
<link rel="stylesheet" type="text/css" href="/assets/css/custom.css" />
<script>
window.addEventListener("load", function(){
	var load_screen = document.getElementById("load_screen");
	document.body.removeChild(load_screen);
});
</script>    

<body>
<!--<div id="load_screen"><div id="loading"><img width="100px" src="../assets/pre-loader/Thin fading line.gif" alt="Triangles indicator" /></div></div>-->
<div class="wrapper main_center">
<?php
    
    
    $type = $_GET['type'];
	
		// this is called from the sub menu which directly opens webview
		$query = "SELECT $type from `app` ";
		$result = mysqli_query($conn,$query);
		$row=mysqli_fetch_assoc($result);
		$param = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $row[$type])));;
//		if($type == 'about') $type = 'About Us';
//		if($type == 'contact') $type = 'Contact Teamsters Local 1932';
//		if($type == 'privacy') $type = 'Terms and condition / Privacy';
        
        echo "<div class='main'>    
                 <div class='wrappers'>
                <div class='content'><img style='width: 379px;border-radius: 5px;' src='$app_name/backoffice/img/logo/$app_logo' />";  

                        
                    echo "<div class='panel'>
                    <iframe src='https://linkedunion.com$app_name/backoffice/info_view.php?type=$type' style='width: 379px;height: 529px;'></iframe>
                        
                        </div>
                </div>
                </div>
             </div>";
	?>
        

		
				</div>

	</body>