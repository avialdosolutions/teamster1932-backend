<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
 <title>VIEW PAGE</title></head>

  <script>
//window.addEventListener("load", function(){
//	var load_screen = document.getElementById("load_screen");
//	document.body.removeChild(load_screen);
//});
</script>    
<style>
        @import url('https://fonts.googleapis.com/css?family=Montserrat');
    
    
        @font-face {
        font-family: Montserrat;
        src: url(https://fonts.googleapis.com/css?family=Montserrat);
    }   

    body{
        background: #EBECED;
        font-family: Montserrat !important;
    }
        
    span,strong,div,p{
        font-family: Montserrat !important;
    }
    
    .title{
        margin-left: 30px;
        padding-top: 38px;
        color: #152C3B;
        font-weight: bold;
        text-transform: uppercase;
        font-size: 25px;
        padding-left: 30px;
    } 
    h1,h2,h3,h4,h5{
        color: #112C3B;
    }
    
.description {
    text-align: justify;
    margin-left: 22px;
    margin-right: 22px;
    background: white;
    padding: 12px;
    padding-top: 11px;
    border-radius: 12px;
    font-family: sans-serif !important;
    padding-right: 13px;
    background: white;
    height: 100%;
    box-shadow: 2px 2px 5px #888888;
    margin-bottom: 34px;
}    
.panel {
    background: #EBECED;
    margin-top: -47px;
    margin-left: -8px;
    margin-right: -8px;
    margin-bottom: 20px;
    
}    
iframe {
    width: 399px;
    padding-left: 4px;
}    
    img{
        width: 306px !important;
        height: auto;
    }  
    textarea,input{
        width: 250px !important;
    }
    .left-align{
/*        float: left;*/
    }
    .right-align{
/*        float: right;*/
    }

</style>
<?php

        include("headers/connect.php");
        $id = $_GET['id'];
        $query = "SELECT * FROM `contact` WHERE `contact_id` = $id";
        $sth = $dbh->prepare($query);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        $name = $row['name'];
        $designation = $row['designation'];
        $address = $row['address'];
        $phone_no1 = $row['phone_no1'];
        $phone_no2 = $row['phone_no2'];
        $fax_no = $row['fax_no'];
        $email = $row['email']; 	
        $app_id_db = $row['app_id'];
        $order = $row['order'];
        $public = $row['public'];
        $image = $row['image'];
        $desc = $row['description'];
        $website = $row['website'];


        $description = "<center><div class='left-align'><img style='width: 160px !important;border-radius: 50% !important;margin-right: 20px;margin-top: 11px;margin-left: 10px;' src='https://linkedunion.com$app_name/backoffice/img/unionrepresentative/$image'></div>
            
        <div class='right-align'><p style='margin-top: 11px;color:#152C3B;font-weight:bold'>$name</p>    
        <p style='color: #152C3B;margin-top: -9px;font-size: 13px;font-weight: bold;'>$designation</p>
        <p style='color: #152C3B;margin-top: -7px;font-size: 12px;'>$email</p>
        <p style='color: #152C3B;margin-top: -7px;font-size: 12px;'>Cell: $phone_no1</p>
        <p style='color: #152C3B;margin-bottom: 32px;margin-top: -7px;font-size: 12px;'>$website</p>
        </div>
        <a href='tel:$phone_no1'><button style='background: #112C3B;color: white;padding: 10px;width: 112px;margin-right: 20px;border: none;border-radius: 9px;cursor: pointer;font-size: 12px;font-family: Montserrat;font-weight: normal;' type='button'>CALL</button></a><a href='mailto:$email'><button style='background: #12547A; color: white; padding: 10px; width: 112px; border: none; border-radius: 9px; cursor: pointer; font-size: 12px; font-family: Montserrat; font-weight: normal;' type='button'>EMAIL</button></a>    
        </center>";
        $description .= "<div class='left-align'>$desc</div>";

        echo "<div class='panel'>";
		echo "<h2 class='title' style='	margin-left:10px;'></h2>";
//        echo "<p class='time'>$time_stamp</p>";
        
        echo"<div class='description'>{$description}</div>";
        echo "</div>";

