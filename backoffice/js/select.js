/*
  jQuery Document ready
*/

	
function select(SelectClass)
{
    	$('.'+SelectClass).ddslick(
	{
		//callback function: do anything with selectedData
		onSelected: function(data)
		{
			/*
				we are calling custom created function.
				that function will display selected option detail.
			*/
			displaySelectedData("Callback Function on Dropdown Selection" , data);
		}
	});
	function displaySelectedData(demoIndex, ddData)
	{
		/*
			add heading to div
		*/
		$('#dd-display-data').html("<h3>Data from Demo " + demoIndex + "</h3>");
		/*
			append selected drop down index to result.
			also added code so you can check
			in browser console for selected object
		*/
		$('#dd-display-data').append('<strong>selectedIndex:</strong> ' + ddData.selectedIndex + '<br/><strong>selectedItem:</strong> Check your browser console for selected "li" html element');
		
		/*
			check if selection made.
		*/
		if (ddData.selectedData)
		{
			/*
				appeding more data to result div.
			*/
			$('#dd-display-data').append
			(
				'<br/><strong>Value:</strong>  ' + ddData.selectedData.value +
				'<br/><strong>Description:</strong>  ' + ddData.selectedData.description +
				'<br/><strong>ImageSrc:</strong>  ' + ddData.selectedData.imageSrc
			);
		}
		/*
			browser console code
		*/
		//console.log(ddData);
	}
}




