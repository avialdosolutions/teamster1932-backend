    <?php
include 'headers/connect.php';
include '_user-details.php';

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
	if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}


    //session_start();
    
    if(@$_GET['lang'])
        {
            $lang = $_GET['lang'];
            @$_SESSION['lang']=$lang;
        }

    $query = "select (SELECT `pushCount` from app where app_id = '{$appID}') - (select count(*) from `pushmessage` pm where pm.authorAppID = '{$appID}') as pushRemaining";
    $stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $pushRemaining = $row['pushRemaining'];
    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
   
<style>
        #profileImage
            {
                width: 48px;
                height: 47px;
                border-radius: 50%;
                background: #2a3542;
                font-size: 21px;
                color: #fff;
                float: left;
                text-align: center;
                line-height: 49px;
                margin: -16px 19px 0px 0;
                text-transform: capitalize;
            }

         a
            {
            color: black;
            }
        .editable-table .dataTables_filter {
                    width: 29% !important;
                    /* padding-top: 0px; */
                }
        .adv-table .dataTables_length {
                    float: left;
                } 
    </style> 
    
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<!--
<meta  name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' 
/>   
<meta name="viewport" content="width=680, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,target-densitydpi=device-dpi, user-scalable=no" />    
<meta name="viewport" content="width=device-width, initial-scale=1.0">    
-->
    <meta name="viewport" content="width=1024">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="icon" href="<?php echo $app_name ?>/backoffice/img/logo/icon.ico" type="image/x-icon">
    	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet" />

    <link href="/theme/css/jquery.fancybox.css" rel="stylesheet">
    <link href="/theme/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="/theme/app_css/style.css" rel="stylesheet">

<!--    <link href="/theme/css/style-responsive.css" rel="stylesheet" />    -->
    <link href="/theme/css/bootstrap-reset.css" rel="stylesheet">
    
    
    <link href="/theme/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="/theme/assets/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href="/theme/assets/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="/theme/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="/theme/assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="/theme/assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="/theme/assets/bootstrap-colorpicker/css/colorpicker.css" />
    
    <link rel="stylesheet" type="text/css" href="/theme/assets/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="/theme/assets/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="/theme/assets/jquery-multi-select/css/multi-select.css" />
    <link href="/theme/assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/theme/assets/chosen-bootstrap/chosen/chosen.css" />
    <link href="/theme/assets/toastr-master/toastr.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/theme/css/nprogress.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="/theme/js/nprogress.js" type="text/javascript"></script>    
<!--
  <div class='progress' id="progress_div">
    <div class='bar' id='bar1'></div>
    <div class='percent' id='percent1'></div>
  </div>
-->
<input type="hidden" id="progress_width" value="70">    
 <section id="container" class="">
      <!--header start-->
      <header class="header white-bg">
        <div class="sidebar-toggle-box">
          <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips">
              <img src="/theme/img/Home-Menu-.svg" style="width:25px;" />
          </div>
      </div>
        <!--logo start-->
        <a href="/teamster1932/" class="logo" ><img style="width: 339px;" src="<?php echo $app_name ?>/backoffice/img/Home-logo-.svg" /></a>
        <!--logo end-->

        <div class="top-nav ">
          <ul class="nav pull-right top-menu">
            <!-- notification dropdown start-->
 
                    <!-- language li dropdown -->   
              <?php 
          if($language_button_enabled == 'true'){
                ?>
              <li class="dropdown language" style="margin-top: 0px;margin-right: 26px;">
                      <a data-close-others="true" style="margin-top: 24px;    margin-right: -21px;" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <?php
                            if(@$_SESSION['lang'] == "spanish")
                                {
                                  echo   ' <span  class="username">Spanish</span>';
                                }
                              else
                              {
                                  echo   '<span class="username">English</span>';   
                              }
                          ?>
                          
                          <img src="/theme/img/Down-arrow-.svg" style="width: 15px;left: 8px;top: -8px;position: relative;">
                      </a>
                      <ul class="dropdown-menu">
                          <?php
                            if(@$_SESSION['lang'] == "spanish")
                                {
                                 echo ' <li><a href="/teamster1932/Home?lang=english"> English</a></li>';
                                }
                                else
                                {  
                                    echo '<li><a href="/teamster1932/Home?lang=spanish"> Spanish</a></li>';
                                }
                      
                      ?>
                     </ul>
                  </li>
          <?php } ?>
                    <!-- Notification li dropdown-->
              
              <li id="header_notification_bar" class="dropdown">
                        <a href="/teamster1932/notification" style="margin-top:16px;">
                            <img src="/theme/img/Notification-on-.svg" style="width: 26px;" />
                            <span class="badge bg-warning"><?php echo $pushRemaining ?></span>
                        </a>
                    </li>	              
              
              
              <!-- user li dropdown -->

              <li class="dropdown" style="margin-right: 27px;">
                  <a data-toggle="dropdown" class="dropdown-toggle" style="margin-top: 16px;text-align: center;" href="#">
                      <?php 
                            if($user_image == 'user.jpg'){
                                echo '<div alt="" id="profileImage"></div>';
                            }
                            else{
                                echo "<img src='$app_name/backoffice/img/user/$user_image' style='width: 46px;border-radius:50px;height: 46px;float: left;position: relative;top: -15px;right: 14px;' />";
                            }    
                      ?>
                      
                      <span style="font-size: 12px;position: relative;top: -8px;color: #414042;font-family: Montserrat;">Hi, </span><span id="firstName" class="username" style="margin-bottom:20px;"><?php echo @$username;   ?></span>
                       <img src="/theme/img/Down-arrow-.svg" style="width: 14px;left: 12px;top: 0px;position: relative;">
                        <br / ><span style="color:#939598;font-size:12px;margin-left: -16px;position: relative;top: -9px;"><?php echo $user_type ?></span>
                 </a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
        <?php 
            if($user_level == 1){
                echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/app-users">Users</a></li>';
            }
        ?>    
      <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/app-appearance">App Appearance</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/history">App History</a></li>  
         <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/view-icons">Icons</a></li>  
      <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/settings">Settings</a></li>
      <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/members">Members</a></li>
	  <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/send-notification">Send Push </a></li>	
      <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/file-managment">File Managment</a></li>	    
<!--	  <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/hustle">Hustle</a></li>		                -->
      <li role="presentation"><a role="menuitem" tabindex="-1" href="/teamster1932/logout">Log Out</a></li>
    </ul>				  
<!--
                  <ul class="dropdown-menu extended logout">
                      <div class="log-arrow-up"></div>
                      <li><a href='app_appearance.php'><i class="fa fa-mobile "></i>Appearance</a></li>   
                      <li><a href='setting.php'><i class="fa fa-cog"></i>Setting</a></li>     
                      <li><a href='allusers.php'><i class="fa fa-user"></i>User</a></li>                 
                      <li><a href="logout.php"><i class="fa fa-key"></i> Log Out</a></li>
                      
                  </ul>
-->
              </li>
              <!-- user login dropdown end -->
                
          </ul>
            </div>
     </section>
    <?php
            if(isset($_SESSION['lang']))
                       {
                                $language = $_SESSION['lang'];
                       }
                         else
                         {
                             $_SESSION['lang'] = 'english';
                              $language = $_SESSION['lang'];
                         }
    
     $query = "SELECT icon,id,name,(select count(*) from `subcategories` sc where sc.menu_id = c.id) as count FROM `categories` c WHERE `app_id` = {$appID} and c.language ='$language'";
		    $sth = $dbh->prepare($query);
		    $sth->execute();
    
        echo '<aside>
          <div id="sidebar"  class="nav-collapse " style="background: #F9FBFC;width:220px;z-index: 1;box-shadow: 4px 7px 10px rgba(211,211,211,1);">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" style="background: white;height: 419px;" id="nav-accordion">	 
              <li class="sub-menu">
                <a class="" href="/teamster1932/">
                <span style="margin-right: 18px;">
                  <img style="width: 22px;" src="'.$app_name.'/backoffice/img/Dashboard-icon-off.svg" />
                </span>
                <span style="top: 3px;position: relative;">Dashboard </span>
                </a>
                </li>';
            
    while($row = $sth->fetch(PDO::FETCH_ASSOC))
		{
			$category = strtolower($row['name']);
            $categoryTitle = strtolower($category);
            $categoryTitle = str_replace(' ','-',$categoryTitle);
            $categoryTitle = str_replace('?', '', $categoryTitle);
            $categoryTitle = str_replace('/','-',$categoryTitle);
			$id = $row['id'];
			$icon = explode('.',$row['icon'])[0];
            
            if(!file_exists($_SERVER['DOCUMENT_ROOT']."$app_name/backoffice/img/$icon-off.svg")){
                $icon = "Dashboard-icon";
            }
            
			echo "<li class='sub-menu'>";
						/* Below code decides whether to put an arrow or now */
                        $imageStyle = "";
                        $textStyle = "";
                        if(strlen($category) > 16 ){
                            $imageStyle = "top: -9px;position: relative;";
                            $textStyle = "top: 3px;position: relative;width: 100px;";
                        }
						
						if(($row['count'])>0)
						{
                            echo "<a ='javascript:;'><span style='margin-right: 18px;$imageStyle'>
                                                      <img style='width: 22px;' src='$app_name/backoffice/img/$icon-off.svg'>
                                                    </span>
							<span style='$textStyle'>{$category}</span></a>";
                        }
                        else
						{
							echo "<a class='' href='/teamster1932/news/$categoryTitle/{$id}'>
							<span style='margin-right: 18px;$imageStyle'>
                                                      <img style='width: 22px;' src='$app_name/backoffice/img/$icon-off.svg'>
                                                    </span>
                                                    
							<span style='$textStyle'>{$category} </span>";
                            
                        }
                        echo "</span>";
            
            $query_subcategories = "SELECT sc.* from `subcategories` sc, `categories` c where c.`id` = sc.`menu_id` AND sc.menu_id = {$id}";
			$sth_subcategories = $dbh->prepare($query_subcategories);
			$sth_subcategories->execute();
            
			echo "<ul class='sub'>";
			while($row_subcategory = $sth_subcategories->fetch(PDO::FETCH_ASSOC))
			{
				$subcategory_id = $row_subcategory['id'];
				$subcategory = strtolower($row_subcategory['name']);
                $subcategorytitle = strtolower($subcategory);
                $subcategorytitle = str_replace(' ','-',$subcategorytitle);
                $subcategorytitle = str_replace('?', '', $subcategorytitle);
                $subcategorytitle = str_replace('/', '-', $subcategorytitle);
				echo "<li><a class='' href='/teamster1932/news/$subcategorytitle/{$subcategory_id}'>{$subcategory}</a></li>";
			}
				echo "</ul></li></a>";     
            
        }
        echo "		
	  <li class='sub-menu' style='margin-top: 37px;'>
	  	<a class='' href='/teamster1932/information'>
		<span style='margin-right: 18px;'>
		  <img style='width: 22px;' src='/teamster1932/backoffice/img/information-off.svg'>
		</span>
		<span style='top: 3px;position: relative;'>Information</span>
		</a>
		</li>
	  <li class='sub-menu'>
	  	<a class='' href='/teamster1932/membership-feedback'>
		<span style='margin-right: 18px;top: -9px;position: relative;'>
		  <img style='width: 22px;' src='/teamster1932/backoffice/img/membership-off.svg'>
		</span>
		<span style='top: 3px;position: relative;width: 87px;'>Membership Feedback </span>
		</a>
		</li>
	  <li class='sub-menu'>
	  	<a style='' class='' href='/teamster1932/request-union-representation'>
		<span style='margin-right: 18px;position: relative;top: -7px;'>
		  <img style='width: 22px;' src='/teamster1932/backoffice/img/Request-union-off.svg'>
		</span>
		<span style='top: 3px;position: relative;width: 120px;'>Request Union Representation </span>
		</a>
		</li>
        <li style='padding-top: 22px;font-size: 11px;color: #aeb2b7;'>
        <p style='padding-left: 47px;'>&copy; 2017 <u><span style='cursor:pointer' onClick="."(window.open('http://linkedunion.com','_blank'))"." > Linkedunion </span></u>, LLC.<br /> All Rights Reserved.</p>
		</li>							 
         </ul>
         
        </div></aside>";
         
     echo "<script>
          NProgress.start();     
          $(window).load(function(){     

          
            var currentapp = '$app_name';
            var previousapp = '$previousapp';
                    
              $('a').each(function(){
                var href = $(this).attr('href');
                if(href != undefined){
                $(this).attr('href', href.replace(previousapp,currentapp));
                }
              }) 
              $('.table').on('click', '.btn_icon', function(e) {                             
                var href = $(this).attr('href');
                if(href != undefined){
                $(this).attr('href', href.replace(previousapp,currentapp));                    
                }
                

              })                             
            $('img').each(function(){
                var img = $(this).attr('src');
                if(img != undefined){
                $(this).attr('src', img.replace(previousapp,currentapp));
                }
              })   
              
              $('form').each(function(){
                 var href = $(this).attr('action');
                if(href != undefined){
                $(this).attr('action', href.replace(previousapp,currentapp));
                }
                 
              })
              NProgress.done();   
//              console.clear();    
              setInterval(function () {
 //        console.clear();    
            },1000);
})



          </script>";
     ?>
    
     
<!--
		<li class='sub-menu dcjq-parent-li'>
							<a hred='javascript:;' class='dcjq-parent'>
							<i class='fa fa-phone'></i>
							<span>CONTACTS</span>							    
							<span class='dcjq-icon'></span></a>
							<ul class='sub'>							    
							<li><a style='font-size: 11px;' class='' href='/teamster1932/union-hall'>UNION HALL</a></li>
							
							<li class='sub-menu dcjq-parent-li'>
							<a hred='javascript:;' class='dcjq-parent'>
							<span style='font-size:11px;'>BUSINESS/STEWARD</span>							    
							<span class='dcjq-icon'></span></a>
							<ul class='sub'>							    
							<li><a class='' href='/teamster1932/worksite'>WORKSITE</a></li>
							 <li><a class='' href='/teamster1932/business-agent-steward'>AGENT/STEWARD</a></li>
							</ul>
						</li>
							
							
							</ul>
						</li>	
-->
     
    </head></html>


    <script src="/theme/js/jscolor.js"></script>

<script>
    $(document).ready(function(){
  var firstName = $('#firstName').text();
  var intials = $('#firstName').text().charAt(0);
  var profileImage = $('#profileImage').text(intials);
});
    </script>
<script>
    

    
    
    $('ul.sidebar-menu li').mouseover(function(){
        var imgName = $(this).find('img').attr('src').split('/');
        imgName = imgName[imgName.length-1];
        imgName = imgName.split('-').slice(0, -1).join("-");
        $(this).find('img').attr('src','<?php echo $app_name ?>/backoffice/img/'+imgName+'-on.svg');
        
    })
    $('ul.sidebar-menu li').mouseout(function(){
        var imgName = $(this).find('img').attr('src').split('/');
        imgName = imgName[imgName.length-1];
        imgName = imgName.split('-').slice(0, -1).join("-");
        $(this).find('img').attr('src','<?php echo $app_name ?>/backoffice/img/'+imgName+'-off.svg');        
    })
     
</script>
    <script>

    </script>
