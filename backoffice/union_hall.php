<?php
    include("headers/connect.php");
    include '_user-details.php';
    include("header.php");

$categoryID = $_GET['categoryID'];
$url = "";
$redirect="";
$name = "";
//$query = "SELECT w.name FROM `webservice_category` wc, `webservices` w WHERE wc.`category` like '{$categoryID}' AND wc.webservice = w.id";
//$result = mysqli_query($con,$query);
//$row = mysqli_fetch_array($result);
//$fileName = $row['name'];
//if(isset($fileName))
//{
//	if($fileName 	!= "view.php" && $fileName != "news_category.php"){
//		$url = "{$fileName}?categoryID={$categoryID}";
//		$redirect = 1;
//		//header("Location: {$fileName}?categoryID={$categoryID}");
//	}
//}

?>


<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    
   <script>
   if(<?php echo $redirect;?> == 1){
			//alert('redirecting');
			window.location.href = '<?php echo $url; ?>';
   }
	</script>
<script>
window.addEventListener("load", function(){
	var load_screen = document.getElementById("load_screen");
	document.body.removeChild(load_screen);
});
</script>
 </head>
<head>
    <?php
    $query = "SELECT `app_name` FROM `app`";
		$stmh = $dbh->prepare($query);
        $stmh->execute();
        $row = $stmh->fetch(PDO::FETCH_ASSOC);
        $title =$row['app_name'];
        echo '<title>Union Hall | '.$title.'</title>';
    ?>   
<body class="fixed-top">
   <!-- BEGIN HEADER -->

<section id="main-content">
  <section class="wrapper"> 
    <!-- page start-->
    <section class="panel" style="margin-left:39px;">
      <header class="panel-heading">UNION HALL</header>
      <div class="panel-body">
       
         
         <div class="adv-table editable-table ">
             <div class="space15"></div>
                <!-- BEGIN ADVANCED TABLE widget-->
                <?php
                if(isset($_GET['insert']) == 'true')
                {
                    echo"
                <div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>×</button>
                        <strong>Success!</strong> The Notification has been added.
                    </div>";
                }
            else if(isset($_GET['update']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been updated.
                </div>";
            }
            else if(isset($_GET['delete']) == 'true'){
          echo"
            <div class='alert alert-success'>
                    <button class='close' data-dismiss='alert'>×</button>
                    <strong>Success!</strong> The Notification has been Deleted.
                </div>";
            }
?>
             
                <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="example">
                                <thead>
                                     <tr>
                                    <th>Address</th>     
                                    <th>Phone</th>
                                    <th>Fax</th>
                                    <th>Mailing Address</th>
                                    <th>Office Hours</th>
                                    <th>Media Inquiries</th>
                                    <th>Edit</th>
								 
                                    </tr>
                                   </thead>
<?php
					$query = "SELECT * FROM `union_hall_contact`";
					$sth = $dbh->prepare($query);
					$sth->execute();
					$count=0;
					while($row = $sth->fetch(PDO::FETCH_ASSOC))
					{
					 $count++;
					$address = $row['address'];
                    $phone = $row['phone'];
                    $fax = $row['fax'];
                    $mailing_address = $row['mailing_address'];
                    $office_hour = $row['office_hour'];
                    $media_inquiries = $row['media_inquiries'];
					echo"
					<tr class=''> 
					<td>{$address}</td>
					<td>{$phone}</td>
                    <td>{$fax}</td>
                    <td>{$mailing_address}</td>
                    <td>{$office_hour}</td>
                    <td>{$media_inquiries}</td>
					<td style='width:3%; text-align:center;'>
                   	<a class='btn_icon' id='edit' href='/teamster1932/union-hall/edit'>
                    <i style='position:relative !important; font-size:18px; color:green;margin:5px 3px 0 0;' class='fa fa-pencil'></i></a>
					</td>
						</tr>";
					}
					?>
					
          </table>
        </div>                                   
      </div>
    </section>
  </section>
</section>

<script src="/theme/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/theme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/theme/js/respond.min.js" ></script>
      <!--common script for all pages-->
    <script src="/theme/js/common-scripts.js"></script>
      <script src="/theme/assets/toastr-master/toastr.js"></script> 
	
      <!--script for this page only-->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>




      <!-- END JAVASCRIPTS -->
      <script>
		  $(document).ready(function() {
    $('#example').DataTable();
} );
		  
		  function  showmessage(shortCutFunction, msg,title) {
            var shortCutFunction =shortCutFunction;
             var title = title;
            var msg = msg;
            var $toast = toastr[shortCutFunction](msg, title);
      }      
          
          
          $("table").on("click", ".delete", function (event) {
            if (confirm("Are you sure you want to delete this Union Representative?")) {
                  var ID = this.id;
                    var $this = $(this);
         
            $.get("<?php echo $app_name ?>/backoffice/delete.php?contact_id="+ID, function (data, status) {
                        
                            var obj = jQuery.parseJSON(data);
								
                                if (obj.response == 1) {

                                    var Row = $this.closest('tr');
                                    var nRow = Row[0];
                                    $('#example').dataTable().fnDeleteRow(nRow)
                                    showmessage('success','Union Representative Delete Successfully','successfully');
                                } 
                          });  
                }
            });
		  
		  
      </script>

</body>
