
<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Stylesheet -->
 <link href="/theme/css/bootstrap.min.css" rel="stylesheet">    
<link href="/theme/app_css/preloader.css" rel="stylesheet" type="text/css">

 
    
    
  
<head>
    
<style>
   
.mt-0
    {
    font-size: 40px;
    margin-top: 25px;    
    }

.font-150 
    {
        font-size: 170px;
        color: #00A4EF !important;
    }    
.para
    {
            font-size: 18px;
            margin-top: 40px;
            margin-bottom: 35px;
    }

.para_footer
    {
        font-size: 15px;
        margin-top: 40px;
    }
    
.submit_button 
{
    background: #21AF86;
    color: white;
    border: none;
    padding: 10px;
    border-radius: 21px;
    padding-left: 35px;
    padding-right: 35px;
    font-size: 10px;
    margin-top: 40px;
    box-shadow: 5px 5px 10px rgba(207,206,206,1) !important;
    font-family: Montserrat !importnat;
}     
    
</style>
    



    
    
</head>
<body class="">
<div id="wrapper" class="clearfix">
  <!-- preloader -->
<!--
  <div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div>
-->

  <!-- Start main-content -->
  <div class="main-content">
     
    <!-- Section: home -->
    <section id="home" class="fullscreen bg-lightest">
      <div class="display-table text-center">
        <div class="display-table-cell">
          <div class="container pt-0 pb-0">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <h1 class="font-150 text-theme-colored mt-0 mb-0"><i class="fa fa-map-signs text-gray-silver"></i>404!</h1>
                <h2 class="mt-0">Oops! Page Not Found</h2>
                <p class="para">The page you were looking for could not be found.</p>
               <a href="#" class="submit_button" type="submit">Return To Home</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>  
  <!-- end main-content -->

  <!-- Footer -->
  <footer id="footer" class="footer bg-black-222">
    <div class="container p-20">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="mb-0 para_footer">© 2017 Linkedunion, LLC. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->


</body>
</html>