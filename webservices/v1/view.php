<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>VIEW PAGE</title>
<!--
<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
</head>
<link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
-->
<!--
<script>
window.addEventListener("load", function(){
	var load_screen = document.getElementById("load_screen");
	document.body.removeChild(load_screen);
});
</script>    
-->
    
<style>
@font-face {
    font-family: Montserrat;
	src: url('/theme/fonts/Montserrat-Regular.eot');
	src: url('/theme/fonts/Montserrat-Regular.eot?#iefix') format('embedded-opentype'),
		url('/theme/fonts/Montserrat-Regular.woff2') format('woff2'),
		url('/theme/fonts/Montserrat-Regular.woff') format('woff'),
		url('/theme/fonts/Montserrat-Regular.ttf') format('truetype'),
		url('/theme/fonts/Montserrat-Regular.svg#Montserrat-Regular') format('svg');
	font-weight: normal;
	font-style: normal;
    
}   
}   

body{
    background: #EBECED;
    font-family: Montserrat !important;
}

span,strong,div,p{
    font-family: Montserrat !important;
    letter-spacing: -1px !important;
    word-spacing: 1px !important;

}

.title{
/*    margin-left: 30px;*/
    padding-top: 38px;
    color: #152C3B;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 25px;
    padding-left: 30px;
} 
h1,h2,h3,h4,h5{
    color: #112C3B;
    letter-spacing: -1px !important;
    word-spacing: 1px !important;

}
    
.description {
    text-align: justify;
    margin-left: 22px;
    margin-right: 22px;
    background: white;
    padding: 12px;
    padding-top: 11px;
    border-radius: 12px;
    font-family: Montserrat !important;
    padding-right: 26px;
    background: white;
    box-shadow: 2px 2px 5px #888888;
    letter-spacing: -1px !important;
    word-spacing: 1px !important;
    margin-bottom: 34px;
        overflow-y: auto;
}    
.panel {
    background: #EBECED;
    margin-top: -25px;
    margin-left: -8px;
    margin-right: -8px;
    margin-bottom: 10px;
    
}    
input[type="button"] {
    background: #112C3B;
    color: white;
    padding: 10px;
    border: none;
    border-radius: 9px;
    cursor: pointer;
    font-size: 12px;
    font-family: Montserrat;
    width: 298px !important;
}    
iframe {
    width: 300px;
    padding-left: 4px;
}    
img{
    width: 295px !important;
    height: auto;
}  
textarea,input{
    width: 250px !important;
}
       img
        {
            width: 100%;
         }    
body{
        
    background: #EBECED;
}
</style>
    
    
<body>
<!--<div id="load_screen"><div id="loading"><img width="100px" src="../assets/pre-loader/Thin fading line.gif" alt="Triangles indicator" /></div></div>-->
<?php

	include '../../connect.php';

    
	$facebook = "";
	$twitter = "";
	$google = "";
	$pinterest = "";
	$title= "";
	$description = "";
	$news_id = "";
	$app_id = "";
    $category = "";
    $user_id = "";
	$display = "display:none";
    $timestamp = "";

	
	$app_id = $_GET['appID'];
	$user_id = @$_GET['user_id'];
    $timestamp = @$_GET['timestamp'];
	if(@$_GET['newsID'] ? $news_id = $_GET['newsID'] : $news_id = "");		
	
	if(@$_GET['category'] ? $category = $_GET['category'] : $category = "");
	
    // visited page counter here
        $query_visit = "INSERT INTO visited_page (category_id,user_id,timestamp) VALUES ('$category','$user_id','$timestamp')";
        $stm = $dbh->prepare($query_visit);
        $stm->execute();
	
	if($category != "" && $news_id == "")
	{
		// this is called from the sub menu which directly opens webview
		
		$query = "SELECT * from `news` WHERE `category` = '{$category}' and published = 1 LIMIT 1";
        $sth = $dbh->prepare($query);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        $rowCount = $sth->rowCount();
        if($rowCount >= 1){
		$title = $row['title'];
		$description = $row['description'];
		$social = $row['social'];
        $new_image = $row['new_image'];
		$time_stamp = strtotime($row['time_cone']);
                
        //Changeing date format
        $month = date('F', $time_stamp);
		$day = date('d',$time_stamp);
		$year = date('Y',$time_stamp);
        

        
        $time_stamp = "{$month} {$day}, {$year}";
        echo "<div class='panel'>";
		echo "<p class='title'>{$title}</p>";     
        if($description != null){
        echo "<div class='description' style=''>";
        if(file_exists("$newsImagePath".$new_image)){ echo "<img src='$newsImagePath$new_image' class='image'  />";}        
        echo"{$description}";
        echo "</div></div>";
        }
        }
        else{
             echo "<div class='panel'>";
            echo "<div class='description' style=''>";
            echo "<img src='http://linkedunion.com/theme/img/empty_page.png' class='image'  />";        
            echo "</div></div>";
        }
    }
	else
	{

		$query = "select * from news WHERE `news_id` = '{$news_id}' and `app_id` = '{$app_id}'";
        $sth = $dbh->prepare($query);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        $rowCount = $sth->rowCount();
        if($rowCount >= 1){
		$title = $row['title'];
        $description = $row['description'];
        $new_image = $row['new_image'];
        $time_stamp = strtotime($row['time_cone']);

        //Changeing date format
        $month = date('F', $time_stamp);
        $day = date('d',$time_stamp);
        $year = date('Y',$time_stamp);

        $time_stamp = "{$month} {$day}, {$year}";		
        echo "<div class='panel'>";
		echo "<p class='title'>{$title}</p>";        
        echo "<div class='description' style=''>";
        if(file_exists("$newsImagePath".$new_image)){ echo "<img src='$newsImagePath$new_image' class='image'  />";}        
        echo"{$description}";
        echo "</div></div>";
        }
        else{
            echo "<div class='panel'>";
            echo "<p class='title'></p>";        
            echo "<div class='description' style=''>";
            echo "<img src='http://linkedunion.com/theme/img/empty_page.png' class='image'  />"; 
            echo "</div><p style='text-align: center;font-weight: bold;font-size: 19px;'>Oops..</p><p style='text-align: center;font-weight: 700;font-size: 16px;color:#989898'>There's nothing here, yet.</p></div>";
        }
	}
		?>
<!--
<div style="<?php echo $display; ?>" id="content" class="box">

                <div class="sharing-box">
<?php		
			if ($facebook != ""){
			echo"
           <a class='facebook' href='https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F{$facebook}&t=' 
target='_blank' onclick='window.open('https://www.facebook.com/sharer/sharer.php?u=' + www.linkedunion.com + '&t=' + encodeURIComponent(document.URL)); return false;'>
		   <img width='20' height='33' src='images/facebook.png' class='attachment-full-size wp-post-image' alt='facebook'><h2 class='face'></h2></a>

			   ";
			}
			if ($twitter != ""){			   
			echo"

              <a class='twitter' href='https://twitter.com/intent/tweet?source=http%3A%2F%2F
	$twitter;?>&text=:%20http%3A%2F%2F{$twitter}' target='_blank' title='Tweet' onclick='window.open('https://twitter.com/intent/tweet?text=' + ':%20'  + 	encodeURIComponent(document.URL)); return false;'>
			  <img width='40' height='33' src='images/twitter.png' class='attachment-full-size wp-post-image' alt='tweet'><h2 class='twet'></h2></a>
		";
			}
		
?>
		
				</div>
      </section>
        
          <section id="promo_wrapper" class="section box">
        
      </section>
      </div>
-->
