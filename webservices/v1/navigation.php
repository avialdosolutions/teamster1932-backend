<?php
	
	include 'headers/connect.php';
	
	$appID = @$_POST['appID'];
    $version = @$_POST['version'];
    $returnArray = array();
    $subcategoryArray = array();
    $temparray = array();

    //checking version first

    $QueryVersion = "SELECT * FROM app where app_id = '$appID' and version = '$version' ";
    $stmt = $dbh->prepare($QueryVersion);
    $stmt->execute();
    $count = $stmt->rowCount();
    if($count >= 1)
    {
        $returnArray['developer_message'] = "Version up to date";
    }
    else{
        $returnArray['language']['english'] = [];
        $returnArray['language']['spanish'] = [];

        $query = "SELECT language,id,name,icon,(SELECT version from app a where a.app_id = app_id) as version,(select count(*) from `subcategories` sc where sc.menu_id = c.id) as count,(select webservice from `webservice_category` n where n.category = c.id) as type
        FROM `categories` c";
     
        $sth = $dbh->prepare($query);
        $sth->execute();
//        var_dump($query);
        while($row = $sth->fetch(PDO::FETCH_ASSOC))
        {
            $id = $row['id'];
            $type = $row['type'];
            $language = $row['language'];
            
            // Note: Describing type for Main Navigation and Sub Navigation as follw:- 
            // 1) Type 1 => For Web view || Condition if Null
            // 2) Type 2 => For Multiple News || Condition if type = 1
            // 3) Type 3 => For Stay Conected || Condition if type  = 4
            // 4) Type 4 => For Other Service || Condition if type = 11
            
            
            $name = $row['name'];
            $icon = $row['icon'];
            $count = $row['count'];
        
            
          if($type == 9){
            $query_wufoo = "select url from wufoo_form where CategoryID = $id";
            $stt = $dbh->prepare($query_wufoo);
            $stt->execute();
            $row_from = $stt->fetch(PDO::FETCH_ASSOC);
            $temparray['url'] = $row_from['url'];
              }
            else{
                $temparray['url'] = "";
            }
        
            $temparray['nid']=$id;
            $temparray['name']=$name;
            $temparray['icon']=$icon;
            $temparray['type']=$type;
            $temparray['count']=$count;   
            
            
            $App_version = $row['version'];
            $row['SubCategory'] = '';
            

            if(($row['count'])>0)
            {
                $query_subcategories = "SELECT (select webservice from `webservice_category` n where n.category = sc.id) as subtype,sc.icon,sc.id as submenu_id,sc.name from `subcategories` sc, `categories` c where c.`id` = sc.`menu_id` AND sc.menu_id = {$id}";
                $sth_subcategories = $dbh->prepare($query_subcategories);
                $sth_subcategories->execute();
                while($row_subcategory = $sth_subcategories->fetch(PDO::FETCH_ASSOC))
                {
                    $subtype = $row_subcategory['subtype'];
                    $submenu_id = $row_subcategory['submenu_id'];
                    
                      if($subtype == 9){
                            $query_wufoo = "select url from wufoo_form where CategoryID = $submenu_id";
                            $stt = $dbh->prepare($query_wufoo);
                            $stt->execute();
                            $row_from = $stt->fetch(PDO::FETCH_ASSOC);
                            $row_subcategory['url'] = $row_from['url'];
                      }
                        else{
                            $row_subcategory['url'] = "";
                        }
                            
                        $subcategoryArray[] = $row_subcategory;                                
                }
            }
            
                $temparray['SubCategory'] = $subcategoryArray;                
                if($language == 'english' ? $returnArray['language']['english'][] = $temparray : $returnArray['language']['spanish'][] = $temparray);
                

                $subcategoryArray = [];

        }
        $returnArray['version'] = $App_version;
    }
    echo json_encode($returnArray);
	

?>
