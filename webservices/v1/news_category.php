<?php
	
	include 'headers/connect.php';
	
	$category = $_GET['category'];
    $appID = $_GET['app_id'];

	$returnArray = array();
	$query = "SELECT * from news WHERE category like '$category' and app_id = '$appID' and published = 1 order by `order` asc";
    $sth = $dbh->prepare($query);
    $sth->execute();
	while($row = $sth->fetch(PDO::FETCH_ASSOC))
	{
		$newArray = array();
		$description = $row['description'];
		
		if(strlen($description) > 200)
			$description_toclean = 	strip_tags(substr($description,0,200));
		else
			$description_toclean = strip_tags($description); 
		
		
		
		
		$title = $row['title'];
		$fileImage = $row['new_image'];
        $private = $row['is_private'];
		$news_id = $row['news_id'];
		$time_stamp = strtotime($row['time_cone']);
		
		$news_table = $row['news_table'];
        $newArray['isPrivate'] = $private;
		$newArray['description'] = $description_toclean;
		$newArray['title'] = $title;
		$newArray['file'] = $fileImage;
		$newArray['news_id'] = $news_id;
		
		$month = date('F', $time_stamp);
		$day = date('d',$time_stamp);
		$year = date('Y',$time_stamp);
		
		$time_stamp = "{$month} {$day}, {$year}";
		
		$newArray['time_stamp'] = $time_stamp;
		$newArray['news_table'] = $news_table;
		
		
		$returnArray[] = $newArray;

	}
	
	echo json_encode($returnArray);
	
/*function clean($string) {
   $string = str_replace(array("<p>","</p>","<b>","</b>","<b>","</b>","<em>","</em>","<i>","</i>","<u>","</u>","<h1>","</h1>","<h2>","</h2>","<h3>","</h3>","<li>","</li>","<ul>","</ul>","<a>","</a>","<strong>","</strong>","<h4>","</h4>","<style>","</style>","<ol>","</ol>","<ul>","</ul>"), "", $string);	
   $string = preg_replace('/[^A-Za-z0-9\-:],.; /', ' ', $string); // Removes special chars.
   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}
	*/
?>